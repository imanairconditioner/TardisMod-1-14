package net.tardis.api.space;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonWriter;

import net.minecraft.block.AbstractGlassBlock;
import net.minecraft.block.BlockState;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class OxygenHelper {

	private static File config = new File("./config/oxygen_config.json");
	
	private static List<ResourceLocation> permeable = new ArrayList<ResourceLocation>();
	private static List<ResourceLocation> nonpermeable = new ArrayList<ResourceLocation>();

	public static boolean canOxygenPass(World world, BlockState state, BlockPos pos) {
		if(permeable.contains(state.getBlock().getRegistryName()))
			return true;
		if(nonpermeable.contains(state.getBlock().getRegistryName()))
			return false;
		return state.isAir(world, pos) || (!state.isSolid() && !(state.getBlock() instanceof AbstractGlassBlock));
	}
	
	public static void readOrCreate() {
		try {
			if(config.exists()) {
				JsonObject doc = new JsonParser().parse(new FileReader(config)).getAsJsonObject();
				for(JsonElement perm : doc.get("permeable").getAsJsonArray()) {
					permeable.add(new ResourceLocation(perm.getAsString()));
				}
				for(JsonElement perm : doc.get("nonpermeable").getAsJsonArray()) {
					nonpermeable.add(new ResourceLocation(perm.getAsString()));
				}
			}
			else {
				JsonWriter write = new GsonBuilder().setPrettyPrinting().create().newJsonWriter(new FileWriter(config));
				write.beginObject();
				
				write.name("permeable");
				write.beginArray();
				write.endArray();
				
				write.name("nonpermeable");
				write.beginArray();
				write.endArray();
				
				write.endObject();
				write.close();
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

}
