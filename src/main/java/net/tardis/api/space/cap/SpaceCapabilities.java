package net.tardis.api.space.cap;

import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;

public class SpaceCapabilities {

	@CapabilityInject(IOxygenSealer.class)
	public static final Capability<IOxygenSealer> OXYGEN_SEALER = null;
}
