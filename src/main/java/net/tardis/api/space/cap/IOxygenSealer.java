package net.tardis.api.space.cap;

import java.util.List;

import net.minecraft.util.math.BlockPos;

public interface IOxygenSealer {

	List<BlockPos> getSealedPositions();
	void setSealedPositions(List<BlockPos> list);
}
