package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.Constants.Translations;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class StabilizerControl extends BaseControl{
	
	public StabilizerControl(ConsoleTile console, ControlEntity entity) {
		super(console, entity);
	}

	@Override
	public EntitySize getSize(){
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		return EntitySize.flexible(0.125F, 0.125F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote) {
			console.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
			    if (sys.canBeUsed()) {
			        sys.setActivated(!sys.isActivated());
	                player.sendStatusMessage(new TranslationTextComponent("status.tardis.control.stabilizer." + sys.isActivated()), true);
			    }
			    else {
			        ItemStack name = new ItemStack(TItems.STABILIZERS);
                    player.sendStatusMessage(new TranslationTextComponent(Translations.NO_COMPONENT, name.getDisplayName().getFormattedText()), true);
			    }
			});
			this.setAnimationTicks(20);
			this.markDirty();
		}
		return true;
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return new Vec3d(13.5 / 16.0, 7 / 16.0, 2.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.5455551641720471, 0.425, -0.5866870884910467);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.5442238848074868, 0.34375, 0.7);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(0.769493356617752, 0.46875, -0.803508609511675);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.3751087059966548, 0.3125, 1.2359473783507973);

		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.30178040037581466, 0.65625, 0.9308479946860322);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(0.45423586733985566, 0.3125, -0.769970078434481);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(0.018837774181529765, 0.375, -1.0038470729956595);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vec3d(0.6131817947812537, 0.4375, -0.49161027485812314);
		
		return new Vec3d(-7 / 16.0, 10 / 16.0, 4 / 16.0);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		ObjectWrapper<SoundEvent> sound = new ObjectWrapper<SoundEvent>(TSounds.STABILIZER_ON);
		console.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
			if(!sys.isActivated())
				sound.setValue(TSounds.STABILIZER_OFF);
		});
		return sound.getValue();
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

	@Override
	public void setConsole(ConsoleTile console, ControlEntity entity) {
		super.setConsole(console, entity);
	}

}
