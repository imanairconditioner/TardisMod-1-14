package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class HandbrakeControl extends BaseControl {

	public static final ResourceLocation SAVE_KEY = new ResourceLocation(Tardis.MODID, "handbrake_data");
	/*
	 * True if brake is off and TARDIS can fly
	 */
	private boolean isFree = false;

	public HandbrakeControl(ConsoleTile console, ControlEntity entity) {
		super(console, entity);
	}
	
	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile || this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		return EntitySize.flexible(0.1875F, 0.1875F);
	}
	
	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return new Vec3d(-2 / 16.0, 8 / 16.0, -13 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.2982304929003854, 0.375, 0.8465142260574359);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.7203601993321677, 0.5625, -0.3625);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(0.824851606153647, 0.5625, -0.47144988078117916);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(1.373418848676632, 0.3125, 0.2848165959388401);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(0.39935110764754933, 0.59375, 1.1421816921823003);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(-1.127392157437948, 0.1875, -0.0012375564178497278);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(-1.1546022251848151, 0.59375, -0.00377360907123081);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vec3d(0.4691775280536088, 0.375, 1.0251364745327698);
		
		return new Vec3d(0.31798977635472236, 0.48749999701976776, 0.9024203281819716);
	}

	
	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.isFree = tag.getBoolean("free");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("free", this.isFree);
		return tag;
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(console == null || !console.hasWorld())
			return false;
		if(!console.getWorld().isRemote) {
			this.isFree = !this.isFree;
			
			//Crash
			if(console.isInFlight() && !this.isFree)
				console.crash();
			
			if(console.getControl(ThrottleControl.class).getAmount() > 0.0F) {
				if(this.isFree()) {
					console.takeoff();
				}
			}
			
			this.markDirty();
		}
		return true;
	}
	
	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return isFree() ? TSounds.HANDBRAKE_RELEASE : TSounds.HANDBRAKE_ENGAGE;
	}

	public boolean isFree() {
		return this.isFree;
	}
	
	public void setFree(boolean free) {
		this.isFree = free;
		this.markDirty();
	}
	
	@Override
	public ResourceLocation getSaveKey() {
		return SAVE_KEY;
	}

	@Override
	public void setConsole(ConsoleTile console, ControlEntity entity) {
		super.setConsole(console, entity);
		console.registerDataHandler(SAVE_KEY, this);
	}

}
