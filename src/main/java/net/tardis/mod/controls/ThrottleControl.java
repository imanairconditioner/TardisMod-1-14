package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class ThrottleControl extends BaseControl {

	private float throttle = 0F;

	public ThrottleControl(ConsoleTile console, ControlEntity entity) {
		super(console, entity);
	}
	
	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.throttle = tag.getFloat("throttle");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putFloat("throttle", this.throttle);
		return tag;
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(3 / 16.0F, 3 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.25F, 0.25F);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.3125F, 0.3125F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		return EntitySize.flexible(0.225F, 0.225F);
	}
	
	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(1 / 16.0, 8 / 16.0, -13 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(0.2658954996435158, 0.5, 0.8247037291009587);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(-0.015556977219671198, 0.65625, 0.5999143096893602);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(1.0070054078235704, 0.5625, -0.2252161444978215);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(0.9841104932080567, 0.34375, 0.9699464314312813);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.41347265301191566, 0.5, 1.2127743685836245);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(-0.4830320079849174, 0.25, -0.8288558019362365);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(0.5754439439677996, 0.5625, 0.9782320680707324);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vec3d(0.2848366089721458, 0.375, 1.022849836743878);
		
		return new Vec3d(-0.9568176187101369, 0.48749999701976776, -0.2428564747162537);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		return this.doThrottleAction(console, player, 0.1F);
	}

	@Override
	public void onHit(ConsoleTile console, PlayerEntity player) {
		this.doThrottleAction(console, player, 1.0F);
	}
	
	private boolean doThrottleAction(ConsoleTile console, PlayerEntity player, float amt) {
		if(!player.world.isRemote) {
			this.throttle = (float)MathHelper.clamp(throttle + (player.isSneaking() ? -amt : amt), 0.0, 1.0);
			if(this.shouldTakeoff(console))
				console.takeoff();
			else if(console.isInFlight()) {
				if(this.throttle <= 0.0F) {
					player.world.getServer().enqueue(new TickDelayedTask(1, () -> console.initLand()));//Scheduling this lets the animation play normally
				}
				else console.updateFlightTime();
				
			}
		}
		return true;
	}
	
	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.THROTTLE;
	}
	
	public float getAmount() {
		return this.throttle;
	}
	
	public void setAmount(float amt) {
		this.throttle = amt;
		this.markDirty();
	}
	
	private boolean shouldTakeoff(ConsoleTile console) {
		return console.getControl(HandbrakeControl.class).isFree() && this.throttle > 0 && !console.isInFlight();
	}

}
