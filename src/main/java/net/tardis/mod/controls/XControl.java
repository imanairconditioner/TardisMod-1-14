package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.ArtDecoConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnelConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class XControl extends AxisControl{
	
	public XControl(ConsoleTile console, ControlEntity entity) {
		super(console, entity, Axis.X);
	}

	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof CoralConsoleTile) 
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		if (this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		
		return EntitySize.flexible(0.0625F, 0.0625F);
	}

	@Override
	public Vec3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vec3d(-8 / 16.0, 10 / 16.0, 2.5 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vec3d(-0.512132253102513, 0.625, 0.17666778356266222);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vec3d(0.40493682077482974, 0.5, -0.6);
		}
		
		if(this.getConsole() instanceof HartnelConsoleTile)
			return new Vec3d(-0.9054690056290194, 0.5625, 0.39195598787366204);
		
		if(this.getConsole() instanceof ArtDecoConsoleTile)
			return new Vec3d(1.01234647419064, 0.4375, 0.2777061131802042);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vec3d(-0.6649904910322374, 0.71875, 0.20260675213669666);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return new Vec3d(0.625, 0.4, 0.49744557207215556);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vec3d(0.6282769199569804, 0.53125, 0.6326796505520895);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vec3d(0.8402123105877362, 0.4375, -0.42248972720479133);
		
		return new Vec3d(-7.25 / 16.0, 9.85 / 16.0, -6 / 16.0);
	}
	
	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
}
