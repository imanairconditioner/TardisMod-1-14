package net.tardis.mod.blocks;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.util.Direction;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.AntiGravityTile;

/**
 * Created by Swirtzly
 * on 06/04/2020 @ 22:08
 */
public class AntiGravBlock extends TileBlock implements IARS{

    public static final BooleanProperty ACTIVATED = BooleanProperty.create("activated");

    public AntiGravBlock() {
        super(Prop.Blocks.BASIC_TECH.get());
        this.setDefaultState(this.getDefaultState().with(ACTIVATED, Boolean.FALSE));
    }

    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (player.isSneaking()) {
            AntiGravityTile gravityTile = (AntiGravityTile) worldIn.getTileEntity(pos);
            if (gravityTile != null) {
                gravityTile.setRange(gravityTile.getRange() + 1);
            }
        }
        return true;
    }

    public void neighborChanged(BlockState state, World worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
        if (!worldIn.isRemote) {
            boolean flag = state.get(ACTIVATED);
            if (flag != worldIn.isBlockPowered(pos)) {
                if (flag) {
                    worldIn.getPendingBlockTicks().scheduleTick(pos, this, 4);
                } else {
                    worldIn.setBlockState(pos, state.cycle(ACTIVATED), 2);
                }
            }

        }
    }

    public void tick(BlockState state, World worldIn, BlockPos pos, Random random) {
        if (!worldIn.isRemote) {
            if (state.get(ACTIVATED) && !worldIn.isBlockPowered(pos)) {
                worldIn.setBlockState(pos, state.cycle(ACTIVATED), 2);
            }

        }
    }

    // Adds the "Activated" block state to the block.

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) {
        builder.add(ACTIVATED);
        super.fillStateContainer(builder);
    }

    // Determines the default state for when a block is placed.

    @Override
    public BlockState getStateForPlacement(BlockState state, Direction facing, BlockState state2, IWorld world, BlockPos pos1, BlockPos pos2, Hand hand) {
        return this.getDefaultState().with(ACTIVATED, true);
    }

	@Override
	public void addInformation(ItemStack stack, IBlockReader worldIn, List<ITextComponent> tooltip,
			ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(new StringTextComponent("This requires a Powered Redstone Signal to be activated!"));
	}
    
    

}
