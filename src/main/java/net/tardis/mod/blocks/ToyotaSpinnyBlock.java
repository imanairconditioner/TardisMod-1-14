package net.tardis.mod.blocks;

import net.minecraft.item.Item;
import net.tardis.mod.ars.IARS;
import net.tardis.mod.blocks.multiblock.MultiblockPatterns;
import net.tardis.mod.items.MultiblockBlockItem;
import net.tardis.mod.misc.INeedItem;
import net.tardis.mod.properties.Prop;

public class ToyotaSpinnyBlock extends MultiblockBlock implements IARS, INeedItem{

	public final MultiblockBlockItem item = new MultiblockBlockItem(this, MultiblockPatterns.SPINNY_BOI, Prop.Items.SIXTY_FOUR.get());
	
	public ToyotaSpinnyBlock() {
		super(Prop.Blocks.BASIC_TECH.get());
	}
	
	@Override
	public Item getItem() {
		return item;
	}

}
