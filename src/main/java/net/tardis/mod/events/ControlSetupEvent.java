package net.tardis.mod.events;

import net.minecraftforge.eventbus.api.Event;
import net.tardis.mod.controls.IControl;

public class ControlSetupEvent extends Event {

	IControl control;
	
	public ControlSetupEvent(IControl control) {
		this.control = control;
	}
	
	public IControl getControl() {
		return this.control;
	}
}
