package net.tardis.mod.events;

import net.minecraft.entity.LivingEntity;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.Cancelable;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class LivingEvents {

	
	@Cancelable
	public static class SpaceAir extends LivingEvent{

		public SpaceAir(LivingEntity entity) {
			super(entity);
		}
		
	}
	
	public static class TardisEnterEvent extends LivingEvent{

		private final DimensionType leaving;
		private final ExteriorTile exterior;
		
		public TardisEnterEvent(LivingEntity entity, ExteriorTile tile, DimensionType leaving) {
			super(entity);
			this.leaving = leaving;
			this.exterior = tile;
		}
		
		public ExteriorTile getExterior() {
			return this.exterior;
		}

		public DimensionType getLeaving() {
			return leaving;
		}
	}
	
	public static class TardisLeaveEvent extends LivingEvent{

		private final DimensionType entering;
		private final DoorEntity door;
		
		
		public TardisLeaveEvent(LivingEntity entity, DoorEntity door, DimensionType entering) {
			super(entity);
			this.door = door;
			this.entering = entering;
		}
		
		public DimensionType getEntering() {
			return entering;
		}


		public DoorEntity getDoor() {
			return door;
		}
	}
}
