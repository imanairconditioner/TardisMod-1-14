package net.tardis.mod.protocols;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class Protocol implements IRegisterable<Protocol>{
	
	private ResourceLocation regName;
	
	public abstract void call(World world, PlayerEntity player, ConsoleTile console);
	
	public abstract String getDisplayName(ConsoleTile tile);
	
	public boolean shouldBeAdded(ConsoleTile tile) {
		return true;
	}

	@Override
	public Protocol setRegistryName(ResourceLocation regName) {
		this.regName = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.regName;
	}
	
	public static void registerAll() {
		TardisRegistries.registerRegisters(() -> {
			TardisRegistries.PROTOCOL_REGISTRY.register(Constants.Protocols.LIFE_SCAN, new LifeScanProtocol());
			TardisRegistries.PROTOCOL_REGISTRY.register(Constants.Protocols.TOGGLE_ALARM, new ToggleAlarmProtocol());
			TardisRegistries.PROTOCOL_REGISTRY.register(Constants.Protocols.ANTI_GRAV, new AntiGravProtocol());
			TardisRegistries.PROTOCOL_REGISTRY.register(Constants.Protocols.FORCEFIELD, new ForcefieldProtocol());
		});
	}

	public String getSubmenu() {
		return "main";
	}

}
