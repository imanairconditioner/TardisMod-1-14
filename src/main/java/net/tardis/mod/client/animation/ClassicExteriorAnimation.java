package net.tardis.mod.client.animation;

import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ClassicExteriorAnimation extends ExteriorAnimation {
	
	public ClassicExteriorAnimation(ExteriorAnimationEntry<?> entry, ExteriorTile tile) {
		super(entry, tile);
	}

	@Override
	public void tick() {
		if(exterior.getMatterState() == EnumMatterState.DEMAT)
			exterior.alpha -= 0.005F;
		else if(exterior.getMatterState() == EnumMatterState.REMAT)
			exterior.alpha += 0.005F;
	}

}
