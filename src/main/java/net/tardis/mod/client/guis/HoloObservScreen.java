package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.Tardis;
import net.tardis.mod.contexts.gui.BlockPosGuiContext;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.Partition;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.HoloObservatoryMessage;

public class HoloObservScreen extends Screen {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/holo_observ.png");
	public static final StringTextComponent TITLE = new StringTextComponent("holo");
	private Partition<DimensionType> partition;
	private int index = 0;
	private BlockPos pos;
	
	public HoloObservScreen(BlockPosGuiContext pos) {
		super(TITLE);
		this.pos = pos.pos;
	}

	@Override
	protected void init() {
		super.init();
		
		this.partition = new Partition<>(Helper.getAllValidDimensionTypes(), 6);
		
		for(Widget b : this.buttons)
			b.active = false;
		this.buttons.clear();
		
		int i = 0;
		int height = this.font.FONT_HEIGHT + 11;
		for(DimensionType type : this.partition.get(this.index)) {
			int startY = this.height / 2 - 75;
			this.addButton(new Button(width / 2 - 6, startY + ((height + 0) * i), 110, height, Helper.formatDimName(type),
					but -> {
						Network.sendToServer(new HoloObservatoryMessage(pos, type));
					}));
			++i;
		}
		
		this.addButton(new Button(width / 2 - 6, this.height / 2 + 51, 55, height, "Prev", but -> modIndex(-1)));
		this.addButton(new Button(width / 2 + 50, this.height / 2 + 51, 55, height, "Next", but -> modIndex(1)));
		
	}
	
	public void modIndex(int mod) {
		if(this.index + mod >= this.partition.size())
			this.index = 0;
		else if(this.index + mod < 0)
			this.index = this.partition.size() - 1;
		else this.index += mod;
		
		init();
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground();
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		this.blit(width / 2 - 248 / 2, height / 2 - 166 / 2, 0, 0, 248, 166);
		
		
		super.render(p_render_1_, p_render_2_, p_render_3_);
		this.font.drawStringWithShadow("Holo Observatory", width / 2 - 230 / 2, height / 2 - 75, 0xFFFFFF);
		this.font.drawStringWithShadow("Set Dimension Skybox:", width / 2 - 230 / 2, height / 2 - 60, 0xFFFFF);
		//HoloObservatoryTile tile = (HoloObservatoryTile)this.minecraft.world.getTileEntity(pos);
		
		//this.font.drawStringWithShadow(Helper.formatDimName(tile.getWorld().dimension.getType()), this.width / 2, this.height / 2, 0x000000);
	}
}
