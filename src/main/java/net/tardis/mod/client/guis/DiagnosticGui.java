package net.tardis.mod.client.guis;

import java.text.DecimalFormat;
import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.contexts.gui.GuiContextSubsytem;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.packets.DiagnosticMessage.ArtronUseInfo;
import net.tardis.mod.subsystem.SubsystemInfo;

public class DiagnosticGui extends Screen {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/diagnostic.png");
	public static DecimalFormat FORMAT = new DecimalFormat("###");
	List<SubsystemInfo> list;
	private Window window = null;
	private SpaceTimeCoord location;
	private List<ArtronUseInfo> artronInfos;
	
	public DiagnosticGui(GuiContextSubsytem cont) {
		super(new StringTextComponent(""));
		this.list = cont.infos;
		this.location = cont.location;
		this.artronInfos = cont.artronInfo;
	}

	@Override
	public void render(int mouseX, int mouseY, float partialTicks) {
		
		this.renderBackground();
		
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		int width = 255, height = 182;
		this.blit(this.width / 2 - width / 2, this.height / 2 - height / 2, 0, 0, width, height);
		
		super.render(mouseX, mouseY, partialTicks);
		
		if(this.window != null)
			this.window.render();
		
	}

	@Override
	protected void init() {
		super.init();
		
		if(this.window != null)
			return;
		
		int x = width / 2 - 95, y = height / 2;
		this.addButton(new TextButton(x, y - ((this.font.FONT_HEIGHT + 2) * 0), "> Subsystem Info", but -> {
			this.setWindow(SUBSYSTEM);
			
		}));
		
		this.addButton(new TextButton(x, y - ((this.font.FONT_HEIGHT + 2) * 1), "> Timeship Location", but -> {
			this.setWindow(() -> {
				int locX = this.width / 2, locY = this.height / 2 - 70;
				this.drawCenteredString(this.font, "Timeship Location", locX, locY, 0xFFFFFF);
				this.drawCenteredString(this.font, String.format("Located at %s in %s",
						Helper.formatBlockPos(this.location.getPos()),
						Helper.formatDimName(this.location.getDim())),
						locX, locY + font.FONT_HEIGHT + 6, 0xFFFFFF);
			});
			
		}));
		
		this.addButton(new TextButton(x, y - ((this.font.FONT_HEIGHT + 2) * 2), "> Artron Use", but -> {
			this.setWindow(() -> {
				int locX = this.width / 2, locY = this.height / 2 - 70;
				this.drawCenteredString(this.font, "Artron Uses", locX, locY, 0xFFFFFF);
				int index = 0;
				for(ArtronUseInfo info : this.artronInfos) {
					this.drawCenteredString(this.font, info.key.getFormattedText() + " " + (info.use * 20) + "AU/S", locX, locY + 20 + (index * (font.FONT_HEIGHT + 2)), 0xFFFFFF);
					System.out.print(info);
					++index;
				}
			});
		}));
		
	}
	
	public void setWindow(Window win) {
		this.window = win;
		
		for(Widget w : this.buttons){
			w.active = false;
		}
		this.buttons.clear();
	}
	
	public static interface Window{
		void render();
	}
	
	public Window SUBSYSTEM = () -> {
		int i = 0;
		int x = this.width / 2 - 70;
		int y = this.height / 2 - 80;
		net.minecraft.client.renderer.RenderHelper.enableGUIStandardItemLighting();
		TranslationTextComponent subsystem_title = new TranslationTextComponent("overlay.tardis.system_title");
		drawString(this.font, subsystem_title.getFormattedText(), x - 20, y + 10, 0xFFFFFF);
		for(SubsystemInfo info : list) {
			y = (this.height / 2 - 55) + (i * (font.FONT_HEIGHT + 2));
			float percent = info.health * 100.0F;
			
			TextFormatting color = TextFormatting.GREEN;
			
			if(percent <= 100) {
				color = TextFormatting.DARK_GREEN;
				
				if(percent <= 75){
					color = TextFormatting.GREEN;
					
					if(percent <= 50) {
						color = TextFormatting.YELLOW;
						
						if(percent <= 25)
							color = TextFormatting.DARK_RED;
					}
				}
			}
			StringTextComponent percentToString = new StringTextComponent(FORMAT.format(percent) + "%");
			drawString(this.font, new TranslationTextComponent(info.translationKey).getFormattedText() + ": " +
					percentToString.applyTextStyle(color).getFormattedText(), x, y, 0xFFFFFF);
			Minecraft.getInstance().getItemRenderer()
				.renderItemAndEffectIntoGUI(new ItemStack(info.key), x - 16, y - 4);
			++i;
		}
		net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
	};

}
