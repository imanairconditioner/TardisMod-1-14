package net.tardis.mod.client.guis.vm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.lwjgl.glfw.GLFW;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMDistressMessage;

public class VortexMDistressScreen extends VortexMFunctionScreen{

	private TextFieldWidget userInput;
	private TextFieldWidget messageBox;
	private Button send;
	private Button back;
	private Button increment;
	private Button decrement;
	private Button checkName;
	private int index = 0;
	private Collection<NetworkPlayerInfo> players;
	private ArrayList<UUID> UUIDs = new ArrayList<>();
	private UUID selectedUUID; //UUID of player via username input
	private boolean isValid; //Used for rendering error messages on gui
	private boolean hasCheckedName; //Second flag to check if we should render error message
	private final TranslationTextComponent title = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.title");
	private final TranslationTextComponent selectUser = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.select_user");
	private final TranslationTextComponent messageDesc = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.message");
	private final TranslationTextComponent userFound = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.user_found");
    private final TranslationTextComponent userNotFound = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.user_invalid");
    private final TranslationTextComponent sentText = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.send_signal");
    private final TranslationTextComponent checkNameText = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.check_name");
	public VortexMDistressScreen(ITextComponent title) {
		super(title);
	}

	public VortexMDistressScreen() {
	}

	@Override
	public void init() {
		super.init();
		this.createPlayerList();
		String next = ">";
		String previous = "<";
		String sendButton = sentText.getFormattedText();
		String backButton = Constants.Translations.GUI_BACK.getFormattedText();
		String checkNames = checkNameText.getFormattedText();
		final int btnH = 20;

		userInput = new TextFieldWidget(this.font, this.getMinX() + 42, this.getMaxY() + 95, 60, this.font.FONT_HEIGHT + 2, "");
		messageBox = new TextFieldWidget(this.font, this.getMinX() + 165, this.getMaxY() + 115, 60, this.font.FONT_HEIGHT + 2, "");
		send = new Button(this.getMinX() + 40,this.getMaxY() + 115, this.font.getStringWidth(sendButton) + 10,btnH, sendButton, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				//Check if text box input has valid player.
				checkInputName();
				if (isValid) {
					if (PlayerHelper.doesPlayerInfoExist(selectedUUID)) {
						Network.sendToServer(new VMDistressMessage(selectedUUID, messageBox.getText()));
						onClose();
					}
				}
			}
		});
		back = new Button(this.getMinX(),this.getMaxY() + 35, this.font.getStringWidth(backButton) + 8, this.font.FONT_HEIGHT + 11, backButton, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				Tardis.proxy.openGUI(Constants.Gui.VORTEX_MAIN, null);
			}
		});
		increment = new Button(this.getMinX() + 110, this.getMaxY() + 90, 20, this.font.FONT_HEIGHT + 11, next, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				modIndex(1);
			}
		});
        decrement = new Button(this.getMinX() + 15, this.getMaxY() + 90, 20, this.font.FONT_HEIGHT + 11, previous, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				modIndex(-1);
			}
		});
        checkName = new Button(this.getMinX() + 165, this.getMaxY() + 50, 65, this.font.FONT_HEIGHT + 11, checkNames, new Button.IPressable() {
			@Override
			public void onPress(Button button) {
				checkInputName();
			}
		});

		this.buttons.clear();
		this.addButton(userInput);
		this.addButton(send);
		this.addButton(increment);
		this.addButton(decrement);
		this.addButton(back);
		this.addButton(checkName);
		this.addButton(messageBox);
		userInput.setFocused2(true);
	}

	@Override
    public void render(int mouseX, int mouseY, float partialTicks) {
        super.render(mouseX, mouseY, partialTicks);
        drawCenteredString(this.font, title.getFormattedText(), this.getMinX() + 73, this.getMaxY() + 40, 0xFFFFFF);
		drawCenteredString(this.font, selectUser.getFormattedText(), this.getMinX() + 75, this.getMaxY() + 76, 0xFFFFFF);
		drawCenteredString(this.font, messageDesc.getFormattedText(), this.getMinX() + 197, this.getMaxY() + 100, 0xFFFFFF);
		if (this.hasCheckedName) {
			if (!this.isValid && !this.userInput.isFocused()) {
				drawCenteredString(this.font, userNotFound.getFormattedText(), this.getMinX() + 197, this.getMaxY() + 75, 0xFF0000);
				String user = userInput.getText();
				drawCenteredString(this.font, userInput.getText().isEmpty() || userInput.getText() == null ? "Null" : user, this.getMinX() + 196, this.getMaxY() + 85, 0xffcc00);
			}
			else if (this.isValid && !this.userInput.getText().isEmpty() && !this.userInput.isFocused()){
				drawCenteredString(this.font, userFound.getFormattedText(), this.getMinX() + 197, this.getMaxY() + 75, 0x00FF00);
			}
		}
    }

	@Override
	public boolean shouldCloseOnEsc() {
		return true;
	}

	@Override
	public void onClose() {
        super.onClose();
	}

	@Override
	public boolean isPauseScreen() {
	   return true;
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int bitmaskModifier) {
		if (this.userInput.isFocused()) {
			if (keyCode == GLFW.GLFW_KEY_TAB) {
				modIndex(1);
			}
			super.keyPressed(keyCode, scanCode, bitmaskModifier);
			return true;
		}
		return super.keyPressed(keyCode, scanCode, bitmaskModifier);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseZ) {
		if (!this.checkName.mouseClicked(mouseX, mouseY, mouseZ))
			this.hasCheckedName = false;
		return super.mouseClicked(mouseX, mouseY, mouseZ);
	}

	@Override
	public void renderBackground() {
		super.renderBackground();
	}

	@Override
	public int texWidth() {
		return super.texWidth();
	}

	@Override
	public int texHeight() {
		return super.texHeight();
	}

	@Override
	public int getMinY() {
		return super.getMinY();
	}

	@Override
	public int getMinX() {
		return super.getMinX();
	}

	@Override
	public int getMaxX() {
		return super.getMaxX();
	}

	@Override
	public int getMaxY() {
		return super.getMaxY();
	}
	
	public void modIndex (int amount) {
		int temp = index + amount;
		if (temp < UUIDs.size() && temp > 0) {
			this.index = temp;
		}
		if (temp > UUIDs.size() - 1) {
			this.index = 0;
		}
		else if (temp < 0) {
			this.index = UUIDs.size() - 1;
		}
		this.selectedUUID = UUIDs.get(this.index);
		this.userInput.setText(PlayerHelper.getClientPlayerUsername(selectedUUID));
		this.userInput.setCursorPositionZero();
	}

	public void createPlayerList() {
		this.players = Minecraft.getInstance().getConnection().getPlayerInfoMap();
		this.players.forEach(player -> this.UUIDs.add(player.getGameProfile().getId()));
	}

    /**
     * Used to check if textbox contents match an online player
     * @implSpec Only use this for the Check name button
     */
	public void checkInputName() {
		//getPlayerInfo has one other overload. This overload method will not work for uuids, so we need this in a seperate method
		String name = this.userInput.getText();
		NetworkPlayerInfo info = PlayerHelper.getClientPlayerFromUsername(name);
		if (info != null) {
			isValid = true;
			this.selectedUUID = info.getGameProfile().getId(); //set the selected uuid to the one found by the server. This allows doesSelectedUUIDExist() to validate this further.
		}
		else isValid = false;
		this.hasCheckedName = true;
	}

}
