package net.tardis.mod.client.guis.vm;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.PlayerHelper;

/**
* Extend this class to make new function GUIs and override methods where necessary
* E.g. If you want to implement different background, override renderBackground
**/
public class VortexMFunctionScreen extends Screen implements IVortexMScreen{

	public static StringTextComponent TITLE = new StringTextComponent("Function");
	public static ResourceLocation BACKGROUND = new ResourceLocation(Tardis.MODID, "textures/gui/vm_ui_function.png");
	
	protected VortexMFunctionScreen(ITextComponent titleIn) {
		super(titleIn);
	}
	
	public VortexMFunctionScreen() {
		this(TITLE);
	}
	
	@Override
	public void init() {
		super.init();
	}
	
	@Override
	public int getMinY() {
		return this.height / 2 + 61;
	}

	@Override
	public int getMinX() {
		return this.width / 2 - 70;
	}

	@Override
	public int getMaxX() {
		return this.getMinX() + 242;
	}

	@Override
	public int getMaxY() {
		return this.getMinY() - 135;
	}
	
	@Override
	public void renderBackground() {
		GlStateManager.pushMatrix();
		GlStateManager.enableAlphaTest();
		GlStateManager.enableBlend();
		Minecraft.getInstance().getTextureManager().bindTexture(BACKGROUND);
		this.blit(this.width / 2 - this.texWidth() / 2 + 50, this.height / 2 - this.texHeight() / 2, 0, 0, this.texWidth(), this.texHeight());
		GlStateManager.popMatrix();
	}

	@Override
	public void renderScreen(){
		this.renderBackground();
	}
	
	@Override
	public void render(int mouseX, int mouseY, float p_render_3_) {
		this.renderScreen();
		super.render(mouseX, mouseY, p_render_3_);
	}
	
	@Override
	public boolean shouldCloseOnEsc() {
		return true;
	}
		
	@Override
	public void onClose() {
		this.minecraft.displayGuiScreen(null);
		PlayerHelper.closeVMModel(this.minecraft.player); //set item model to closed mode
	}
	
	@Override
	public boolean isPauseScreen() {
	   return false;
	}

	@Override
	public int texWidth() {
		return 241;
	}

	@Override
	public int texHeight() {
		return 142;
	}
}
