package net.tardis.mod.client.guis.monitors;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleChangeMessage;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.registries.consoles.Console;

public class ConsoleSelectionScreen extends MonitorScreen{
	
	private List<Console> consoles = Lists.newArrayList();
	private int index = 0;
	
	public ConsoleSelectionScreen(IMonitorGui gui, String name) {
		super(gui, name);
	}
	
	@Override
	protected void init() {
		super.init();
		
		this.consoles.clear();
		this.consoles.addAll(TardisRegistries.CONSOLE_REGISTRY.getRegistry().values());
		
		this.addSubmenu(new StringTextComponent("Previous"), (button) -> modIndex(-1));
		this.addSubmenu(new StringTextComponent("Select"), (button) -> {
			Network.sendToServer(new ConsoleChangeMessage(this.getSelectedConsole().getRegistryName()));
			this.minecraft.displayGuiScreen(null);
		});
		this.addSubmenu(new StringTextComponent("Next"), (button) -> modIndex(1));
		
		TardisHelper.getConsoleInWorld(this.getMinecraft().world).ifPresent(tile -> {
			int temp = 0;
			for(Console console : this.consoles) {
				if(console.getState().getBlock() == this.getMinecraft().world.getBlockState(tile.getPos()).getBlock()) {
					index = temp;
					break;
				}
				++temp;
			}
		});
	}
	
	private Console getSelectedConsole() {
		if(this.index >= this.consoles.size() || this.index < 0)
			return Console.STEAM;
		return this.consoles.get(this.index);
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		super.render(p_render_1_, p_render_2_, p_render_3_);
		
		this.getMinecraft().getTextureManager().bindTexture(this.getSelectedConsole().getTexture());
		blit(((this.parent.getMaxX() - this.parent.getMinX()) / 2) + this.parent.getMinX() - 50,
				this.parent.getMaxY(), 0, 0, 100, 100, 100, 100);
	}
	
	public void modIndex(int mod) {
		if(index + mod > consoles.size() - 1)
			index = 0;
		else if(index + mod < 0)
			index = this.consoles.size() - 1;
		else index += mod;
	}

}
