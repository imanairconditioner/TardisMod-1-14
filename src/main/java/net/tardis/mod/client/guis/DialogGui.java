package net.tardis.mod.client.guis;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.Widget;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.entity.humanoid.AbstractHumanoidEntity;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;

public class DialogGui extends Screen{

	Dialog dialog;
	AbstractHumanoidEntity speaker;
	
	public DialogGui(AbstractHumanoidEntity speaker) {
		super(Constants.Gui.DEFAULT_GUI_TITLE);
		this.speaker = speaker;
	}

	@Override
	protected void init() {
		super.init();
		this.dialog = this.speaker.getCurrentDialog();
		
		this.setupButtons();
	}
	
	public void setupButtons() {
		
		//Close if dialog is null
		if(this.dialog == null) {
			Minecraft.getInstance().displayGuiScreen(null);
			return;
		}
		
		//Clean up old buttons and clear them
		for(Widget w : this.buttons)
			w.active = false;
		this.buttons.clear();
		
		//Setup dialog options
		int i = 0;
		for(DialogOption option : this.dialog.getOptions()) {
			this.addButton(new TextButton(this.width / 2 - 50, this.height / 2 + i * (this.font.FONT_HEIGHT + 4), option.getTrans().getFormattedText(), but -> {
				this.dialog = option.onClick(this.speaker, Minecraft.getInstance().player);
				this.setupButtons();
			}));
			++i;
		}
	}

	@Override
	public boolean isPauseScreen() {
		return false;
	}
	
	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground();
		
		if(this.dialog != null)
			this.drawCenteredString(font, this.dialog.getSpeakerLine().getFormattedText(), this.width / 2, this.height / 2 - 75, 0xFFFFFF);
		
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}

}
