package net.tardis.mod.client.guis;

import java.util.UUID;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.TextButton;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.SendTardisDistressSignal;

public class TardisSendDistressScreen extends Screen {

	public static final StringTextComponent TITLE = new StringTextComponent("");
	private final TranslationTextComponent message_desc = new TranslationTextComponent(Constants.Strings.GUI + "distress_signal.message_desc");
	private final TranslationTextComponent select_ship = new TranslationTextComponent(Constants.Strings.GUI + "distress_signal.select_ship");
	private TextFieldWidget message;
	
	public TardisSendDistressScreen() {
		super(TITLE);
	}

	@Override
	public void render(int p_render_1_, int p_render_2_, float p_render_3_) {
		this.renderBackground();
		this.drawCenteredString(this.font, message_desc.getFormattedText(), width / 2, height / 2 - 60, 0xFFFFFF);
		this.drawCenteredString(this.font, select_ship.getFormattedText(), width / 2, height / 2 - 10, 0xFFFFFF);
		
		super.render(p_render_1_, p_render_2_, p_render_3_);
	}

	@Override
	protected void init() {
		super.init();
		
		{
			int x = width / 2 - 75, y = height / 2 - 40;
			this.message = this.addButton(new TextFieldWidget(this.font, x, y, 150, this.font.FONT_HEIGHT + 8, ""));
		}
		
		{
			int x = width / 2 - 50, y = height / 2 + 10;
			
			int index = 1;
			
			this.addButton(new TextButton(x, y, "All", but -> {
				Network.sendToServer(new SendTardisDistressSignal(this.message.getText(), null));
			}));
			
			for(NetworkPlayerInfo info : Minecraft.getInstance().getConnection().getPlayerInfoMap()) {
				if(info.getGameProfile() != null && info.getGameProfile().getName() != null) {
					this.addButton(new TextButton(x, y + ((this.font.FONT_HEIGHT + 2) * index), info.getGameProfile().getName(), but -> {
						UUID id = info.getGameProfile().getId();
						Network.sendToServer(new SendTardisDistressSignal(this.message.getText(), id));
					}));
					++index;
				}
			}
		}
	}

}
