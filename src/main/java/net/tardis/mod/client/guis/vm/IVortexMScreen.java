package net.tardis.mod.client.guis.vm;

/**
 * Created by 50ap5ud5
 * on 7 May 2020 @ 4:44:30 pm
 * @implNote
 * Override getMinY, getMinX, getMaxX, getMaxY
 * Override renderScreen, then set your own texture dimensions and blit it
 */
public interface IVortexMScreen {
	
	void renderScreen();
	
	/**
	 * Bottom most part of GUI Texture
	 * @implSpec Subtract from this to move widget up
	 * @return
	 */
	int getMinY();
	/**
	 * Left most part of GUI texture
	 * @implSpec Add from this to move widget to the right
	 * @return
	 */
	int getMinX();
	/**
	 * Right most part of GUI texture
	 * @implSpec Subtract from this to move widget to the left
	 * @return
	 */
	int getMaxX();
	/**
	 * Top most part of GUI Texture
	 * @implSpec Add from this to move widget down
	 * @return
	 */
	int getMaxY();
	int texWidth();
	int texHeight();
}
