package net.tardis.mod.client;

import java.util.UUID;

import net.minecraft.client.Minecraft;
import net.minecraft.client.network.play.NetworkPlayerInfo;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ClientHelper {

	public static ITextComponent getUsernameFromUUID(UUID id) {
		
		StringTextComponent idComp = new StringTextComponent(id.toString());
		
		if(Minecraft.getInstance().getConnection() != null) {
			NetworkPlayerInfo info = Minecraft.getInstance().getConnection().getPlayerInfo(id);
			if(info != null && info.getGameProfile() != null) {
				String name = info.getGameProfile().getName();
				return name == null ? idComp : new StringTextComponent(name);
			}
		}
		return idComp;
	}

}
