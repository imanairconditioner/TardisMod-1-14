package net.tardis.mod.client.models;

import java.util.function.Supplier;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.misc.WorldText;

public class TextModelBox extends ModelBox{
	
	Supplier<String> text;
	WorldText textRenderer = new WorldText(1, 1, 1, TextFormatting.DARK_PURPLE);

	public TextModelBox(RendererModel renderer, Supplier<String> text, WorldText textRender, float x, float y, float z) {
		super(renderer, 0, 0, x, y, z, 0, 0, 0, 0F);
		this.text = text;
		this.textRenderer = textRender;
		
	}

	@Override
	public void render(BufferBuilder renderer, float scale) {
		super.render(renderer, scale);
		GlStateManager.pushMatrix();
		GlStateManager.translated(this.posX1, this.posY1, this.posZ1);
		if(text != null && text.get() != null)
			textRenderer.renderMonitor(text.get());
		GlStateManager.popMatrix();
	}

}
