package net.tardis.mod.client.models.interiordoors;// Made with Blockbench 3.6.6
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.misc.IDoorType;

public class InternalModernPoliceBoxDoor extends Model implements IInteriorDoorRenderer {
	private final RendererModel doors;
	private final RendererModel left2;
	private final RendererModel window3;
	private final RendererModel frame3;
	private final RendererModel glow2;
	private final RendererModel right2;
	private final RendererModel window4;
	private final RendererModel frame4;
	private final RendererModel glow5;
	private final RendererModel bone;
	private final RendererModel roof;
	private final RendererModel soto;
	private final RendererModel frame;


	public static final ResourceLocation TEXTURE =
			new ResourceLocation(Tardis.MODID, "textures/exteriors/modern_police_box_alt.png");

	public InternalModernPoliceBoxDoor() {
		textureWidth = 151;
		textureHeight = 151;

		doors = new RendererModel(this);
		doors.setRotationPoint(0.0F, 23.0F, 0.0F);
		setRotationAngle(doors, 0.0F, 1.5708F, 0.0F);


		left2 = new RendererModel(this);
		left2.setRotationPoint(-0.4333F, 1.1833F, 8.25F);
		doors.addChild(left2);
		left2.cubeList.add(new ModelBox(left2, 92, 6, -0.5667F, -2.1833F, -7.0F, 1, 2, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 86, 86, -0.5667F, -31.1833F, -1.25F, 1, 31, 1, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 16, 87, -0.5667F, -31.1833F, -7.75F, 1, 31, 1, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 91, 25, -0.5667F, -31.1833F, -7.0F, 1, 2, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 99, 21, -0.5667F, -9.1833F, -7.0F, 1, 1, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 99, 21, -0.5667F, -16.1833F, -7.0F, 1, 1, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 98, 92, -0.5667F, -23.1833F, -7.0F, 1, 1, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 36, 88, 0.1833F, -8.1833F, -7.0F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 24, 88, -0.3167F, -8.1833F, -7.0F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 36, 88, 0.1833F, -15.1833F, -7.0F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 24, 88, -0.3167F, -15.1833F, -7.0F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 36, 88, 0.1833F, -22.1833F, -7.0F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 24, 88, -0.3167F, -22.1833F, -7.0F, 0, 6, 6, 0.0F, false));
		left2.cubeList.add(new ModelBox(left2, 64, 59, -0.0667F, -17.1833F, -7.25F, 1, 3, 0, 0.0F, false));

		window3 = new RendererModel(this);
		window3.setRotationPoint(0.1833F, -1.1833F, -8.0F);
		left2.addChild(window3);


		frame3 = new RendererModel(this);
		frame3.setRotationPoint(0.0F, 0.0F, 0.0F);
		window3.addChild(frame3);
		frame3.cubeList.add(new ModelBox(frame3, 0, 72, 0.0F, -28.0F, 1.0F, 0, 6, 6, 0.0F, false));
		frame3.cubeList.add(new ModelBox(frame3, 0, 66, -0.5F, -28.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow2 = new RendererModel(this);
		glow2.setRotationPoint(0.0F, 0.0F, 0.0F);
		window3.addChild(glow2);
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -27.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -27.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -27.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -27.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -27.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -27.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -27.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -27.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -27.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -27.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -27.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -27.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -24.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -24.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -24.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -24.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -24.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -24.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -24.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -24.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -24.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -24.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, 0.125F, -24.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 37, -0.625F, -24.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -26.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -26.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -26.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -26.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -26.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -26.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -26.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -26.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -26.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -26.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -26.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -26.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -23.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -23.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -23.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -23.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -23.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -23.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -23.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -23.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -23.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -23.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, 0.125F, -23.5F, 3.585F, 0, 1, 1, 0.0F, false));
		glow2.cubeList.add(new ModelBox(glow2, 63, 38, -0.625F, -23.5F, 3.585F, 0, 1, 1, 0.0F, false));

		right2 = new RendererModel(this);
		right2.setRotationPoint(-0.9F, -1.0F, -8.0F);
		doors.addChild(right2);
		right2.cubeList.add(new ModelBox(right2, 92, 6, -0.1F, 0.0F, 0.75F, 1, 2, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 20, 87, -0.1F, -29.0F, 6.5F, 1, 31, 1, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 16, 87, -0.1F, -29.0F, 0.0F, 1, 31, 1, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 91, 25, -0.1F, -29.0F, 0.75F, 1, 2, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 99, 21, -0.1F, -7.0F, 0.75F, 1, 1, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 99, 21, -0.1F, -14.0F, 0.75F, 1, 1, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 98, 92, -0.1F, -21.0F, 0.75F, 1, 1, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 36, 82, 0.65F, -6.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 24, 82, 0.15F, -6.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 36, 82, 0.65F, -13.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 24, 82, 0.15F, -13.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 24, 82, 0.15F, -20.0F, 0.75F, 0, 6, 6, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 0, 0, 0.65F, -20.5F, 0.25F, 0, 7, 7, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 0, 0, 0.9F, -19.0F, 2.25F, 0, 3, 3, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 14, 14, 0.9F, -18.0F, 2.75F, 0, 3, 2, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 68, 14, 0.15F, -18.0F, 1.25F, 1, 2, 0, 0.0F, false));
		right2.cubeList.add(new ModelBox(right2, 12, 87, 0.15F, -29.0F, 7.5F, 1, 31, 1, 0.0F, false));

		window4 = new RendererModel(this);
		window4.setRotationPoint(0.65F, 1.0F, -0.25F);
		right2.addChild(window4);


		frame4 = new RendererModel(this);
		frame4.setRotationPoint(0.0F, 0.0F, 0.0F);
		window4.addChild(frame4);
		frame4.cubeList.add(new ModelBox(frame4, 0, 72, 0.0F, -28.0F, 1.0F, 0, 6, 6, 0.0F, false));
		frame4.cubeList.add(new ModelBox(frame4, 0, 66, -0.5F, -28.0F, 1.0F, 0, 6, 6, 0.0F, false));

		glow5 = new RendererModel(this);
		glow5.setRotationPoint(0.0F, 0.0F, 0.0F);
		window4.addChild(glow5);
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -27.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -27.5F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -27.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -27.5F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -27.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -27.5F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -27.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -27.5F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -27.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -27.5F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -27.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -27.5F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -24.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -24.75F, 1.75F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -24.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -24.75F, 1.92F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -24.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -24.75F, 5.25F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -24.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -24.75F, 5.08F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -24.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -24.75F, 3.415F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, 0.125F, -24.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 37, -0.625F, -24.75F, 3.585F, 0, 2, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -26.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -26.25F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -26.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -26.25F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -26.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -26.25F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -26.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -26.25F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -26.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -26.25F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -26.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -26.25F, 3.585F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -23.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -23.5F, 1.75F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -23.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -23.5F, 1.92F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -23.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -23.5F, 5.25F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -23.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -23.5F, 5.08F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -23.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -23.5F, 3.415F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, 0.125F, -23.5F, 3.585F, 0, 1, 1, 0.0F, false));
		glow5.cubeList.add(new ModelBox(glow5, 63, 38, -0.625F, -23.5F, 3.585F, 0, 1, 1, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(8.25F, -32.0F, 0.0F);
		doors.addChild(bone);
		setRotationAngle(bone, 0.0F, 3.1416F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 0, 66, 9.0F, -1.5F, -9.0F, 2, 3, 18, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 22, 54, 11.25F, -1.0F, -7.0F, 0, 2, 14, 0.0F, false));

		roof = new RendererModel(this);
		roof.setRotationPoint(0.0F, 24.75F, 0.0F);
		roof.cubeList.add(new ModelBox(roof, 22, 70, -9.5F, -36.25F, -0.5F, 19, 4, 2, 0.0F, false));
		roof.cubeList.add(new ModelBox(roof, 22, 75, -8.5F, -37.25F, -0.5F, 17, 1, 1, 0.0F, false));

		soto = new RendererModel(this);
		soto.setRotationPoint(0.0F, 24.0F, 0.0F);
		soto.cubeList.add(new ModelBox(soto, 119, 119, -8.0F, -32.0F, 0.0F, 16, 32, 0, 0.0F, false));

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 24.0F, 0.0F);
		setRotationAngle(frame, 0.0F, 1.5708F, 0.0F);
		frame.cubeList.add(new ModelBox(frame, 76, 64, -0.5F, -31.5F, -8.0F, 1, 1, 16, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 66, 0, -1.75F, -32.0F, -8.0F, 2, 1, 16, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 58, 85, -2.0F, -35.0F, 8.0F, 2, 35, 2, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 50, 85, -2.0F, -35.0F, -10.0F, 2, 35, 2, 0.0F, false));
	}

	@Override
	public void render(DoorEntity door) {

		GlStateManager.pushMatrix();//Tell rendering engine to add a new object to the render matrix stack
		GlStateManager.enableRescaleNormal(); //Ensures model isn't rendered fullbright all the time
		GlStateManager.translated(0, -0.2, -0.5f); // Translation must be negative as models are loaded in upside down.
		GlStateManager.scalef(1.1f, 1.1f, 1.1f); //Scales the model down by 4

		EnumDoorState state = door.getOpenState();

		switch(state) {
			case ONE:
				this.left2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				//this.rightwindow.rotateAngleY = (float) Math.toRadians(
				//IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

				this.right2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				//this.leftwindow.rotateAngleY = (float) Math.toRadians(
				//IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				break;


			case BOTH:
				this.left2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				//this.rightwindow.rotateAngleY = (float) Math.toRadians(
				//	IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

				this.right2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				//this.leftwindow.rotateAngleY = (float) Math.toRadians(
				//		IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

				break;


			case CLOSED://close both doors
				this.left2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				this.right2.rotateAngleY = (float) Math.toRadians(
						IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

				//this.rightwindow.rotateAngleY = (float) Math.toRadians(
				//IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				//this.leftwindow.rotateAngleY = (float) Math.toRadians(
				//	IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
				break;
			default:
				break;
		}

		//this.botifake.render(0.0625F);
		doors.render(0.0625F);
		roof.render(0.0625F);
		soto.render(0.0625F);
		frame.render(0.0625F);



		GlStateManager.disableRescaleNormal();//Ensures lighting on model is preserved when rescaled

		GlStateManager.popMatrix(); //Finish rendering
	}


	@Override
	public ResourceLocation getTexture() {
		return TEXTURE;
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}
