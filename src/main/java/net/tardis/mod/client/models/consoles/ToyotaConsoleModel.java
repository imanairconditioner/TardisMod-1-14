package net.tardis.mod.client.models.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.tileentities.ConsoleTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports

@SuppressWarnings("deprecation")
public class ToyotaConsoleModel extends Model {
	private final RendererModel baseconsole;
	private final RendererModel panels;
	private final RendererModel bone14;
	private final RendererModel bone9;
	private final RendererModel bone10;
	private final RendererModel bone11;
	private final RendererModel bone12;
	private final RendererModel bone13;
	private final RendererModel bone15;
	private final RendererModel bone16;
	private final RendererModel bone17;
	private final RendererModel bone18;
	private final RendererModel bone19;
	private final RendererModel centre;
	private final RendererModel rotor_top;
	private final RendererModel bone124;
	private final RendererModel bone125;
	private final RendererModel bone126;
	private final RendererModel bone127;
	private final RendererModel bone128;
	private final RendererModel rotor_base;
	private final RendererModel bone52;
	private final RendererModel bone53;
	private final RendererModel bone54;
	private final RendererModel bone55;
	private final RendererModel bone56;
	private final RendererModel bone129;
	private final RendererModel bone130;
	private final RendererModel bone131;
	private final RendererModel bone132;
	private final RendererModel bone133;
	private final RendererModel bone134;
	private final RendererModel bone45;
	private final RendererModel bone46;
	private final RendererModel bone47;
	private final RendererModel bone48;
	private final RendererModel bone49;
	private final RendererModel bone50;
	private final RendererModel rotor_metal;
	private final RendererModel bone58;
	private final RendererModel bone59;
	private final RendererModel bone60;
	private final RendererModel bone61;
	private final RendererModel bone62;
	private final RendererModel rotor_centre;
	private final RendererModel edges;
	private final RendererModel panel_edges;
	private final RendererModel bone34;
	private final RendererModel bone35;
	private final RendererModel bone36;
	private final RendererModel bone37;
	private final RendererModel bone38;
	private final RendererModel bone39;
	private final RendererModel bone40;
	private final RendererModel bone41;
	private final RendererModel bone42;
	private final RendererModel bone43;
	private final RendererModel bone44;
	private final RendererModel top_panel_edges;
	private final RendererModel bone112;
	private final RendererModel bone113;
	private final RendererModel bone114;
	private final RendererModel bone115;
	private final RendererModel bone116;
	private final RendererModel bone117;
	private final RendererModel bone118;
	private final RendererModel bone119;
	private final RendererModel bone120;
	private final RendererModel bone121;
	private final RendererModel bone122;
	private final RendererModel border;
	private final RendererModel bone2;
	private final RendererModel bone3;
	private final RendererModel bone4;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel underconsole;
	private final RendererModel under_edges;
	private final RendererModel bone70;
	private final RendererModel bone71;
	private final RendererModel bone72;
	private final RendererModel bone73;
	private final RendererModel bone74;
	private final RendererModel bone75;
	private final RendererModel bone76;
	private final RendererModel bone77;
	private final RendererModel bone78;
	private final RendererModel bone79;
	private final RendererModel bone80;
	private final RendererModel under_panels;
	private final RendererModel bone22;
	private final RendererModel bone23;
	private final RendererModel bone24;
	private final RendererModel bone25;
	private final RendererModel bone26;
	private final RendererModel bone27;
	private final RendererModel bone28;
	private final RendererModel bone29;
	private final RendererModel bone30;
	private final RendererModel bone31;
	private final RendererModel bone32;
	private final RendererModel base;
	private final RendererModel base_backdrop;
	private final RendererModel bone106;
	private final RendererModel bone107;
	private final RendererModel bone108;
	private final RendererModel bone109;
	private final RendererModel bone110;
	private final RendererModel bone93;
	private final RendererModel bone94;
	private final RendererModel bone95;
	private final RendererModel bone96;
	private final RendererModel bone97;
	private final RendererModel bone98;
	private final RendererModel bone87;
	private final RendererModel bone88;
	private final RendererModel bone89;
	private final RendererModel bone90;
	private final RendererModel bone91;
	private final RendererModel bone92;
	private final RendererModel bone81;
	private final RendererModel bone82;
	private final RendererModel bone83;
	private final RendererModel bone84;
	private final RendererModel bone85;
	private final RendererModel bone86;
	private final RendererModel components;
	private final RendererModel north;
	private final RendererModel rotation;
	private final RendererModel landtype;
	private final RendererModel door_rotate_x;
	private final RendererModel bone8;
	private final RendererModel cutouts;
	private final RendererModel bone;
	private final RendererModel north_west;
	private final RendererModel rotation2;
	private final RendererModel facing_rotate_y;
	private final RendererModel refuel;
	private final RendererModel bone7;
	private final RendererModel bone63;
	private final RendererModel big_button_helm;
	private final RendererModel south_west;
	private final RendererModel rotation3;
	private final RendererModel cutouts2;
	private final RendererModel increment_translate_y;
	private final RendererModel y_control;
	private final RendererModel x_Control;
	private final RendererModel z_control;
	private final RendererModel bone21;
	private final RendererModel bone51;
	private final RendererModel bone57;
	private final RendererModel communicator;
	private final RendererModel sonicport;
	private final RendererModel south;
	private final RendererModel rotation4;
	private final RendererModel throttle_rotate_x;
	private final RendererModel stabilisers;
	private final RendererModel bone33;
	private final RendererModel cutouts3;
	private final RendererModel handbrake_rotate_y;
	private final RendererModel bone20;
	private final RendererModel north_east;
	private final RendererModel rotation6;
	private final RendererModel randomiser;
	private final RendererModel glow;
	private final RendererModel south_east;
	private final RendererModel rotation5;
	private final RendererModel telepathic;
	private final RendererModel north_east2;
	private final RendererModel rotation7;
	private final RendererModel monitor2;
	private final RendererModel north_west2;
	private final RendererModel rotation8;
	private final RendererModel dimensioncontrol;
	private final RendererModel bottom_beams;
	private final RendererModel bone100;
	private final RendererModel bone101;
	private final RendererModel bone102;
	private final RendererModel bone103;
	private final RendererModel bone104;
	private final RendererModel rotor_beams;
	private final RendererModel bone64;
	private final RendererModel bone65;
	private final RendererModel bone66;
	private final RendererModel bone67;
	private final RendererModel bone68;
	private final RendererModel rotor_bottom_translate_5;
	private final RendererModel rotor_top_translate_6;

	public ToyotaConsoleModel() {
		textureWidth = 256;
		textureHeight = 256;

		baseconsole = new RendererModel(this);
		baseconsole.setRotationPoint(0.0F, 23.0F, 0.0F);
		

		panels = new RendererModel(this);
		panels.setRotationPoint(0.0F, -26.0F, 0.0F);
		baseconsole.addChild(panels);
		

		bone14 = new RendererModel(this);
		bone14.setRotationPoint(0.0F, -1.0F, 19.1F);
		panels.addChild(bone14);
		setRotationAngle(bone14, -0.3491F, 0.0F, 0.0F);
		bone14.cubeList.add(new ModelBox(bone14, 160, 60, -20.0F, -5.8835F, 10.0938F, 40, 1, 8, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 160, 60, -15.5F, -5.8835F, 2.0938F, 31, 1, 8, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 160, 60, 5.0F, -5.8835F, -3.9062F, 6, 1, 6, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 160, 60, -11.0F, -5.8835F, -3.9062F, 6, 1, 6, 0.0F, false));
		bone14.cubeList.add(new ModelBox(bone14, 160, 60, -11.0F, -5.8835F, -5.9062F, 22, 1, 2, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		panels.addChild(bone9);
		setRotationAngle(bone9, 0.0F, -1.0472F, 0.0F);
		

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone9.addChild(bone10);
		setRotationAngle(bone10, -0.3491F, 0.0F, 0.0F);
		bone10.cubeList.add(new ModelBox(bone10, 160, 60, -20.0F, -5.8835F, 10.0938F, 40, 1, 8, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 160, 60, -15.5F, -5.8835F, 2.0938F, 31, 1, 8, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 160, 60, -11.0F, -5.8835F, -5.9062F, 22, 1, 8, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone9.addChild(bone11);
		setRotationAngle(bone11, 0.0F, -1.0472F, 0.0F);
		

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone11.addChild(bone12);
		setRotationAngle(bone12, -0.3491F, 0.0F, 0.0F);
		bone12.cubeList.add(new ModelBox(bone12, 160, 60, -20.0F, -5.8835F, 10.0938F, 40, 1, 8, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 160, 60, -15.5F, -5.8835F, 2.0938F, 31, 1, 8, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 160, 60, -11.0F, -5.8835F, -5.9062F, 22, 1, 8, 0.0F, false));

		bone13 = new RendererModel(this);
		bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone11.addChild(bone13);
		setRotationAngle(bone13, 0.0F, -1.0472F, 0.0F);
		

		bone15 = new RendererModel(this);
		bone15.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone13.addChild(bone15);
		setRotationAngle(bone15, -0.3491F, 0.0F, 0.0F);
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, 14.0F, -5.8835F, 10.0938F, 6, 1, 8, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, -10.0F, -5.8835F, 10.0938F, 20, 1, 5, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, -14.0F, -5.8835F, 15.0938F, 28, 1, 3, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, -20.0F, -5.8835F, 10.0938F, 6, 1, 8, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, 5.5F, -5.8835F, 2.0938F, 10, 1, 8, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, -5.5F, -5.8835F, 7.0938F, 11, 1, 3, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, -15.5F, -5.8835F, 2.0938F, 10, 1, 8, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, 5.0F, -5.8835F, 0.0938F, 6, 1, 2, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, -11.0F, -5.8835F, 0.0938F, 6, 1, 2, 0.0F, false));
		bone15.cubeList.add(new ModelBox(bone15, 160, 60, -11.0F, -5.8835F, -5.9062F, 22, 1, 6, 0.0F, false));

		bone16 = new RendererModel(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone13.addChild(bone16);
		setRotationAngle(bone16, 0.0F, -1.0472F, 0.0F);
		

		bone17 = new RendererModel(this);
		bone17.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone16.addChild(bone17);
		setRotationAngle(bone17, -0.3491F, 0.0F, 0.0F);
		bone17.cubeList.add(new ModelBox(bone17, 160, 60, -20.0F, -5.8835F, 10.0938F, 40, 1, 8, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 160, 60, -15.5F, -5.8835F, 2.0938F, 31, 1, 8, 0.0F, false));
		bone17.cubeList.add(new ModelBox(bone17, 160, 60, -11.0F, -5.8835F, -5.9062F, 22, 1, 8, 0.0F, false));

		bone18 = new RendererModel(this);
		bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone16.addChild(bone18);
		setRotationAngle(bone18, 0.0F, -1.0472F, 0.0F);
		

		bone19 = new RendererModel(this);
		bone19.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone18.addChild(bone19);
		setRotationAngle(bone19, -0.3491F, 0.0F, 0.0F);
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, 13.0F, -5.8835F, 10.0938F, 7, 1, 4, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, -20.0F, -5.8835F, 10.0938F, 7, 1, 4, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, -20.0F, -5.8835F, 14.0938F, 40, 1, 4, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, 11.5F, -5.8835F, 2.0938F, 4, 1, 8, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, -15.5F, -5.8835F, 2.0938F, 4, 1, 8, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, -11.5F, -5.8835F, 2.0938F, 23, 1, 3, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, -5.5F, -5.8835F, 5.0938F, 11, 1, 9, 0.0F, false));
		bone19.cubeList.add(new ModelBox(bone19, 160, 60, -11.0F, -5.8835F, -5.9062F, 22, 1, 8, 0.0F, false));

		centre = new RendererModel(this);
		centre.setRotationPoint(0.0F, 0.0F, 0.0F);
		baseconsole.addChild(centre);
		

		rotor_top = new RendererModel(this);
		rotor_top.setRotationPoint(0.0F, -90.0F, 0.0F);
		centre.addChild(rotor_top);
		rotor_top.cubeList.add(new ModelBox(rotor_top, 191, 75, -6.0F, -3.0F, -1.6F, 12, 3, 12, 0.0F, false));

		bone124 = new RendererModel(this);
		bone124.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor_top.addChild(bone124);
		setRotationAngle(bone124, 0.0F, -1.0472F, 0.0F);
		bone124.cubeList.add(new ModelBox(bone124, 191, 75, -6.0F, -3.0F, -1.6F, 12, 3, 12, 0.0F, false));

		bone125 = new RendererModel(this);
		bone125.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone124.addChild(bone125);
		setRotationAngle(bone125, 0.0F, -1.0472F, 0.0F);
		bone125.cubeList.add(new ModelBox(bone125, 191, 75, -6.0F, -3.0F, -1.6F, 12, 3, 12, 0.0F, false));

		bone126 = new RendererModel(this);
		bone126.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone125.addChild(bone126);
		setRotationAngle(bone126, 0.0F, -1.0472F, 0.0F);
		bone126.cubeList.add(new ModelBox(bone126, 191, 75, -6.0F, -3.0F, -1.6F, 12, 3, 12, 0.0F, false));

		bone127 = new RendererModel(this);
		bone127.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone126.addChild(bone127);
		setRotationAngle(bone127, 0.0F, -1.0472F, 0.0F);
		bone127.cubeList.add(new ModelBox(bone127, 191, 75, -6.0F, -3.0F, -1.6F, 12, 3, 12, 0.0F, false));

		bone128 = new RendererModel(this);
		bone128.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone127.addChild(bone128);
		setRotationAngle(bone128, 0.0F, -1.0472F, 0.0F);
		bone128.cubeList.add(new ModelBox(bone128, 191, 75, -6.0F, -3.0F, -1.6F, 12, 3, 12, 0.0F, false));

		rotor_base = new RendererModel(this);
		rotor_base.setRotationPoint(0.0F, -37.0F, 0.0F);
		centre.addChild(rotor_base);
		rotor_base.cubeList.add(new ModelBox(rotor_base, 191, 94, -6.0F, -2.05F, -1.6F, 12, 2, 12, 0.0F, false));

		bone52 = new RendererModel(this);
		bone52.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor_base.addChild(bone52);
		setRotationAngle(bone52, 0.0F, -1.0472F, 0.0F);
		bone52.cubeList.add(new ModelBox(bone52, 191, 94, -6.0F, -2.05F, -1.6F, 12, 2, 12, 0.0F, false));

		bone53 = new RendererModel(this);
		bone53.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone52.addChild(bone53);
		setRotationAngle(bone53, 0.0F, -1.0472F, 0.0F);
		bone53.cubeList.add(new ModelBox(bone53, 191, 94, -6.0F, -2.05F, -1.6F, 12, 2, 12, 0.0F, false));

		bone54 = new RendererModel(this);
		bone54.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone53.addChild(bone54);
		setRotationAngle(bone54, 0.0F, -1.0472F, 0.0F);
		bone54.cubeList.add(new ModelBox(bone54, 191, 94, -6.0F, -2.05F, -1.6F, 12, 2, 12, 0.0F, false));

		bone55 = new RendererModel(this);
		bone55.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone54.addChild(bone55);
		setRotationAngle(bone55, 0.0F, -1.0472F, 0.0F);
		bone55.cubeList.add(new ModelBox(bone55, 191, 94, -6.0F, -2.05F, -1.6F, 12, 2, 12, 0.0F, false));

		bone56 = new RendererModel(this);
		bone56.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone55.addChild(bone56);
		setRotationAngle(bone56, 0.0F, -1.0472F, 0.0F);
		bone56.cubeList.add(new ModelBox(bone56, 191, 94, -6.0F, -2.05F, -1.6F, 12, 2, 12, 0.0F, false));

		bone129 = new RendererModel(this);
		bone129.setRotationPoint(0.0F, -93.0F, 0.0F);
		centre.addChild(bone129);
		bone129.cubeList.add(new ModelBox(bone129, 134, 75, -7.0F, -2.0F, -0.9F, 14, 2, 13, 0.0F, false));

		bone130 = new RendererModel(this);
		bone130.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone129.addChild(bone130);
		setRotationAngle(bone130, 0.0F, -1.0472F, 0.0F);
		bone130.cubeList.add(new ModelBox(bone130, 134, 75, -7.0F, -2.0F, -0.9F, 14, 2, 13, 0.0F, false));

		bone131 = new RendererModel(this);
		bone131.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone130.addChild(bone131);
		setRotationAngle(bone131, 0.0F, -1.0472F, 0.0F);
		bone131.cubeList.add(new ModelBox(bone131, 134, 75, -7.0F, -2.0F, -0.9F, 14, 2, 13, 0.0F, false));

		bone132 = new RendererModel(this);
		bone132.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone131.addChild(bone132);
		setRotationAngle(bone132, 0.0F, -1.0472F, 0.0F);
		bone132.cubeList.add(new ModelBox(bone132, 134, 75, -7.0F, -2.0F, -0.9F, 14, 2, 13, 0.0F, false));

		bone133 = new RendererModel(this);
		bone133.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone132.addChild(bone133);
		setRotationAngle(bone133, 0.0F, -1.0472F, 0.0F);
		bone133.cubeList.add(new ModelBox(bone133, 134, 75, -7.0F, -2.0F, -0.9F, 14, 2, 13, 0.0F, false));

		bone134 = new RendererModel(this);
		bone134.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone133.addChild(bone134);
		setRotationAngle(bone134, 0.0F, -1.0472F, 0.0F);
		bone134.cubeList.add(new ModelBox(bone134, 134, 75, -7.0F, -2.0F, -0.9F, 14, 2, 13, 0.0F, false));

		bone45 = new RendererModel(this);
		bone45.setRotationPoint(0.0F, -35.0F, 0.0F);
		centre.addChild(bone45);
		bone45.cubeList.add(new ModelBox(bone45, 144, 93, -7.0F, -2.05F, 8.1F, 14, 2, 4, 0.0F, false));

		bone46 = new RendererModel(this);
		bone46.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone45.addChild(bone46);
		setRotationAngle(bone46, 0.0F, -1.0472F, 0.0F);
		bone46.cubeList.add(new ModelBox(bone46, 144, 93, -7.0F, -2.05F, 8.1F, 14, 2, 4, 0.0F, false));

		bone47 = new RendererModel(this);
		bone47.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone46.addChild(bone47);
		setRotationAngle(bone47, 0.0F, -1.0472F, 0.0F);
		bone47.cubeList.add(new ModelBox(bone47, 144, 93, -7.0F, -2.05F, 8.1F, 14, 2, 4, 0.0F, false));

		bone48 = new RendererModel(this);
		bone48.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone47.addChild(bone48);
		setRotationAngle(bone48, 0.0F, -1.0472F, 0.0F);
		bone48.cubeList.add(new ModelBox(bone48, 144, 93, -7.0F, -2.05F, 8.1F, 14, 2, 4, 0.0F, false));

		bone49 = new RendererModel(this);
		bone49.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone48.addChild(bone49);
		setRotationAngle(bone49, 0.0F, -1.0472F, 0.0F);
		bone49.cubeList.add(new ModelBox(bone49, 144, 93, -7.0F, -2.05F, 8.1F, 14, 2, 4, 0.0F, false));

		bone50 = new RendererModel(this);
		bone50.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone49.addChild(bone50);
		setRotationAngle(bone50, 0.0F, -1.0472F, 0.0F);
		bone50.cubeList.add(new ModelBox(bone50, 144, 93, -7.0F, -2.05F, 8.1F, 14, 2, 4, 0.0F, false));

		rotor_metal = new RendererModel(this);
		rotor_metal.setRotationPoint(0.0F, -39.0F, 0.0F);
		centre.addChild(rotor_metal);
		rotor_metal.cubeList.add(new ModelBox(rotor_metal, 240, 121, -2.0F, -9.05F, 5.9F, 4, 9, 4, 0.0F, false));
		rotor_metal.cubeList.add(new ModelBox(rotor_metal, 240, 121, -2.0F, -51.05F, 5.9F, 4, 9, 4, 0.0F, false));

		bone58 = new RendererModel(this);
		bone58.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor_metal.addChild(bone58);
		setRotationAngle(bone58, 0.0F, -1.0472F, 0.0F);
		bone58.cubeList.add(new ModelBox(bone58, 240, 121, -2.0F, -9.05F, 5.9F, 4, 9, 4, 0.0F, false));
		bone58.cubeList.add(new ModelBox(bone58, 240, 121, -2.0F, -51.05F, 5.9F, 4, 9, 4, 0.0F, false));

		bone59 = new RendererModel(this);
		bone59.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone58.addChild(bone59);
		setRotationAngle(bone59, 0.0F, -1.0472F, 0.0F);
		bone59.cubeList.add(new ModelBox(bone59, 240, 121, -2.0F, -9.05F, 5.9F, 4, 9, 4, 0.0F, false));
		bone59.cubeList.add(new ModelBox(bone59, 240, 121, -2.0F, -51.05F, 5.9F, 4, 9, 4, 0.0F, false));

		bone60 = new RendererModel(this);
		bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone59.addChild(bone60);
		setRotationAngle(bone60, 0.0F, -1.0472F, 0.0F);
		bone60.cubeList.add(new ModelBox(bone60, 240, 121, -2.0F, -9.05F, 5.9F, 4, 9, 4, 0.0F, false));
		bone60.cubeList.add(new ModelBox(bone60, 240, 121, -2.0F, -51.05F, 5.9F, 4, 9, 4, 0.0F, false));

		bone61 = new RendererModel(this);
		bone61.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone60.addChild(bone61);
		setRotationAngle(bone61, 0.0F, -1.0472F, 0.0F);
		bone61.cubeList.add(new ModelBox(bone61, 240, 121, -2.0F, -9.05F, 5.9F, 4, 9, 4, 0.0F, false));
		bone61.cubeList.add(new ModelBox(bone61, 240, 121, -2.0F, -51.05F, 5.9F, 4, 9, 4, 0.0F, false));

		bone62 = new RendererModel(this);
		bone62.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone61.addChild(bone62);
		setRotationAngle(bone62, 0.0F, -1.0472F, 0.0F);
		bone62.cubeList.add(new ModelBox(bone62, 240, 121, -2.0F, -9.05F, 5.9F, 4, 9, 4, 0.0F, false));
		bone62.cubeList.add(new ModelBox(bone62, 240, 121, -2.0F, -51.05F, 5.9F, 4, 9, 4, 0.0F, false));

		rotor_centre = new RendererModel(this);
		rotor_centre.setRotationPoint(-1.0F, -50.0F, 12.0F);
		centre.addChild(rotor_centre);
		rotor_centre.cubeList.add(new ModelBox(rotor_centre, 216, 118, -1.0F, 0.0F, -14.0F, 4, 11, 4, 0.0F, false));
		rotor_centre.cubeList.add(new ModelBox(rotor_centre, 216, 118, -1.0F, -40.0F, -14.0F, 4, 11, 4, 0.0F, false));

		edges = new RendererModel(this);
		edges.setRotationPoint(0.0F, 0.0F, 0.0F);
		baseconsole.addChild(edges);
		

		panel_edges = new RendererModel(this);
		panel_edges.setRotationPoint(0.0F, -27.75F, 0.0F);
		edges.addChild(panel_edges);
		setRotationAngle(panel_edges, 0.0F, -0.5236F, 0.0F);
		

		bone34 = new RendererModel(this);
		bone34.setRotationPoint(0.0F, -1.0F, 19.1F);
		panel_edges.addChild(bone34);
		setRotationAngle(bone34, -0.2967F, 0.0F, 0.0F);
		bone34.cubeList.add(new ModelBox(bone34, 184, 153, -2.0F, -5.8835F, -9.0062F, 4, 2, 32, 0.0F, false));

		bone35 = new RendererModel(this);
		bone35.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel_edges.addChild(bone35);
		setRotationAngle(bone35, 0.0F, -1.0472F, 0.0F);
		

		bone36 = new RendererModel(this);
		bone36.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone35.addChild(bone36);
		setRotationAngle(bone36, -0.2967F, 0.0F, 0.0F);
		bone36.cubeList.add(new ModelBox(bone36, 184, 153, -2.0F, -5.8835F, -9.0062F, 4, 2, 32, 0.0F, false));

		bone37 = new RendererModel(this);
		bone37.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone35.addChild(bone37);
		setRotationAngle(bone37, 0.0F, -1.0472F, 0.0F);
		

		bone38 = new RendererModel(this);
		bone38.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone37.addChild(bone38);
		setRotationAngle(bone38, -0.2967F, 0.0F, 0.0F);
		bone38.cubeList.add(new ModelBox(bone38, 184, 153, -2.0F, -5.8835F, -9.0062F, 4, 2, 32, 0.0F, false));

		bone39 = new RendererModel(this);
		bone39.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone37.addChild(bone39);
		setRotationAngle(bone39, 0.0F, -1.0472F, 0.0F);
		

		bone40 = new RendererModel(this);
		bone40.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone39.addChild(bone40);
		setRotationAngle(bone40, -0.2967F, 0.0F, 0.0F);
		bone40.cubeList.add(new ModelBox(bone40, 184, 153, -2.0F, -5.8835F, -9.0062F, 4, 2, 32, 0.0F, false));

		bone41 = new RendererModel(this);
		bone41.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone39.addChild(bone41);
		setRotationAngle(bone41, 0.0F, -1.0472F, 0.0F);
		

		bone42 = new RendererModel(this);
		bone42.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone41.addChild(bone42);
		setRotationAngle(bone42, -0.2967F, 0.0F, 0.0F);
		bone42.cubeList.add(new ModelBox(bone42, 184, 153, -2.0F, -5.8835F, -9.0062F, 4, 2, 32, 0.0F, false));

		bone43 = new RendererModel(this);
		bone43.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone41.addChild(bone43);
		setRotationAngle(bone43, 0.0F, -1.0472F, 0.0F);
		

		bone44 = new RendererModel(this);
		bone44.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone43.addChild(bone44);
		setRotationAngle(bone44, -0.2967F, 0.0F, 0.0F);
		bone44.cubeList.add(new ModelBox(bone44, 184, 153, -2.0F, -5.8835F, -9.0062F, 4, 2, 32, 0.0F, false));

		top_panel_edges = new RendererModel(this);
		top_panel_edges.setRotationPoint(0.0F, -26.0F, 0.0F);
		edges.addChild(top_panel_edges);
		

		bone112 = new RendererModel(this);
		bone112.setRotationPoint(0.0F, -1.0F, 19.1F);
		top_panel_edges.addChild(bone112);
		setRotationAngle(bone112, -0.3491F, 0.0F, 0.0F);
		bone112.cubeList.add(new ModelBox(bone112, 163, 128, -7.0F, -6.4943F, -9.8785F, 14, 1, 4, 0.0F, false));

		bone113 = new RendererModel(this);
		bone113.setRotationPoint(0.0F, 0.0F, 0.0F);
		top_panel_edges.addChild(bone113);
		setRotationAngle(bone113, 0.0F, -1.0472F, 0.0F);
		

		bone114 = new RendererModel(this);
		bone114.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone113.addChild(bone114);
		setRotationAngle(bone114, -0.3491F, 0.0F, 0.0F);
		bone114.cubeList.add(new ModelBox(bone114, 163, 128, -7.0F, -6.4943F, -9.8785F, 14, 1, 4, 0.0F, false));

		bone115 = new RendererModel(this);
		bone115.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone113.addChild(bone115);
		setRotationAngle(bone115, 0.0F, -1.0472F, 0.0F);
		

		bone116 = new RendererModel(this);
		bone116.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone115.addChild(bone116);
		setRotationAngle(bone116, -0.3491F, 0.0F, 0.0F);
		bone116.cubeList.add(new ModelBox(bone116, 163, 128, -7.0F, -6.4943F, -9.8785F, 14, 1, 4, 0.0F, false));

		bone117 = new RendererModel(this);
		bone117.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone115.addChild(bone117);
		setRotationAngle(bone117, 0.0F, -1.0472F, 0.0F);
		

		bone118 = new RendererModel(this);
		bone118.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone117.addChild(bone118);
		setRotationAngle(bone118, -0.3491F, 0.0F, 0.0F);
		bone118.cubeList.add(new ModelBox(bone118, 163, 128, -7.0F, -6.4943F, -9.8785F, 14, 1, 4, 0.0F, false));

		bone119 = new RendererModel(this);
		bone119.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone117.addChild(bone119);
		setRotationAngle(bone119, 0.0F, -1.0472F, 0.0F);
		

		bone120 = new RendererModel(this);
		bone120.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone119.addChild(bone120);
		setRotationAngle(bone120, -0.3491F, 0.0F, 0.0F);
		bone120.cubeList.add(new ModelBox(bone120, 163, 128, -7.0F, -6.4943F, -9.8785F, 14, 1, 4, 0.0F, false));

		bone121 = new RendererModel(this);
		bone121.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone119.addChild(bone121);
		setRotationAngle(bone121, 0.0F, -1.0472F, 0.0F);
		

		bone122 = new RendererModel(this);
		bone122.setRotationPoint(0.0F, -1.0F, 19.1F);
		bone121.addChild(bone122);
		setRotationAngle(bone122, -0.3491F, 0.0F, 0.0F);
		bone122.cubeList.add(new ModelBox(bone122, 163, 128, -7.0F, -6.4943F, -9.8785F, 14, 1, 4, 0.0F, false));

		border = new RendererModel(this);
		border.setRotationPoint(0.0F, -26.0F, 0.0F);
		edges.addChild(border);
		border.cubeList.add(new ModelBox(border, 164, 143, -22.0F, -2.0F, 36.125F, 44, 4, 2, 0.0F, false));
		border.cubeList.add(new ModelBox(border, 143, 135, -16.0F, 0.0F, 34.125F, 32, 4, 2, 0.0F, false));
		border.cubeList.add(new ModelBox(border, 164, 143, -6.0F, 2.0F, 36.125F, 14, 2, 2, 0.0F, false));
		border.cubeList.add(new ModelBox(border, 164, 143, 16.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));
		border.cubeList.add(new ModelBox(border, 164, 143, -22.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));

		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		border.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		bone2.cubeList.add(new ModelBox(bone2, 164, 143, -22.0F, -2.0F, 36.125F, 44, 4, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 143, 135, -16.0F, 0.0F, 34.125F, 32, 4, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 164, 143, -6.0F, 2.0F, 36.125F, 14, 2, 2, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 164, 143, 16.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));
		bone2.cubeList.add(new ModelBox(bone2, 164, 143, -22.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
		bone3.cubeList.add(new ModelBox(bone3, 164, 143, -22.0F, -2.0F, 36.125F, 44, 4, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 143, 135, -16.0F, 0.0F, 34.125F, 32, 4, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 164, 143, -6.0F, 2.0F, 36.125F, 14, 2, 2, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 164, 143, 16.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));
		bone3.cubeList.add(new ModelBox(bone3, 164, 143, -22.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -1.0472F, 0.0F);
		bone4.cubeList.add(new ModelBox(bone4, 164, 143, -22.0F, -2.0F, 36.125F, 44, 4, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 143, 135, -16.0F, 0.0F, 34.125F, 32, 4, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 164, 143, -6.0F, 2.0F, 36.125F, 14, 2, 2, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 164, 143, 16.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));
		bone4.cubeList.add(new ModelBox(bone4, 164, 143, -22.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(bone5);
		setRotationAngle(bone5, 0.0F, -1.0472F, 0.0F);
		bone5.cubeList.add(new ModelBox(bone5, 164, 143, -22.0F, -2.0F, 36.125F, 44, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 143, 135, -16.0F, 0.0F, 34.125F, 32, 4, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 164, 143, -6.0F, 2.0F, 36.125F, 14, 2, 2, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 164, 143, 16.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));
		bone5.cubeList.add(new ModelBox(bone5, 164, 143, -22.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone5.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -1.0472F, 0.0F);
		bone6.cubeList.add(new ModelBox(bone6, 164, 143, -22.0F, -2.0F, 36.125F, 44, 4, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 143, 135, -16.0F, 0.0F, 34.125F, 32, 4, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 164, 143, -6.0F, 2.0F, 36.125F, 14, 2, 2, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 164, 143, 16.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));
		bone6.cubeList.add(new ModelBox(bone6, 164, 143, -22.0F, 2.0F, 34.125F, 6, 2, 4, 0.0F, false));

		underconsole = new RendererModel(this);
		underconsole.setRotationPoint(0.0F, 0.0F, 0.0F);
		baseconsole.addChild(underconsole);
		

		under_edges = new RendererModel(this);
		under_edges.setRotationPoint(0.0F, -27.75F, 0.0F);
		underconsole.addChild(under_edges);
		setRotationAngle(under_edges, 0.0F, -0.5236F, 0.0F);
		

		bone70 = new RendererModel(this);
		bone70.setRotationPoint(4.0F, -1.0F, 19.1F);
		under_edges.addChild(bone70);
		setRotationAngle(bone70, 0.2269F, 0.0F, 0.0F);
		bone70.cubeList.add(new ModelBox(bone70, 186, 155, -6.0F, 9.9796F, -11.8734F, 4, 1, 30, 0.0F, false));

		bone71 = new RendererModel(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		under_edges.addChild(bone71);
		setRotationAngle(bone71, 0.0F, -1.0472F, 0.0F);
		

		bone72 = new RendererModel(this);
		bone72.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone71.addChild(bone72);
		setRotationAngle(bone72, 0.2269F, 0.0F, 0.0F);
		bone72.cubeList.add(new ModelBox(bone72, 186, 155, -6.0F, 9.9796F, -11.8734F, 4, 1, 30, 0.0F, false));

		bone73 = new RendererModel(this);
		bone73.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone71.addChild(bone73);
		setRotationAngle(bone73, 0.0F, -1.0472F, 0.0F);
		

		bone74 = new RendererModel(this);
		bone74.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone73.addChild(bone74);
		setRotationAngle(bone74, 0.2269F, 0.0F, 0.0F);
		bone74.cubeList.add(new ModelBox(bone74, 186, 155, -6.0F, 9.9796F, -11.8734F, 4, 1, 30, 0.0F, false));

		bone75 = new RendererModel(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone73.addChild(bone75);
		setRotationAngle(bone75, 0.0F, -1.0472F, 0.0F);
		

		bone76 = new RendererModel(this);
		bone76.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone75.addChild(bone76);
		setRotationAngle(bone76, 0.2269F, 0.0F, 0.0F);
		bone76.cubeList.add(new ModelBox(bone76, 186, 155, -6.0F, 9.9796F, -11.8734F, 4, 1, 30, 0.0F, false));

		bone77 = new RendererModel(this);
		bone77.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone75.addChild(bone77);
		setRotationAngle(bone77, 0.0F, -1.0472F, 0.0F);
		

		bone78 = new RendererModel(this);
		bone78.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone77.addChild(bone78);
		setRotationAngle(bone78, 0.2269F, 0.0F, 0.0F);
		bone78.cubeList.add(new ModelBox(bone78, 186, 155, -6.0F, 9.9796F, -11.8734F, 4, 1, 30, 0.0F, false));

		bone79 = new RendererModel(this);
		bone79.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone77.addChild(bone79);
		setRotationAngle(bone79, 0.0F, -1.0472F, 0.0F);
		

		bone80 = new RendererModel(this);
		bone80.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone79.addChild(bone80);
		setRotationAngle(bone80, 0.2269F, 0.0F, 0.0F);
		bone80.cubeList.add(new ModelBox(bone80, 186, 155, -6.0F, 9.9796F, -11.8734F, 4, 1, 30, 0.0F, false));

		under_panels = new RendererModel(this);
		under_panels.setRotationPoint(0.0F, -27.0F, 0.0F);
		underconsole.addChild(under_panels);
		

		bone22 = new RendererModel(this);
		bone22.setRotationPoint(4.0F, -1.0F, 19.1F);
		under_panels.addChild(bone22);
		setRotationAngle(bone22, 0.2618F, 0.0F, 0.0F);
		bone22.cubeList.add(new ModelBox(bone22, 170, 45, -22.5F, 8.2296F, 7.1266F, 37, 1, 6, 0.0F, false));
		bone22.cubeList.add(new ModelBox(bone22, 170, 45, -18.5F, 8.2296F, -0.8734F, 29, 1, 8, 0.0F, false));
		bone22.cubeList.add(new ModelBox(bone22, 170, 45, -13.5F, 8.2296F, -8.8734F, 19, 1, 8, 0.0F, false));
		bone22.cubeList.add(new ModelBox(bone22, 170, 45, -9.5F, 8.2296F, -12.8734F, 11, 1, 4, 0.0F, false));

		bone23 = new RendererModel(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		under_panels.addChild(bone23);
		setRotationAngle(bone23, 0.0F, -1.0472F, 0.0F);
		

		bone24 = new RendererModel(this);
		bone24.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone23.addChild(bone24);
		setRotationAngle(bone24, 0.2618F, 0.0F, 0.0F);
		bone24.cubeList.add(new ModelBox(bone24, 170, 45, -22.5F, 8.2296F, 7.1266F, 37, 1, 6, 0.0F, false));
		bone24.cubeList.add(new ModelBox(bone24, 170, 45, -18.5F, 8.2296F, -0.8734F, 29, 1, 8, 0.0F, false));
		bone24.cubeList.add(new ModelBox(bone24, 170, 45, -13.5F, 8.2296F, -8.8734F, 19, 1, 8, 0.0F, false));
		bone24.cubeList.add(new ModelBox(bone24, 170, 45, -9.5F, 8.2296F, -12.8734F, 11, 1, 4, 0.0F, false));

		bone25 = new RendererModel(this);
		bone25.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone23.addChild(bone25);
		setRotationAngle(bone25, 0.0F, -1.0472F, 0.0F);
		

		bone26 = new RendererModel(this);
		bone26.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone25.addChild(bone26);
		setRotationAngle(bone26, 0.2618F, 0.0F, 0.0F);
		bone26.cubeList.add(new ModelBox(bone26, 170, 45, -22.5F, 8.2296F, 7.1266F, 37, 1, 6, 0.0F, false));
		bone26.cubeList.add(new ModelBox(bone26, 170, 45, -18.5F, 8.2296F, -0.8734F, 29, 1, 8, 0.0F, false));
		bone26.cubeList.add(new ModelBox(bone26, 170, 45, -13.5F, 8.2296F, -8.8734F, 19, 1, 8, 0.0F, false));
		bone26.cubeList.add(new ModelBox(bone26, 170, 45, -9.5F, 8.2296F, -12.8734F, 11, 1, 4, 0.0F, false));

		bone27 = new RendererModel(this);
		bone27.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone25.addChild(bone27);
		setRotationAngle(bone27, 0.0F, -1.0472F, 0.0F);
		

		bone28 = new RendererModel(this);
		bone28.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone27.addChild(bone28);
		setRotationAngle(bone28, 0.2618F, 0.0F, 0.0F);
		bone28.cubeList.add(new ModelBox(bone28, 170, 45, -22.5F, 8.2296F, 7.1266F, 37, 1, 6, 0.0F, false));
		bone28.cubeList.add(new ModelBox(bone28, 170, 45, -18.5F, 8.2296F, -0.8734F, 29, 1, 8, 0.0F, false));
		bone28.cubeList.add(new ModelBox(bone28, 170, 45, -13.5F, 8.2296F, -8.8734F, 19, 1, 8, 0.0F, false));
		bone28.cubeList.add(new ModelBox(bone28, 170, 45, -9.5F, 8.2296F, -12.8734F, 11, 1, 4, 0.0F, false));

		bone29 = new RendererModel(this);
		bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone27.addChild(bone29);
		setRotationAngle(bone29, 0.0F, -1.0472F, 0.0F);
		

		bone30 = new RendererModel(this);
		bone30.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone29.addChild(bone30);
		setRotationAngle(bone30, 0.2618F, 0.0F, 0.0F);
		bone30.cubeList.add(new ModelBox(bone30, 170, 45, -22.5F, 8.2296F, 7.1266F, 37, 1, 6, 0.0F, false));
		bone30.cubeList.add(new ModelBox(bone30, 170, 45, -18.5F, 8.2296F, -0.8734F, 29, 1, 8, 0.0F, false));
		bone30.cubeList.add(new ModelBox(bone30, 170, 45, -13.5F, 8.2296F, -8.8734F, 19, 1, 8, 0.0F, false));
		bone30.cubeList.add(new ModelBox(bone30, 170, 45, -9.5F, 8.2296F, -12.8734F, 11, 1, 4, 0.0F, false));

		bone31 = new RendererModel(this);
		bone31.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone29.addChild(bone31);
		setRotationAngle(bone31, 0.0F, -1.0472F, 0.0F);
		

		bone32 = new RendererModel(this);
		bone32.setRotationPoint(4.0F, -1.0F, 19.1F);
		bone31.addChild(bone32);
		setRotationAngle(bone32, 0.2618F, 0.0F, 0.0F);
		bone32.cubeList.add(new ModelBox(bone32, 170, 45, -22.5F, 8.2296F, 7.1266F, 37, 1, 6, 0.0F, false));
		bone32.cubeList.add(new ModelBox(bone32, 170, 45, -18.5F, 8.2296F, -0.8734F, 29, 1, 8, 0.0F, false));
		bone32.cubeList.add(new ModelBox(bone32, 170, 45, -13.5F, 8.2296F, -8.8734F, 19, 1, 8, 0.0F, false));
		bone32.cubeList.add(new ModelBox(bone32, 170, 45, -9.5F, 8.2296F, -12.8734F, 11, 1, 4, 0.0F, false));

		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		baseconsole.addChild(base);
		

		base_backdrop = new RendererModel(this);
		base_backdrop.setRotationPoint(0.0F, -4.0F, 0.0F);
		base.addChild(base_backdrop);
		base_backdrop.cubeList.add(new ModelBox(base_backdrop, 190, 156, -4.0F, -11.0F, 5.4F, 8, 14, 1, 0.0F, false));

		bone106 = new RendererModel(this);
		bone106.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_backdrop.addChild(bone106);
		setRotationAngle(bone106, 0.0F, -1.0472F, 0.0F);
		bone106.cubeList.add(new ModelBox(bone106, 190, 156, -4.0F, -11.0F, 5.4F, 8, 14, 1, 0.0F, false));

		bone107 = new RendererModel(this);
		bone107.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone106.addChild(bone107);
		setRotationAngle(bone107, 0.0F, -1.0472F, 0.0F);
		bone107.cubeList.add(new ModelBox(bone107, 190, 156, -4.0F, -11.0F, 5.4F, 8, 14, 1, 0.0F, false));

		bone108 = new RendererModel(this);
		bone108.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone107.addChild(bone108);
		setRotationAngle(bone108, 0.0F, -1.0472F, 0.0F);
		bone108.cubeList.add(new ModelBox(bone108, 190, 156, -4.0F, -11.0F, 5.4F, 8, 14, 1, 0.0F, false));

		bone109 = new RendererModel(this);
		bone109.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone108.addChild(bone109);
		setRotationAngle(bone109, 0.0F, -1.0472F, 0.0F);
		bone109.cubeList.add(new ModelBox(bone109, 190, 156, -4.0F, -11.0F, 5.4F, 8, 14, 1, 0.0F, false));

		bone110 = new RendererModel(this);
		bone110.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone109.addChild(bone110);
		setRotationAngle(bone110, 0.0F, -1.0472F, 0.0F);
		bone110.cubeList.add(new ModelBox(bone110, 190, 156, -4.0F, -11.0F, 5.4F, 8, 14, 1, 0.0F, false));

		bone93 = new RendererModel(this);
		bone93.setRotationPoint(0.0F, -1.0F, 0.0F);
		base.addChild(bone93);
		setRotationAngle(bone93, 0.0F, -0.5236F, 0.0F);
		bone93.cubeList.add(new ModelBox(bone93, 158, 156, -0.5F, -14.0F, 7.6F, 1, 14, 6, 0.0F, false));

		bone94 = new RendererModel(this);
		bone94.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone93.addChild(bone94);
		setRotationAngle(bone94, 0.0F, -1.0472F, 0.0F);
		bone94.cubeList.add(new ModelBox(bone94, 158, 156, -0.5F, -14.0F, 7.6F, 1, 14, 6, 0.0F, false));

		bone95 = new RendererModel(this);
		bone95.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone94.addChild(bone95);
		setRotationAngle(bone95, 0.0F, -1.0472F, 0.0F);
		bone95.cubeList.add(new ModelBox(bone95, 158, 156, -0.5F, -14.0F, 7.6F, 1, 14, 6, 0.0F, false));

		bone96 = new RendererModel(this);
		bone96.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone95.addChild(bone96);
		setRotationAngle(bone96, 0.0F, -1.0472F, 0.0F);
		bone96.cubeList.add(new ModelBox(bone96, 158, 156, -0.5F, -14.0F, 7.6F, 1, 14, 6, 0.0F, false));

		bone97 = new RendererModel(this);
		bone97.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone96.addChild(bone97);
		setRotationAngle(bone97, 0.0F, -1.0472F, 0.0F);
		bone97.cubeList.add(new ModelBox(bone97, 158, 156, -0.5F, -14.0F, 7.6F, 1, 14, 6, 0.0F, false));

		bone98 = new RendererModel(this);
		bone98.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone97.addChild(bone98);
		setRotationAngle(bone98, 0.0F, -1.0472F, 0.0F);
		bone98.cubeList.add(new ModelBox(bone98, 158, 156, -0.5F, -14.0F, 7.6F, 1, 14, 6, 0.0F, false));

		bone87 = new RendererModel(this);
		bone87.setRotationPoint(0.0F, 1.0F, 0.0F);
		base.addChild(bone87);
		bone87.cubeList.add(new ModelBox(bone87, 136, 179, -8.0F, -2.0F, -0.15F, 16, 2, 14, 0.0F, false));

		bone88 = new RendererModel(this);
		bone88.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone87.addChild(bone88);
		setRotationAngle(bone88, 0.0F, -1.0472F, 0.0F);
		bone88.cubeList.add(new ModelBox(bone88, 136, 179, -8.0F, -2.0F, -0.15F, 16, 2, 14, 0.0F, false));

		bone89 = new RendererModel(this);
		bone89.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone88.addChild(bone89);
		setRotationAngle(bone89, 0.0F, -1.0472F, 0.0F);
		bone89.cubeList.add(new ModelBox(bone89, 136, 179, -8.0F, -2.0F, -0.15F, 16, 2, 14, 0.0F, false));

		bone90 = new RendererModel(this);
		bone90.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone89.addChild(bone90);
		setRotationAngle(bone90, 0.0F, -1.0472F, 0.0F);
		bone90.cubeList.add(new ModelBox(bone90, 136, 179, -8.0F, -2.0F, -0.15F, 16, 2, 14, 0.0F, false));

		bone91 = new RendererModel(this);
		bone91.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone90.addChild(bone91);
		setRotationAngle(bone91, 0.0F, -1.0472F, 0.0F);
		bone91.cubeList.add(new ModelBox(bone91, 136, 179, -8.0F, -2.0F, -0.15F, 16, 2, 14, 0.0F, false));

		bone92 = new RendererModel(this);
		bone92.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone91.addChild(bone92);
		setRotationAngle(bone92, 0.0F, -1.0472F, 0.0F);
		bone92.cubeList.add(new ModelBox(bone92, 136, 179, -8.0F, -2.0F, -0.15F, 16, 2, 14, 0.0F, false));

		bone81 = new RendererModel(this);
		bone81.setRotationPoint(0.0F, -15.0F, 0.0F);
		base.addChild(bone81);
		bone81.cubeList.add(new ModelBox(bone81, 146, 187, -7.0F, -2.05F, 6.1F, 14, 2, 6, 0.0F, false));

		bone82 = new RendererModel(this);
		bone82.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone81.addChild(bone82);
		setRotationAngle(bone82, 0.0F, -1.0472F, 0.0F);
		bone82.cubeList.add(new ModelBox(bone82, 146, 187, -7.0F, -2.05F, 6.1F, 14, 2, 6, 0.0F, false));

		bone83 = new RendererModel(this);
		bone83.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone82.addChild(bone83);
		setRotationAngle(bone83, 0.0F, -1.0472F, 0.0F);
		bone83.cubeList.add(new ModelBox(bone83, 146, 187, -7.0F, -2.05F, 6.1F, 14, 2, 6, 0.0F, false));

		bone84 = new RendererModel(this);
		bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone83.addChild(bone84);
		setRotationAngle(bone84, 0.0F, -1.0472F, 0.0F);
		bone84.cubeList.add(new ModelBox(bone84, 146, 187, -7.0F, -2.05F, 6.1F, 14, 2, 6, 0.0F, false));

		bone85 = new RendererModel(this);
		bone85.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone84.addChild(bone85);
		setRotationAngle(bone85, 0.0F, -1.0472F, 0.0F);
		bone85.cubeList.add(new ModelBox(bone85, 146, 187, -7.0F, -2.05F, 6.1F, 14, 2, 6, 0.0F, false));

		bone86 = new RendererModel(this);
		bone86.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone85.addChild(bone86);
		setRotationAngle(bone86, 0.0F, -1.0472F, 0.0F);
		bone86.cubeList.add(new ModelBox(bone86, 146, 187, -7.0F, -2.05F, 6.1F, 14, 2, 6, 0.0F, false));

		components = new RendererModel(this);
		components.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		north = new RendererModel(this);
		north.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(north);
		setRotationAngle(north, 0.0F, 3.1416F, 0.0F);
		

		rotation = new RendererModel(this);
		rotation.setRotationPoint(0.0F, -1.0F, 19.1F);
		north.addChild(rotation);
		setRotationAngle(rotation, -0.3491F, 0.0F, 0.0F);
		rotation.cubeList.add(new ModelBox(rotation, 40, 13, -7.0F, -6.1335F, -3.9062F, 4, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 40, 18, -1.5F, -6.3835F, -4.4062F, 3, 1, 3, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 6, -1.0F, -6.3835F, 10.5938F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 10, 7, -0.5F, -6.1335F, 13.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 6, 1.5F, -6.3835F, 10.5938F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 10, 7, 2.0F, -6.1335F, 13.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 6, -3.5F, -6.3835F, 10.5938F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 10, 7, -3.0F, -6.1335F, 13.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 6, -6.0F, -6.3835F, 10.5938F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 10, 7, -5.5F, -6.1335F, 13.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 6, 4.0F, -6.3835F, 10.5938F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 10, 7, 4.5F, -6.1335F, 13.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 10, 10.5F, -5.6335F, 10.5938F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 15, 10, -12.5F, -5.6335F, 12.5938F, 2, 1, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 22, 9, -12.5F, -5.8835F, 10.5938F, 2, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 17, 6, -13.75F, -6.3835F, 13.0938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 9, 11, 10.5F, -5.8835F, 13.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 9, 11, 12.5F, -5.8835F, 13.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 0, 6.5F, -7.1335F, 6.5938F, 2, 2, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 32, 0, 6.5F, -6.3835F, 2.5938F, 3, 1, 3, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 46, 2, 6.5F, -6.3835F, 0.5938F, 1, 1, 1, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 0, 0, -8.5F, -7.1335F, 6.5938F, 2, 2, 2, 0.0F, false));
		rotation.cubeList.add(new ModelBox(rotation, 40, 13, 3.0F, -6.1335F, -3.9062F, 4, 1, 2, 0.0F, false));

		landtype = new RendererModel(this);
		landtype.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation.addChild(landtype);
		landtype.cubeList.add(new ModelBox(landtype, 52, 6, -8.5F, -7.8835F, 7.4438F, 1, 1, 1, 0.0F, false));
		landtype.cubeList.add(new ModelBox(landtype, 30, 6, -10.0F, -7.1335F, 6.4438F, 4, 1, 3, 0.0F, false));
		landtype.cubeList.add(new ModelBox(landtype, 42, 6, -9.5F, -7.3835F, 7.4438F, 3, 1, 1, 0.0F, false));

		door_rotate_x = new RendererModel(this);
		door_rotate_x.setRotationPoint(7.5F, -6.8835F, 7.5938F);
		rotation.addChild(door_rotate_x);
		door_rotate_x.cubeList.add(new ModelBox(door_rotate_x, 9, 0, -0.5F, -0.5F, 0.0F, 1, 1, 4, 0.0F, false));
		door_rotate_x.cubeList.add(new ModelBox(door_rotate_x, 20, 0, -0.5F, -0.75F, 4.0F, 1, 1, 4, 0.0F, false));

		bone8 = new RendererModel(this);
		bone8.setRotationPoint(-7.5F, -6.8835F, 7.5938F);
		rotation.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 9, 0, -0.5F, -0.5F, 0.0F, 1, 1, 4, 0.0F, false));
		bone8.cubeList.add(new ModelBox(bone8, 20, 0, -0.5F, -0.75F, 4.0F, 1, 1, 4, 0.0F, false));

		cutouts = new RendererModel(this);
		cutouts.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation.addChild(cutouts);
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, 5.5F, -6.8585F, 7.1938F, 1, 2, 5, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, 5.0F, -6.8585F, 5.1938F, 1, 2, 2, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, -6.0F, -6.8585F, 5.1938F, 1, 2, 2, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, -5.0F, -6.8585F, 4.1938F, 10, 2, 1, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, -6.5F, -6.8585F, 7.1938F, 1, 2, 5, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, -15.0F, -6.8595F, 15.1938F, 1, 1, 5, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, 9.0F, -6.8595F, 15.1938F, 1, 1, 5, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, -14.0F, -6.8595F, 14.1938F, 4, 1, 1, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, 10.0F, -6.8595F, 14.1938F, 4, 1, 1, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, -14.0F, -6.8595F, 20.1938F, 4, 1, 1, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, 10.0F, -6.8595F, 20.1938F, 4, 1, 1, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 61, 5, -14.0F, -6.3595F, 15.1938F, 4, 1, 5, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 61, 5, 10.0F, -6.3595F, 15.1938F, 4, 1, 5, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, -10.0F, -6.8595F, 15.1938F, 1, 1, 5, 0.0F, false));
		cutouts.cubeList.add(new ModelBox(cutouts, 56, 14, 14.0F, -6.8595F, 15.1938F, 1, 1, 5, 0.0F, false));

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, -6.5417F, 4.4836F);
		rotation.addChild(bone);
		setRotationAngle(bone, 0.1745F, 0.0F, 0.0F);
		bone.cubeList.add(new ModelBox(bone, 51, 2, -5.5F, 1.1005F, -5.5436F, 11, 1, 8, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 6, 15, -1.0F, 0.8505F, 0.2064F, 2, 1, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 14, 1.5F, 0.8505F, 0.7064F, 1, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 14, 3.5F, 0.8505F, 0.7064F, 1, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 14, -2.5F, 0.8505F, 0.7064F, 1, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 0, 14, -4.5F, 0.8505F, 0.7064F, 1, 1, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 15, 14, 2.5F, 0.6005F, -4.0436F, 1, 1, 3, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 21, 15, -4.0F, 0.3505F, -3.7936F, 8, 1, 0, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 15, 14, 0.5F, 0.6005F, -4.0436F, 1, 1, 3, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 15, 14, -1.5F, 0.6005F, -4.0436F, 1, 1, 3, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 15, 14, -3.5F, 0.6005F, -4.0436F, 1, 1, 3, 0.0F, false));

		north_west = new RendererModel(this);
		north_west.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(north_west);
		setRotationAngle(north_west, 0.0F, 2.0944F, 0.0F);
		

		rotation2 = new RendererModel(this);
		rotation2.setRotationPoint(0.0F, -1.0F, 19.1F);
		north_west.addChild(rotation2);
		setRotationAngle(rotation2, -0.3491F, 0.0F, 0.0F);
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, 6.0F, -6.8835F, 2.5938F, 1, 1, 6, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -7.0F, -6.8835F, 2.5938F, 1, 1, 6, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, 5.0F, -6.8835F, 0.5938F, 1, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, 3.0F, -6.8835F, -0.4062F, 2, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, 3.0F, -6.8835F, 10.5938F, 2, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -3.0F, -6.8835F, 11.5938F, 6, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -3.0F, -6.8835F, -1.4062F, 6, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -5.0F, -6.8835F, -0.4062F, 2, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -5.0F, -6.8835F, 10.5938F, 2, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -6.0F, -6.8835F, 0.5938F, 1, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, 5.0F, -6.8835F, 8.5938F, 1, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 9, 62, 12.0F, -6.1335F, 11.5938F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 59, 7.0F, -6.1335F, 11.5938F, 2, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 34, 51, -12.0F, -6.1335F, 10.5938F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 8, 59, 7.5F, -6.3835F, 11.8438F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 13, 59, 7.5F, -6.6335F, 13.3438F, 1, 1, 1, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 20, 58, 9.5F, -6.1335F, 8.0938F, 2, 1, 2, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 22, 69, 4.0F, -6.1335F, -4.9062F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 33, 62, -7.0F, -6.1335F, -4.9062F, 3, 1, 3, 0.0F, false));
		rotation2.cubeList.add(new ModelBox(rotation2, 0, 50, -6.0F, -6.8835F, 8.5938F, 1, 1, 2, 0.0F, false));

		facing_rotate_y = new RendererModel(this);
		facing_rotate_y.setRotationPoint(-10.5F, -6.9252F, 12.0938F);
		rotation2.addChild(facing_rotate_y);
		facing_rotate_y.cubeList.add(new ModelBox(facing_rotate_y, 38, 57, -0.5F, -0.7083F, -0.5F, 1, 2, 1, 0.0F, false));
		facing_rotate_y.cubeList.add(new ModelBox(facing_rotate_y, 48, 54, -1.0F, -0.4583F, -0.5F, 2, 1, 1, 0.0F, false));
		facing_rotate_y.cubeList.add(new ModelBox(facing_rotate_y, 45, 56, -2.0F, -0.4583F, -1.5F, 1, 1, 3, 0.0F, false));
		facing_rotate_y.cubeList.add(new ModelBox(facing_rotate_y, 55, 61, -1.5F, -0.4583F, 1.0F, 3, 1, 1, 0.0F, false));
		facing_rotate_y.cubeList.add(new ModelBox(facing_rotate_y, 56, 56, 1.0F, -0.4583F, -1.5F, 1, 1, 3, 0.0F, false));
		facing_rotate_y.cubeList.add(new ModelBox(facing_rotate_y, 55, 61, -1.5F, -0.4583F, -2.0F, 3, 1, 1, 0.0F, false));

		refuel = new RendererModel(this);
		refuel.setRotationPoint(5.5F, -6.8835F, -3.4062F);
		rotation2.addChild(refuel);
		refuel.cubeList.add(new ModelBox(refuel, 33, 69, -0.5F, -0.25F, -0.5F, 1, 1, 1, 0.0F, false));
		refuel.cubeList.add(new ModelBox(refuel, 36, 70, 0.0F, -0.75F, -1.0F, 0, 1, 2, 0.0F, false));

		bone7 = new RendererModel(this);
		bone7.setRotationPoint(-5.5F, -6.1335F, -3.4062F);
		rotation2.addChild(bone7);
		setRotationAngle(bone7, 0.0F, -0.7854F, 0.0F);
		bone7.cubeList.add(new ModelBox(bone7, 46, 62, -0.5F, -0.5F, -1.5F, 1, 1, 3, 0.0F, false));

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation2.addChild(bone63);
		bone63.cubeList.add(new ModelBox(bone63, 23, 62, 10.0F, -7.8835F, 13.6938F, 1, 1, 1, 0.0F, false));

		big_button_helm = new RendererModel(this);
		big_button_helm.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation2.addChild(big_button_helm);
		big_button_helm.cubeList.add(new ModelBox(big_button_helm, 0, 65, 12.5F, -7.8835F, 17.1938F, 2, 1, 2, 0.0F, false));

		south_west = new RendererModel(this);
		south_west.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(south_west);
		setRotationAngle(south_west, 0.0F, 1.0472F, 0.0F);
		

		rotation3 = new RendererModel(this);
		rotation3.setRotationPoint(0.0F, -1.0F, 19.1F);
		south_west.addChild(rotation3);
		setRotationAngle(rotation3, -0.3491F, 0.0F, 0.0F);
		rotation3.cubeList.add(new ModelBox(rotation3, 38, 97, 0.25F, -6.3585F, -1.4062F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 89, -1.25F, -6.1085F, 8.0938F, 6, 1, 6, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 2, 111, 6.25F, -5.6085F, 11.8438F, 1, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 49, 112, 6.0F, -5.6085F, 5.5938F, 2, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 49, 112, 9.0F, -5.6085F, 5.5938F, 2, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 31, 110, 6.0F, -5.6085F, 7.0938F, 5, 1, 3, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 2, 111, 7.75F, -5.6085F, 11.8438F, 1, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 2, 111, 9.25F, -5.6085F, 11.8438F, 1, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 2, 111, 10.75F, -5.6085F, 11.8438F, 1, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 1, 115, 11.75F, -5.6085F, 10.3438F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 24, 95, 3.75F, -6.1085F, 0.0938F, 1, 1, 5, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 38, 97, 3.75F, -6.3585F, -1.4062F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 24, 95, 2.0F, -6.1085F, 0.0938F, 1, 1, 5, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 38, 97, 2.0F, -6.3585F, -1.4062F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 24, 95, 0.25F, -6.1085F, 0.0938F, 1, 1, 5, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 28, 104, -6.5F, -6.1085F, -4.9062F, 13, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 24, 95, -1.5F, -6.1085F, 0.0938F, 1, 1, 5, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 38, 97, -1.5F, -6.3585F, -1.4062F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 22, 88, -6.25F, -6.1085F, 0.0938F, 4, 1, 4, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 35, 88, -4.75F, -6.3585F, 0.5938F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 40, 92, -6.0F, -6.8585F, 2.8438F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 45, 88, -7.75F, -6.1085F, 0.0938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 45, 88, -7.75F, -6.1085F, 1.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 45, 92, -7.75F, -6.8585F, 3.0938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 10, 107, -8.0F, -5.6085F, 5.5938F, 2, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 10, 111, -10.0F, -5.6085F, 8.0938F, 3, 1, 3, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 5, 107, -6.75F, -5.8585F, 8.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 5, 107, -6.75F, -5.8585F, 10.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 5, 107, -6.75F, -5.8585F, 12.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 107, -12.5F, -5.6085F, 12.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 107, -11.0F, -5.6085F, 12.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 107, -9.5F, -5.6085F, 12.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 21, 109, -11.0F, -5.8585F, 5.5938F, 1, 1, 1, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 17, 105, -7.0F, -6.1085F, 5.5938F, 0, 1, 2, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 0, 97, -5.0F, -6.1085F, 6.5938F, 3, 1, 7, 0.0F, false));
		rotation3.cubeList.add(new ModelBox(rotation3, 46, 97, 6.25F, -6.1085F, 0.0938F, 4, 1, 4, 0.0F, false));

		cutouts2 = new RendererModel(this);
		cutouts2.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(cutouts2);
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, -5.5F, -6.8585F, 10.1938F, 1, 1, 9, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, 4.5F, -6.8585F, 10.1938F, 1, 1, 9, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, -11.5F, -6.8585F, 9.1938F, 6, 1, 1, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 66, 86, -13.5F, -6.3585F, 10.1938F, 8, 1, 9, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 66, 86, 5.5F, -6.3585F, 10.1938F, 8, 1, 9, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, 5.5F, -6.8585F, 9.1938F, 6, 1, 1, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, -13.5F, -6.8585F, 10.1938F, 2, 1, 5, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, 11.5F, -6.8585F, 10.1938F, 2, 1, 5, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, -14.0F, -6.8585F, 15.1938F, 1, 1, 4, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, 13.0F, -6.8585F, 15.1938F, 1, 1, 4, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, -13.5F, -6.8585F, 19.1938F, 8, 1, 1, 0.0F, false));
		cutouts2.cubeList.add(new ModelBox(cutouts2, 74, 100, 5.5F, -6.8585F, 19.1938F, 8, 1, 1, 0.0F, false));

		increment_translate_y = new RendererModel(this);
		increment_translate_y.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(increment_translate_y);
		increment_translate_y.cubeList.add(new ModelBox(increment_translate_y, 32, 97, 3.75F, -7.3585F, 8.9438F, 1, 1, 1, 0.0F, false));

		y_control = new RendererModel(this);
		y_control.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(y_control);
		y_control.cubeList.add(new ModelBox(y_control, 60, 105, -1.5F, -7.6085F, 0.6938F, 3, 1, 1, 0.0F, false));

		x_Control = new RendererModel(this);
		x_Control.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(x_Control);
		x_Control.cubeList.add(new ModelBox(x_Control, 60, 105, 2.5F, -7.6085F, 0.6938F, 3, 1, 1, 0.0F, false));

		z_control = new RendererModel(this);
		z_control.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(z_control);
		z_control.cubeList.add(new ModelBox(z_control, 60, 105, -5.5F, -7.6085F, 0.6938F, 3, 1, 1, 0.0F, false));

		bone21 = new RendererModel(this);
		bone21.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(bone21);
		bone21.cubeList.add(new ModelBox(bone21, 32, 97, 2.0F, -7.3585F, 5.4438F, 1, 1, 1, 0.0F, false));

		bone51 = new RendererModel(this);
		bone51.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(bone51);
		bone51.cubeList.add(new ModelBox(bone51, 32, 97, 0.25F, -7.3585F, 6.4438F, 1, 1, 1, 0.0F, false));

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(bone57);
		bone57.cubeList.add(new ModelBox(bone57, 32, 97, -1.5F, -7.3585F, 5.4438F, 1, 1, 1, 0.0F, false));

		communicator = new RendererModel(this);
		communicator.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation3.addChild(communicator);
		communicator.cubeList.add(new ModelBox(communicator, 14, 97, -4.5F, -7.6085F, 12.6938F, 2, 1, 5, 0.0F, false));

		sonicport = new RendererModel(this);
		sonicport.setRotationPoint(3.5F, 1.0F, -6.1F);
		rotation3.addChild(sonicport);
		sonicport.cubeList.add(new ModelBox(sonicport, 54, 86, 3.25F, -7.6085F, 6.6938F, 3, 1, 3, 0.0F, false));

		south = new RendererModel(this);
		south.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(south);
		

		rotation4 = new RendererModel(this);
		rotation4.setRotationPoint(0.0F, -1.0F, 19.1F);
		south.addChild(rotation4);
		setRotationAngle(rotation4, -0.3491F, 0.0F, 0.0F);
		rotation4.cubeList.add(new ModelBox(rotation4, 20, 132, 10.75F, -6.1085F, 7.8438F, 3, 1, 1, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 0, 131, 7.75F, -6.1085F, 8.8438F, 6, 1, 6, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 30, 130, 6.75F, -6.1085F, 11.8438F, 1, 1, 3, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 0, 142, -4.5F, -6.1085F, 8.8438F, 9, 1, 5, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 0, 161, -13.5F, -6.6085F, 8.8438F, 6, 1, 4, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 1, 167, -13.5F, -7.3585F, 9.3438F, 6, 1, 3, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 0, 152, -5.5F, -6.1085F, 3.3438F, 11, 1, 5, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 29, 145, 2.25F, -6.6085F, 9.0938F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 24, 143, 2.75F, -6.3585F, 11.3438F, 1, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 29, 145, -4.25F, -6.6085F, 9.0938F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 24, 143, -3.75F, -6.3585F, 11.3438F, 1, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 29, 145, -1.0F, -6.6085F, 9.0938F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 36, 150, -0.5F, -6.6085F, 4.3438F, 1, 1, 3, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 36, 150, 1.25F, -6.6085F, 4.3438F, 1, 1, 3, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 28, 153, 2.75F, -6.3585F, 4.3438F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 28, 153, -4.75F, -6.3585F, 4.3438F, 2, 1, 2, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 36, 150, -2.25F, -6.6085F, 4.3438F, 1, 1, 3, 0.0F, false));
		rotation4.cubeList.add(new ModelBox(rotation4, 24, 143, -0.5F, -6.3585F, 11.3438F, 1, 1, 2, 0.0F, false));

		throttle_rotate_x = new RendererModel(this);
		throttle_rotate_x.setRotationPoint(-12.0F, -5.8585F, 10.8438F);
		rotation4.addChild(throttle_rotate_x);
		setRotationAngle(throttle_rotate_x, 1.0472F, 0.0F, 0.0F);
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 18, 160, -1.0F, -2.0F, -1.0F, 1, 2, 2, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 25, 161, -1.0F, -4.0F, -0.5F, 1, 2, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 31, 162, -1.5F, -5.0F, -0.5F, 6, 1, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 47, 162, -1.0F, -5.25F, -0.25F, 5, 1, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 25, 161, 3.0F, -4.0F, -0.5F, 1, 2, 1, 0.0F, false));
		throttle_rotate_x.cubeList.add(new ModelBox(throttle_rotate_x, 18, 160, 3.0F, -2.0F, -1.0F, 1, 2, 2, 0.0F, false));

		stabilisers = new RendererModel(this);
		stabilisers.setRotationPoint(3.5F, 0.5F, -11.35F);
		rotation4.addChild(stabilisers);
		stabilisers.cubeList.add(new ModelBox(stabilisers, 40, 143, 3.0F, -6.6085F, 12.6938F, 3, 1, 4, 0.0F, false));
		stabilisers.cubeList.add(new ModelBox(stabilisers, 51, 144, 3.25F, -7.1085F, 14.1938F, 1, 1, 1, 0.0F, false));
		stabilisers.cubeList.add(new ModelBox(stabilisers, 51, 144, 4.75F, -7.1085F, 14.1938F, 1, 1, 1, 0.0F, false));

		bone33 = new RendererModel(this);
		bone33.setRotationPoint(-3.25F, 0.5F, -2.85F);
		rotation4.addChild(bone33);
		setRotationAngle(bone33, 0.3491F, 0.0F, 0.0F);
		bone33.cubeList.add(new ModelBox(bone33, 63, 142, -1.75F, -4.2842F, 0.8204F, 10, 1, 6, 0.0F, false));
		bone33.cubeList.add(new ModelBox(bone33, 48, 150, -0.25F, -5.0342F, 1.8204F, 1, 1, 4, 0.0F, false));
		bone33.cubeList.add(new ModelBox(bone33, 48, 150, 1.75F, -5.0342F, 1.8204F, 1, 1, 4, 0.0F, false));
		bone33.cubeList.add(new ModelBox(bone33, 48, 150, 3.75F, -5.0342F, 1.8204F, 1, 1, 4, 0.0F, false));
		bone33.cubeList.add(new ModelBox(bone33, 48, 150, 5.75F, -5.0342F, 1.8204F, 1, 1, 4, 0.0F, false));
		bone33.cubeList.add(new ModelBox(bone33, 67, 132, -1.75F, -6.3363F, 0.1822F, 10, 3, 1, 0.0F, false));

		cutouts3 = new RendererModel(this);
		cutouts3.setRotationPoint(-2.75F, 0.75F, -8.1F);
		rotation4.addChild(cutouts3);
		cutouts3.cubeList.add(new ModelBox(cutouts3, 67, 132, -3.25F, -6.6085F, 4.1938F, 1, 2, 6, 0.0F, false));
		cutouts3.cubeList.add(new ModelBox(cutouts3, 67, 132, 7.75F, -6.6085F, 4.1938F, 1, 2, 6, 0.0F, false));

		handbrake_rotate_y = new RendererModel(this);
		handbrake_rotate_y.setRotationPoint(11.25F, -5.8585F, 12.3438F);
		rotation4.addChild(handbrake_rotate_y);
		setRotationAngle(handbrake_rotate_y, 0.0F, 0.4363F, 0.0F);
		handbrake_rotate_y.cubeList.add(new ModelBox(handbrake_rotate_y, 37, 131, -2.0F, -1.0F, -2.0F, 4, 1, 4, 0.0F, false));
		handbrake_rotate_y.cubeList.add(new ModelBox(handbrake_rotate_y, 54, 138, -6.9088F, -2.0419F, -0.5F, 3, 1, 1, 0.0F, false));

		bone20 = new RendererModel(this);
		bone20.setRotationPoint(-4.75F, -0.5F, 0.25F);
		handbrake_rotate_y.addChild(bone20);
		setRotationAngle(bone20, 0.0F, 0.0F, 0.1745F);
		bone20.cubeList.add(new ModelBox(bone20, 38, 138, 0.5606F, -1.6645F, -0.75F, 6, 1, 1, 0.0F, false));

		north_east = new RendererModel(this);
		north_east.setRotationPoint(0.0F, -27.0F, 0.0F);
		components.addChild(north_east);
		setRotationAngle(north_east, 0.0F, -2.0944F, 0.0F);
		

		rotation6 = new RendererModel(this);
		rotation6.setRotationPoint(0.0F, -1.0F, 19.1F);
		north_east.addChild(rotation6);
		setRotationAngle(rotation6, -0.3491F, 0.0F, 0.0F);
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -3.5F, -6.6085F, -0.1562F, 7, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 7, 228, -1.0F, -6.3585F, -5.1562F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 36, 230, 5.0F, -6.3585F, -4.1562F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 36, 230, -7.0F, -6.3585F, -4.1562F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 18, 228, 11.0F, -6.3585F, 10.8438F, 4, 1, 4, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 22, 223, 10.0F, -6.3585F, 7.8438F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 22, 223, -12.0F, -6.3585F, 7.8438F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 32, 223, 8.0F, -6.1085F, 10.8438F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 32, 223, -10.0F, -6.1085F, 10.8438F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 42, 223, 5.0F, -6.1085F, 12.8438F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 42, 223, -7.0F, -6.1085F, 12.8438F, 2, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 227, -2.25F, -6.1085F, -4.6562F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 227, 2.75F, -6.1085F, -4.6562F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 227, -3.75F, -6.1085F, -4.6562F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 227, 1.25F, -6.1085F, -4.6562F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, 3.5F, -6.6085F, 0.8438F, 3, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -6.5F, -6.6085F, 0.8438F, 3, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, 3.5F, -6.6085F, 9.8438F, 3, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -6.5F, -6.6085F, 9.8438F, 3, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, 8.5F, -6.6085F, 4.8438F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -9.5F, -6.6085F, 4.8438F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, 7.5F, -6.6085F, 2.8438F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -8.5F, -6.6085F, 2.8438F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, 7.5F, -6.6085F, 6.8438F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -8.5F, -6.6085F, 6.8438F, 1, 1, 2, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, 6.5F, -6.6085F, 1.8438F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -7.5F, -6.6085F, 1.8438F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, 6.5F, -6.6085F, 8.8438F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -7.5F, -6.6085F, 8.8438F, 1, 1, 1, 0.0F, false));
		rotation6.cubeList.add(new ModelBox(rotation6, 0, 222, -3.5F, -6.6085F, 10.8438F, 7, 1, 1, 0.0F, false));

		randomiser = new RendererModel(this);
		randomiser.setRotationPoint(-15.5F, 1.0F, 0.65F);
		rotation6.addChild(randomiser);
		randomiser.cubeList.add(new ModelBox(randomiser, 18, 228, 0.5F, -7.3585F, 10.1938F, 4, 1, 4, 0.0F, false));

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		south_east = new RendererModel(this);
		south_east.setRotationPoint(0.0F, -27.0F, 0.0F);
		glow.addChild(south_east);
		setRotationAngle(south_east, 0.0F, -1.0472F, 0.0F);
		

		rotation5 = new RendererModel(this);
		rotation5.setRotationPoint(0.0F, -1.0F, 19.1F);
		south_east.addChild(rotation5);
		setRotationAngle(rotation5, -0.3491F, 0.0F, 0.0F);
		

		telepathic = new RendererModel(this);
		telepathic.setRotationPoint(-3.5F, 1.0F, -17.35F);
		rotation5.addChild(telepathic);
		telepathic.cubeList.add(new ModelBox(telepathic, 31, 177, 4.0F, -7.1085F, 16.1938F, 3, 1, 2, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 34, 183, 4.5F, -7.1085F, 19.1938F, 4, 1, 2, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 46, 188, -0.5F, -7.1085F, 22.6938F, 8, 1, 5, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 24, 188, -8.5F, -7.1085F, 23.6938F, 8, 1, 3, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 0, 188, 7.5F, -7.1085F, 23.6938F, 8, 1, 3, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 0, 195, 11.5F, -7.1085F, 28.1938F, 6, 1, 4, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 72, 197, -0.5F, -7.1085F, 28.6938F, 8, 1, 2, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 42, 196, 7.5F, -7.1085F, 28.1938F, 4, 1, 3, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 58, 196, -4.5F, -7.1085F, 28.1938F, 4, 1, 3, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 21, 195, -10.5F, -7.1085F, 28.1938F, 6, 1, 4, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 42, 177, 0.0F, -7.1085F, 16.1938F, 3, 1, 2, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 47, 183, -1.5F, -7.1085F, 19.1938F, 4, 1, 2, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 16, 176, -4.0F, -7.1085F, 15.1938F, 4, 1, 3, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 17, 182, -6.5F, -7.1085F, 19.1938F, 5, 1, 3, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 0, 176, 7.0F, -7.1085F, 15.1938F, 4, 1, 3, 0.0F, false));
		telepathic.cubeList.add(new ModelBox(telepathic, 0, 182, 8.5F, -7.1085F, 19.1938F, 5, 1, 3, 0.0F, false));

		north_east2 = new RendererModel(this);
		north_east2.setRotationPoint(0.0F, -27.0F, 0.0F);
		glow.addChild(north_east2);
		setRotationAngle(north_east2, 0.0F, -2.0944F, 0.0F);
		

		rotation7 = new RendererModel(this);
		rotation7.setRotationPoint(0.0F, -1.0F, 19.1F);
		north_east2.addChild(rotation7);
		setRotationAngle(rotation7, -0.3491F, 0.0F, 0.0F);
		

		monitor2 = new RendererModel(this);
		monitor2.setRotationPoint(-3.5F, 1.0F, -11.35F);
		rotation7.addChild(monitor2);
		monitor2.cubeList.add(new ModelBox(monitor2, 0, 210, -6.0F, -7.1085F, 12.1938F, 19, 0, 10, 0.0F, false));

		north_west2 = new RendererModel(this);
		north_west2.setRotationPoint(0.0F, -27.0F, 0.0F);
		glow.addChild(north_west2);
		setRotationAngle(north_west2, 0.0F, 2.0944F, 0.0F);
		

		rotation8 = new RendererModel(this);
		rotation8.setRotationPoint(0.0F, -1.0F, 19.1F);
		north_west2.addChild(rotation8);
		setRotationAngle(rotation8, -0.3491F, 0.0F, 0.0F);
		

		dimensioncontrol = new RendererModel(this);
		dimensioncontrol.setRotationPoint(0.0F, 1.0F, -5.1F);
		rotation8.addChild(dimensioncontrol);
		dimensioncontrol.cubeList.add(new ModelBox(dimensioncontrol, 0, 36, -6.0F, -7.3835F, 4.6938F, 12, 0, 12, 0.0F, false));

		bottom_beams = new RendererModel(this);
		bottom_beams.setRotationPoint(0.0F, -5.0F, 0.0F);
		glow.addChild(bottom_beams);
		bottom_beams.cubeList.add(new ModelBox(bottom_beams, 175, 156, -1.5F, -11.0F, 6.4F, 3, 14, 3, 0.0F, false));

		bone100 = new RendererModel(this);
		bone100.setRotationPoint(0.0F, 0.0F, 0.0F);
		bottom_beams.addChild(bone100);
		setRotationAngle(bone100, 0.0F, -1.0472F, 0.0F);
		bone100.cubeList.add(new ModelBox(bone100, 175, 156, -1.5F, -11.0F, 6.4F, 3, 14, 3, 0.0F, false));

		bone101 = new RendererModel(this);
		bone101.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone100.addChild(bone101);
		setRotationAngle(bone101, 0.0F, -1.0472F, 0.0F);
		bone101.cubeList.add(new ModelBox(bone101, 175, 156, -1.5F, -11.0F, 6.4F, 3, 14, 3, 0.0F, false));

		bone102 = new RendererModel(this);
		bone102.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone101.addChild(bone102);
		setRotationAngle(bone102, 0.0F, -1.0472F, 0.0F);
		bone102.cubeList.add(new ModelBox(bone102, 175, 156, -1.5F, -11.0F, 6.4F, 3, 14, 3, 0.0F, false));

		bone103 = new RendererModel(this);
		bone103.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone102.addChild(bone103);
		setRotationAngle(bone103, 0.0F, -1.0472F, 0.0F);
		bone103.cubeList.add(new ModelBox(bone103, 175, 156, -1.5F, -11.0F, 6.4F, 3, 14, 3, 0.0F, false));

		bone104 = new RendererModel(this);
		bone104.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone103.addChild(bone104);
		setRotationAngle(bone104, 0.0F, -1.0472F, 0.0F);
		bone104.cubeList.add(new ModelBox(bone104, 175, 156, -1.5F, -11.0F, 6.4F, 3, 14, 3, 0.0F, false));

		rotor_beams = new RendererModel(this);
		rotor_beams.setRotationPoint(0.0F, -50.0F, 0.0F);
		glow.addChild(rotor_beams);
		rotor_beams.cubeList.add(new ModelBox(rotor_beams, 244, 81, -1.5F, -32.05F, 6.4F, 3, 33, 3, 0.0F, false));

		bone64 = new RendererModel(this);
		bone64.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor_beams.addChild(bone64);
		setRotationAngle(bone64, 0.0F, -1.0472F, 0.0F);
		bone64.cubeList.add(new ModelBox(bone64, 244, 81, -1.5F, -32.05F, 6.4F, 3, 33, 3, 0.0F, false));

		bone65 = new RendererModel(this);
		bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone64.addChild(bone65);
		setRotationAngle(bone65, 0.0F, -1.0472F, 0.0F);
		bone65.cubeList.add(new ModelBox(bone65, 244, 81, -1.5F, -32.05F, 6.4F, 3, 33, 3, 0.0F, false));

		bone66 = new RendererModel(this);
		bone66.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone65.addChild(bone66);
		setRotationAngle(bone66, 0.0F, -1.0472F, 0.0F);
		bone66.cubeList.add(new ModelBox(bone66, 244, 81, -1.5F, -32.05F, 6.4F, 3, 33, 3, 0.0F, false));

		bone67 = new RendererModel(this);
		bone67.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone66.addChild(bone67);
		setRotationAngle(bone67, 0.0F, -1.0472F, 0.0F);
		bone67.cubeList.add(new ModelBox(bone67, 244, 81, -1.5F, -32.05F, 6.4F, 3, 33, 3, 0.0F, false));

		bone68 = new RendererModel(this);
		bone68.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone67.addChild(bone68);
		setRotationAngle(bone68, 0.0F, -1.0472F, 0.0F);
		bone68.cubeList.add(new ModelBox(bone68, 244, 81, -1.5F, -32.05F, 6.4F, 3, 33, 3, 0.0F, false));

		rotor_bottom_translate_5 = new RendererModel(this);
		rotor_bottom_translate_5.setRotationPoint(-1.0F, -27.0F, 12.0F);
		rotor_bottom_translate_5.cubeList.add(new ModelBox(rotor_bottom_translate_5, 204, 116, 0.0F, -9.0F, -13.0F, 2, 14, 2, 0.0F, false));

		rotor_top_translate_6 = new RendererModel(this);
		rotor_top_translate_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		rotor_bottom_translate_5.addChild(rotor_top_translate_6);
		rotor_top_translate_6.cubeList.add(new ModelBox(rotor_top_translate_6, 204, 116, 0.0F, -35.0F, -13.0F, 2, 15, 2, 0.0F, false));
	}

	
	public void render(ConsoleTile tile) {
		
		this.rotor_bottom_translate_5.offsetY = (float)Math.cos(tile.flightTicks * 0.1) * 0.3F;
		
		this.handbrake_rotate_y.rotateAngleY = (float) Math.toRadians(-90 + 110 * tile.getControl(ThrottleControl.class).getAmount());
		this.throttle_rotate_x.rotateAngleX = (float) Math.toRadians(tile.getControl(HandbrakeControl.class).isFree() ? -60 : 40);
		
		this.facing_rotate_y.rotateAngleY = (float) Math.toRadians(tile.getControl(FacingControl.class).getAnimationTicks() * 20);
		
		this.big_button_helm.offsetY = tile.getControl(RefuelerControl.class).isRefueling() ? 0.0325F : 0;
		
		this.x_Control.offsetY = tile.getControl(XControl.class).getAnimationTicks() == 0 ? 0 : 0.025F;
		this.y_control.offsetY = tile.getControl(YControl.class).getAnimationTicks() == 0 ? 0 : 0.025F;
		this.z_control.offsetY = tile.getControl(ZControl.class).getAnimationTicks() == 0 ? 0 : 0.025F;
		
		
		baseconsole.render(0.0625F);
		components.render(0.0625F);
		rotor_bottom_translate_5.render(0.0625F);
		ModelHelper.renderPartBrightness(1.0F, glow);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(1.75, -0.15, 1);
		GlStateManager.scaled(0.75, 0.75, 0.75);
		GlStateManager.rotated(60, 0, 1, 0);
		GlStateManager.rotated(-22, 1, 0, 0);
		GlStateManager.translated(0.9, 0, -0.275);
		GlStateManager.scaled(0.75, 0.75, 0.75);
		Minecraft.getInstance().getItemRenderer().renderItem(tile.getSonicItem(), TransformType.NONE);
		GlStateManager.popMatrix();
		
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}