package net.tardis.mod.client.models;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.tileentities.ToyotaSpinnyTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class ToyotaSpinnyThingModel extends Model {
	private final RendererModel spinnytop;
	private final RendererModel bone57;
	private final RendererModel bone63;
	private final RendererModel bone69;
	private final RendererModel bone99;
	private final RendererModel bone105;
	private final RendererModel bone111;
	private final RendererModel bone218;
	private final RendererModel bone219;
	private final RendererModel bone220;
	private final RendererModel bone221;
	private final RendererModel bone222;
	private final RendererModel bone223;
	private final RendererModel bone224;
	private final RendererModel bone225;
	private final RendererModel bone226;
	private final RendererModel bone227;
	private final RendererModel bone228;
	private final RendererModel bone229;
	private final RendererModel bone123;
	private final RendererModel bone135;
	private final RendererModel bone136;
	private final RendererModel bone137;
	private final RendererModel bone138;
	private final RendererModel bone139;
	private final RendererModel bone140;
	private final RendererModel bone141;
	private final RendererModel bone142;
	private final RendererModel bone143;
	private final RendererModel bone144;
	private final RendererModel bone145;
	private final RendererModel bone157;
	private final RendererModel bone158;
	private final RendererModel bone159;
	private final RendererModel bone160;
	private final RendererModel bone161;
	private final RendererModel bone162;
	private final RendererModel bone163;
	private final RendererModel bone164;
	private final RendererModel bone165;
	private final RendererModel bone166;
	private final RendererModel bone167;
	private final RendererModel bone168;
	private final RendererModel bone146;
	private final RendererModel bone147;
	private final RendererModel bone148;
	private final RendererModel bone149;
	private final RendererModel bone150;
	private final RendererModel bone151;
	private final RendererModel bone152;
	private final RendererModel bone153;
	private final RendererModel bone154;
	private final RendererModel bone155;
	private final RendererModel bone156;
	private final RendererModel bone169;
	private final RendererModel bone170;
	private final RendererModel bone171;
	private final RendererModel bone172;
	private final RendererModel bone173;
	private final RendererModel bone174;
	private final RendererModel bone175;
	private final RendererModel bone176;
	private final RendererModel bone177;
	private final RendererModel bone178;
	private final RendererModel bone179;
	private final RendererModel bone180;
	private final RendererModel bone181;
	private final RendererModel bottom;
	private final RendererModel bone183;
	private final RendererModel bone184;
	private final RendererModel bone185;
	private final RendererModel bone186;
	private final RendererModel bone187;
	private final RendererModel bone188;
	private final RendererModel bone189;
	private final RendererModel bone190;
	private final RendererModel bone191;
	private final RendererModel bone192;
	private final RendererModel bone193;
	private final RendererModel middle;
	private final RendererModel bone195;
	private final RendererModel bone196;
	private final RendererModel bone197;
	private final RendererModel bone198;
	private final RendererModel bone199;
	private final RendererModel bone200;
	private final RendererModel bone201;
	private final RendererModel bone202;
	private final RendererModel bone203;
	private final RendererModel bone204;
	private final RendererModel bone205;
	private final RendererModel top;
	private final RendererModel bone207;
	private final RendererModel bone208;
	private final RendererModel bone209;
	private final RendererModel bone210;
	private final RendererModel bone211;
	private final RendererModel bone212;
	private final RendererModel bone213;
	private final RendererModel bone214;
	private final RendererModel bone215;
	private final RendererModel bone216;
	private final RendererModel bone217;

	public ToyotaSpinnyThingModel() {
		textureWidth = 256;
		textureHeight = 256;

		spinnytop = new RendererModel(this);
		spinnytop.setRotationPoint(0.0F, 118.0F, 0.0F);
		

		bone57 = new RendererModel(this);
		bone57.setRotationPoint(0.0F, -92.0F, 0.0F);
		spinnytop.addChild(bone57);
		bone57.cubeList.add(new ModelBox(bone57, 114, 0, -9.0F, -9.0F, -0.4F, 18, 7, 16, 0.0F, false));

		bone63 = new RendererModel(this);
		bone63.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone57.addChild(bone63);
		setRotationAngle(bone63, 0.0F, -1.0472F, 0.0F);
		bone63.cubeList.add(new ModelBox(bone63, 114, 0, -9.0F, -9.0F, -0.4F, 18, 7, 16, 0.0F, false));

		bone69 = new RendererModel(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone63.addChild(bone69);
		setRotationAngle(bone69, 0.0F, -1.0472F, 0.0F);
		bone69.cubeList.add(new ModelBox(bone69, 114, 0, -9.0F, -9.0F, -0.4F, 18, 7, 16, 0.0F, false));

		bone99 = new RendererModel(this);
		bone99.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone69.addChild(bone99);
		setRotationAngle(bone99, 0.0F, -1.0472F, 0.0F);
		bone99.cubeList.add(new ModelBox(bone99, 114, 0, -9.0F, -9.0F, -0.4F, 18, 7, 16, 0.0F, false));

		bone105 = new RendererModel(this);
		bone105.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone99.addChild(bone105);
		setRotationAngle(bone105, 0.0F, -1.0472F, 0.0F);
		bone105.cubeList.add(new ModelBox(bone105, 114, 0, -9.0F, -9.0F, -0.4F, 18, 7, 16, 0.0F, false));

		bone111 = new RendererModel(this);
		bone111.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone105.addChild(bone111);
		setRotationAngle(bone111, 0.0F, -1.0472F, 0.0F);
		bone111.cubeList.add(new ModelBox(bone111, 114, 0, -9.0F, -9.0F, -0.4F, 18, 7, 16, 0.0F, false));

		bone218 = new RendererModel(this);
		bone218.setRotationPoint(0.0F, -92.0F, 0.0F);
		spinnytop.addChild(bone218);
		bone218.cubeList.add(new ModelBox(bone218, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone219 = new RendererModel(this);
		bone219.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone218.addChild(bone219);
		setRotationAngle(bone219, 0.0F, -0.5236F, 0.0F);
		bone219.cubeList.add(new ModelBox(bone219, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone220 = new RendererModel(this);
		bone220.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone219.addChild(bone220);
		setRotationAngle(bone220, 0.0F, -0.5236F, 0.0F);
		bone220.cubeList.add(new ModelBox(bone220, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone221 = new RendererModel(this);
		bone221.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone220.addChild(bone221);
		setRotationAngle(bone221, 0.0F, -0.5236F, 0.0F);
		bone221.cubeList.add(new ModelBox(bone221, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone222 = new RendererModel(this);
		bone222.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone221.addChild(bone222);
		setRotationAngle(bone222, 0.0F, -0.5236F, 0.0F);
		bone222.cubeList.add(new ModelBox(bone222, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone223 = new RendererModel(this);
		bone223.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone222.addChild(bone223);
		setRotationAngle(bone223, 0.0F, -0.5236F, 0.0F);
		bone223.cubeList.add(new ModelBox(bone223, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone224 = new RendererModel(this);
		bone224.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone223.addChild(bone224);
		setRotationAngle(bone224, 0.0F, -0.5236F, 0.0F);
		bone224.cubeList.add(new ModelBox(bone224, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone225 = new RendererModel(this);
		bone225.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone224.addChild(bone225);
		setRotationAngle(bone225, 0.0F, -0.5236F, 0.0F);
		bone225.cubeList.add(new ModelBox(bone225, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone226 = new RendererModel(this);
		bone226.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone225.addChild(bone226);
		setRotationAngle(bone226, 0.0F, -0.5236F, 0.0F);
		bone226.cubeList.add(new ModelBox(bone226, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone227 = new RendererModel(this);
		bone227.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone226.addChild(bone227);
		setRotationAngle(bone227, 0.0F, -0.5236F, 0.0F);
		bone227.cubeList.add(new ModelBox(bone227, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone228 = new RendererModel(this);
		bone228.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone227.addChild(bone228);
		setRotationAngle(bone228, 0.0F, -0.5236F, 0.0F);
		bone228.cubeList.add(new ModelBox(bone228, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone229 = new RendererModel(this);
		bone229.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone228.addChild(bone229);
		setRotationAngle(bone229, 0.0F, -0.5236F, 0.0F);
		bone229.cubeList.add(new ModelBox(bone229, 62, 0, -5.0F, -13.0F, 5.7F, 10, 4, 13, 0.0F, false));

		bone123 = new RendererModel(this);
		bone123.setRotationPoint(0.0F, -97.0F, 0.0F);
		spinnytop.addChild(bone123);
		

		bone135 = new RendererModel(this);
		bone135.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone123.addChild(bone135);
		setRotationAngle(bone135, 0.5236F, 0.0F, 0.0F);
		bone135.cubeList.add(new ModelBox(bone135, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone135.cubeList.add(new ModelBox(bone135, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone135.cubeList.add(new ModelBox(bone135, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone136 = new RendererModel(this);
		bone136.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone123.addChild(bone136);
		setRotationAngle(bone136, 0.0F, -0.5236F, 0.0F);
		

		bone137 = new RendererModel(this);
		bone137.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone136.addChild(bone137);
		setRotationAngle(bone137, 0.5236F, 0.0F, 0.0F);
		bone137.cubeList.add(new ModelBox(bone137, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone137.cubeList.add(new ModelBox(bone137, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone137.cubeList.add(new ModelBox(bone137, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone138 = new RendererModel(this);
		bone138.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone136.addChild(bone138);
		setRotationAngle(bone138, 0.0F, -0.5236F, 0.0F);
		

		bone139 = new RendererModel(this);
		bone139.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone138.addChild(bone139);
		setRotationAngle(bone139, 0.5236F, 0.0F, 0.0F);
		bone139.cubeList.add(new ModelBox(bone139, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone139.cubeList.add(new ModelBox(bone139, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone139.cubeList.add(new ModelBox(bone139, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone140 = new RendererModel(this);
		bone140.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone138.addChild(bone140);
		setRotationAngle(bone140, 0.0F, -0.5236F, 0.0F);
		

		bone141 = new RendererModel(this);
		bone141.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone140.addChild(bone141);
		setRotationAngle(bone141, 0.5236F, 0.0F, 0.0F);
		bone141.cubeList.add(new ModelBox(bone141, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone141.cubeList.add(new ModelBox(bone141, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone141.cubeList.add(new ModelBox(bone141, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone142 = new RendererModel(this);
		bone142.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone140.addChild(bone142);
		setRotationAngle(bone142, 0.0F, -0.5236F, 0.0F);
		

		bone143 = new RendererModel(this);
		bone143.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone142.addChild(bone143);
		setRotationAngle(bone143, 0.5236F, 0.0F, 0.0F);
		bone143.cubeList.add(new ModelBox(bone143, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone143.cubeList.add(new ModelBox(bone143, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone143.cubeList.add(new ModelBox(bone143, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone144 = new RendererModel(this);
		bone144.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone142.addChild(bone144);
		setRotationAngle(bone144, 0.0F, -0.5236F, 0.0F);
		

		bone145 = new RendererModel(this);
		bone145.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone144.addChild(bone145);
		setRotationAngle(bone145, 0.5236F, 0.0F, 0.0F);
		bone145.cubeList.add(new ModelBox(bone145, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone145.cubeList.add(new ModelBox(bone145, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone145.cubeList.add(new ModelBox(bone145, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone157 = new RendererModel(this);
		bone157.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone144.addChild(bone157);
		setRotationAngle(bone157, 0.0F, -0.5236F, 0.0F);
		

		bone158 = new RendererModel(this);
		bone158.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone157.addChild(bone158);
		setRotationAngle(bone158, 0.5236F, 0.0F, 0.0F);
		bone158.cubeList.add(new ModelBox(bone158, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone158.cubeList.add(new ModelBox(bone158, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone158.cubeList.add(new ModelBox(bone158, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone159 = new RendererModel(this);
		bone159.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone157.addChild(bone159);
		setRotationAngle(bone159, 0.0F, -0.5236F, 0.0F);
		

		bone160 = new RendererModel(this);
		bone160.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone159.addChild(bone160);
		setRotationAngle(bone160, 0.5236F, 0.0F, 0.0F);
		bone160.cubeList.add(new ModelBox(bone160, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone160.cubeList.add(new ModelBox(bone160, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone160.cubeList.add(new ModelBox(bone160, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone161 = new RendererModel(this);
		bone161.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone159.addChild(bone161);
		setRotationAngle(bone161, 0.0F, -0.5236F, 0.0F);
		

		bone162 = new RendererModel(this);
		bone162.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone161.addChild(bone162);
		setRotationAngle(bone162, 0.5236F, 0.0F, 0.0F);
		bone162.cubeList.add(new ModelBox(bone162, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone162.cubeList.add(new ModelBox(bone162, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone162.cubeList.add(new ModelBox(bone162, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone163 = new RendererModel(this);
		bone163.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone161.addChild(bone163);
		setRotationAngle(bone163, 0.0F, -0.5236F, 0.0F);
		

		bone164 = new RendererModel(this);
		bone164.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone163.addChild(bone164);
		setRotationAngle(bone164, 0.5236F, 0.0F, 0.0F);
		bone164.cubeList.add(new ModelBox(bone164, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone164.cubeList.add(new ModelBox(bone164, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone164.cubeList.add(new ModelBox(bone164, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone165 = new RendererModel(this);
		bone165.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone163.addChild(bone165);
		setRotationAngle(bone165, 0.0F, -0.5236F, 0.0F);
		

		bone166 = new RendererModel(this);
		bone166.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone165.addChild(bone166);
		setRotationAngle(bone166, 0.5236F, 0.0F, 0.0F);
		bone166.cubeList.add(new ModelBox(bone166, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone166.cubeList.add(new ModelBox(bone166, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone166.cubeList.add(new ModelBox(bone166, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone167 = new RendererModel(this);
		bone167.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone165.addChild(bone167);
		setRotationAngle(bone167, 0.0F, -0.5236F, 0.0F);
		

		bone168 = new RendererModel(this);
		bone168.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone167.addChild(bone168);
		setRotationAngle(bone168, 0.5236F, 0.0F, 0.0F);
		bone168.cubeList.add(new ModelBox(bone168, 68, 33, -4.5F, 0.0258F, 0.4447F, 9, 1, 6, 0.0F, false));
		bone168.cubeList.add(new ModelBox(bone168, 50, 28, -8.0F, 0.0258F, 6.4447F, 16, 1, 17, 0.0F, false));
		bone168.cubeList.add(new ModelBox(bone168, 61, 41, -9.0F, 0.0258F, 23.4447F, 18, 1, 4, 0.0F, false));

		bone146 = new RendererModel(this);
		bone146.setRotationPoint(0.0F, -97.0F, 0.0F);
		spinnytop.addChild(bone146);
		setRotationAngle(bone146, 0.0F, -0.2618F, 0.0F);
		

		bone147 = new RendererModel(this);
		bone147.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone146.addChild(bone147);
		setRotationAngle(bone147, 0.5236F, 0.0F, 0.0F);
		bone147.cubeList.add(new ModelBox(bone147, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone148 = new RendererModel(this);
		bone148.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone146.addChild(bone148);
		setRotationAngle(bone148, 0.0F, -0.5236F, 0.0F);
		

		bone149 = new RendererModel(this);
		bone149.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone148.addChild(bone149);
		setRotationAngle(bone149, 0.5236F, 0.0F, 0.0F);
		bone149.cubeList.add(new ModelBox(bone149, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone150 = new RendererModel(this);
		bone150.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone148.addChild(bone150);
		setRotationAngle(bone150, 0.0F, -0.5236F, 0.0F);
		

		bone151 = new RendererModel(this);
		bone151.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone150.addChild(bone151);
		setRotationAngle(bone151, 0.5236F, 0.0F, 0.0F);
		bone151.cubeList.add(new ModelBox(bone151, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone152 = new RendererModel(this);
		bone152.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone150.addChild(bone152);
		setRotationAngle(bone152, 0.0F, -0.5236F, 0.0F);
		

		bone153 = new RendererModel(this);
		bone153.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone152.addChild(bone153);
		setRotationAngle(bone153, 0.5236F, 0.0F, 0.0F);
		bone153.cubeList.add(new ModelBox(bone153, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone154 = new RendererModel(this);
		bone154.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone152.addChild(bone154);
		setRotationAngle(bone154, 0.0F, -0.5236F, 0.0F);
		

		bone155 = new RendererModel(this);
		bone155.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone154.addChild(bone155);
		setRotationAngle(bone155, 0.5236F, 0.0F, 0.0F);
		bone155.cubeList.add(new ModelBox(bone155, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone156 = new RendererModel(this);
		bone156.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone154.addChild(bone156);
		setRotationAngle(bone156, 0.0F, -0.5236F, 0.0F);
		

		bone169 = new RendererModel(this);
		bone169.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone156.addChild(bone169);
		setRotationAngle(bone169, 0.5236F, 0.0F, 0.0F);
		bone169.cubeList.add(new ModelBox(bone169, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone170 = new RendererModel(this);
		bone170.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone156.addChild(bone170);
		setRotationAngle(bone170, 0.0F, -0.5236F, 0.0F);
		

		bone171 = new RendererModel(this);
		bone171.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone170.addChild(bone171);
		setRotationAngle(bone171, 0.5236F, 0.0F, 0.0F);
		bone171.cubeList.add(new ModelBox(bone171, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone172 = new RendererModel(this);
		bone172.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone170.addChild(bone172);
		setRotationAngle(bone172, 0.0F, -0.5236F, 0.0F);
		

		bone173 = new RendererModel(this);
		bone173.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone172.addChild(bone173);
		setRotationAngle(bone173, 0.5236F, 0.0F, 0.0F);
		bone173.cubeList.add(new ModelBox(bone173, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone174 = new RendererModel(this);
		bone174.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone172.addChild(bone174);
		setRotationAngle(bone174, 0.0F, -0.5236F, 0.0F);
		

		bone175 = new RendererModel(this);
		bone175.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone174.addChild(bone175);
		setRotationAngle(bone175, 0.5236F, 0.0F, 0.0F);
		bone175.cubeList.add(new ModelBox(bone175, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone176 = new RendererModel(this);
		bone176.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone174.addChild(bone176);
		setRotationAngle(bone176, 0.0F, -0.5236F, 0.0F);
		

		bone177 = new RendererModel(this);
		bone177.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone176.addChild(bone177);
		setRotationAngle(bone177, 0.5236F, 0.0F, 0.0F);
		bone177.cubeList.add(new ModelBox(bone177, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone178 = new RendererModel(this);
		bone178.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone176.addChild(bone178);
		setRotationAngle(bone178, 0.0F, -0.5236F, 0.0F);
		

		bone179 = new RendererModel(this);
		bone179.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone178.addChild(bone179);
		setRotationAngle(bone179, 0.5236F, 0.0F, 0.0F);
		bone179.cubeList.add(new ModelBox(bone179, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bone180 = new RendererModel(this);
		bone180.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone178.addChild(bone180);
		setRotationAngle(bone180, 0.0F, -0.5236F, 0.0F);
		

		bone181 = new RendererModel(this);
		bone181.setRotationPoint(0.0F, -8.5F, 17.2F);
		bone180.addChild(bone181);
		setRotationAngle(bone181, 0.5236F, 0.0F, 0.0F);
		bone181.cubeList.add(new ModelBox(bone181, 0, 24, -2.5F, 1.5258F, -1.2F, 5, 1, 29, 0.0F, false));

		bottom = new RendererModel(this);
		bottom.setRotationPoint(0.0F, -111.8161F, 0.0F);
		spinnytop.addChild(bottom);
		bottom.cubeList.add(new ModelBox(bottom, 64, 68, -14.5F, -6.4081F, 41.0F, 29, 1, 8, 0.0F, false));
		bottom.cubeList.add(new ModelBox(bottom, 0, 66, -14.5F, -17.4081F, 48.0F, 29, 11, 1, 0.0F, false));

		bone183 = new RendererModel(this);
		bone183.setRotationPoint(0.0F, 0.5661F, 0.0F);
		bottom.addChild(bone183);
		setRotationAngle(bone183, 0.0F, -0.5236F, 0.0F);
		bone183.cubeList.add(new ModelBox(bone183, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone183.cubeList.add(new ModelBox(bone183, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone184 = new RendererModel(this);
		bone184.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone183.addChild(bone184);
		setRotationAngle(bone184, 0.0F, -0.5236F, 0.0F);
		bone184.cubeList.add(new ModelBox(bone184, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone184.cubeList.add(new ModelBox(bone184, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone185 = new RendererModel(this);
		bone185.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone184.addChild(bone185);
		setRotationAngle(bone185, 0.0F, -0.5236F, 0.0F);
		bone185.cubeList.add(new ModelBox(bone185, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone185.cubeList.add(new ModelBox(bone185, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone186 = new RendererModel(this);
		bone186.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone185.addChild(bone186);
		setRotationAngle(bone186, 0.0F, -0.5236F, 0.0F);
		bone186.cubeList.add(new ModelBox(bone186, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone186.cubeList.add(new ModelBox(bone186, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone187 = new RendererModel(this);
		bone187.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone186.addChild(bone187);
		setRotationAngle(bone187, 0.0F, -0.5236F, 0.0F);
		bone187.cubeList.add(new ModelBox(bone187, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone187.cubeList.add(new ModelBox(bone187, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone188 = new RendererModel(this);
		bone188.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone187.addChild(bone188);
		setRotationAngle(bone188, 0.0F, -0.5236F, 0.0F);
		bone188.cubeList.add(new ModelBox(bone188, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone188.cubeList.add(new ModelBox(bone188, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone189 = new RendererModel(this);
		bone189.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone188.addChild(bone189);
		setRotationAngle(bone189, 0.0F, -0.5236F, 0.0F);
		bone189.cubeList.add(new ModelBox(bone189, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone189.cubeList.add(new ModelBox(bone189, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone190 = new RendererModel(this);
		bone190.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone189.addChild(bone190);
		setRotationAngle(bone190, 0.0F, -0.5236F, 0.0F);
		bone190.cubeList.add(new ModelBox(bone190, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone190.cubeList.add(new ModelBox(bone190, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone191 = new RendererModel(this);
		bone191.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone190.addChild(bone191);
		setRotationAngle(bone191, 0.0F, -0.5236F, 0.0F);
		bone191.cubeList.add(new ModelBox(bone191, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone191.cubeList.add(new ModelBox(bone191, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone192 = new RendererModel(this);
		bone192.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone191.addChild(bone192);
		setRotationAngle(bone192, 0.0F, -0.5236F, 0.0F);
		bone192.cubeList.add(new ModelBox(bone192, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone192.cubeList.add(new ModelBox(bone192, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		bone193 = new RendererModel(this);
		bone193.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone192.addChild(bone193);
		setRotationAngle(bone193, 0.0F, -0.5236F, 0.0F);
		bone193.cubeList.add(new ModelBox(bone193, 64, 68, -14.5F, -6.9742F, 41.0F, 29, 1, 8, 0.0F, false));
		bone193.cubeList.add(new ModelBox(bone193, 0, 66, -14.5F, -17.9742F, 48.0F, 29, 11, 1, 0.0F, false));

		middle = new RendererModel(this);
		middle.setRotationPoint(0.0F, -123.25F, 0.0F);
		spinnytop.addChild(middle);
		middle.cubeList.add(new ModelBox(middle, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		middle.cubeList.add(new ModelBox(middle, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone195 = new RendererModel(this);
		bone195.setRotationPoint(0.0F, 0.0F, 0.0F);
		middle.addChild(bone195);
		setRotationAngle(bone195, 0.0F, -0.5236F, 0.0F);
		bone195.cubeList.add(new ModelBox(bone195, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone195.cubeList.add(new ModelBox(bone195, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone196 = new RendererModel(this);
		bone196.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone195.addChild(bone196);
		setRotationAngle(bone196, 0.0F, -0.5236F, 0.0F);
		bone196.cubeList.add(new ModelBox(bone196, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone196.cubeList.add(new ModelBox(bone196, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone197 = new RendererModel(this);
		bone197.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone196.addChild(bone197);
		setRotationAngle(bone197, 0.0F, -0.5236F, 0.0F);
		bone197.cubeList.add(new ModelBox(bone197, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone197.cubeList.add(new ModelBox(bone197, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone198 = new RendererModel(this);
		bone198.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone197.addChild(bone198);
		setRotationAngle(bone198, 0.0F, -0.5236F, 0.0F);
		bone198.cubeList.add(new ModelBox(bone198, 90, 86, -17.5F, -6.9742F, 49.6F, 35, 1, 10, 0.0F, false));
		bone198.cubeList.add(new ModelBox(bone198, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone199 = new RendererModel(this);
		bone199.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone198.addChild(bone199);
		setRotationAngle(bone199, 0.0F, -0.5236F, 0.0F);
		bone199.cubeList.add(new ModelBox(bone199, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone199.cubeList.add(new ModelBox(bone199, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone200 = new RendererModel(this);
		bone200.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone199.addChild(bone200);
		setRotationAngle(bone200, 0.0F, -0.5236F, 0.0F);
		bone200.cubeList.add(new ModelBox(bone200, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone200.cubeList.add(new ModelBox(bone200, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone201 = new RendererModel(this);
		bone201.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone200.addChild(bone201);
		setRotationAngle(bone201, 0.0F, -0.5236F, 0.0F);
		bone201.cubeList.add(new ModelBox(bone201, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone201.cubeList.add(new ModelBox(bone201, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone202 = new RendererModel(this);
		bone202.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone201.addChild(bone202);
		setRotationAngle(bone202, 0.0F, -0.5236F, 0.0F);
		bone202.cubeList.add(new ModelBox(bone202, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone202.cubeList.add(new ModelBox(bone202, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone203 = new RendererModel(this);
		bone203.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone202.addChild(bone203);
		setRotationAngle(bone203, 0.0F, -0.5236F, 0.0F);
		bone203.cubeList.add(new ModelBox(bone203, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone203.cubeList.add(new ModelBox(bone203, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone204 = new RendererModel(this);
		bone204.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone203.addChild(bone204);
		setRotationAngle(bone204, 0.0F, -0.5236F, 0.0F);
		bone204.cubeList.add(new ModelBox(bone204, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone204.cubeList.add(new ModelBox(bone204, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		bone205 = new RendererModel(this);
		bone205.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone204.addChild(bone205);
		setRotationAngle(bone205, 0.0F, -0.5236F, 0.0F);
		bone205.cubeList.add(new ModelBox(bone205, 90, 86, -17.5F, -6.9742F, 49.0F, 35, 1, 10, 0.0F, false));
		bone205.cubeList.add(new ModelBox(bone205, 0, 83, -17.5F, -19.9742F, 58.0F, 35, 13, 1, 0.0F, false));

		top = new RendererModel(this);
		top.setRotationPoint(0.0F, -137.25F, 0.0F);
		spinnytop.addChild(top);
		top.cubeList.add(new ModelBox(top, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		top.cubeList.add(new ModelBox(top, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone207 = new RendererModel(this);
		bone207.setRotationPoint(0.0F, 0.0F, 0.0F);
		top.addChild(bone207);
		setRotationAngle(bone207, 0.0F, -0.5236F, 0.0F);
		bone207.cubeList.add(new ModelBox(bone207, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone207.cubeList.add(new ModelBox(bone207, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone207.cubeList.add(new ModelBox(bone207, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone208 = new RendererModel(this);
		bone208.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone207.addChild(bone208);
		setRotationAngle(bone208, 0.0F, -0.5236F, 0.0F);
		bone208.cubeList.add(new ModelBox(bone208, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone208.cubeList.add(new ModelBox(bone208, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone208.cubeList.add(new ModelBox(bone208, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone209 = new RendererModel(this);
		bone209.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone208.addChild(bone209);
		setRotationAngle(bone209, 0.0F, -0.5236F, 0.0F);
		bone209.cubeList.add(new ModelBox(bone209, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone209.cubeList.add(new ModelBox(bone209, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone209.cubeList.add(new ModelBox(bone209, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone210 = new RendererModel(this);
		bone210.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone209.addChild(bone210);
		setRotationAngle(bone210, 0.0F, -0.5236F, 0.0F);
		bone210.cubeList.add(new ModelBox(bone210, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone210.cubeList.add(new ModelBox(bone210, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone210.cubeList.add(new ModelBox(bone210, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone211 = new RendererModel(this);
		bone211.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone210.addChild(bone211);
		setRotationAngle(bone211, 0.0F, -0.5236F, 0.0F);
		bone211.cubeList.add(new ModelBox(bone211, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone211.cubeList.add(new ModelBox(bone211, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone211.cubeList.add(new ModelBox(bone211, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone212 = new RendererModel(this);
		bone212.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone211.addChild(bone212);
		setRotationAngle(bone212, 0.0F, -0.5236F, 0.0F);
		bone212.cubeList.add(new ModelBox(bone212, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone212.cubeList.add(new ModelBox(bone212, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone212.cubeList.add(new ModelBox(bone212, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone213 = new RendererModel(this);
		bone213.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone212.addChild(bone213);
		setRotationAngle(bone213, 0.0F, -0.5236F, 0.0F);
		bone213.cubeList.add(new ModelBox(bone213, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone213.cubeList.add(new ModelBox(bone213, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone213.cubeList.add(new ModelBox(bone213, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone214 = new RendererModel(this);
		bone214.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone213.addChild(bone214);
		setRotationAngle(bone214, 0.0F, -0.5236F, 0.0F);
		bone214.cubeList.add(new ModelBox(bone214, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone214.cubeList.add(new ModelBox(bone214, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone214.cubeList.add(new ModelBox(bone214, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone215 = new RendererModel(this);
		bone215.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone214.addChild(bone215);
		setRotationAngle(bone215, 0.0F, -0.5236F, 0.0F);
		bone215.cubeList.add(new ModelBox(bone215, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone215.cubeList.add(new ModelBox(bone215, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone215.cubeList.add(new ModelBox(bone215, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone216 = new RendererModel(this);
		bone216.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone215.addChild(bone216);
		setRotationAngle(bone216, 0.0F, -0.5236F, 0.0F);
		bone216.cubeList.add(new ModelBox(bone216, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone216.cubeList.add(new ModelBox(bone216, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone216.cubeList.add(new ModelBox(bone216, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));

		bone217 = new RendererModel(this);
		bone217.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone216.addChild(bone217);
		setRotationAngle(bone217, 0.0F, -0.5236F, 0.0F);
		bone217.cubeList.add(new ModelBox(bone217, 0, 105, -21.5F, -6.9742F, 58.0F, 43, 1, 13, 0.0F, false));
		bone217.cubeList.add(new ModelBox(bone217, 0, 128, -21.5F, -20.9742F, 70.0F, 43, 14, 1, 0.0F, false));
		bone217.cubeList.add(new ModelBox(bone217, 0, 146, -21.5F, -21.75F, 0.0F, 43, 1, 71, 0.0F, false));
	}

	public void render(ToyotaSpinnyTile tile) {
		
		float rot = (float)Math.toRadians(tile.prevRotation + (tile.rotation - tile.prevRotation) * Minecraft.getInstance().getRenderPartialTicks());
		this.top.rotateAngleY = this.bottom.rotateAngleY = rot;
		this.middle.rotateAngleY = -rot;
		
		spinnytop.render(0.0625F);
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}