package net.tardis.mod.client.models.interiordoors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;

//Made with Blockbench
//Paste this code into your mod.

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.FortuneExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.tileentities.ConsoleTile;

public class InteriorFortuneModel extends Model implements IInteriorDoorRenderer{
	private final RendererModel door;
	private final RendererModel latch;
	private final RendererModel frame;
	private final RendererModel latch2;
	private final RendererModel boti;

	public InteriorFortuneModel() {
		textureWidth = 512;
		textureHeight = 512;

		door = new RendererModel(this);
		door.setRotationPoint(-24.0F, 24.0F, 28.0F);
		door.cubeList.add(new ModelBox(door, 253, 106, 40.0F, -154.0F, 0.0F, 8, 148, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 179, 110, 2.0F, -151.6F, 2.0F, 44, 144, 1, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 256, 108, 0.0F, -154.0F, 0.0F, 8, 148, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 224, 241, 8.0F, -18.0F, 0.0F, 32, 12, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 292, 125, 8.0F, -154.0F, 0.0F, 32, 12, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 139, 105, 8.0F, -112.4F, 0.0F, 32, 4, 4, 0.0F, false));
		door.cubeList.add(new ModelBox(door, 139, 105, 8.0F, -54.0F, 0.0F, 32, 4, 4, 0.0F, false));

		latch = new RendererModel(this);
		latch.setRotationPoint(0.0F, 0.0F, 0.0F);
		door.addChild(latch);
		latch.cubeList.add(new ModelBox(latch, 166, 8, 42.0F, -88.0F, -2.0F, 4, 8, 8, 0.0F, false));
		latch.cubeList.add(new ModelBox(latch, 110, 16, 42.0F, -78.0F, 1.0F, 4, 4, 4, 0.0F, false));

		frame = new RendererModel(this);
		frame.setRotationPoint(-24.0F, 24.0F, 21.0F);
		frame.cubeList.add(new ModelBox(frame, 256, 108, -4.0F, -154.0F, 4.75F, 4, 148, 6, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 256, 108, 48.0F, -154.0F, 4.75F, 4, 148, 6, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 224, 241, -2.0F, -8.0F, 3.0F, 52, 2, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 339, 131, -4.0F, -158.25F, 4.5F, 56, 4, 6, 0.0F, false));

		latch2 = new RendererModel(this);
		latch2.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame.addChild(latch2);

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 22.75F, 7.0F);
		boti.cubeList.add(new ModelBox(boti, 1, 97, -24.0F, -156.0F, 25.0F, 48, 152, 4, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity door) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, 1.2, -0.05);
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.enableRescaleNormal();
		GlStateManager.scaled(0.25, 0.25, 0.25);
		this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.FORTUNE.getRotationForState(door.getOpenState()));
		
		this.door.render(0.0625F);
		frame.render(0.0625F);
		boti.render(0.0625F);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

	@Override
	public ResourceLocation getTexture() {
		ConsoleTile tile = TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).orElse(null);
		if(tile != null) {
			int index = tile.getExteriorManager().getExteriorVariant();
			TexVariant[] vars = tile.getExterior().getVariants();
			if(vars != null && index < vars.length)
				return vars[index].getTexture();
		}
		return FortuneExteriorRenderer.TEXTURE;
	}
}