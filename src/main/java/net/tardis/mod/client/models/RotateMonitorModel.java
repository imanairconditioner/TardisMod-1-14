package net.tardis.mod.client.models;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.monitor.RenderScanner;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

// Made with Blockbench 3.6.5
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class RotateMonitorModel extends Model {
	private final RendererModel monitor;
	private final RendererModel spin_y;
	private final RendererModel sholder;
	private final RendererModel forearm;
	private final RendererModel wrist;
	private final RendererModel screen;
	private final RendererModel glow_screen;
	private final RendererModel fixed_base;
	private TextLabelRendererModel label;
	
	private static WorldText TEXT = new WorldText(0.75F, 0.47F, 0.007F, TextFormatting.WHITE);
	
	private String[] text = new String[0];
	
	private ResourceLocation textureToRebind;

	public RotateMonitorModel() {
		textureWidth = 64;
		textureHeight = 64;

		monitor = new RendererModel(this);
		monitor.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		spin_y = new RendererModel(this);
		spin_y.setRotationPoint(0.0F, 0.0F, 0.0F);
		monitor.addChild(spin_y);
		spin_y.cubeList.add(new ModelBox(spin_y, 37, 8, -2.0F, -3.0F, -2.0F, 4, 2, 4, 0.0F, false));

		sholder = new RendererModel(this);
		sholder.setRotationPoint(0.0F, -2.5F, 0.0F);
		spin_y.addChild(sholder);
		setRotationAngle(sholder, -0.9599F, 0.0F, 0.0F);
		sholder.cubeList.add(new ModelBox(sholder, 21, 26, -1.5F, -0.5F, -11.0F, 1, 1, 11, 0.0F, false));
		sholder.cubeList.add(new ModelBox(sholder, 24, 12, 0.5F, -0.5F, -11.0F, 1, 1, 11, 0.0F, false));

		forearm = new RendererModel(this);
		forearm.setRotationPoint(0.5F, 0.0155F, -10.5401F);
		sholder.addChild(forearm);
		setRotationAngle(forearm, 2.3998F, 0.0F, 0.0F);
		forearm.cubeList.add(new ModelBox(forearm, 0, 31, -1.0F, -0.5F, -7.5F, 1, 1, 8, 0.0F, false));

		wrist = new RendererModel(this);
		wrist.setRotationPoint(-0.5F, 0.183F, -7.0F);
		forearm.addChild(wrist);
		setRotationAngle(wrist, -2.3562F, 0.0F, 0.0F);
		wrist.cubeList.add(new ModelBox(wrist, 34, 26, -1.25F, -0.5F, -8.0F, 1, 1, 8, 0.0F, false));
		wrist.cubeList.add(new ModelBox(wrist, 10, 32, 0.25F, -0.5F, -8.0F, 1, 1, 8, 0.0F, false));

		screen = new RendererModel(this);
		screen.setRotationPoint(0.0F, -0.1F, -7.25F);
		wrist.addChild(screen);
		setRotationAngle(screen, 1.0908F, 0.0F, 0.0F);
		screen.cubeList.add(new ModelBox(screen, 0, 0, -7.0F, -4.4F, -2.0F, 14, 9, 1, 0.0F, false));
		screen.cubeList.add(new ModelBox(screen, 28, 38, -2.0F, -3.4F, -1.0F, 1, 7, 2, 0.0F, false));
		screen.cubeList.add(new ModelBox(screen, 37, 14, 1.0F, -3.4F, -1.0F, 1, 7, 2, 0.0F, false));
		screen.cubeList.add(new ModelBox(screen, 44, 24, 0.0625F, 4.1114F, -2.1198F, 7, 1, 1, 0.0F, false));
		screen.cubeList.add(new ModelBox(screen, 27, 24, -0.5F, 4.0988F, -2.3714F, 1, 1, 1, 0.0F, false));
		screen.cubeList.add(new ModelBox(screen, 27, 24, -1.0F, 4.2798F, -2.2446F, 2, 1, 1, 0.0F, false));
		screen.cubeList.add(new ModelBox(screen, 24, 24, -7.0625F, 4.1114F, -2.1198F, 7, 1, 1, 0.0F, false));

		glow_screen = new RendererModel(this);
		glow_screen.setRotationPoint(0.0F, 5.6F, 23.25F);
		screen.addChild(glow_screen);
		glow_screen.cubeList.add(new ModelBox(glow_screen, 0, 12, -6.5F, -9.7538F, -25.5434F, 13, 8, 1, 0.0F, false));

		fixed_base = new RendererModel(this);
		fixed_base.setRotationPoint(0.0F, 0.0F, 7.0F);
		monitor.addChild(fixed_base);
		fixed_base.cubeList.add(new ModelBox(fixed_base, 0, 22, -4.0F, -1.0F, -11.0F, 8, 1, 8, 0.0F, false));
		
		label = new TextLabelRendererModel(this, TEXT, () -> text, () -> textureToRebind);
		label.setRotationPoint(-6F, -4, -2.3F);
		this.screen.addChild(label);

	}

	public void render(RotateMonitorTile tile, ResourceLocation texture) {
		
		this.textureToRebind = texture;
		
		float rot = tile.previousRot + (tile.rotation - tile.previousRot) * Minecraft.getInstance().getRenderPartialTicks();
		
		text = tile.mode == MonitorMode.INFO ? tile.getInfo() : new String[] {""};
		
		float progress = tile.extendAmount;
		
		this.sholder.rotateAngleX = -(float)Math.toRadians(25 + (50 * progress));
		this.forearm.rotateAngleX = (float)Math.toRadians(60 + (80 * progress));
		this.wrist.rotateAngleX = -(float)Math.toRadians(90 + (30 * progress));
		this.screen.rotateAngleX = (float)Math.toRadians(60);
		
		
		
		this.spin_y.rotateAngleY = (float)Math.toRadians(rot);
		this.monitor.render(0.0625F);
		
		if(tile.getMode() == MonitorMode.SCANNER && TConfig.CLIENT.externalMonitor.get()) {
			Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
				GlStateManager.pushMatrix();
				this.spin_y.postRender(0.0625F);
				this.sholder.postRender(0.0625F);
				this.forearm.postRender(0.0625F);
				this.wrist.postRender(0.0625F);
				this.screen.postRender(0.0625F);
				GlStateManager.rotated(180, 0, 0, 1);
				RenderScanner.renderWorldToPlane(data, -0.4F, -1.725F, 0.4F, -1.245F, -0.275F, tile.getView().getAngle(), Minecraft.getInstance().getRenderPartialTicks());
				GlStateManager.popMatrix();
			});
		}
		
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}