package net.tardis.mod.client.models.entity.dalek;

import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.entity.DalekEntity;
import net.tardis.mod.helper.ModelHelper;

public class Dalek2005Model extends EntityModel<DalekEntity> {
	private final RendererModel base;
	private final RendererModel base2;
	private final RendererModel bone2;
	private final RendererModel bone5;
	private final RendererModel bone6;
	private final RendererModel bone4;
	private final RendererModel bone3;
	private final RendererModel bone;
	private final RendererModel eggs;
	private final RendererModel bone7;
	private final RendererModel bone11;
	private final RendererModel bone10;
	private final RendererModel bone12;
	private final RendererModel eggseperation;
	private final RendererModel dalek;
	private final RendererModel bone8;
	private final RendererModel bone9;
	private final RendererModel lights;
	private final RendererModel eye;
	private final RendererModel eyestalk;
	private final RendererModel head;
	private final RendererModel hover;
	private final RendererModel lighthover;
	private final RendererModel gun;
	private final RendererModel whatareyougoingtodosuckermetodeath;
	
	public Dalek2005Model() {
		textureWidth = 128;
		textureHeight = 128;

		dalek = new RendererModel(this);
		dalek.setRotationPoint(0.0F, 24.0F, 0.0F);
		
		base = new RendererModel(this);
		base.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek.addChild(base);
		base.cubeList.add(new ModelBox(base, 70, 12, -8.2F, -3.0F, -3.9F, 16, 3, 12, 0.0F, false));

		base2 = new RendererModel(this);
		base2.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek.addChild(base2);
		base2.cubeList.add(new ModelBox(base2, 93, 12, -7.25F, -3.0F, -4.875F, 15, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -7.25F, -3.0F, 8.125F, 15, 3, 1, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -6.15F, -3.0F, -7.8F, 12, 3, 3, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -6.15F, -3.0F, 7.475F, 12, 3, 3, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 93, 12, -4.45F, -3.0F, -9.75F, 9, 3, 2, 0.0F, false));
		base2.cubeList.add(new ModelBox(base2, 98, 28, -3.75F, -3.0F, -10.75F, 7, 3, 2, 0.0F, false));

		eggs = new RendererModel(this);
		eggs.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek.addChild(eggs);
		eggs.cubeList.add(new ModelBox(eggs, 84, 90, -4.45F, -14.6F, -3.9F, 9, 12, 13, 0.0F, false));
		eggs.cubeList.add(new ModelBox(eggs, 38, 66, -1.0F, -13.375F, 8.33F, 2, 2, 1, 0.0F, false));
		eggs.cubeList.add(new ModelBox(eggs, 38, 66, -1.0F, -9.375F, 8.33F, 2, 2, 1, 0.0F, false));
		eggs.cubeList.add(new ModelBox(eggs, 38, 66, -1.0F, -5.375F, 8.33F, 2, 2, 1, 0.0F, false));
		
		bone2 = new RendererModel(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone2, 0.0F, 0.0F, 0.0873F);
		eggs.addChild(bone2);
		bone2.cubeList.add(new ModelBox(bone2, 89, 98, -7.55F, -14.3F, -3.9F, 3, 13, 12, 0.0F, false));

		bone5 = new RendererModel(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone5, 0.0F, 0.0F, 0.0873F);
		eggs.addChild(bone5);
		bone5.cubeList.add(new ModelBox(bone5, 102, 109, -6.25F, -14.3F, 6.75F, 3, 13, 3, 0.0F, false));

		bone6 = new RendererModel(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone6, 0.0F, 0.0F, -0.0873F);
		eggs.addChild(bone6);
		bone6.cubeList.add(new ModelBox(bone6, 102, 109, 2.85F, -14.3F, 6.75F, 3, 13, 3, 0.0F, false));

		bone4 = new RendererModel(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone4, 0.0F, 0.0F, -0.0873F);
		eggs.addChild(bone4);
		bone4.cubeList.add(new ModelBox(bone4, 97, 102, 4.55F, -14.3F, -3.9F, 3, 13, 12, 0.0F, false));

		bone3 = new RendererModel(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone3, -0.1745F, -0.6109F, 0.3491F);
		eggs.addChild(bone3);

		bone = new RendererModel(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone, -0.1745F, 0.0F, 0.0F);
		eggs.addChild(bone);
		bone.cubeList.add(new ModelBox(bone, 99, 111, -4.1F, -13.0F, -9.1F, 8, 13, 3, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, -3.0F, -12.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, 1.25F, -12.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, 1.25F, -8.5F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, 1.25F, -5.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, -3.0F, -8.5F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 38, 66, -3.0F, -5.0F, -9.425F, 2, 2, 2, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 109, 110, -0.35F, -13.0F, -10.075F, 1, 13, 1, 0.0F, false));
		bone.cubeList.add(new ModelBox(bone, 96, 107, -4.8F, -13.325F, -7.8F, 10, 13, 4, 0.0F, false));
		
		eggseperation = new RendererModel(this);
		eggseperation.setRotationPoint(0.0F, 0.0F, 0.0F);
		dalek.addChild(eggseperation);
		eggseperation.cubeList.add(new ModelBox(eggseperation, 18, 105, -4.45F, -17.3F, -6.5F, 9, 3, 1, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 22, 111, -4.45F, -20.925F, -4.875F, 9, 5, 4, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 22, 111, -4.45F, -21.9F, -4.75F, 9, 5, 4, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 98, 104, 3.0F, -19.25F, -7.475F, 2, 2, 4, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 98, 104, -5.0F, -19.25F, -7.475F, 2, 2, 4, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 79, 106, -5.0F, -22.5F, -4.55F, 10, 8, 13, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 79, 106, -5.5F, -22.5F, -4.55F, 1, 6, 13, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 79, 106, 4.25F, -22.5F, -4.55F, 1, 6, 13, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 8, 92, -5.125F, -24.125F, -3.25F, 10, 3, 10, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 8, 92, -5.125F, -26.025F, -3.25F, 10, 1, 10, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 94, 62, -4.1F, -27.0F, -1.95F, 8, 1, 8, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 94, 62, -4.1F, -25.0F, -1.95F, 8, 1, 8, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 85, 112, -4.8F, -22.125F, -3.9F, 10, 1, 12, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 117, 6, 3.5F, -18.75F, -7.8F, 1, 1, 1, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 19, 96, 3.25F, -19.85F, -6.5F, 1, 1, 2, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 21, 97, -4.55F, -19.85F, -6.5F, 1, 1, 2, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 12, 104, -3.5F, -17.3F, -7.875F, 7, 3, 2, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 18, 105, -6.15F, -17.3F, -5.5F, 12, 3, 1, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 12, 104, -5.65F, -17.3F, 7.75F, 11, 3, 2, 0.0F, false));
		eggseperation.cubeList.add(new ModelBox(eggseperation, 2, 112, -6.55F, -17.3F, -4.55F, 13, 3, 13, 0.0F, false));
		
		bone7 = new RendererModel(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone7, -0.3491F, 0.0F, 0.0F);
		eggseperation.addChild(bone7);
		bone7.cubeList.add(new ModelBox(bone7, 111, 110, -3.5F, -19.675F, -11.7F, 7, 7, 1, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 68, 109, -1.65F, -18.325F, -12.025F, 1, 5, 1, 0.0F, false));
		bone7.cubeList.add(new ModelBox(bone7, 61, 113, 0.95F, -18.325F, -12.025F, 1, 5, 1, 0.0F, false));

		bone11 = new RendererModel(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone11, 0.0F, 0.0F, -0.0873F);
		eggseperation.addChild(bone11);
		bone11.cubeList.add(new ModelBox(bone11, 67, 105, 6.5F, -20.925F, 5.525F, 1, 5, 1, 0.0F, false));//Slats Left side
		bone11.cubeList.add(new ModelBox(bone11, 67, 105, 6.5F, -20.925F, 3.25F, 1, 5, 1, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 67, 121, 6.5F, -20.925F, 0.975F, 1, 5, 1, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 67, 121, 6.5F, -20.925F, -1.3F, 1, 5, 1, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 67, 121, 6.5F, -20.925F, -3.25F, 1, 5, 1, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -12.725F, -1.45F, 1, 2, 2, 0.0F, false)); //Eggs Left side
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -12.725F, 1.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -12.725F, 4.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -8.725F, -1.45F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -8.725F, 1.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -8.725F, 4.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -4.925F, -1.45F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -4.925F, 1.55F, 1, 2, 2, 0.0F, false));
		bone11.cubeList.add(new ModelBox(bone11, 26, 56, 6.75F, -4.925F, 4.55F, 1, 2, 2, 0.0F, false));

		bone10 = new RendererModel(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		setRotationAngle(bone10, 0.0F, 0.0F, 0.0873F);
		eggseperation.addChild(bone10);
		bone10.cubeList.add(new ModelBox(bone10, 67, 110, -7.75F, -20.925F, -3.25F, 1, 5, 1, 0.0F, false)); //Slats right side
		bone10.cubeList.add(new ModelBox(bone10, 67, 105, -7.75F, -20.925F, -1.3F, 1, 5, 1, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 67, 105, -7.75F, -20.925F, 0.975F, 1, 5, 1, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 67, 105, -7.75F, -20.925F, 3.25F, 1, 5, 1, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 67, 105, -7.75F, -20.925F, 5.525F, 1, 5, 1, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -8.825F, 4.55F, 1, 2, 2, 0.0F, false)); //Eggs right side
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -8.825F, 1.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -8.825F, -1.45F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -4.925F, 4.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -4.925F, 1.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -4.925F, -1.45F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -12.725F, 4.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -12.725F, 1.55F, 1, 2, 2, 0.0F, false));
		bone10.cubeList.add(new ModelBox(bone10, 38, 66, -7.875F, -12.725F, -1.45F, 1, 2, 2, 0.0F, false));

		bone12 = new RendererModel(this);
		bone12.setRotationPoint(0.0F, -15.0F, 6.25F);
		setRotationAngle(bone12, 0.0873F, 0.0F, 0.0F);
		eggseperation.addChild(bone12);
		bone12.cubeList.add(new ModelBox(bone12, 67, 105, 3.5F, -6.575F, 2.25F, 1, 5, 1, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 67, 105, 1.5F, -6.575F, 2.25F, 1, 5, 1, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 67, 122, -0.5F, -6.575F, 2.25F, 1, 5, 1, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 67, 122, -2.5F, -6.575F, 2.25F, 1, 5, 1, 0.0F, false));
		bone12.cubeList.add(new ModelBox(bone12, 67, 122, -4.5F, -6.575F, 2.25F, 1, 5, 1, 0.0F, false));

		lights = new RendererModel(this);
		lights.setRotationPoint(0.0F, 1.0F, 1.5F);
		
		bone8 = new RendererModel(this);
		bone8.setRotationPoint(0.0F, 23.0F, -1.5F);
		setRotationAngle(bone8, 0.0F, 0.0F, -0.0873F);
		lights.addChild(bone8);
		bone8.cubeList.add(new ModelBox(bone8, 66, 60, -1.65F, -32.75F, 1.3F, 1, 3, 1, 0.0F, false));

		bone9 = new RendererModel(this);
		bone9.setRotationPoint(0.0F, 10.0F, 0.0F);
		setRotationAngle(bone9, 0.0F, 0.0F, 0.0873F);
		lights.addChild(bone9);
		bone9.cubeList.add(new ModelBox(bone9, 72, 59, 1.75F, -19.75F, -0.2F, 1, 3, 1, 0.0F, false));

		head = new RendererModel(this);
		head.setRotationPoint(0.0F, 1.0F, 1.5F);
		head.cubeList.add(new ModelBox(head, 0, 84, -4.5F, -7.0F, -4.0F, 9, 3, 9, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 0, 84, -4.0F, -8.0F, -3.5F, 8, 1, 8, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 0, 84, -3.5F, -8.75F, -3.0F, 7, 1, 7, 0.0F, false));
		head.cubeList.add(new ModelBox(head, 0, 84, -1.0F, -8.0F, -4.5F, 2, 2, 4, 0.0F, false));
		
		eyestalk = new RendererModel(this);
		eyestalk.setRotationPoint(0.0F, -7.0F, -3.5F);
		head.addChild(eyestalk);
		eyestalk.cubeList.add(new ModelBox(eyestalk, 37, 0, -0.5F, -0.5F, -8.275F, 1, 1, 8, 0.0F, false));
		eyestalk.cubeList.add(new ModelBox(eyestalk, 86, 39, -1.5F, -1.5F, -9.25F, 3, 3, 1, 0.0F, false));
		eyestalk.cubeList.add(new ModelBox(eyestalk, 60, 70, -1.5F, -1.5F, -6.75F, 3, 3, 0, 0.0F, false));
		eyestalk.cubeList.add(new ModelBox(eyestalk, 60, 70, -1.5F, -1.5F, -6.0F, 3, 3, 0, 0.0F, false));
		eyestalk.cubeList.add(new ModelBox(eyestalk, 60, 70, -1.5F, -1.5F, -5.25F, 3, 3, 0, 0.0F, false));
		
		eye = new RendererModel(this);
		eye.setRotationPoint(0.0F, 7.0F, 0.0F);
		eyestalk.addChild(eye);
		eye.cubeList.add(new ModelBox(eye, 10, 0, -0.5F, -7.5F, -9.35F, 1, 1, 0, 0.0F, false));

		hover = new RendererModel(this);
		hover.setRotationPoint(0.0F, 0.6944F, -3.25F);
		hover.cubeList.add(new ModelBox(hover, 0, 85, 2.75F, 22.4306F, -1.0F, 3, 1, 3, 0.0F, false));
		hover.cubeList.add(new ModelBox(hover, 0, 85, 2.75F, 22.4306F, 7.0F, 3, 1, 3, 0.0F, false));
		hover.cubeList.add(new ModelBox(hover, 0, 85, -5.75F, 22.4306F, 7.0F, 3, 1, 3, 0.0F, false));
		hover.cubeList.add(new ModelBox(hover, 0, 85, -5.75F, 22.4306F, -1.0F, 3, 1, 3, 0.0F, false));

		lighthover = new RendererModel(this);
		lighthover.setRotationPoint(0.0F, 0.6944F, -3.25F);
		lighthover.cubeList.add(new ModelBox(lighthover, 10, 1, -2.5F, 22.4306F, 2.0F, 5, 1, 5, 0.0F, false));
		lighthover.cubeList.add(new ModelBox(lighthover, 10, 1, 3.0F, 22.4306F, 3.0F, 3, 1, 3, 0.0F, false));
		lighthover.cubeList.add(new ModelBox(lighthover, 10, 1, -6.0F, 22.4306F, 3.0F, 3, 1, 3, 0.0F, false));
		lighthover.cubeList.add(new ModelBox(lighthover, 10, 1, -1.5F, 22.4306F, 7.5F, 3, 1, 3, 0.0F, false));
		lighthover.cubeList.add(new ModelBox(lighthover, 10, 1, -1.5F, 22.4306F, -1.5F, 3, 1, 3, 0.0F, false));

		gun = new RendererModel(this);
		gun.setRotationPoint(4.0F, 5.65F, -7.75F);
		gun.cubeList.add(new ModelBox(gun, 42, 12, -0.5F, 0.1F, -4.9326F, 1, 0, 5, 0.0F, false));
		gun.cubeList.add(new ModelBox(gun, 42, 16, 0.0F, -0.4F, -4.9326F, 0, 1, 5, 0.0F, false));

		whatareyougoingtodosuckermetodeath = new RendererModel(this);
		whatareyougoingtodosuckermetodeath.setRotationPoint(-4.0F, 6.0F, -7.0F);
		whatareyougoingtodosuckermetodeath.cubeList.add(new ModelBox(whatareyougoingtodosuckermetodeath, 42, 12, -0.5F, -0.75F, -9.9375F, 1, 1, 10, 0.0F, false));
		whatareyougoingtodosuckermetodeath.cubeList.add(new ModelBox(whatareyougoingtodosuckermetodeath, 101, 68, -1.5F, -1.75F, -10.295F, 3, 3, 1, 0.0F, false));
	}

	@Override
	public void render(DalekEntity entity, float f, float f1, float f2, float f3, float f4, float f5) {
        setRotationAngles(entity, f, f1, f2, f3, f4, f5);
		whatareyougoingtodosuckermetodeath.render(f5);
		gun.render(f5);
		lighthover.render(f5);
		hover.render(f5);
		head.render(f5);
		dalek.render(f5);
		ModelHelper.renderPartBrightness(1F, lights, lighthover);
		ModelHelper.renderPartBrightness(1, lights);
	}


    @Override
    public void setRotationAngles(DalekEntity entityIn, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor) {
        head.rotateAngleX = 0;
		head.rotateAngleY = (float) MathHelper.clamp(Math.toRadians(netHeadYaw), -90, 90);
        head.rotateAngleZ = 0;

		eyestalk.rotateAngleX = (float) MathHelper.clamp(Math.toRadians(headPitch), -90, 90);
        eyestalk.rotateAngleY = 0;
        eyestalk.rotateAngleZ = 0;

        whatareyougoingtodosuckermetodeath.rotateAngleZ = 0;
		whatareyougoingtodosuckermetodeath.rotateAngleY = (float) MathHelper.clamp(Math.toRadians(netHeadYaw), -90, 90);
		whatareyougoingtodosuckermetodeath.rotateAngleX = (float) MathHelper.clamp(Math.toRadians(headPitch), -90, 90);
    }

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}


}