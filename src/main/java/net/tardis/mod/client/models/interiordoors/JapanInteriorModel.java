package net.tardis.mod.client.models.interiordoors;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.renderers.exteriors.JapanExteriorRenderer;
import net.tardis.mod.entity.DoorEntity;

// Made with Blockbench 3.6.5
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class JapanInteriorModel extends Model implements IInteriorDoorRenderer{
	private final RendererModel main;
	private final RendererModel Posts;
	private final RendererModel Signage;
	private final RendererModel panels;
	private final RendererModel door;
	private final RendererModel boti;

	public JapanInteriorModel() {
		textureWidth = 256;
		textureHeight = 256;

		main = new RendererModel(this);
		main.setRotationPoint(0.0F, 24.0F, -5.0F);
		main.cubeList.add(new ModelBox(main, 15, 53, -14.0F, -41.0F, 12.0F, 28, 2, 1, 0.0F, false));

		Posts = new RendererModel(this);
		Posts.setRotationPoint(-12.0F, -4.0F, 10.0F);
		main.addChild(Posts);
		Posts.cubeList.add(new ModelBox(Posts, 161, 70, -3.0F, -36.0F, -2.0F, 4, 2, 4, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 161, 70, 23.0F, -36.0F, -2.0F, 4, 2, 4, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, 24.0F, -35.0F, -1.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, -2.0F, -35.0F, -1.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, 25.0F, -38.0F, -2.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 145, 76, -3.0F, -38.0F, -2.0F, 2, 2, 2, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 131, 31, 22.0F, -35.0F, 0.0F, 3, 39, 3, 0.0F, false));
		Posts.cubeList.add(new ModelBox(Posts, 131, 31, -1.0F, -35.0F, 0.0F, 3, 39, 3, 0.0F, false));

		Signage = new RendererModel(this);
		Signage.setRotationPoint(14.0F, -42.0F, -14.0F);
		main.addChild(Signage);
		Signage.cubeList.add(new ModelBox(Signage, 32, 78, -26.0F, 3.0F, 25.0F, 24, 4, 3, 0.0F, false));

		panels = new RendererModel(this);
		panels.setRotationPoint(9.0F, -4.0F, -12.0F);
		main.addChild(panels);
		panels.cubeList.add(new ModelBox(panels, 96, 87, -9.0F, -31.0F, 23.0F, 10, 35, 1, 0.0F, false));

		door = new RendererModel(this);
		door.setRotationPoint(0.0F, -4.0F, 10.0F);
		main.addChild(door);
		door.cubeList.add(new ModelBox(door, 118, 87, -10.0F, -31.0F, 0.0F, 10, 35, 1, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, -4.0F, 12.0F);
		main.addChild(boti);
		boti.cubeList.add(new ModelBox(boti, 0, 126, -10.0F, -31.0F, 0.0F, 20, 35, 1, 0.0F, false));
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(DoorEntity door) {
		main.render(0.0625F);
	}

	@Override
	public ResourceLocation getTexture() {
		return JapanExteriorRenderer.TEXTURE;
	}
}