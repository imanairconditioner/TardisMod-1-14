package net.tardis.mod.client.models.interiordoors;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.DoorEntity;

public interface IInteriorDoorRenderer {

	void render(DoorEntity door);
	ResourceLocation getTexture();
}
