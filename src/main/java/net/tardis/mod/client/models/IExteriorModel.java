package net.tardis.mod.client.models;

import net.tardis.mod.entity.TardisEntity;

public interface IExteriorModel {

	void renderEntity(TardisEntity ent);
}
