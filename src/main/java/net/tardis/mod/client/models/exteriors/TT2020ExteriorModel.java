package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraft.client.renderer.model.Model;
import net.minecraft.client.renderer.model.ModelBox;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

// Made with Blockbench 3.5.2
// Exported for Minecraft version 1.14
// Paste this class into your mod and generate all required imports


public class TT2020ExteriorModel extends Model {
	private final RendererModel glow;
	private final RendererModel glow_plate_2;
	private final RendererModel glow_plate_3;
	private final RendererModel glow_plate_4;
	private final RendererModel glow_plate_5;
	private final RendererModel glow_plate_6;
	private final RendererModel door;
	private final RendererModel door_west_rotate_y;
	private final RendererModel door_dot_west_a;
	private final RendererModel door_dot_west_b;
	private final RendererModel door_dot_west_c;
	private final RendererModel door_dot_west_d;
	private final RendererModel door_dot_west_e;
	private final RendererModel door_east_rotate_y;
	private final RendererModel door_dot_east_a;
	private final RendererModel door_dot_east_b;
	private final RendererModel door_dot_east_c;
	private final RendererModel door_dot_east_d;
	private final RendererModel door_dot_east_e;
	private final RendererModel side_walls;
	private final RendererModel front;
	private final RendererModel door_frame;
	private final RendererModel frame;
	private final RendererModel panel2;
	private final RendererModel main_panel_2;
	private final RendererModel deviders2;
	private final RendererModel lower_roundel_2;
	private final RendererModel upper_roundel_2;
	private final RendererModel panel3;
	private final RendererModel main_panel_3;
	private final RendererModel deviders3;
	private final RendererModel upper_roundel_3;
	private final RendererModel mid_roundel_3;
	private final RendererModel lower_roundel_3;
	private final RendererModel panel4;
	private final RendererModel main_panel_4;
	private final RendererModel deviders4;
	private final RendererModel lower_roundel_4;
	private final RendererModel upper_roundel_4;
	private final RendererModel panel5;
	private final RendererModel main_panel_5;
	private final RendererModel deviders5;
	private final RendererModel upper_roundel_5;
	private final RendererModel mid_roundel_2;
	private final RendererModel lower_roundel_5;
	private final RendererModel panel6;
	private final RendererModel main_panel_6;
	private final RendererModel deviders6;
	private final RendererModel lower_roundel_6;
	private final RendererModel upper_roundel_6;
	private final RendererModel center;
	private final RendererModel door_jam;
	private final RendererModel frames;
	private final RendererModel front_frame;
	private final RendererModel cap_1;
	private final RendererModel head_nub_1;
	private final RendererModel head_1;
	private final RendererModel spine_1;
	private final RendererModel upper_peak_1;
	private final RendererModel base_1;
	private final RendererModel foot_nub_1;
	private final RendererModel head_2;
	private final RendererModel spine_2;
	private final RendererModel lower_peak_1;
	private final RendererModel lamp_trim;
	private final RendererModel frame2;
	private final RendererModel cap_2;
	private final RendererModel head_nub_2;
	private final RendererModel head_3;
	private final RendererModel spine_3;
	private final RendererModel upper_peak_2;
	private final RendererModel base_2;
	private final RendererModel foot_nub_2;
	private final RendererModel head_4;
	private final RendererModel spine_4;
	private final RendererModel lower_peak_2;
	private final RendererModel lamp_trim2;
	private final RendererModel frame3;
	private final RendererModel cap_3;
	private final RendererModel head_nub_3;
	private final RendererModel head_5;
	private final RendererModel spine_5;
	private final RendererModel upper_peak_3;
	private final RendererModel base_3;
	private final RendererModel foot_nub_3;
	private final RendererModel head_6;
	private final RendererModel spine_6;
	private final RendererModel lower_peak_3;
	private final RendererModel lamp_trim3;
	private final RendererModel frame4;
	private final RendererModel cap_4;
	private final RendererModel head_nub_4;
	private final RendererModel head_7;
	private final RendererModel spine_7;
	private final RendererModel upper_peak_4;
	private final RendererModel base_4;
	private final RendererModel foot_nub_4;
	private final RendererModel head_8;
	private final RendererModel spine_8;
	private final RendererModel lower_peak_4;
	private final RendererModel lamp_trim4;
	private final RendererModel frame5;
	private final RendererModel cap_5;
	private final RendererModel head_nub_5;
	private final RendererModel head_9;
	private final RendererModel spine_9;
	private final RendererModel upper_peak_5;
	private final RendererModel base_5;
	private final RendererModel foot_nub_5;
	private final RendererModel head_10;
	private final RendererModel spine_10;
	private final RendererModel lower_peak_5;
	private final RendererModel lamp_trim5;
	private final RendererModel frame6;
	private final RendererModel cap_6;
	private final RendererModel head_nub_6;
	private final RendererModel head_11;
	private final RendererModel spine_11;
	private final RendererModel upper_peak_6;
	private final RendererModel base_6;
	private final RendererModel foot_nub_6;
	private final RendererModel head_12;
	private final RendererModel spine_12;
	private final RendererModel lower_peak_6;
	private final RendererModel lamp_trim6;
	private final RendererModel boti;

	public TT2020ExteriorModel() {
		textureWidth = 512;
		textureHeight = 512;

		glow = new RendererModel(this);
		glow.setRotationPoint(0.0F, 24.0F, 1.0F);
		

		glow_plate_2 = new RendererModel(this);
		glow_plate_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_plate_2);
		setRotationAngle(glow_plate_2, 0.0F, -1.0472F, 0.0F);
		glow_plate_2.cubeList.add(new ModelBox(glow_plate_2, 248, 110, -24.0F, -74.0F, -47.0F, 48, 64, 1, 0.0F, false));
		glow_plate_2.cubeList.add(new ModelBox(glow_plate_2, 248, 110, -24.0F, -122.0F, -47.0F, 48, 48, 1, 0.0F, false));
		glow_plate_2.cubeList.add(new ModelBox(glow_plate_2, 248, 110, -24.0F, -186.0F, -47.0F, 48, 64, 1, 0.0F, false));

		glow_plate_3 = new RendererModel(this);
		glow_plate_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_plate_3);
		setRotationAngle(glow_plate_3, 0.0F, -2.0944F, 0.0F);
		glow_plate_3.cubeList.add(new ModelBox(glow_plate_3, 248, 110, -24.0F, -74.0F, -47.0F, 48, 64, 1, 0.0F, false));
		glow_plate_3.cubeList.add(new ModelBox(glow_plate_3, 248, 110, -24.0F, -122.0F, -47.0F, 48, 48, 1, 0.0F, false));
		glow_plate_3.cubeList.add(new ModelBox(glow_plate_3, 248, 110, -24.0F, -186.0F, -47.0F, 48, 64, 1, 0.0F, false));

		glow_plate_4 = new RendererModel(this);
		glow_plate_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_plate_4);
		setRotationAngle(glow_plate_4, 0.0F, 3.1416F, 0.0F);
		glow_plate_4.cubeList.add(new ModelBox(glow_plate_4, 248, 110, -23.0F, -74.0F, -47.0F, 48, 64, 1, 0.0F, false));
		glow_plate_4.cubeList.add(new ModelBox(glow_plate_4, 248, 110, -23.0F, -122.0F, -47.0F, 48, 48, 1, 0.0F, false));
		glow_plate_4.cubeList.add(new ModelBox(glow_plate_4, 248, 110, -23.0F, -186.0F, -47.0F, 48, 64, 1, 0.0F, false));

		glow_plate_5 = new RendererModel(this);
		glow_plate_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_plate_5);
		setRotationAngle(glow_plate_5, 0.0F, 2.0944F, 0.0F);
		glow_plate_5.cubeList.add(new ModelBox(glow_plate_5, 248, 110, -22.0F, -74.0F, -47.0F, 48, 64, 1, 0.0F, false));
		glow_plate_5.cubeList.add(new ModelBox(glow_plate_5, 248, 110, -22.0F, -122.0F, -47.0F, 48, 48, 1, 0.0F, false));
		glow_plate_5.cubeList.add(new ModelBox(glow_plate_5, 248, 110, -22.0F, -186.0F, -47.0F, 48, 64, 1, 0.0F, false));

		glow_plate_6 = new RendererModel(this);
		glow_plate_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		glow.addChild(glow_plate_6);
		setRotationAngle(glow_plate_6, 0.0F, 1.0472F, 0.0F);
		glow_plate_6.cubeList.add(new ModelBox(glow_plate_6, 248, 110, -22.0F, -74.0F, -47.0F, 48, 64, 1, 0.0F, false));
		glow_plate_6.cubeList.add(new ModelBox(glow_plate_6, 248, 110, -22.0F, -122.0F, -47.0F, 48, 48, 1, 0.0F, false));
		glow_plate_6.cubeList.add(new ModelBox(glow_plate_6, 248, 110, -22.0F, -186.0F, -47.0F, 48, 64, 1, 0.0F, false));

		door = new RendererModel(this);
		door.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		door_west_rotate_y = new RendererModel(this);
		door_west_rotate_y.setRotationPoint(-60.0F, -72.0F, -30.0F);
		door.addChild(door_west_rotate_y);
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 397, 249, 36.0F, 24.0F, -19.0F, 24, 44, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 397, 206, 36.0F, -20.0F, -19.0F, 24, 44, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 397, 163, 36.0F, -64.0F, -19.0F, 24, 44, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 56, 105, 38.0F, -62.0F, -15.0F, 20, 42, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 56, 105, 38.0F, 24.0F, -15.0F, 20, 42, 4, 0.0F, false));
		door_west_rotate_y.cubeList.add(new ModelBox(door_west_rotate_y, 56, 105, 38.0F, -20.0F, -15.0F, 20, 44, 4, 0.0F, false));

		door_dot_west_a = new RendererModel(this);
		door_dot_west_a.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_a);
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -18.0F, -130.0F, -48.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -17.0F, -131.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -19.0F, -129.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -6.0F, -129.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -5.0F, -127.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -20.0F, -127.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -17.0F, -118.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -15.0F, -117.0F, -48.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_a.cubeList.add(new ModelBox(door_dot_west_a, 288, 98, -15.0F, -132.0F, -48.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_b = new RendererModel(this);
		door_dot_west_b.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_b);
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -18.0F, -103.0F, -48.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -17.0F, -104.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -19.0F, -102.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -6.0F, -102.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -5.0F, -100.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -20.0F, -100.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -17.0F, -91.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -15.0F, -90.0F, -48.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_b.cubeList.add(new ModelBox(door_dot_west_b, 288, 98, -15.0F, -105.0F, -48.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_c = new RendererModel(this);
		door_dot_west_c.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_c);
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -18.0F, -76.0F, -48.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -17.0F, -77.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -19.0F, -75.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -6.0F, -75.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -5.0F, -73.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -20.0F, -73.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -17.0F, -64.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -15.0F, -63.0F, -48.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_c.cubeList.add(new ModelBox(door_dot_west_c, 288, 98, -15.0F, -78.0F, -48.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_d = new RendererModel(this);
		door_dot_west_d.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_d);
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -18.0F, -49.0F, -48.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -17.0F, -50.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -19.0F, -48.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -6.0F, -48.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -5.0F, -46.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -20.0F, -46.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -17.0F, -37.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -15.0F, -36.0F, -48.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_d.cubeList.add(new ModelBox(door_dot_west_d, 288, 98, -15.0F, -51.0F, -48.0F, 6, 1, 8, 0.0F, false));

		door_dot_west_e = new RendererModel(this);
		door_dot_west_e.setRotationPoint(60.0F, 72.0F, 30.0F);
		door_west_rotate_y.addChild(door_dot_west_e);
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -18.0F, -22.0F, -48.0F, 12, 12, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -17.0F, -23.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -19.0F, -21.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -6.0F, -21.0F, -48.0F, 1, 10, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -5.0F, -19.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -20.0F, -19.0F, -48.0F, 1, 6, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -17.0F, -10.0F, -48.0F, 10, 1, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -15.0F, -9.0F, -48.0F, 6, 1, 8, 0.0F, false));
		door_dot_west_e.cubeList.add(new ModelBox(door_dot_west_e, 288, 98, -15.0F, -24.0F, -48.0F, 6, 1, 8, 0.0F, false));

		door_east_rotate_y = new RendererModel(this);
		door_east_rotate_y.setRotationPoint(60.0F, -72.0F, -30.0F);
		door.addChild(door_east_rotate_y);
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 422, 249, -60.0F, 24.0F, -19.0F, 24, 44, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 419, 207, -60.0F, -20.0F, -19.0F, 24, 44, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 422, 163, -60.0F, -64.0F, -19.0F, 24, 44, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 56, 105, -58.0F, -62.0F, -15.0F, 20, 42, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 56, 105, -58.0F, 24.0F, -15.0F, 20, 42, 4, 0.0F, false));
		door_east_rotate_y.cubeList.add(new ModelBox(door_east_rotate_y, 56, 105, -58.0F, -20.0F, -15.0F, 20, 44, 4, 0.0F, false));

		door_dot_east_a = new RendererModel(this);
		door_dot_east_a.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_a);
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 5.9734F, -130.0F, -48.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 6.9734F, -131.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 4.9734F, -129.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 17.9734F, -129.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 18.9734F, -127.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 3.9734F, -127.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 6.9734F, -118.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 8.9734F, -117.0F, -48.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_a.cubeList.add(new ModelBox(door_dot_east_a, 288, 98, 8.9734F, -132.0F, -48.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_b = new RendererModel(this);
		door_dot_east_b.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_b);
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 5.9734F, -103.0F, -48.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 6.9734F, -104.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 4.9734F, -102.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 17.9734F, -102.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 18.9734F, -100.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 3.9734F, -100.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 6.9734F, -91.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 8.9734F, -90.0F, -48.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_b.cubeList.add(new ModelBox(door_dot_east_b, 288, 98, 8.9734F, -105.0F, -48.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_c = new RendererModel(this);
		door_dot_east_c.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_c);
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 5.9734F, -76.0F, -48.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 6.9734F, -77.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 4.9734F, -75.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 17.9734F, -75.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 18.9734F, -73.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 3.9734F, -73.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 6.9734F, -64.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 8.9734F, -63.0F, -48.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_c.cubeList.add(new ModelBox(door_dot_east_c, 288, 98, 8.9734F, -78.0F, -48.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_d = new RendererModel(this);
		door_dot_east_d.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_d);
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 5.9734F, -49.0F, -48.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 6.9734F, -50.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 4.9734F, -48.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 17.9734F, -48.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 18.9734F, -46.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 3.9734F, -46.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 6.9734F, -37.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 8.9734F, -36.0F, -48.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_d.cubeList.add(new ModelBox(door_dot_east_d, 288, 98, 8.9734F, -51.0F, -48.6101F, 6, 1, 8, 0.0F, false));

		door_dot_east_e = new RendererModel(this);
		door_dot_east_e.setRotationPoint(-59.9734F, 72.0F, 30.6101F);
		door_east_rotate_y.addChild(door_dot_east_e);
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 5.9734F, -22.0F, -48.6101F, 12, 12, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 6.9734F, -23.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 4.9734F, -21.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 17.9734F, -21.0F, -48.6101F, 1, 10, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 18.9734F, -19.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 3.9734F, -19.0F, -48.6101F, 1, 6, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 6.9734F, -10.0F, -48.6101F, 10, 1, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 8.9734F, -9.0F, -48.6101F, 6, 1, 8, 0.0F, false));
		door_dot_east_e.cubeList.add(new ModelBox(door_dot_east_e, 288, 98, 8.9734F, -24.0F, -48.6101F, 6, 1, 8, 0.0F, false));

		side_walls = new RendererModel(this);
		side_walls.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		front = new RendererModel(this);
		front.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_walls.addChild(front);
		

		door_frame = new RendererModel(this);
		door_frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		front.addChild(door_frame);
		

		frame = new RendererModel(this);
		frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		door_frame.addChild(frame);
		frame.cubeList.add(new ModelBox(frame, 421, 227, -28.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 470, 228, 24.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 424, 111, 24.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 424, 107, -28.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 424, 163, -28.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 424, 176, 24.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 370, 302, -24.0F, -4.0F, -49.0F, 48, 4, 4, 0.0F, false));
		frame.cubeList.add(new ModelBox(frame, 372, 107, -24.0F, -194.0F, -49.0F, 48, 58, 6, 0.0F, false));

		panel2 = new RendererModel(this);
		panel2.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_walls.addChild(panel2);
		setRotationAngle(panel2, 0.0F, -1.0472F, 0.0F);
		

		main_panel_2 = new RendererModel(this);
		main_panel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel2.addChild(main_panel_2);
		

		deviders2 = new RendererModel(this);
		deviders2.setRotationPoint(0.0F, 0.0F, 0.0F);
		main_panel_2.addChild(deviders2);
		deviders2.cubeList.add(new ModelBox(deviders2, 455, 220, 25.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 424, 104, 25.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 370, 109, -27.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 370, 165, -27.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 424, 171, 25.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 370, 285, -23.0F, -12.0F, -49.0F, 48, 12, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 455, 220, -27.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 380, 99, -23.0F, -194.0F, -49.0F, 48, 14, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 370, 177, 5.0F, -132.0F, -49.0F, 20, 72, 4, 0.0F, false));
		deviders2.cubeList.add(new ModelBox(deviders2, 370, 171, -23.0F, -132.0F, -49.0F, 20, 72, 4, 0.0F, false));

		lower_roundel_2 = new RendererModel(this);
		lower_roundel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders2.addChild(lower_roundel_2);
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 370, 249, 9.0F, -60.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 370, 249, -23.0F, -60.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 370, 288, -23.0F, -16.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 445, 272, 9.0F, -16.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 424, 203, 21.0F, -56.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 370, 249, -23.0F, -56.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 370, 276, -23.0F, -28.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 445, 261, 21.0F, -28.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 445, 272, 17.0F, -20.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 424, 203, 17.0F, -56.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 370, 249, -19.0F, -56.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_2.cubeList.add(new ModelBox(lower_roundel_2, 370, 288, -19.0F, -20.0F, -49.0F, 4, 4, 4, 0.0F, false));

		upper_roundel_2 = new RendererModel(this);
		upper_roundel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders2.addChild(upper_roundel_2);
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 424, 108, 9.0F, -180.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 424, 104, -23.0F, -180.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 370, 171, -23.0F, -136.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 370, 171, 9.0F, -136.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 449, 106, 21.0F, -176.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 406, 104, -23.0F, -176.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 370, 171, -23.0F, -148.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 370, 171, 21.0F, -148.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 370, 171, 17.0F, -140.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 424, 110, 17.0F, -176.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 424, 104, -19.0F, -176.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_2.cubeList.add(new ModelBox(upper_roundel_2, 370, 171, -19.0F, -140.0F, -49.0F, 4, 4, 4, 0.0F, false));

		panel3 = new RendererModel(this);
		panel3.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_walls.addChild(panel3);
		setRotationAngle(panel3, 0.0F, -2.0944F, 0.0F);
		

		main_panel_3 = new RendererModel(this);
		main_panel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel3.addChild(main_panel_3);
		

		deviders3 = new RendererModel(this);
		deviders3.setRotationPoint(0.0F, 0.0F, 0.0F);
		main_panel_3.addChild(deviders3);
		deviders3.cubeList.add(new ModelBox(deviders3, 448, 130, 25.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 455, 243, 25.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 370, 302, -23.0F, -12.0F, -49.0F, 48, 12, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 381, 128, -27.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 455, 236, -27.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 424, 172, -27.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 424, 184, 25.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 380, 133, -23.0F, -194.0F, -49.0F, 48, 14, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 367, 171, -23.0F, -156.0F, -49.0F, 20, 36, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 370, 250, -23.0F, -72.0F, -49.0F, 20, 36, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 428, 232, 5.0F, -72.0F, -49.0F, 20, 36, 4, 0.0F, false));
		deviders3.cubeList.add(new ModelBox(deviders3, 411, 171, 5.0F, -156.0F, -49.0F, 20, 36, 4, 0.0F, false));

		upper_roundel_3 = new RendererModel(this);
		upper_roundel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders3.addChild(upper_roundel_3);
		upper_roundel_3.cubeList.add(new ModelBox(upper_roundel_3, 406, 166, -23.0F, -160.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_3.cubeList.add(new ModelBox(upper_roundel_3, 411, 171, 9.0F, -160.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_3.cubeList.add(new ModelBox(upper_roundel_3, 406, 166, -23.0F, -172.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_3.cubeList.add(new ModelBox(upper_roundel_3, 451, 158, 21.0F, -172.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_3.cubeList.add(new ModelBox(upper_roundel_3, 411, 171, 17.0F, -164.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_3.cubeList.add(new ModelBox(upper_roundel_3, 406, 166, -19.0F, -164.0F, -49.0F, 4, 4, 4, 0.0F, false));

		mid_roundel_3 = new RendererModel(this);
		mid_roundel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders3.addChild(mid_roundel_3);
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 199, 9.0F, -120.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 196, -23.0F, -120.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 214, -23.0F, -76.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 217, 9.0F, -76.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 199, 21.0F, -116.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 199, -23.0F, -116.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 217, -23.0F, -88.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 217, 21.0F, -88.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 219, 17.0F, -80.0F, -49.0F, 4, 4, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 199, 17.0F, -116.0F, -49.0F, 4, 4, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 207, -19.0F, -116.0F, -49.0F, 4, 4, 4, 0.0F, false));
		mid_roundel_3.cubeList.add(new ModelBox(mid_roundel_3, 424, 215, -19.0F, -80.0F, -49.0F, 4, 4, 4, 0.0F, false));

		lower_roundel_3 = new RendererModel(this);
		lower_roundel_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders3.addChild(lower_roundel_3);
		lower_roundel_3.cubeList.add(new ModelBox(lower_roundel_3, 428, 266, 9.0F, -36.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_3.cubeList.add(new ModelBox(lower_roundel_3, 370, 284, -23.0F, -36.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_3.cubeList.add(new ModelBox(lower_roundel_3, 428, 267, 21.0F, -32.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_3.cubeList.add(new ModelBox(lower_roundel_3, 379, 285, -23.0F, -32.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_3.cubeList.add(new ModelBox(lower_roundel_3, 428, 267, 17.0F, -32.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_3.cubeList.add(new ModelBox(lower_roundel_3, 381, 284, -19.0F, -32.0F, -49.0F, 4, 4, 4, 0.0F, false));

		panel4 = new RendererModel(this);
		panel4.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_walls.addChild(panel4);
		setRotationAngle(panel4, 0.0F, 3.1416F, 0.0F);
		

		main_panel_4 = new RendererModel(this);
		main_panel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel4.addChild(main_panel_4);
		

		deviders4 = new RendererModel(this);
		deviders4.setRotationPoint(0.0F, 0.0F, 0.0F);
		main_panel_4.addChild(deviders4);
		deviders4.cubeList.add(new ModelBox(deviders4, 455, 220, 25.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 424, 104, 25.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 370, 109, -27.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 370, 165, -27.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 424, 171, 25.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 370, 285, -23.0F, -12.0F, -49.0F, 48, 12, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 455, 220, -27.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 380, 99, -23.0F, -194.0F, -49.0F, 48, 14, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 370, 177, 5.0F, -132.0F, -49.0F, 20, 72, 4, 0.0F, false));
		deviders4.cubeList.add(new ModelBox(deviders4, 370, 171, -23.0F, -132.0F, -49.0F, 20, 72, 4, 0.0F, false));

		lower_roundel_4 = new RendererModel(this);
		lower_roundel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders4.addChild(lower_roundel_4);
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 370, 249, 9.0F, -60.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 370, 249, -23.0F, -60.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 370, 288, -23.0F, -16.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 445, 272, 9.0F, -16.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 424, 203, 21.0F, -56.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 370, 249, -23.0F, -56.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 370, 276, -23.0F, -28.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 445, 261, 21.0F, -28.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 445, 272, 17.0F, -20.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 424, 203, 17.0F, -56.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 370, 249, -19.0F, -56.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_4.cubeList.add(new ModelBox(lower_roundel_4, 370, 288, -19.0F, -20.0F, -49.0F, 4, 4, 4, 0.0F, false));

		upper_roundel_4 = new RendererModel(this);
		upper_roundel_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders4.addChild(upper_roundel_4);
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 424, 108, 9.0F, -180.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 424, 104, -23.0F, -180.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 370, 171, -23.0F, -136.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 370, 171, 9.0F, -136.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 449, 106, 21.0F, -176.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 406, 104, -23.0F, -176.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 370, 171, -23.0F, -148.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 370, 171, 21.0F, -148.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 370, 171, 17.0F, -140.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 424, 110, 17.0F, -176.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 424, 104, -19.0F, -176.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_4.cubeList.add(new ModelBox(upper_roundel_4, 370, 171, -19.0F, -140.0F, -49.0F, 4, 4, 4, 0.0F, false));

		panel5 = new RendererModel(this);
		panel5.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_walls.addChild(panel5);
		setRotationAngle(panel5, 0.0F, 2.0944F, 0.0F);
		

		main_panel_5 = new RendererModel(this);
		main_panel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel5.addChild(main_panel_5);
		

		deviders5 = new RendererModel(this);
		deviders5.setRotationPoint(0.0F, 0.0F, 0.0F);
		main_panel_5.addChild(deviders5);
		deviders5.cubeList.add(new ModelBox(deviders5, 448, 130, 25.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 455, 243, 25.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 370, 302, -23.0F, -12.0F, -49.0F, 48, 12, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 381, 128, -27.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 455, 236, -27.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 424, 172, -27.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 424, 184, 25.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 380, 133, -23.0F, -194.0F, -49.0F, 48, 14, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 367, 171, -23.0F, -156.0F, -49.0F, 20, 36, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 370, 250, -23.0F, -72.0F, -49.0F, 20, 36, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 428, 232, 5.0F, -72.0F, -49.0F, 20, 36, 4, 0.0F, false));
		deviders5.cubeList.add(new ModelBox(deviders5, 411, 171, 5.0F, -156.0F, -49.0F, 20, 36, 4, 0.0F, false));

		upper_roundel_5 = new RendererModel(this);
		upper_roundel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders5.addChild(upper_roundel_5);
		upper_roundel_5.cubeList.add(new ModelBox(upper_roundel_5, 406, 166, -23.0F, -160.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_5.cubeList.add(new ModelBox(upper_roundel_5, 411, 171, 9.0F, -160.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_5.cubeList.add(new ModelBox(upper_roundel_5, 406, 166, -23.0F, -172.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_5.cubeList.add(new ModelBox(upper_roundel_5, 451, 158, 21.0F, -172.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_5.cubeList.add(new ModelBox(upper_roundel_5, 411, 171, 17.0F, -164.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_5.cubeList.add(new ModelBox(upper_roundel_5, 406, 166, -19.0F, -164.0F, -49.0F, 4, 4, 4, 0.0F, false));

		mid_roundel_2 = new RendererModel(this);
		mid_roundel_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders5.addChild(mid_roundel_2);
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 199, 9.0F, -120.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 196, -23.0F, -120.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 214, -23.0F, -76.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 217, 9.0F, -76.0F, -49.0F, 16, 4, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 199, 21.0F, -116.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 199, -23.0F, -116.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 217, -23.0F, -88.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 217, 21.0F, -88.0F, -49.0F, 4, 12, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 219, 17.0F, -80.0F, -49.0F, 4, 4, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 199, 17.0F, -116.0F, -49.0F, 4, 4, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 207, -19.0F, -116.0F, -49.0F, 4, 4, 4, 0.0F, false));
		mid_roundel_2.cubeList.add(new ModelBox(mid_roundel_2, 424, 215, -19.0F, -80.0F, -49.0F, 4, 4, 4, 0.0F, false));

		lower_roundel_5 = new RendererModel(this);
		lower_roundel_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders5.addChild(lower_roundel_5);
		lower_roundel_5.cubeList.add(new ModelBox(lower_roundel_5, 428, 266, 9.0F, -36.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_5.cubeList.add(new ModelBox(lower_roundel_5, 370, 284, -23.0F, -36.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_5.cubeList.add(new ModelBox(lower_roundel_5, 428, 267, 21.0F, -32.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_5.cubeList.add(new ModelBox(lower_roundel_5, 379, 285, -23.0F, -32.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_5.cubeList.add(new ModelBox(lower_roundel_5, 428, 267, 17.0F, -32.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_5.cubeList.add(new ModelBox(lower_roundel_5, 381, 284, -19.0F, -32.0F, -49.0F, 4, 4, 4, 0.0F, false));

		panel6 = new RendererModel(this);
		panel6.setRotationPoint(0.0F, 0.0F, 0.0F);
		side_walls.addChild(panel6);
		setRotationAngle(panel6, 0.0F, 1.0472F, 0.0F);
		

		main_panel_6 = new RendererModel(this);
		main_panel_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		panel6.addChild(main_panel_6);
		

		deviders6 = new RendererModel(this);
		deviders6.setRotationPoint(0.0F, 0.0F, 0.0F);
		main_panel_6.addChild(deviders6);
		deviders6.cubeList.add(new ModelBox(deviders6, 455, 220, 25.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 424, 104, 25.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 370, 109, -27.0F, -194.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 370, 165, -27.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 424, 171, 25.0F, -130.0F, -49.0F, 4, 66, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 370, 285, -23.0F, -12.0F, -49.0F, 48, 12, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 455, 220, -27.0F, -64.0F, -49.0F, 4, 64, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 380, 99, -23.0F, -194.0F, -49.0F, 48, 14, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 370, 177, 5.0F, -132.0F, -49.0F, 20, 72, 4, 0.0F, false));
		deviders6.cubeList.add(new ModelBox(deviders6, 370, 171, -23.0F, -132.0F, -49.0F, 20, 72, 4, 0.0F, false));

		lower_roundel_6 = new RendererModel(this);
		lower_roundel_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders6.addChild(lower_roundel_6);
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 370, 249, 9.0F, -60.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 370, 249, -23.0F, -60.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 370, 288, -23.0F, -16.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 445, 272, 9.0F, -16.0F, -49.0F, 16, 4, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 424, 203, 21.0F, -56.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 370, 249, -23.0F, -56.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 370, 276, -23.0F, -28.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 445, 261, 21.0F, -28.0F, -49.0F, 4, 12, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 445, 272, 17.0F, -20.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 424, 203, 17.0F, -56.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 370, 249, -19.0F, -56.0F, -49.0F, 4, 4, 4, 0.0F, false));
		lower_roundel_6.cubeList.add(new ModelBox(lower_roundel_6, 370, 288, -19.0F, -20.0F, -49.0F, 4, 4, 4, 0.0F, false));

		upper_roundel_6 = new RendererModel(this);
		upper_roundel_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		deviders6.addChild(upper_roundel_6);
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 424, 108, 9.0F, -180.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 424, 104, -23.0F, -180.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 370, 171, -23.0F, -136.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 370, 171, 9.0F, -136.0F, -49.0F, 16, 4, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 449, 106, 21.0F, -176.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 406, 104, -23.0F, -176.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 370, 171, -23.0F, -148.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 370, 171, 21.0F, -148.0F, -49.0F, 4, 12, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 370, 171, 17.0F, -140.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 424, 110, 17.0F, -176.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 424, 104, -19.0F, -176.0F, -49.0F, 4, 4, 4, 0.0F, false));
		upper_roundel_6.cubeList.add(new ModelBox(upper_roundel_6, 370, 171, -19.0F, -140.0F, -49.0F, 4, 4, 4, 0.0F, false));

		center = new RendererModel(this);
		center.setRotationPoint(0.0F, 24.0F, 0.0F);
		center.cubeList.add(new ModelBox(center, 49, 43, -4.0F, -218.0F, -4.5F, 8, 8, 8, 0.0F, false));
		center.cubeList.add(new ModelBox(center, 51, 64, -4.0F, 13.6F, -4.0F, 8, 8, 8, 0.0F, false));

		door_jam = new RendererModel(this);
		door_jam.setRotationPoint(0.0F, 24.0F, 0.0F);
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, 22.0F, -69.0F, -42.0F, 4, 64, 12, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, 22.0F, -133.0F, -42.0F, 4, 64, 12, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, -26.0F, -69.0F, -42.0F, 4, 64, 12, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 64, 57, -26.0F, -133.0F, -42.0F, 4, 64, 12, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 34, 116, 0.0F, -141.0F, -42.0F, 26, 8, 12, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 34, 116, -26.0F, -141.0F, -42.0F, 26, 8, 12, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 34, 76, 0.0F, -5.0F, -46.0F, 26, 4, 16, 0.0F, false));
		door_jam.cubeList.add(new ModelBox(door_jam, 39, 103, -26.0F, -5.0F, -46.0F, 26, 4, 16, 0.0F, false));

		frames = new RendererModel(this);
		frames.setRotationPoint(0.0F, 24.0F, 0.0F);
		

		front_frame = new RendererModel(this);
		front_frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		frames.addChild(front_frame);
		

		cap_1 = new RendererModel(this);
		cap_1.setRotationPoint(-0.2F, -193.0F, 0.0F);
		front_frame.addChild(cap_1);
		

		head_nub_1 = new RendererModel(this);
		head_nub_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_1.addChild(head_nub_1);
		setRotationAngle(head_nub_1, 0.0F, -0.5236F, 0.0F);
		head_nub_1.cubeList.add(new ModelBox(head_nub_1, 455, 155, -2.8F, -3.4F, -58.4F, 7, 10, 10, 0.0F, false));
		head_nub_1.cubeList.add(new ModelBox(head_nub_1, 77, 124, -2.0F, -22.9F, -11.4F, 5, 7, 5, 0.0F, false));
		head_nub_1.cubeList.add(new ModelBox(head_nub_1, 80, 102, -1.0F, -22.9F, -6.4F, 3, 6, 4, 0.0F, false));

		head_1 = new RendererModel(this);
		head_1.setRotationPoint(1.2F, -0.4F, -58.4F);
		head_nub_1.addChild(head_1);
		setRotationAngle(head_1, -1.2217F, 0.0F, 0.0F);
		head_1.cubeList.add(new ModelBox(head_1, 455, 105, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_1 = new RendererModel(this);
		spine_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_1.addChild(spine_1);
		setRotationAngle(spine_1, 0.0F, -0.5236F, 0.0F);
		spine_1.cubeList.add(new ModelBox(spine_1, 455, 108, -0.8F, 6.6F, -57.4F, 3, 64, 9, 0.0F, false));
		spine_1.cubeList.add(new ModelBox(spine_1, 455, 169, -0.8F, 70.6F, -57.4F, 3, 48, 9, 0.0F, false));
		spine_1.cubeList.add(new ModelBox(spine_1, 455, 220, -0.8F, 118.6F, -57.4F, 3, 64, 9, 0.0F, false));

		upper_peak_1 = new RendererModel(this);
		upper_peak_1.setRotationPoint(0.0F, -1.0F, -46.0F);
		cap_1.addChild(upper_peak_1);
		setRotationAngle(upper_peak_1, 0.4363F, 0.0F, 0.0F);
		upper_peak_1.cubeList.add(new ModelBox(upper_peak_1, 368, 134, -23.0F, -1.0F, -2.0F, 48, 1, 11, 0.0F, false));
		upper_peak_1.cubeList.add(new ModelBox(upper_peak_1, 381, 117, -19.0F, -1.0F, 9.0F, 40, 1, 7, 0.0F, false));
		upper_peak_1.cubeList.add(new ModelBox(upper_peak_1, 379, 111, -15.5F, -1.0F, 16.0F, 33, 1, 7, 0.0F, false));
		upper_peak_1.cubeList.add(new ModelBox(upper_peak_1, 391, 103, -12.0F, -1.0F, 23.0F, 26, 1, 7, 0.0F, false));
		upper_peak_1.cubeList.add(new ModelBox(upper_peak_1, 400, 72, -8.0F, -1.0F, 30.0F, 18, 1, 7, 0.0F, false));
		upper_peak_1.cubeList.add(new ModelBox(upper_peak_1, 405, 91, -5.0F, -1.0F, 37.0F, 11, 1, 7, 0.0F, false));
		upper_peak_1.cubeList.add(new ModelBox(upper_peak_1, 286, 137, -3.0F, -2.25F, 41.0F, 7, 2, 3, 0.0F, false));

		base_1 = new RendererModel(this);
		base_1.setRotationPoint(-0.2F, 1.0F, 0.0F);
		front_frame.addChild(base_1);
		

		foot_nub_1 = new RendererModel(this);
		foot_nub_1.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_1.addChild(foot_nub_1);
		setRotationAngle(foot_nub_1, 0.0F, -0.5236F, 0.0F);
		foot_nub_1.cubeList.add(new ModelBox(foot_nub_1, 455, 277, -2.8F, -11.4F, -58.4F, 7, 13, 10, 0.0F, false));
		foot_nub_1.cubeList.add(new ModelBox(foot_nub_1, 77, 124, -2.0F, 13.1F, -11.4F, 5, 7, 5, 0.0F, false));
		foot_nub_1.cubeList.add(new ModelBox(foot_nub_1, 80, 102, -1.0F, 14.1F, -6.4F, 3, 6, 4, 0.0F, false));

		head_2 = new RendererModel(this);
		head_2.setRotationPoint(1.2F, -0.4F, -58.4F);
		foot_nub_1.addChild(head_2);
		setRotationAngle(head_2, -1.9199F, 0.0F, 0.0F);
		head_2.cubeList.add(new ModelBox(head_2, 455, 228, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_2 = new RendererModel(this);
		spine_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_1.addChild(spine_2);
		setRotationAngle(spine_2, 0.0F, -0.5236F, 0.0F);
		

		lower_peak_1 = new RendererModel(this);
		lower_peak_1.setRotationPoint(0.0F, -1.0F, -46.0F);
		base_1.addChild(lower_peak_1);
		setRotationAngle(lower_peak_1, -0.4363F, 0.0F, 0.0F);
		lower_peak_1.cubeList.add(new ModelBox(lower_peak_1, 368, 270, -23.0F, -1.0F, -2.0F, 48, 1, 10, 0.0F, false));
		lower_peak_1.cubeList.add(new ModelBox(lower_peak_1, 380, 264, -20.0F, -1.0F, 8.0F, 41, 1, 7, 0.0F, false));
		lower_peak_1.cubeList.add(new ModelBox(lower_peak_1, 380, 258, -16.5F, -1.0F, 15.0F, 34, 1, 7, 0.0F, false));
		lower_peak_1.cubeList.add(new ModelBox(lower_peak_1, 380, 254, -13.0F, -1.0F, 22.0F, 27, 1, 7, 0.0F, false));
		lower_peak_1.cubeList.add(new ModelBox(lower_peak_1, 380, 252, -9.0F, -1.0F, 29.0F, 19, 1, 7, 0.0F, false));
		lower_peak_1.cubeList.add(new ModelBox(lower_peak_1, 380, 252, -5.0F, -1.0F, 36.0F, 11, 1, 7, 0.0F, false));
		lower_peak_1.cubeList.add(new ModelBox(lower_peak_1, 286, 137, -3.0F, -1.25F, 41.0F, 7, 2, 3, 0.0F, false));

		lamp_trim = new RendererModel(this);
		lamp_trim.setRotationPoint(0.0F, 0.0F, 0.0F);
		front_frame.addChild(lamp_trim);
		lamp_trim.cubeList.add(new ModelBox(lamp_trim, 295, 118, -3.0F, -215.25F, -7.4F, 7, 2, 4, 0.0F, false));
		lamp_trim.cubeList.add(new ModelBox(lamp_trim, 280, 97, -2.0F, 17.25F, -7.4F, 6, 2, 4, 0.0F, false));

		frame2 = new RendererModel(this);
		frame2.setRotationPoint(0.0F, 0.0F, 0.0F);
		frames.addChild(frame2);
		setRotationAngle(frame2, 0.0F, -1.0472F, 0.0F);
		

		cap_2 = new RendererModel(this);
		cap_2.setRotationPoint(-0.2F, -193.0F, 0.0F);
		frame2.addChild(cap_2);
		

		head_nub_2 = new RendererModel(this);
		head_nub_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_2.addChild(head_nub_2);
		setRotationAngle(head_nub_2, 0.0F, -0.5236F, 0.0F);
		head_nub_2.cubeList.add(new ModelBox(head_nub_2, 455, 155, -2.8F, -3.4F, -58.4F, 7, 10, 10, 0.0F, false));
		head_nub_2.cubeList.add(new ModelBox(head_nub_2, 77, 124, -2.0F, -22.9F, -11.4F, 5, 7, 5, 0.0F, false));
		head_nub_2.cubeList.add(new ModelBox(head_nub_2, 80, 102, -1.0F, -22.9F, -6.4F, 3, 6, 4, 0.0F, false));

		head_3 = new RendererModel(this);
		head_3.setRotationPoint(1.2F, -0.4F, -58.4F);
		head_nub_2.addChild(head_3);
		setRotationAngle(head_3, -1.2217F, 0.0F, 0.0F);
		head_3.cubeList.add(new ModelBox(head_3, 455, 105, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_3 = new RendererModel(this);
		spine_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_2.addChild(spine_3);
		setRotationAngle(spine_3, 0.0F, -0.5236F, 0.0F);
		spine_3.cubeList.add(new ModelBox(spine_3, 455, 108, -0.8F, 6.6F, -57.4F, 3, 64, 9, 0.0F, false));
		spine_3.cubeList.add(new ModelBox(spine_3, 455, 169, -0.8F, 70.6F, -57.4F, 3, 48, 9, 0.0F, false));
		spine_3.cubeList.add(new ModelBox(spine_3, 455, 220, -0.8F, 118.6F, -57.4F, 3, 64, 9, 0.0F, false));

		upper_peak_2 = new RendererModel(this);
		upper_peak_2.setRotationPoint(0.0F, -1.0F, -46.0F);
		cap_2.addChild(upper_peak_2);
		setRotationAngle(upper_peak_2, 0.4363F, 0.0F, 0.0F);
		upper_peak_2.cubeList.add(new ModelBox(upper_peak_2, 368, 134, -23.0F, -1.0F, -2.0F, 48, 1, 11, 0.0F, false));
		upper_peak_2.cubeList.add(new ModelBox(upper_peak_2, 381, 117, -19.0F, -1.0F, 9.0F, 40, 1, 7, 0.0F, false));
		upper_peak_2.cubeList.add(new ModelBox(upper_peak_2, 379, 111, -15.5F, -1.0F, 16.0F, 33, 1, 7, 0.0F, false));
		upper_peak_2.cubeList.add(new ModelBox(upper_peak_2, 391, 103, -12.0F, -1.0F, 23.0F, 26, 1, 7, 0.0F, false));
		upper_peak_2.cubeList.add(new ModelBox(upper_peak_2, 400, 72, -8.0F, -1.0F, 30.0F, 18, 1, 7, 0.0F, false));
		upper_peak_2.cubeList.add(new ModelBox(upper_peak_2, 405, 91, -5.0F, -1.0F, 37.0F, 11, 1, 7, 0.0F, false));
		upper_peak_2.cubeList.add(new ModelBox(upper_peak_2, 286, 137, -3.0F, -2.25F, 41.0F, 7, 2, 3, 0.0F, false));

		base_2 = new RendererModel(this);
		base_2.setRotationPoint(-0.2F, 1.0F, 0.0F);
		frame2.addChild(base_2);
		

		foot_nub_2 = new RendererModel(this);
		foot_nub_2.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_2.addChild(foot_nub_2);
		setRotationAngle(foot_nub_2, 0.0F, -0.5236F, 0.0F);
		foot_nub_2.cubeList.add(new ModelBox(foot_nub_2, 455, 277, -2.8F, -11.4F, -58.4F, 7, 13, 10, 0.0F, false));
		foot_nub_2.cubeList.add(new ModelBox(foot_nub_2, 77, 124, -2.0F, 13.1F, -11.4F, 5, 7, 5, 0.0F, false));
		foot_nub_2.cubeList.add(new ModelBox(foot_nub_2, 80, 102, -1.0F, 14.1F, -6.4F, 3, 6, 4, 0.0F, false));

		head_4 = new RendererModel(this);
		head_4.setRotationPoint(1.2F, -0.4F, -58.4F);
		foot_nub_2.addChild(head_4);
		setRotationAngle(head_4, -1.9199F, 0.0F, 0.0F);
		head_4.cubeList.add(new ModelBox(head_4, 455, 228, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_4 = new RendererModel(this);
		spine_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_2.addChild(spine_4);
		setRotationAngle(spine_4, 0.0F, -0.5236F, 0.0F);
		

		lower_peak_2 = new RendererModel(this);
		lower_peak_2.setRotationPoint(0.0F, -1.0F, -46.0F);
		base_2.addChild(lower_peak_2);
		setRotationAngle(lower_peak_2, -0.4363F, 0.0F, 0.0F);
		lower_peak_2.cubeList.add(new ModelBox(lower_peak_2, 368, 270, -23.0F, -1.0F, -2.0F, 48, 1, 10, 0.0F, false));
		lower_peak_2.cubeList.add(new ModelBox(lower_peak_2, 380, 264, -20.0F, -1.0F, 8.0F, 41, 1, 7, 0.0F, false));
		lower_peak_2.cubeList.add(new ModelBox(lower_peak_2, 380, 258, -16.5F, -1.0F, 15.0F, 34, 1, 7, 0.0F, false));
		lower_peak_2.cubeList.add(new ModelBox(lower_peak_2, 380, 254, -13.0F, -1.0F, 22.0F, 27, 1, 7, 0.0F, false));
		lower_peak_2.cubeList.add(new ModelBox(lower_peak_2, 380, 252, -9.0F, -1.0F, 29.0F, 19, 1, 7, 0.0F, false));
		lower_peak_2.cubeList.add(new ModelBox(lower_peak_2, 380, 252, -5.0F, -1.0F, 36.0F, 11, 1, 7, 0.0F, false));
		lower_peak_2.cubeList.add(new ModelBox(lower_peak_2, 286, 137, -3.0F, -1.25F, 41.0F, 7, 2, 3, 0.0F, false));

		lamp_trim2 = new RendererModel(this);
		lamp_trim2.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame2.addChild(lamp_trim2);
		lamp_trim2.cubeList.add(new ModelBox(lamp_trim2, 295, 118, -3.0F, -215.25F, -7.4F, 7, 2, 4, 0.0F, false));
		lamp_trim2.cubeList.add(new ModelBox(lamp_trim2, 280, 97, -2.0F, 17.25F, -7.4F, 6, 2, 4, 0.0F, false));

		frame3 = new RendererModel(this);
		frame3.setRotationPoint(0.0F, 0.0F, 0.0F);
		frames.addChild(frame3);
		setRotationAngle(frame3, 0.0F, -2.0944F, 0.0F);
		

		cap_3 = new RendererModel(this);
		cap_3.setRotationPoint(-0.2F, -193.0F, 0.0F);
		frame3.addChild(cap_3);
		

		head_nub_3 = new RendererModel(this);
		head_nub_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_3.addChild(head_nub_3);
		setRotationAngle(head_nub_3, 0.0F, -0.5236F, 0.0F);
		head_nub_3.cubeList.add(new ModelBox(head_nub_3, 455, 155, -2.8F, -3.4F, -58.4F, 7, 10, 10, 0.0F, false));
		head_nub_3.cubeList.add(new ModelBox(head_nub_3, 77, 124, -2.0F, -22.9F, -11.4F, 5, 7, 5, 0.0F, false));
		head_nub_3.cubeList.add(new ModelBox(head_nub_3, 80, 102, -1.0F, -22.9F, -6.4F, 3, 6, 4, 0.0F, false));

		head_5 = new RendererModel(this);
		head_5.setRotationPoint(1.2F, -0.4F, -58.4F);
		head_nub_3.addChild(head_5);
		setRotationAngle(head_5, -1.2217F, 0.0F, 0.0F);
		head_5.cubeList.add(new ModelBox(head_5, 455, 105, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_5 = new RendererModel(this);
		spine_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_3.addChild(spine_5);
		setRotationAngle(spine_5, 0.0F, -0.5236F, 0.0F);
		spine_5.cubeList.add(new ModelBox(spine_5, 455, 108, -0.8F, 6.6F, -57.4F, 3, 64, 9, 0.0F, false));
		spine_5.cubeList.add(new ModelBox(spine_5, 455, 169, -0.8F, 70.6F, -57.4F, 3, 48, 9, 0.0F, false));
		spine_5.cubeList.add(new ModelBox(spine_5, 455, 220, -0.8F, 118.6F, -57.4F, 3, 64, 9, 0.0F, false));

		upper_peak_3 = new RendererModel(this);
		upper_peak_3.setRotationPoint(0.0F, -1.0F, -46.0F);
		cap_3.addChild(upper_peak_3);
		setRotationAngle(upper_peak_3, 0.4363F, 0.0F, 0.0F);
		upper_peak_3.cubeList.add(new ModelBox(upper_peak_3, 368, 134, -23.0F, -1.0F, -2.0F, 48, 1, 11, 0.0F, false));
		upper_peak_3.cubeList.add(new ModelBox(upper_peak_3, 381, 117, -19.0F, -1.0F, 9.0F, 40, 1, 7, 0.0F, false));
		upper_peak_3.cubeList.add(new ModelBox(upper_peak_3, 379, 111, -15.5F, -1.0F, 16.0F, 33, 1, 7, 0.0F, false));
		upper_peak_3.cubeList.add(new ModelBox(upper_peak_3, 391, 103, -12.0F, -1.0F, 23.0F, 26, 1, 7, 0.0F, false));
		upper_peak_3.cubeList.add(new ModelBox(upper_peak_3, 400, 72, -8.0F, -1.0F, 30.0F, 18, 1, 7, 0.0F, false));
		upper_peak_3.cubeList.add(new ModelBox(upper_peak_3, 405, 91, -5.0F, -1.0F, 37.0F, 11, 1, 7, 0.0F, false));
		upper_peak_3.cubeList.add(new ModelBox(upper_peak_3, 286, 137, -3.0F, -2.25F, 41.0F, 7, 2, 3, 0.0F, false));

		base_3 = new RendererModel(this);
		base_3.setRotationPoint(-0.2F, 1.0F, 0.0F);
		frame3.addChild(base_3);
		

		foot_nub_3 = new RendererModel(this);
		foot_nub_3.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_3.addChild(foot_nub_3);
		setRotationAngle(foot_nub_3, 0.0F, -0.5236F, 0.0F);
		foot_nub_3.cubeList.add(new ModelBox(foot_nub_3, 455, 277, -2.8F, -11.4F, -58.4F, 7, 13, 10, 0.0F, false));
		foot_nub_3.cubeList.add(new ModelBox(foot_nub_3, 77, 124, -2.0F, 13.1F, -11.4F, 5, 7, 5, 0.0F, false));
		foot_nub_3.cubeList.add(new ModelBox(foot_nub_3, 80, 102, -1.0F, 14.1F, -6.4F, 3, 6, 4, 0.0F, false));

		head_6 = new RendererModel(this);
		head_6.setRotationPoint(1.2F, -0.4F, -58.4F);
		foot_nub_3.addChild(head_6);
		setRotationAngle(head_6, -1.9199F, 0.0F, 0.0F);
		head_6.cubeList.add(new ModelBox(head_6, 455, 228, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_6 = new RendererModel(this);
		spine_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_3.addChild(spine_6);
		setRotationAngle(spine_6, 0.0F, -0.5236F, 0.0F);
		

		lower_peak_3 = new RendererModel(this);
		lower_peak_3.setRotationPoint(0.0F, -1.0F, -46.0F);
		base_3.addChild(lower_peak_3);
		setRotationAngle(lower_peak_3, -0.4363F, 0.0F, 0.0F);
		lower_peak_3.cubeList.add(new ModelBox(lower_peak_3, 368, 270, -23.0F, -1.0F, -2.0F, 48, 1, 10, 0.0F, false));
		lower_peak_3.cubeList.add(new ModelBox(lower_peak_3, 380, 264, -20.0F, -1.0F, 8.0F, 41, 1, 7, 0.0F, false));
		lower_peak_3.cubeList.add(new ModelBox(lower_peak_3, 380, 258, -16.5F, -1.0F, 15.0F, 34, 1, 7, 0.0F, false));
		lower_peak_3.cubeList.add(new ModelBox(lower_peak_3, 380, 254, -13.0F, -1.0F, 22.0F, 27, 1, 7, 0.0F, false));
		lower_peak_3.cubeList.add(new ModelBox(lower_peak_3, 380, 252, -9.0F, -1.0F, 29.0F, 19, 1, 7, 0.0F, false));
		lower_peak_3.cubeList.add(new ModelBox(lower_peak_3, 380, 252, -5.0F, -1.0F, 36.0F, 11, 1, 7, 0.0F, false));
		lower_peak_3.cubeList.add(new ModelBox(lower_peak_3, 286, 137, -3.0F, -1.25F, 41.0F, 7, 2, 3, 0.0F, false));

		lamp_trim3 = new RendererModel(this);
		lamp_trim3.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame3.addChild(lamp_trim3);
		lamp_trim3.cubeList.add(new ModelBox(lamp_trim3, 295, 118, -3.0F, -215.25F, -7.4F, 7, 2, 4, 0.0F, false));
		lamp_trim3.cubeList.add(new ModelBox(lamp_trim3, 280, 97, -2.0F, 17.25F, -7.4F, 6, 2, 4, 0.0F, false));

		frame4 = new RendererModel(this);
		frame4.setRotationPoint(0.0F, 0.0F, 0.0F);
		frames.addChild(frame4);
		setRotationAngle(frame4, 0.0F, 3.1416F, 0.0F);
		

		cap_4 = new RendererModel(this);
		cap_4.setRotationPoint(-0.2F, -193.0F, 0.0F);
		frame4.addChild(cap_4);
		

		head_nub_4 = new RendererModel(this);
		head_nub_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_4.addChild(head_nub_4);
		setRotationAngle(head_nub_4, 0.0F, -0.5236F, 0.0F);
		head_nub_4.cubeList.add(new ModelBox(head_nub_4, 455, 155, -2.8F, -3.4F, -58.4F, 7, 10, 10, 0.0F, false));
		head_nub_4.cubeList.add(new ModelBox(head_nub_4, 77, 124, -2.0F, -22.9F, -11.4F, 5, 7, 5, 0.0F, false));
		head_nub_4.cubeList.add(new ModelBox(head_nub_4, 80, 102, -1.0F, -22.9F, -6.4F, 3, 6, 4, 0.0F, false));

		head_7 = new RendererModel(this);
		head_7.setRotationPoint(1.2F, -0.4F, -58.4F);
		head_nub_4.addChild(head_7);
		setRotationAngle(head_7, -1.2217F, 0.0F, 0.0F);
		head_7.cubeList.add(new ModelBox(head_7, 455, 105, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_7 = new RendererModel(this);
		spine_7.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_4.addChild(spine_7);
		setRotationAngle(spine_7, 0.0F, -0.5236F, 0.0F);
		spine_7.cubeList.add(new ModelBox(spine_7, 455, 108, -0.8F, 6.6F, -57.4F, 3, 64, 9, 0.0F, false));
		spine_7.cubeList.add(new ModelBox(spine_7, 455, 169, -0.8F, 70.6F, -57.4F, 3, 48, 9, 0.0F, false));
		spine_7.cubeList.add(new ModelBox(spine_7, 455, 220, -0.8F, 118.6F, -57.4F, 3, 64, 9, 0.0F, false));

		upper_peak_4 = new RendererModel(this);
		upper_peak_4.setRotationPoint(0.0F, -1.0F, -46.0F);
		cap_4.addChild(upper_peak_4);
		setRotationAngle(upper_peak_4, 0.4363F, 0.0F, 0.0F);
		upper_peak_4.cubeList.add(new ModelBox(upper_peak_4, 368, 134, -23.0F, -1.0F, -2.0F, 48, 1, 11, 0.0F, false));
		upper_peak_4.cubeList.add(new ModelBox(upper_peak_4, 381, 117, -19.0F, -1.0F, 9.0F, 40, 1, 7, 0.0F, false));
		upper_peak_4.cubeList.add(new ModelBox(upper_peak_4, 379, 111, -15.5F, -1.0F, 16.0F, 33, 1, 7, 0.0F, false));
		upper_peak_4.cubeList.add(new ModelBox(upper_peak_4, 391, 103, -12.0F, -1.0F, 23.0F, 26, 1, 7, 0.0F, false));
		upper_peak_4.cubeList.add(new ModelBox(upper_peak_4, 400, 72, -8.0F, -1.0F, 30.0F, 18, 1, 7, 0.0F, false));
		upper_peak_4.cubeList.add(new ModelBox(upper_peak_4, 405, 91, -5.0F, -1.0F, 37.0F, 11, 1, 7, 0.0F, false));
		upper_peak_4.cubeList.add(new ModelBox(upper_peak_4, 286, 137, -3.0F, -2.25F, 41.0F, 7, 2, 3, 0.0F, false));

		base_4 = new RendererModel(this);
		base_4.setRotationPoint(-0.2F, 1.0F, 0.0F);
		frame4.addChild(base_4);
		

		foot_nub_4 = new RendererModel(this);
		foot_nub_4.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_4.addChild(foot_nub_4);
		setRotationAngle(foot_nub_4, 0.0F, -0.5236F, 0.0F);
		foot_nub_4.cubeList.add(new ModelBox(foot_nub_4, 455, 277, -2.8F, -11.4F, -58.4F, 7, 13, 10, 0.0F, false));
		foot_nub_4.cubeList.add(new ModelBox(foot_nub_4, 77, 124, -2.0F, 13.1F, -11.4F, 5, 7, 5, 0.0F, false));
		foot_nub_4.cubeList.add(new ModelBox(foot_nub_4, 80, 102, -1.0F, 14.1F, -6.4F, 3, 6, 4, 0.0F, false));

		head_8 = new RendererModel(this);
		head_8.setRotationPoint(1.2F, -0.4F, -58.4F);
		foot_nub_4.addChild(head_8);
		setRotationAngle(head_8, -1.9199F, 0.0F, 0.0F);
		head_8.cubeList.add(new ModelBox(head_8, 455, 228, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_8 = new RendererModel(this);
		spine_8.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_4.addChild(spine_8);
		setRotationAngle(spine_8, 0.0F, -0.5236F, 0.0F);
		

		lower_peak_4 = new RendererModel(this);
		lower_peak_4.setRotationPoint(0.0F, -1.0F, -46.0F);
		base_4.addChild(lower_peak_4);
		setRotationAngle(lower_peak_4, -0.4363F, 0.0F, 0.0F);
		lower_peak_4.cubeList.add(new ModelBox(lower_peak_4, 368, 270, -23.0F, -1.0F, -2.0F, 48, 1, 10, 0.0F, false));
		lower_peak_4.cubeList.add(new ModelBox(lower_peak_4, 380, 264, -20.0F, -1.0F, 8.0F, 41, 1, 7, 0.0F, false));
		lower_peak_4.cubeList.add(new ModelBox(lower_peak_4, 380, 258, -16.5F, -1.0F, 15.0F, 34, 1, 7, 0.0F, false));
		lower_peak_4.cubeList.add(new ModelBox(lower_peak_4, 380, 254, -13.0F, -1.0F, 22.0F, 27, 1, 7, 0.0F, false));
		lower_peak_4.cubeList.add(new ModelBox(lower_peak_4, 380, 252, -9.0F, -1.0F, 29.0F, 19, 1, 7, 0.0F, false));
		lower_peak_4.cubeList.add(new ModelBox(lower_peak_4, 380, 252, -5.0F, -1.0F, 36.0F, 11, 1, 7, 0.0F, false));
		lower_peak_4.cubeList.add(new ModelBox(lower_peak_4, 286, 137, -3.0F, -1.25F, 41.0F, 7, 2, 3, 0.0F, false));

		lamp_trim4 = new RendererModel(this);
		lamp_trim4.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame4.addChild(lamp_trim4);
		lamp_trim4.cubeList.add(new ModelBox(lamp_trim4, 295, 118, -3.0F, -215.25F, -7.4F, 7, 2, 4, 0.0F, false));
		lamp_trim4.cubeList.add(new ModelBox(lamp_trim4, 280, 97, -2.0F, 17.25F, -7.4F, 6, 2, 4, 0.0F, false));

		frame5 = new RendererModel(this);
		frame5.setRotationPoint(0.0F, 0.0F, 0.0F);
		frames.addChild(frame5);
		setRotationAngle(frame5, 0.0F, 2.0944F, 0.0F);
		

		cap_5 = new RendererModel(this);
		cap_5.setRotationPoint(-0.2F, -193.0F, 0.0F);
		frame5.addChild(cap_5);
		

		head_nub_5 = new RendererModel(this);
		head_nub_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_5.addChild(head_nub_5);
		setRotationAngle(head_nub_5, 0.0F, -0.5236F, 0.0F);
		head_nub_5.cubeList.add(new ModelBox(head_nub_5, 455, 155, -2.8F, -3.4F, -58.4F, 7, 10, 10, 0.0F, false));
		head_nub_5.cubeList.add(new ModelBox(head_nub_5, 77, 124, -2.0F, -22.9F, -11.4F, 5, 7, 5, 0.0F, false));
		head_nub_5.cubeList.add(new ModelBox(head_nub_5, 80, 102, -1.0F, -22.9F, -6.4F, 3, 6, 4, 0.0F, false));

		head_9 = new RendererModel(this);
		head_9.setRotationPoint(1.2F, -0.4F, -58.4F);
		head_nub_5.addChild(head_9);
		setRotationAngle(head_9, -1.2217F, 0.0F, 0.0F);
		head_9.cubeList.add(new ModelBox(head_9, 455, 105, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_9 = new RendererModel(this);
		spine_9.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_5.addChild(spine_9);
		setRotationAngle(spine_9, 0.0F, -0.5236F, 0.0F);
		spine_9.cubeList.add(new ModelBox(spine_9, 455, 108, -0.8F, 6.6F, -57.4F, 3, 64, 9, 0.0F, false));
		spine_9.cubeList.add(new ModelBox(spine_9, 455, 169, -0.8F, 70.6F, -57.4F, 3, 48, 9, 0.0F, false));
		spine_9.cubeList.add(new ModelBox(spine_9, 455, 220, -0.8F, 118.6F, -57.4F, 3, 64, 9, 0.0F, false));

		upper_peak_5 = new RendererModel(this);
		upper_peak_5.setRotationPoint(0.0F, -1.0F, -46.0F);
		cap_5.addChild(upper_peak_5);
		setRotationAngle(upper_peak_5, 0.4363F, 0.0F, 0.0F);
		upper_peak_5.cubeList.add(new ModelBox(upper_peak_5, 368, 134, -23.0F, -1.0F, -2.0F, 48, 1, 11, 0.0F, false));
		upper_peak_5.cubeList.add(new ModelBox(upper_peak_5, 381, 117, -19.0F, -1.0F, 9.0F, 40, 1, 7, 0.0F, false));
		upper_peak_5.cubeList.add(new ModelBox(upper_peak_5, 379, 111, -15.5F, -1.0F, 16.0F, 33, 1, 7, 0.0F, false));
		upper_peak_5.cubeList.add(new ModelBox(upper_peak_5, 391, 103, -12.0F, -1.0F, 23.0F, 26, 1, 7, 0.0F, false));
		upper_peak_5.cubeList.add(new ModelBox(upper_peak_5, 400, 72, -8.0F, -1.0F, 30.0F, 18, 1, 7, 0.0F, false));
		upper_peak_5.cubeList.add(new ModelBox(upper_peak_5, 405, 91, -5.0F, -1.0F, 37.0F, 11, 1, 7, 0.0F, false));
		upper_peak_5.cubeList.add(new ModelBox(upper_peak_5, 286, 137, -3.0F, -2.25F, 41.0F, 7, 2, 3, 0.0F, false));

		base_5 = new RendererModel(this);
		base_5.setRotationPoint(-0.2F, 1.0F, 0.0F);
		frame5.addChild(base_5);
		

		foot_nub_5 = new RendererModel(this);
		foot_nub_5.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_5.addChild(foot_nub_5);
		setRotationAngle(foot_nub_5, 0.0F, -0.5236F, 0.0F);
		foot_nub_5.cubeList.add(new ModelBox(foot_nub_5, 455, 277, -2.8F, -11.4F, -58.4F, 7, 13, 10, 0.0F, false));
		foot_nub_5.cubeList.add(new ModelBox(foot_nub_5, 77, 124, -2.0F, 13.1F, -11.4F, 5, 7, 5, 0.0F, false));
		foot_nub_5.cubeList.add(new ModelBox(foot_nub_5, 80, 102, -1.0F, 14.1F, -6.4F, 3, 6, 4, 0.0F, false));

		head_10 = new RendererModel(this);
		head_10.setRotationPoint(1.2F, -0.4F, -58.4F);
		foot_nub_5.addChild(head_10);
		setRotationAngle(head_10, -1.9199F, 0.0F, 0.0F);
		head_10.cubeList.add(new ModelBox(head_10, 455, 228, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_10 = new RendererModel(this);
		spine_10.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_5.addChild(spine_10);
		setRotationAngle(spine_10, 0.0F, -0.5236F, 0.0F);
		

		lower_peak_5 = new RendererModel(this);
		lower_peak_5.setRotationPoint(0.0F, -1.0F, -46.0F);
		base_5.addChild(lower_peak_5);
		setRotationAngle(lower_peak_5, -0.4363F, 0.0F, 0.0F);
		lower_peak_5.cubeList.add(new ModelBox(lower_peak_5, 368, 270, -23.0F, -1.0F, -2.0F, 48, 1, 10, 0.0F, false));
		lower_peak_5.cubeList.add(new ModelBox(lower_peak_5, 380, 264, -20.0F, -1.0F, 8.0F, 41, 1, 7, 0.0F, false));
		lower_peak_5.cubeList.add(new ModelBox(lower_peak_5, 380, 258, -16.5F, -1.0F, 15.0F, 34, 1, 7, 0.0F, false));
		lower_peak_5.cubeList.add(new ModelBox(lower_peak_5, 380, 254, -13.0F, -1.0F, 22.0F, 27, 1, 7, 0.0F, false));
		lower_peak_5.cubeList.add(new ModelBox(lower_peak_5, 380, 252, -9.0F, -1.0F, 29.0F, 19, 1, 7, 0.0F, false));
		lower_peak_5.cubeList.add(new ModelBox(lower_peak_5, 380, 252, -5.0F, -1.0F, 36.0F, 11, 1, 7, 0.0F, false));
		lower_peak_5.cubeList.add(new ModelBox(lower_peak_5, 286, 137, -3.0F, -1.25F, 41.0F, 7, 2, 3, 0.0F, false));

		lamp_trim5 = new RendererModel(this);
		lamp_trim5.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame5.addChild(lamp_trim5);
		lamp_trim5.cubeList.add(new ModelBox(lamp_trim5, 295, 118, -3.0F, -215.25F, -7.4F, 7, 2, 4, 0.0F, false));
		lamp_trim5.cubeList.add(new ModelBox(lamp_trim5, 280, 97, -2.0F, 17.25F, -7.4F, 6, 2, 4, 0.0F, false));

		frame6 = new RendererModel(this);
		frame6.setRotationPoint(0.0F, 0.0F, 0.0F);
		frames.addChild(frame6);
		setRotationAngle(frame6, 0.0F, 1.0472F, 0.0F);
		

		cap_6 = new RendererModel(this);
		cap_6.setRotationPoint(-0.2F, -193.0F, 0.0F);
		frame6.addChild(cap_6);
		

		head_nub_6 = new RendererModel(this);
		head_nub_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_6.addChild(head_nub_6);
		setRotationAngle(head_nub_6, 0.0F, -0.5236F, 0.0F);
		head_nub_6.cubeList.add(new ModelBox(head_nub_6, 455, 155, -2.8F, -3.4F, -58.4F, 7, 10, 10, 0.0F, false));
		head_nub_6.cubeList.add(new ModelBox(head_nub_6, 77, 124, -2.0F, -22.9F, -11.4F, 5, 7, 5, 0.0F, false));
		head_nub_6.cubeList.add(new ModelBox(head_nub_6, 80, 102, -1.0F, -22.9F, -6.4F, 3, 6, 4, 0.0F, false));

		head_11 = new RendererModel(this);
		head_11.setRotationPoint(1.2F, -0.4F, -58.4F);
		head_nub_6.addChild(head_11);
		setRotationAngle(head_11, -1.2217F, 0.0F, 0.0F);
		head_11.cubeList.add(new ModelBox(head_11, 455, 105, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_11 = new RendererModel(this);
		spine_11.setRotationPoint(0.0F, 0.0F, 0.0F);
		cap_6.addChild(spine_11);
		setRotationAngle(spine_11, 0.0F, -0.5236F, 0.0F);
		spine_11.cubeList.add(new ModelBox(spine_11, 455, 108, -0.8F, 6.6F, -57.4F, 3, 64, 9, 0.0F, false));
		spine_11.cubeList.add(new ModelBox(spine_11, 455, 169, -0.8F, 70.6F, -57.4F, 3, 48, 9, 0.0F, false));
		spine_11.cubeList.add(new ModelBox(spine_11, 455, 220, -0.8F, 118.6F, -57.4F, 3, 64, 9, 0.0F, false));

		upper_peak_6 = new RendererModel(this);
		upper_peak_6.setRotationPoint(0.0F, -1.0F, -46.0F);
		cap_6.addChild(upper_peak_6);
		setRotationAngle(upper_peak_6, 0.4363F, 0.0F, 0.0F);
		upper_peak_6.cubeList.add(new ModelBox(upper_peak_6, 368, 134, -23.0F, -1.0F, -2.0F, 48, 1, 11, 0.0F, false));
		upper_peak_6.cubeList.add(new ModelBox(upper_peak_6, 381, 117, -19.0F, -1.0F, 9.0F, 40, 1, 7, 0.0F, false));
		upper_peak_6.cubeList.add(new ModelBox(upper_peak_6, 379, 111, -15.5F, -1.0F, 16.0F, 33, 1, 7, 0.0F, false));
		upper_peak_6.cubeList.add(new ModelBox(upper_peak_6, 391, 103, -12.0F, -1.0F, 23.0F, 26, 1, 7, 0.0F, false));
		upper_peak_6.cubeList.add(new ModelBox(upper_peak_6, 400, 72, -8.0F, -1.0F, 30.0F, 18, 1, 7, 0.0F, false));
		upper_peak_6.cubeList.add(new ModelBox(upper_peak_6, 405, 91, -5.0F, -1.0F, 37.0F, 11, 1, 7, 0.0F, false));
		upper_peak_6.cubeList.add(new ModelBox(upper_peak_6, 286, 137, -3.0F, -2.25F, 41.0F, 7, 2, 3, 0.0F, false));

		base_6 = new RendererModel(this);
		base_6.setRotationPoint(-0.2F, 1.0F, 0.0F);
		frame6.addChild(base_6);
		

		foot_nub_6 = new RendererModel(this);
		foot_nub_6.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_6.addChild(foot_nub_6);
		setRotationAngle(foot_nub_6, 0.0F, -0.5236F, 0.0F);
		foot_nub_6.cubeList.add(new ModelBox(foot_nub_6, 455, 277, -2.8F, -11.4F, -58.4F, 7, 13, 10, 0.0F, false));
		foot_nub_6.cubeList.add(new ModelBox(foot_nub_6, 77, 124, -2.0F, 13.1F, -11.4F, 5, 7, 5, 0.0F, false));
		foot_nub_6.cubeList.add(new ModelBox(foot_nub_6, 80, 102, -1.0F, 14.1F, -6.4F, 3, 6, 4, 0.0F, false));

		head_12 = new RendererModel(this);
		head_12.setRotationPoint(1.2F, -0.4F, -58.4F);
		foot_nub_6.addChild(head_12);
		setRotationAngle(head_12, -1.9199F, 0.0F, 0.0F);
		head_12.cubeList.add(new ModelBox(head_12, 455, 228, -2.7F, -52.0F, -2.75F, 4, 51, 4, 0.0F, false));

		spine_12 = new RendererModel(this);
		spine_12.setRotationPoint(0.0F, 0.0F, 0.0F);
		base_6.addChild(spine_12);
		setRotationAngle(spine_12, 0.0F, -0.5236F, 0.0F);
		

		lower_peak_6 = new RendererModel(this);
		lower_peak_6.setRotationPoint(0.0F, -1.0F, -46.0F);
		base_6.addChild(lower_peak_6);
		setRotationAngle(lower_peak_6, -0.4363F, 0.0F, 0.0F);
		lower_peak_6.cubeList.add(new ModelBox(lower_peak_6, 368, 270, -23.0F, -1.0F, -2.0F, 48, 1, 10, 0.0F, false));
		lower_peak_6.cubeList.add(new ModelBox(lower_peak_6, 380, 264, -20.0F, -1.0F, 8.0F, 41, 1, 7, 0.0F, false));
		lower_peak_6.cubeList.add(new ModelBox(lower_peak_6, 380, 258, -16.5F, -1.0F, 15.0F, 34, 1, 7, 0.0F, false));
		lower_peak_6.cubeList.add(new ModelBox(lower_peak_6, 380, 254, -13.0F, -1.0F, 22.0F, 27, 1, 7, 0.0F, false));
		lower_peak_6.cubeList.add(new ModelBox(lower_peak_6, 380, 252, -9.0F, -1.0F, 29.0F, 19, 1, 7, 0.0F, false));
		lower_peak_6.cubeList.add(new ModelBox(lower_peak_6, 380, 252, -5.0F, -1.0F, 36.0F, 11, 1, 7, 0.0F, false));
		lower_peak_6.cubeList.add(new ModelBox(lower_peak_6, 286, 137, -3.0F, -1.25F, 41.0F, 7, 2, 3, 0.0F, false));

		lamp_trim6 = new RendererModel(this);
		lamp_trim6.setRotationPoint(0.0F, 0.0F, 0.0F);
		frame6.addChild(lamp_trim6);
		lamp_trim6.cubeList.add(new ModelBox(lamp_trim6, 295, 118, -3.0F, -215.25F, -7.4F, 7, 2, 4, 0.0F, false));
		lamp_trim6.cubeList.add(new ModelBox(lamp_trim6, 280, 97, -2.0F, 17.25F, -7.4F, 6, 2, 4, 0.0F, false));

		boti = new RendererModel(this);
		boti.setRotationPoint(0.0F, 24.0F, 0.0F);
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -65.0F, -30.0F, 52, 64, 1, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -141.0F, -30.0F, 52, 64, 1, 0.0F, false));
		boti.cubeList.add(new ModelBox(boti, 22, 263, -26.0F, -77.0F, -30.0F, 52, 12, 1, 0.0F, false));
	}

	public void render(ExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.scaled(0.25, 0.25, 0.25);
		
		this.door_east_rotate_y.rotateAngleY = (float)Math.toRadians(EnumDoorType.TT_2020_CAPSULE.getRotationForState(tile.getOpen()));
		this.door_west_rotate_y.rotateAngleY = -(float)Math.toRadians(EnumDoorType.TT_2020_CAPSULE.getRotationForState(tile.getOpen()));
		
		door.render(0.0625F);
		side_walls.render(0.0625F);
		center.render(0.0625F);
		door_jam.render(0.0625F);
		frames.render(0.0625F);
		boti.render(0.0625F);
		
		ModelHelper.renderPartBrightness(tile.getLightLevel(), glow);
		
		GlStateManager.popMatrix();
	}

	public void setRotationAngle(RendererModel modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}
}