package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ModernPoliceBoxExteriorModel;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.ModernPoliceBoxExteriorTile;

/**
 * Created by 50ap5ud5
 * on 18 Apr 2020 @ 12:55:35 pm
 */
public class ModernPoliceBoxExteriorRenderer extends ExteriorRenderer<ModernPoliceBoxExteriorTile>{
	
	//File path to exterior texture, 
	//happens to be same for interior door
	//the texture file name HAS to be the same as the exterior registry name
	
	private static BotiManager boti = new BotiManager();
	
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, 
			"textures/exteriors/modern_police_box_alt.png");
	
	private ModernPoliceBoxExteriorModel model = new ModernPoliceBoxExteriorModel();
	
	
	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}
	
	@Override
	public void renderExterior(ModernPoliceBoxExteriorTile tile) {
		GlStateManager.pushMatrix();
		/*We want to shift the render up in this case 
		so that when we place the exterior two blocks up from the ground,
		the base of the exterior will be rendered as standing on the ground
		*/
		GlStateManager.enableRescaleNormal(); //Ensures model isn't rendered fullbright all the time
		GlStateManager.translated(0, -1.15, 0); // Translation must be negative as models are loaded in upside down.
		GlStateManager.scalef(1.1f, 1.1f, 1.1f); //Scales the model down by 4
		
		this.bindTexture(TEXTURE);
		this.model.render(tile);
		GlStateManager.disableRescaleNormal();
		
		GlStateManager.popMatrix();
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(-0.55, -1.75, -11.5 / 16.0F);
		WorldText TEXT = new WorldText(1.1F, 0.125F, 0.015F, 0xFFFFFF);
		TEXT.renderMonitor(tile.getCustomName());
		GlStateManager.popMatrix();
		
	}

}
