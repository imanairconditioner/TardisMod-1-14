package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ExteriorTrunkModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.TrunkExteriorTile;

public class TrunkExteriorRenderer extends ExteriorRenderer<TrunkExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/trunk_base.png");
	public static final ExteriorTrunkModel MODEL = new ExteriorTrunkModel();
	
	public static final WorldText TEXT = new WorldText(0.3F, 0.2F, 0.25F, 0x000000);
	
	@Override
	public void renderExterior(TrunkExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -0.25, 0);
		GlStateManager.enableRescaleNormal();
		
		GlStateManager.pushMatrix();
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.rotated(-30, 0, 0, 1);
		GlStateManager.translated(-0.5, -0.025, -(8.05 / 16.0F));
		if(tile.getWorld() != null) {
			TEXT.renderMonitor(tile.getWorld().getBiome(tile.getPos()).getDisplayName().getFormattedText());
		}
		GlStateManager.popMatrix();
		
		GlStateManager.scaled(0.5, 0.5, 0.5);
		ResourceLocation texture = tile.getVariant() != null ? tile.getVariant().getTexture() : TEXTURE;
		Minecraft.getInstance().getTextureManager().bindTexture(texture);
		MODEL.render(tile, texture);
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
	}

}
