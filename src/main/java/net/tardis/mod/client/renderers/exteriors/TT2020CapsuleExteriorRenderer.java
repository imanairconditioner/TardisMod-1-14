package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.TT2020ExteriorModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.TT2020ExteriorTile;

public class TT2020CapsuleExteriorRenderer extends ExteriorRenderer<TT2020ExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/tt2020.png");
	public static final TT2020ExteriorModel MODEL = new TT2020ExteriorModel();
	public static final WorldText TEXT = new WorldText(1, 1, 1, TextFormatting.DARK_AQUA.getColor());
	
	@Override
	public void renderExterior(TT2020ExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -0.5, 0);
		GlStateManager.translated(0, Math.cos(Minecraft.getInstance().world.getGameTime() * 0.05) * 0.25, 0);
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.pushMatrix();
		
		//nameplate
		long time = Minecraft.getInstance().world.getGameTime();
		GlStateManager.rotated(time % 360.0, 0, 1, 0);
		GlStateManager.translated(-0.5, -1.5, -1.2);
		GlStateManager.scaled(1, (time % 120 < 20) ? Math.cos(time % 20 * 0.1) : 1, 1);
		TEXT.renderMonitor(tile.getCustomName());
		GlStateManager.popMatrix();
		
		GlStateManager.popMatrix();
	}
	
	@Override
	public boolean floatInAir() {
		return true;
	}

}
