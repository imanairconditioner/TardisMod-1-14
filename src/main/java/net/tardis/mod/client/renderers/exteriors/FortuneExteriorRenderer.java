package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.FortuneExteriorModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.FortuneExteriorTile;

public class FortuneExteriorRenderer extends ExteriorRenderer<FortuneExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/fortune.png");
	public static final FortuneExteriorModel MODEL = new FortuneExteriorModel();
	
	@Override
	public void renderExterior(FortuneExteriorTile tile) {
		GlStateManager.translated(0, 0.13, 0);
		if(tile.getVariant() != null)
			Minecraft.getInstance().getTextureManager().bindTexture(tile.getVariant().getTexture());
		else Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
		
		GlStateManager.pushMatrix();
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.translated(-5 / 16.0, -32 / 16.0, -7.6 / 16.0);
		WorldText TEXT = new WorldText(0.65F, 0.1F, 0.01F, 0xFFFFFF);
		TEXT.renderMonitor(tile.getCustomName());
		GlStateManager.popMatrix();
		
	}

}
