package net.tardis.mod.client.renderers.exteriors;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.state.properties.BlockStateProperties;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.tileentities.exteriors.DisguiseExteriorTile;

public class DisguiseExteriorTileRenderer extends ExteriorRenderer<DisguiseExteriorTile> {
	
	@SuppressWarnings("deprecation")
	@Override
	public void renderExterior(DisguiseExteriorTile tile) {}

	@SuppressWarnings("deprecation")
	@Override
	public void render(DisguiseExteriorTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(-TileEntityRendererDispatcher.staticPlayerX, -TileEntityRendererDispatcher.staticPlayerY, -TileEntityRendererDispatcher.staticPlayerZ);
		
		Disguise disguise = tile.disguise;
		if(disguise != null) {
			
			Minecraft.getInstance().textureManager.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
			BufferBuilder bb = Tessellator.getInstance().getBuffer();
			bb.begin(GL11.GL_QUADS, DefaultVertexFormats.BLOCK);
			
			RenderHelper.disableStandardItemLighting();

			
			Minecraft.getInstance().getBlockRendererDispatcher()
				.renderBlock(disguise.getTopState(), tile.getPos(),
						Minecraft.getInstance().world, bb, Minecraft.getInstance().world.rand);
			
			Minecraft.getInstance().getBlockRendererDispatcher()
			.renderBlock(disguise.getBottomState(), tile.getPos().down(),
					Minecraft.getInstance().world, bb, Minecraft.getInstance().world.rand);
			
			Tessellator.getInstance().draw();
			
			RenderHelper.enableStandardItemLighting();
			
		}
		
		GlStateManager.popMatrix();
		
		if(tile.getBlockState().isAir(Minecraft.getInstance().world, tile.getPos()))
			return;
		
		//Door
		GlStateManager.pushMatrix();
		GlStateManager.blendFunc(SourceFactor.DST_COLOR, DestFactor.ZERO);
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		GlStateManager.translated(x + 0.5125, y, z + 0.5);
		GlStateManager.rotated(360 - Helper.getAngleFromFacing(tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING)), 0, 1, 0);
		GlStateManager.disableTexture();
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		
		double width = tile.getOpen() == EnumDoorState.CLOSED ? 0 : (tile.getOpen() == EnumDoorState.ONE ? 0.25 : 0.4);
		
		bb.pos(-width, -1, -0.53).color(0F, 0, 0, 1).endVertex();
		bb.pos(-width, 1, -0.53).color(0F, 0, 0, 1).endVertex();
		bb.pos(width, 1, -0.53).color(0F, 0, 0, 1).endVertex();
		bb.pos(width, -1, -0.53).color(0F, 0, 0, 1).endVertex();
		
		Tessellator.getInstance().draw();
		GlStateManager.enableTexture();
		GlStateManager.popMatrix();
	}

}
