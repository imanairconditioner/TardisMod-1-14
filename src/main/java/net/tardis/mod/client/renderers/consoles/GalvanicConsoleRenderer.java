package net.tardis.mod.client.renderers.consoles;

import java.text.DecimalFormat;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.models.consoles.ConsoleGalvanicModel;
import net.tardis.mod.client.renderers.monitor.RenderScanner;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;

public class GalvanicConsoleRenderer extends TileEntityRenderer<GalvanicConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/galvanic.png");
	public static final ConsoleGalvanicModel MODEL = new ConsoleGalvanicModel();
	
	public static final DecimalFormat FORMAT = new DecimalFormat("###");
	public static final WorldText TEXT = new WorldText(0.3F, 0.22F, 0.0025F, 0x000000);
	
	
	@Override
	public void render(GalvanicConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.35, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		Minecraft.getInstance().getTextureManager().bindTexture(console.getVariant() != null ? console.getVariant().getTexture() : TEXTURE);
		MODEL.render(console);
		
		MonitorControl monitor = console.getControl(MonitorControl.class);
        if(monitor != null) {
            if (monitor.getMode() == MonitorMode.INFO) {
                GlStateManager.pushMatrix();
                GlStateManager.rotated(-60, 0, 1, 0);
                GlStateManager.rotated(-35, 1, 0, 0);
                GlStateManager.translated(-0.15, -0.357, -15.5 / 16.0);
                
                Minecraft.getInstance().gameRenderer.disableLightmap();
                WorldText TEXT = new WorldText(0.3F, 0.22F, 0.0025F, 0x000000);
                TEXT.renderMonitor(Helper.getConsoleText(console));
                Minecraft.getInstance().gameRenderer.enableLightmap();
                GlStateManager.popMatrix();
            }
        }
		
		GlStateManager.popMatrix();
		Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
	           if(console.getControl(MonitorControl.class).getMode() == MonitorMode.SCANNER) {
	               GlStateManager.pushMatrix();
	               GlStateManager.translated(x + 0.57, y + 0.42, z + 0.6);
	               GlStateManager.rotated(240, 0, 1, 0); //Rotate to be in line with monitor on y axis
	               GlStateManager.rotated(-35, 1, 0, 0); //Make it inline on x axis
	               GlStateManager.color3f(1, 1, 1);
	               RenderHelper.setupRenderLightning();
	               GlStateManager.rotated(180, 0, 1, 0);
	               RenderScanner.renderWorldToPlane(data, -0.11F, 0.005F, 0.21F, 0.25F, -1.02F, monitor.getView().getAngle(), partialTicks);
	               RenderHelper.finishRenderLightning();
	               GlStateManager.popMatrix();
	           }
	    });
	}

}
