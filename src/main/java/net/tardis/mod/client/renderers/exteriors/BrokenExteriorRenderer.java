package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class BrokenExteriorRenderer extends TileEntityRenderer<BrokenExteriorTile> {
	
	public BrokenExteriorRenderer() {}

	@Override
	public void render(BrokenExteriorTile te, double x, double y, double z, float partialTicks, int destroyStage) {
		
		GlStateManager.pushMatrix();
		
		ExteriorTile tile = te.getBrokenType().getExteriorTile(Minecraft.getInstance().world);
		if(tile != null && tile.getWorld() != null) {
			tile.setPos(te.getPos());
			TileEntityRendererDispatcher.instance.render(tile, x, y, z, partialTicks, destroyStage, true);
		}
		
		GlStateManager.popMatrix();
	}
}
