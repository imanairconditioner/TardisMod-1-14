package net.tardis.mod.client.renderers.tiles;

import net.minecraft.block.BlockState;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.tardis.mod.blocks.AntiGravBlock;
import net.tardis.mod.tileentities.AntiGravityTile;

/**
 * Created by Swirtzly
 * on 07/04/2020 @ 11:50
 */
public class AntiGravTileRenderer extends TileEntityRenderer<AntiGravityTile> {
    @Override
    public void render(AntiGravityTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
        BlockState state = getWorld().getBlockState(tileEntityIn.getPos());
        if (state.has(AntiGravBlock.ACTIVATED) && state != null) {
            super.render(tileEntityIn, x, y, z, partialTicks, destroyStage);
        }
    }
}