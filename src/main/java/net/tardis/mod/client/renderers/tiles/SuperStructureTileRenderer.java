package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.state.properties.StructureMode;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.tileentities.SuperStructureTile;


public class SuperStructureTileRenderer  extends TileEntityRenderer<SuperStructureTile>{
@Override 
public void render(SuperStructureTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
      if (Minecraft.getInstance().player.canUseCommandBlock() || Minecraft.getInstance().player.isSpectator()) {
         super.render(tileEntityIn, x, y, z, partialTicks, destroyStage);
         BlockPos blockpos = tileEntityIn.getPosition();
         BlockPos blockpos1 = tileEntityIn.getStructureSize();
         if (blockpos1.getX() >= 1 && blockpos1.getY() >= 1 && blockpos1.getZ() >= 1) {
            if (tileEntityIn.getMode() == StructureMode.SAVE || tileEntityIn.getMode() == StructureMode.LOAD) {
               double d0 = 0.01D;
               double d1 = (double)blockpos.getX();
               double d2 = (double)blockpos.getZ();
               double d6 = y + (double)blockpos.getY() - 0.01D;
               double d9 = d6 + (double)blockpos1.getY() + 0.02D;
               double d3;
               double d4;
               switch(tileEntityIn.getMirror()) {
               case LEFT_RIGHT:
                  d3 = (double)blockpos1.getX() + 0.02D;
                  d4 = -((double)blockpos1.getZ() + 0.02D);
                  break;
               case FRONT_BACK:
                  d3 = -((double)blockpos1.getX() + 0.02D);
                  d4 = (double)blockpos1.getZ() + 0.02D;
                  break;
               default:
                  d3 = (double)blockpos1.getX() + 0.02D;
                  d4 = (double)blockpos1.getZ() + 0.02D;
               }

               double d5;
               double d7;
               double d8;
               double d10;
               switch(tileEntityIn.getRotation()) {
               case CLOCKWISE_90:
                  d5 = x + (d4 < 0.0D ? d1 - 0.01D : d1 + 1.0D + 0.01D);
                  d7 = z + (d3 < 0.0D ? d2 + 1.0D + 0.01D : d2 - 0.01D);
                  d8 = d5 - d4;
                  d10 = d7 + d3;
                  break;
               case CLOCKWISE_180:
                  d5 = x + (d3 < 0.0D ? d1 - 0.01D : d1 + 1.0D + 0.01D);
                  d7 = z + (d4 < 0.0D ? d2 - 0.01D : d2 + 1.0D + 0.01D);
                  d8 = d5 - d3;
                  d10 = d7 - d4;
                  break;
               case COUNTERCLOCKWISE_90:
                  d5 = x + (d4 < 0.0D ? d1 + 1.0D + 0.01D : d1 - 0.01D);
                  d7 = z + (d3 < 0.0D ? d2 - 0.01D : d2 + 1.0D + 0.01D);
                  d8 = d5 + d4;
                  d10 = d7 - d3;
                  break;
               default:
                  d5 = x + (d3 < 0.0D ? d1 + 1.0D + 0.01D : d1 - 0.01D);
                  d7 = z + (d4 < 0.0D ? d2 + 1.0D + 0.01D : d2 - 0.01D);
                  d8 = d5 + d3;
                  d10 = d7 + d4;
               }

               int i = 255;
               int j = 223;
               int k = 127;
               Tessellator tessellator = Tessellator.getInstance();
               BufferBuilder bufferbuilder = tessellator.getBuffer();
               GlStateManager.disableFog();
               GlStateManager.disableLighting();
               GlStateManager.disableTexture();
               GlStateManager.enableBlend();
               GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
               this.setLightmapDisabled(true);
               if (tileEntityIn.getMode() == StructureMode.SAVE || tileEntityIn.showsBoundingBox()) {
                  this.renderBox(tessellator, bufferbuilder, d5, d6, d7, d8, d9, d10, 255, 223, 127);
               }

               if (tileEntityIn.getMode() == StructureMode.SAVE && tileEntityIn.showsAir()) {
                  this.renderInvisibleBlocks(tileEntityIn, x, y, z, blockpos, tessellator, bufferbuilder, true);
                  this.renderInvisibleBlocks(tileEntityIn, x, y, z, blockpos, tessellator, bufferbuilder, false);
               }

               this.setLightmapDisabled(false);
               GlStateManager.lineWidth(1.0F);
               GlStateManager.enableLighting();
               GlStateManager.enableTexture();
               GlStateManager.enableDepthTest();
               GlStateManager.depthMask(true);
               GlStateManager.enableFog();
            }
         }
      }
   }

   private void renderInvisibleBlocks(SuperStructureTile tile, double x, double y, double z, BlockPos pos, Tessellator tes, BufferBuilder buf, boolean shouldRender) {
      GlStateManager.lineWidth(shouldRender ? 3.0F : 1.0F);
      buf.begin(3, DefaultVertexFormats.POSITION_COLOR);
      IBlockReader iblockreader = tile.getWorld();
      BlockPos blockpos = tile.getPos();
      BlockPos blockpos1 = blockpos.add(pos);

      for(BlockPos blockpos2 : BlockPos.getAllInBoxMutable(blockpos1, blockpos1.add(tile.getStructureSize()).add(-1, -1, -1))) {
         BlockState blockstate = iblockreader.getBlockState(blockpos2);
         boolean flag = blockstate.isAir(tile.getWorld(), blockpos2);
         boolean flag1 = blockstate.getBlock() == Blocks.STRUCTURE_VOID;
         if (flag || flag1) {
            float f = flag ? 0.05F : 0.0F;
            double d0 = (double)((float)(blockpos2.getX() - blockpos.getX()) + 0.45F) + x - (double)f;
            double d1 = (double)((float)(blockpos2.getY() - blockpos.getY()) + 0.45F) + y - (double)f;
            double d2 = (double)((float)(blockpos2.getZ() - blockpos.getZ()) + 0.45F) + z - (double)f;
            double d3 = (double)((float)(blockpos2.getX() - blockpos.getX()) + 0.55F) + x + (double)f;
            double d4 = (double)((float)(blockpos2.getY() - blockpos.getY()) + 0.55F) + y + (double)f;
            double d5 = (double)((float)(blockpos2.getZ() - blockpos.getZ()) + 0.55F) + z + (double)f;
            if (shouldRender) {
               WorldRenderer.drawBoundingBox(buf, d0, d1, d2, d3, d4, d5, 0.0F, 0.0F, 0.0F, 1.0F);
            } else if (flag) {
               WorldRenderer.drawBoundingBox(buf, d0, d1, d2, d3, d4, d5, 0.5F, 0.5F, 1.0F, 1.0F);
            } else {
               WorldRenderer.drawBoundingBox(buf, d0, d1, d2, d3, d4, d5, 1.0F, 0.25F, 0.25F, 1.0F);
            }
         }
      }

      tes.draw();
   }
   
   private void renderBox(Tessellator tes, BufferBuilder buf, double startX, double startY, double startZ, double endX, double endY, double endZ, int red, int green, int blue) {
      GlStateManager.lineWidth(2.0F);
      buf.begin(3, DefaultVertexFormats.POSITION_COLOR);
      buf.pos(startX, startY, startZ).color((float)green, (float)green, (float)green, 0.0F).endVertex();
      buf.pos(startX, startY, startZ).color(green, green, green, red).endVertex();
      buf.pos(endX, startY, startZ).color(green, blue, blue, red).endVertex();
      buf.pos(endX, startY, endZ).color(green, green, green, red).endVertex();
      buf.pos(startX, startY, endZ).color(green, green, green, red).endVertex();
      buf.pos(startX, startY, startZ).color(blue, blue, green, red).endVertex();
      buf.pos(startX, endY, startZ).color(blue, green, blue, red).endVertex();
      buf.pos(endX, endY, startZ).color(green, green, green, red).endVertex();
      buf.pos(endX, endY, endZ).color(green, green, green, red).endVertex();
      buf.pos(startX, endY, endZ).color(green, green, green, red).endVertex();
      buf.pos(startX, endY, startZ).color(green, green, green, red).endVertex();
      buf.pos(startX, endY, endZ).color(green, green, green, red).endVertex();
      buf.pos(startX, startY, endZ).color(green, green, green, red).endVertex();
      buf.pos(endX, startY, endZ).color(green, green, green, red).endVertex();
      buf.pos(endX, endY, endZ).color(green, green, green, red).endVertex();
      buf.pos(endX, endY, startZ).color(green, green, green, red).endVertex();
      buf.pos(endX, startY, startZ).color(green, green, green, red).endVertex();
      buf.pos(endX, startY, startZ).color((float)green, (float)green, (float)green, 0.0F).endVertex();
      tes.draw();
      GlStateManager.lineWidth(1.0F);
   }
   @Override
   public boolean isGlobalRenderer(SuperStructureTile te) {
      return true;
   }
}
