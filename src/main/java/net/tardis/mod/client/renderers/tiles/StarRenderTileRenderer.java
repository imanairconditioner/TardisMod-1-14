package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.tileentities.HoloObservatoryTile;

public class StarRenderTileRenderer extends TileEntityRenderer<HoloObservatoryTile> {

	@Override
	public void render(HoloObservatoryTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		PlayerEntity player = Minecraft.getInstance().player;
		if(player.getPosition().withinDistance(tile.getPos(), 32)) {
			GlStateManager.pushMatrix();
			//GlStateManager.translated(tile.getPos().getX(), 0, tile.getPos().getZ());
			GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
			GlStateManager.clearCurrentColor();
			
			Minecraft.getInstance().gameRenderer.disableLightmap();
			
			WorldRenderer wr = tile.getWorldRenderer();
			
			ClientWorld old = Minecraft.getInstance().world;
			Minecraft.getInstance().world = (ClientWorld)tile.renderWorld;
			
			wr.renderSky(partialTicks);
			
			Minecraft.getInstance().world = old;
			
			
			Minecraft.getInstance().gameRenderer.enableLightmap();
			GlStateManager.popMatrix();
		}
		
	}

	@Override
	public boolean isGlobalRenderer(HoloObservatoryTile te) {
		return true;
	}

}
