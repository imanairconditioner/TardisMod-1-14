package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.state.properties.BlockStateProperties;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.PlaqueTile;

public class PlaqueRenderer extends TileEntityRenderer<PlaqueTile> {

	public static final WorldText TEXT = new WorldText(0.9F, 0.335F, 0.009F, 0xFFFFFF);
	
	@Override
	public void render(PlaqueTile plaque, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		GlStateManager.rotated(Helper.getAngleFromFacing(plaque.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING)), 0, 1, 0);
		GlStateManager.translated(-0.45, -0.62, 6.8 / 16.0);
		TEXT.renderMonitor(plaque.getText());
		GlStateManager.popMatrix();
	}

}
