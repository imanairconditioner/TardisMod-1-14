package net.tardis.mod.client.renderers.sky;

import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.IRenderHandler;
import net.tardis.mod.Tardis;

@OnlyIn(Dist.CLIENT)
public class MoonSkyRenderer implements IRenderHandler{

	public static MoonSkyRenderer INSTANCE = new MoonSkyRenderer();
	private static VertexFormat FORMAT = DefaultVertexFormats.POSITION_COLOR;
	private static VertexBuffer STAR_VBO;
	private static VertexBuffer PLANET_VBO;
	
	@Override
	public void render(int ticks, float partialTicks, ClientWorld world, Minecraft mc) {
		GlStateManager.pushMatrix();
		GlStateManager.disableFog();
		GlStateManager.disableTexture();
		GlStateManager.blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SourceFactor.ONE, GlStateManager.DestFactor.ZERO);
		RenderHelper.disableStandardItemLighting();
	    GlStateManager.depthMask(false);
		if(STAR_VBO == null) {
			STAR_VBO = new VertexBuffer(FORMAT);
			STAR_VBO.bindBuffer();
			BufferBuilder bb = Tessellator.getInstance().getBuffer();
			this.renderSky(bb);
			
			bb.finishDrawing();
			bb.reset();
			STAR_VBO.bufferData(bb.getByteBuffer());
			VertexBuffer.unbindBuffer();
		}
		else{
			STAR_VBO.bindBuffer();
			this.drawData();
			VertexBuffer.unbindBuffer();
		}
		
		GlStateManager.enableTexture();
		
		GlStateManager.pushMatrix();
		GlStateManager.rotated(world.getCelestialAngle(partialTicks) * 360.0, 1, 0, 0);
		
		Minecraft.getInstance().getTextureManager().bindTexture(new ResourceLocation(Tardis.MODID, "textures/sky/planets.png"));
		
		if(PLANET_VBO == null) {
			PLANET_VBO = new VertexBuffer(DefaultVertexFormats.POSITION_TEX);
			PLANET_VBO.bindBuffer();
			
			BufferBuilder bb = Tessellator.getInstance().getBuffer();
			bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_TEX);
			
			
			Planet earth = new Planet();
			earth.setHorizontalUVs(0, 0, 0.08984375, 0.08984375);
			earth.setVerticleUVs(0.1015625, 0, 0.19140625, 0.08984375);
			
			Planet sun = new Planet();
			sun.setHorizontalUVs(0, 0.1015625, 0.03125, 0.1328125);
			sun.setVerticleUVs(0, 0.1015625, 0.03125, 0.1328125);
			
			this.renderPlanet(bb, 45, -26, -40, 25, earth);
			this.renderPlanet(bb, 23, 100, 0, 10, sun);
			
			bb.finishDrawing();
			bb.reset();
			PLANET_VBO.bufferData(bb.getByteBuffer());
			VertexBuffer.unbindBuffer();
		}
		else {
			PLANET_VBO.bindBuffer();
			this.drawPlanetData();
			VertexBuffer.unbindBuffer();
		}
		
		GlStateManager.popMatrix();
		
		GlStateManager.depthMask(true);
		GlStateManager.enableFog();
		GlStateManager.popMatrix();
	}
	
	public void renderSky(BufferBuilder bb) {
		bb.begin(GL11.GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		Random rand = new Random(34534L);
		int skySize = 190;
		for(int i = 0; i < 1000; ++i){
			this.renderStarUp(bb, skySize - rand.nextFloat() * (skySize * 2), skySize, skySize - rand.nextFloat() * (skySize * 2));
		}
		
		for(int i = 0; i < 1000; ++i){
			this.renderStarSouth(bb, skySize - rand.nextFloat() * (skySize * 2), skySize - rand.nextFloat() * (skySize * 2), skySize);
		}
		
		for(int i = 0; i < 1000; ++i){
			this.renderStarWest(bb, -skySize, skySize - rand.nextFloat() * (skySize * 2), skySize - rand.nextFloat() * (skySize * 2));
		}
		
		for(int i = 0; i < 1000; ++i){
			this.renderStarNorth(bb, skySize - rand.nextFloat() * (skySize * 2), skySize - rand.nextFloat() * (skySize * 2), -skySize);
		}
		
		for(int i = 0; i < 1000; ++i){
			this.renderStarEast(bb, skySize, skySize - rand.nextFloat() * (skySize * 2), skySize - rand.nextFloat() * (skySize * 2));
		}
		
		for(int i = 0; i < 1000; ++i){
			this.renderStarDown(bb, skySize - rand.nextFloat() * (skySize * 2), -skySize, skySize - rand.nextFloat() * (skySize * 2));
		}
		
	}
	
	public void renderPlanet(BufferBuilder bb, float x, float y, float z, double size, Planet planet) {
		//NORTH
		bb.pos(x, y, z).tex(planet.maxHU, planet.minHV).endVertex();
		bb.pos(x, y + size, z).tex(planet.maxHU, planet.maxHV).endVertex();
		bb.pos(x - size, y + size, z).tex(planet.minHU, planet.maxHV).endVertex();
		bb.pos(x - size, y, z).tex(planet.minHU, planet.minHV).endVertex();
		
		//UP
		bb.pos(x - size, y + size, z - size).tex(planet.minVU, planet.minVV).endVertex();
		bb.pos(x - size, y + size, z).tex(planet.minVU, planet.maxVV).endVertex();
		bb.pos(x, y + size, z).tex(planet.maxVU, planet.maxVV).endVertex();
		bb.pos(x, y + size, z - size).tex(planet.maxVU, planet.minVV).endVertex();
		
		//East
		bb.pos(x, y, z - size).tex(planet.minHU, planet.minHV).endVertex();
		bb.pos(x, y + size, z - size).tex(planet.minHU, planet.maxHV).endVertex();
		bb.pos(x, y + size, z).tex(planet.maxHU, planet.maxHV).endVertex();
		bb.pos(x, y, z).tex(planet.maxHU, planet.minHV).endVertex();
		
		//West
		bb.pos(x - size, y, z).tex(planet.minHU, planet.minHV).endVertex();
		bb.pos(x - size, y + size, z).tex(planet.minHU, planet.maxHV).endVertex();
		bb.pos(x - size, y + size, z - size).tex(planet.maxHU, planet.maxHV).endVertex();
		bb.pos(x - size, y, z - size).tex(planet.maxHU, planet.minHV).endVertex();
		
		//SOUTH
		bb.pos(x - size, y, z - size).tex(planet.minHU, planet.minHV).endVertex();
		bb.pos(x - size, y + size, z - size).tex(planet.minHU, planet.maxHV).endVertex();
		bb.pos(x, y + size, z - size).tex(planet.maxHU, planet.maxHV).endVertex();
		bb.pos(x, y, z - size).tex(planet.maxHU, planet.minHV).endVertex();
		
		//Down
		bb.pos(x, y, z - size).tex(planet.maxVU, planet.minVV).endVertex();
		bb.pos(x, y, z).tex(planet.maxVU, planet.maxVV).endVertex();
		bb.pos(x - size, y, z).tex(planet.minVU, planet.maxVV).endVertex();
		bb.pos(x - size, y, z - size).tex(planet.minVU, planet.minVV).endVertex();
		
	}

	public void renderStarUp(BufferBuilder bb, float x, float y, float z){
		float size = 0.5F;
		bb.pos(x, y, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x + size, y, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x + size, y, z + size).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y, z + size).color(1F, 1, 1, 1).endVertex();
	}
	
	public void renderStarSouth(BufferBuilder bb, float x, float y, float z){
		float size = 0.5F;
		bb.pos(x, y, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y + size, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x + size, y + size, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x + size, y, z).color(1F, 1, 1, 1).endVertex();
	}
	
	public void renderStarWest(BufferBuilder bb, float x, float y, float z){
		float size = 0.5F;
		bb.pos(x, y, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y, z - size).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y + size, z - size).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y + size, z).color(1F, 1, 1, 1).endVertex();
	}
	
	public void renderStarNorth(BufferBuilder bb, float x, float y, float z){
		float size = 0.5F;
		bb.pos(x, y, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y + size, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x - size, y + size, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x - size, y, z).color(1F, 1, 1, 1).endVertex();
	}
	
	public void renderStarEast(BufferBuilder bb, float x, float y, float z){
		float size = 0.5F;
		bb.pos(x, y, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y + size, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y + size, z - size).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y, z - size).color(1F, 1, 1, 1).endVertex();
	}
	
	public void renderStarDown(BufferBuilder bb, float x, float y, float z){
		float size = 0.5F;
		bb.pos(x, y, z).color(1F, 1, 1, 1).endVertex();
		bb.pos(x, y, z + size).color(1F, 1, 1, 1).endVertex();
		bb.pos(x + size, y, z + size).color(1F, 1, 1, 1).endVertex();
		bb.pos(x + size, y, z).color(1F, 1, 1, 1).endVertex();
	}
	
	public void drawData() {
		GlStateManager.vertexPointer(3, GL11.GL_FLOAT, FORMAT.getSize(), 0);
		GlStateManager.colorPointer(4, GL11.GL_UNSIGNED_BYTE, FORMAT.getSize(), FORMAT.getColorOffset());
		
		GlStateManager.enableClientState(GL11.GL_VERTEX_ARRAY);
		GlStateManager.enableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		GlStateManager.enableClientState(GL11.GL_COLOR_ARRAY);
		STAR_VBO.drawArrays(GL11.GL_QUADS);
        GlStateManager.disableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.disableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.disableClientState(GL11.GL_COLOR_ARRAY);
	}
	
	public void drawPlanetData() {
		VertexFormat format = DefaultVertexFormats.POSITION_TEX;
		GlStateManager.vertexPointer(3, GL11.GL_FLOAT, format.getSize(), 0);
		GlStateManager.texCoordPointer(2, GL11.GL_FLOAT, format.getSize(), format.getUvOffsetById(0));
		
		GlStateManager.enableClientState(GL11.GL_VERTEX_ARRAY);
		GlStateManager.enableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		GlStateManager.enableClientState(GL11.GL_COLOR_ARRAY);
		PLANET_VBO.drawArrays(GL11.GL_QUADS);
        GlStateManager.disableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.disableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.disableClientState(GL11.GL_COLOR_ARRAY);
	}
}
