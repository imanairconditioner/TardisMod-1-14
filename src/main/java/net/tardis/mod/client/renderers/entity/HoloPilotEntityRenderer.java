package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.client.renderer.entity.model.PlayerModel;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.HoloPilotEntity;

public class HoloPilotEntityRenderer extends LivingRenderer<HoloPilotEntity, PlayerModel<HoloPilotEntity>>{

	public HoloPilotEntityRenderer(EntityRendererManager rendererManager) {
		super(rendererManager, new PlayerModel<HoloPilotEntity>(0.0625F, true), 0.3F);
	}

	@Override
	protected ResourceLocation getEntityTexture(HoloPilotEntity entity) {
		return new ResourceLocation("textures/entity/steve.png");
	}
	
	@Override
	public void doRender(HoloPilotEntity entity, double x, double y, double z, float entityYaw, float partialTicks) {
		GlStateManager.pushMatrix();
		GlStateManager.enableBlend();
		GlStateManager.color4f(0.7F, 0.7F, 1F, 0.5F);
		super.doRender(entity, x, y, z, entityYaw, partialTicks);
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
	}

}
