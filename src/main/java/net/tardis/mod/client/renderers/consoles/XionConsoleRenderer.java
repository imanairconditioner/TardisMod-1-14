package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.models.consoles.XionConsoleModel;
import net.tardis.mod.client.renderers.monitor.RenderScanner;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.ModelHelper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;

public class XionConsoleRenderer extends TileEntityRenderer<XionConsoleTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/consoles/xion.png");
	public static final XionConsoleModel MODEL = new XionConsoleModel();
	
	public static final WorldText TEXT = new WorldText(0.44F, 0.25F, 0.003F, 0xFFFFFF);
	
	@Override
	public void render(XionConsoleTile console, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 0.335, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1); //Rotate 180 degrees so model isn't upside down
		
		//Monitor text
		MonitorControl monitor = console.getControl(MonitorControl.class);
		if(monitor != null) {
		    if (monitor.getMode() == MonitorMode.INFO) {
    			GlStateManager.pushMatrix();
    			GlStateManager.rotated(-210, 0, 1, 0);
    			GlStateManager.rotated(5, 1, 0, 0);
    			GlStateManager.translated(-0.222, -1.235, -7.55 / 16.0);
    			TEXT.renderMonitor(Helper.getConsoleText(console));
    			GlStateManager.popMatrix();
		    }
		}
		
		if(console.getVariant() != null) //U
			Minecraft.getInstance().getTextureManager().bindTexture(console.getVariant().getTexture());
		else Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		
		double scale = 0.225;
		GlStateManager.scaled(scale, scale, scale); //Use a local variable to scale console easier
		GlStateManager.enableRescaleNormal();
		MODEL.render(console, ModelHelper.RENDER_SCALE); //Scale down by 4
		GlStateManager.disableRescaleNormal();
		GlStateManager.popMatrix();
		
		//Scanner
        Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
            if(console.getControl(MonitorControl.class).getMode() == MonitorMode.SCANNER) {
                GlStateManager.pushMatrix();
                GlStateManager.translated(x + 0.235, y + 1.35, z + 0.25);
                GlStateManager.rotated(30, 0, 1, 0); //Rotate to be in line with monitor on y axis
                GlStateManager.rotated(5, 1, 0, 0); //Make it inline on x axis
                GlStateManager.color3f(1, 1, 1);
                RenderHelper.setupRenderLightning();
                GlStateManager.rotated(180, 0, 1, 0);
                RenderScanner.renderWorldToPlane(data, -0.33F, 0.005F, 0.12F, 0.26F, -0.908F, monitor.getView().getAngle(), partialTicks);
                RenderHelper.finishRenderLightning();
                GlStateManager.popMatrix();
            }
        });
	}
}
