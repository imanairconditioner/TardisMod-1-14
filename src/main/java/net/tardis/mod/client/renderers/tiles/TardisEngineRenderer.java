package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.EngineModel;
import net.tardis.mod.tileentities.TardisEngineTile;

public class TardisEngineRenderer extends TileEntityRenderer<TardisEngineTile> {

	public static final EngineModel MODEL = new EngineModel();
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/tiles/engine.png");
	
	@Override
	public void render(TardisEngineTile tileEntityIn, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 1.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		this.bindTexture(TEXTURE);
		MODEL.render(tileEntityIn);
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
