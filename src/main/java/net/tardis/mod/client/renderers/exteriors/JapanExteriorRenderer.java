package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.exteriors.JapanExteriorModel;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.exteriors.JapanExteriorTile;

public class JapanExteriorRenderer extends ExteriorRenderer<JapanExteriorTile> {

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/exteriors/japan_box.png");
	public static final JapanExteriorModel MODEL = new JapanExteriorModel();
	
	@Override
	public void renderExterior(JapanExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.rotated(180, 0, 1, 0);
		GlStateManager.translated(0, -1, 0);
		this.bindTexture(TEXTURE);
		MODEL.render(tile);
		GlStateManager.popMatrix();
	}



}
