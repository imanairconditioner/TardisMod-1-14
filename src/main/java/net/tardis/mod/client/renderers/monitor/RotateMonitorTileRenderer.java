package net.tardis.mod.client.renderers.monitor;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.RotateMonitorModel;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class RotateMonitorTileRenderer extends TileEntityRenderer<RotateMonitorTile> {

	public static final RotateMonitorModel MODEL = new RotateMonitorModel();
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/tiles/rotate_monitor.png");
	
	
	@Override
	public void render(RotateMonitorTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(x + 0.5, y + 1.5, z + 0.5);
		GlStateManager.rotated(180, 0, 0, 1);
		this.bindTexture(TEXTURE);
		MODEL.render(tile, TEXTURE);
		GlStateManager.popMatrix();
	}

	@Override
	protected void bindTexture(ResourceLocation location) {
		Minecraft.getInstance().getTextureManager().bindTexture(location);
	}

}
