package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.RedExteriorModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.RedExteriorTile;

public class RedExteriorRenderer extends ExteriorRenderer<RedExteriorTile> {

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/red.png");
	public static RedExteriorModel MODEL = new RedExteriorModel();
	public static WorldText TEXT = new WorldText(0.87F, 0.125F, 0.015F, 0x000000);
	
	@Override
	public void renderExterior(RedExteriorTile tile) {
		GlStateManager.pushMatrix();
		GlStateManager.translated(0, -1, 0);
		Minecraft.getInstance().getTextureManager().bindTexture(TEXTURE);
		MODEL.render(tile);
		
		GlStateManager.pushMatrix();
		GlStateManager.translated(-7 / 16.0, -12.5 / 16.0, -(8.3 / 16.0));
		GlStateManager.rotated(0, 0, 0, 0);
		
		TEXT.renderMonitor(tile.getCustomName());
		
		GlStateManager.popMatrix();
		
		GlStateManager.popMatrix();
	}

}
