package net.tardis.mod.client.renderers.tiles;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.text.TextFormatting;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.monitor.RenderScanner;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.MonitorDynamicTile;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;

public class MonitorDynamicRenderer extends TileEntityRenderer<MonitorDynamicTile> {

	@Override
	public void render(MonitorDynamicTile tile, double x, double y, double z, float partialTicks, int destroyStage) {
		
		if(!tile.isCorener())
				return;
		
		if(tile.getMode() == MonitorMode.INFO) {
			TardisHelper.getConsoleInWorld(Minecraft.getInstance().world).ifPresent(console -> {
				GlStateManager.pushMatrix();
				GlStateManager.translated(x + 0.5, y + 1, z + 0.5);
				GlStateManager.rotated(360 - Helper.getAngleFromFacing(tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING)) - 180, 0, 1, 0);
				int width = tile.getWidth();
				GlStateManager.translated(-width + 0.5, 0, 0.51);
				GlStateManager.rotated(180, 1, 0, 0);
				WorldText text = new WorldText(width, tile.getHeight(), 1F, TextFormatting.WHITE.getColor());
				text.renderMonitor(Helper.getConsoleText(console));
				GlStateManager.popMatrix();
			});
		}
		else {
			Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
				GlStateManager.pushMatrix();
				GlStateManager.translated(x + 0.5, y, z + 0.5);
				GlStateManager.rotated(360 - Helper.getAngleFromFacing(tile.getBlockState().get(BlockStateProperties.HORIZONTAL_FACING)), 0, 1, 0);
				GlStateManager.translated(-0.5, 0, -0.51);
				RenderScanner.renderWorldToPlane(data, 0, -(tile.getHeight() - 1), tile.getWidth(), 1, 0, tile.getView().getAngle(), partialTicks);
				GlStateManager.popMatrix();
			});
		}
		
	}

	@Override
	public boolean isGlobalRenderer(MonitorDynamicTile te) {
		return te.isCorener();
	}

}
