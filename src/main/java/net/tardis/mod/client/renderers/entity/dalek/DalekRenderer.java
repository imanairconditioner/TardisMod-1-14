package net.tardis.mod.client.renderers.entity.dalek;

import java.util.HashMap;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.client.models.entity.dalek.ClassicDalekModel;
import net.tardis.mod.client.models.entity.dalek.Dalek2005Model;
import net.tardis.mod.client.models.entity.dalek.ImperialDalekModel;
import net.tardis.mod.client.models.entity.dalek.SWDalekModel;
import net.tardis.mod.client.models.entity.dalek.SupremeDalekModel;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.entity.DalekEntity;

public class DalekRenderer extends MobRenderer<DalekEntity, Dalek2005Model> {

	//TODO maybe this could be a part of the registry? I did it this way so I could work though
	private static HashMap<ResourceLocation, EntityModel<DalekEntity>> DALEK_MODELS = new HashMap<>();

	@Override
	public void doRender(DalekEntity dalek, double x, double y, double z, float entityYaw, float partialTicks) {
		super.doRender(dalek, x, y, z, entityYaw, partialTicks);
		if(dalek.isCommander())
			//TODO Fix "Dalek" also displaying above the head
			this.renderLivingLabel(dalek, "Commander", x, y+0.205, z, 64); //Temporary fix - It's a feature not a bug ;)
	}

	public DalekRenderer(EntityRendererManager render, Dalek2005Model model) {
		super(render, model, 1);
	}

	private static void registerModel(EntityModel<DalekEntity> model, ResourceLocation... location) {
		for (ResourceLocation resourceLocation : location) {
			DALEK_MODELS.put(resourceLocation, model);
		}
	}

	@Override
	protected boolean canRenderName(DalekEntity entity) {
		return entity.isCommander();
	}

	public static void registerAllModels() {
		registerModel(new Dalek2005Model(), Constants.DalekTypes.DALEK_TIMEWAR, Constants.DalekTypes.DALEK_SEC);
		registerModel(new ClassicDalekModel(), Constants.DalekTypes.DALEK_CLASSIC);
		registerModel(new SupremeDalekModel(), Constants.DalekTypes.DALEK_SUPREME);
		registerModel(new SWDalekModel(), Constants.DalekTypes.DALEK_SW);
		registerModel(new ImperialDalekModel(), Constants.DalekTypes.DALEK_IMPERIAL);
	}

	@Override
	protected ResourceLocation getEntityTexture(DalekEntity arg0) {
		return arg0.getDalekType().getTexture();
	}

	@Override
	protected void renderModel(DalekEntity dalek, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch, float scaleFactor) {
		boolean flag = this.isVisible(dalek);
		boolean flag1 = !flag && !dalek.isInvisibleToPlayer(Minecraft.getInstance().player);
		if (flag || flag1) {
			if (!this.bindEntityTexture(dalek)) {
				return;
			}
			if (flag1) {
				GlStateManager.setProfile(GlStateManager.Profile.TRANSPARENT_MODEL);
			}
			getModelToUse(dalek).render(dalek, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scaleFactor);
			if (flag1) {
				GlStateManager.unsetProfile(GlStateManager.Profile.TRANSPARENT_MODEL);
			}
		}
	}

	public EntityModel<DalekEntity> getModelToUse(DalekEntity dalek) {
		ResourceLocation location = dalek.getDalekType().getRegistryName();
		if (DALEK_MODELS.containsKey(location)) {
			return DALEK_MODELS.get(location);
		}
		return entityModel;
	}
}
