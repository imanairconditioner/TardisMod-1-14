package net.tardis.mod.client;

import static net.minecraft.client.renderer.WorldRenderer.drawShape;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.UUID;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.block.BlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.InputEvent;
import net.minecraftforge.client.event.InputEvent.MouseScrollEvent;
import net.minecraftforge.client.event.InputUpdateEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderWorldLastEvent;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.client.renderers.boti.BotiManager;
import net.tardis.mod.client.renderers.layers.SonicLaserRenderLayer;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.helper.RenderHelper;
import net.tardis.mod.items.DebugItem;
import net.tardis.mod.items.PlasmicShellItem;
import net.tardis.mod.items.SquarenessGunItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.items.TardisDiagnosticItem;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BessieHornMessage;
import net.tardis.mod.network.packets.SnapMessage;
import net.tardis.mod.subsystem.SubsystemInfo;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.tileentities.machines.TransductionBarrierTile;


@Mod.EventBusSubscriber(modid = Tardis.MODID, value = Dist.CLIENT)
public class ClientEvents {

    public static BotiManager draw;
    public static DecimalFormat FORMAT = new DecimalFormat("###");


    @SubscribeEvent
    public static void onBlockHighlight(DrawBlockHighlightEvent event) {
        ActiveRenderInfo information = event.getInfo();

        // Squareness gun
        ClientPlayerEntity player = Minecraft.getInstance().player;
        if (PlayerHelper.isInEitherHand(player, TItems.SQUARENESS_GUN)) {
            event.setCanceled(true);
            RayTraceResult result = PlayerHelper.getPosLookingAt(player, 5);
            if (result != null && result.getType() == RayTraceResult.Type.BLOCK) {
                BlockRayTraceResult blockRayTraceResult = (BlockRayTraceResult) result;
                BlockPos pos = blockRayTraceResult.getPos();
                ClientWorld world = Minecraft.getInstance().world;

                double correctedRenderPosX = (pos.getX() - TileEntityRendererDispatcher.staticPlayerX);
                double correctedRenderPosY = (pos.getY() - TileEntityRendererDispatcher.staticPlayerY);
                double correctedRenderPosZ = (pos.getZ() - TileEntityRendererDispatcher.staticPlayerZ);

                //Create a cube that we can just grow one in every direction
                AxisAlignedBB box = SquarenessGunItem.getSelection(pos, player.getHorizontalFacing());

                GlStateManager.pushMatrix();
                GlStateManager.translated(correctedRenderPosX, correctedRenderPosY, correctedRenderPosZ);
                for (int iteratedX = (int) box.minX; iteratedX < (int) box.maxX; iteratedX++) {
                    for (int iteratedY = (int) box.minY; iteratedY < (int) box.maxY; iteratedY++) {
                        for (int iteratedZ = (int) box.minZ; iteratedZ < (int) box.maxZ; iteratedZ++) {
                            if (!world.isAirBlock(pos.add(iteratedX, iteratedY, iteratedZ))) {
                                GlStateManager.pushMatrix();
                                GlStateManager.translatef(iteratedX, iteratedY, iteratedZ);
                                BlockState blockState = world.getBlockState(pos.add(iteratedX, iteratedY, iteratedZ));
                                VoxelShape collisionShape = blockState.getCollisionShape(world, pos);
                                if (!collisionShape.isEmpty()) {
                                    RenderHelper.renderAABB(collisionShape.getBoundingBox(), 0.2F, 0.6F, 0.8F, 0.9F);
                                }
                                GlStateManager.popMatrix();
                            }
                        }
                    }
                }
                GlStateManager.popMatrix();

                double projectedX = information.getProjectedView().x;
                double projectedY = information.getProjectedView().y;
                double projectedZ = information.getProjectedView().z;
                for (Iterator<BlockPos> iterator = BlockPos.getAllInBox(new BlockPos(box.maxX, box.maxY, box.maxZ), new BlockPos(box.minX, box.minY, box.minZ)).iterator(); iterator.hasNext(); ) {
                    BlockPos blockPos = iterator.next();
                    BlockState shape = player.world.getBlockState(blockPos);
                    drawShape(shape.getShape((ClientWorld) player.world, blockPos, ISelectionContext.forEntity(information.getRenderViewEntity())), (double) blockPos.getX() - projectedX, (double) blockPos.getY() - projectedY, (double) blockPos.getZ() - projectedZ, 0.0F, 0.0F, 1, 1F);
                }
            }

        }

    }


    @SubscribeEvent
    public static void onRenderWorldLast(RenderWorldLastEvent event) {
        //Sonic
        SonicLaserRenderLayer.onRenderWorldLast(event);

        ClientPlayerEntity player = Minecraft.getInstance().player;
        ItemStack main = player.getHeldItemMainhand();
        GlStateManager.pushMatrix();
        GlStateManager.depthMask(true);
        GlStateManager.enableBlend();
        GlStateManager.translated(-player.posX, -player.posY, -player.posZ);
        if (player != null && main.getItem() == TItems.DEBUG)
            RenderHelper.renderAABB(((DebugItem) TItems.DEBUG).box, 1, 0, 0, 0.5F);
        GlStateManager.disableBlend();
        GlStateManager.popMatrix();

        if (main.getItem() == TItems.PLASMIC_SHELL_GENERATOR) {
            GlStateManager.pushMatrix();
            GlStateManager.enableBlend();
            GlStateManager.translated(-TileEntityRendererDispatcher.staticPlayerX, -TileEntityRendererDispatcher.staticPlayerY, -TileEntityRendererDispatcher.staticPlayerZ);
            if (player.getHeldItemMainhand().getOrCreateTag().contains("pos1")) {

                BlockPos pos = BlockPos.fromLong(main.getOrCreateTag().getLong("pos1"));
                BlockPos pos1 = main.getOrCreateTag().contains("pos2") ? BlockPos.fromLong(main.getOrCreateTag().getLong("pos2")) : player.getPosition();

                boolean green = main.getOrCreateTag().contains("complete") && main.getOrCreateTag().getBoolean("complete");

                RenderHelper.renderAABB(PlasmicShellItem.getBox(pos, pos1).grow(0.1), !green ? 1 : 0, green ? 1 : 0, 0, 0.5F);
            }
            GlStateManager.disableBlend();
            GlStateManager.popMatrix();

        }

    }

    @SubscribeEvent
    public static void input(InputUpdateEvent event) {
        if (event.getMovementInput().jump) {
            Network.sendToServer(new BessieHornMessage());
        }
    }

    @SubscribeEvent
    public static void handleScroll(MouseScrollEvent event) {
        if (Minecraft.getInstance().player != null && Minecraft.getInstance().player.getHeldItemMainhand().getItem() == TItems.DEBUG) {
            event.setCanceled(true);
            if (Minecraft.getInstance().player.isSneaking()) {
                ((DebugItem) TItems.DEBUG).width = ((DebugItem) TItems.DEBUG).height += event.getScrollDelta() > 0 ? 0.0625 : -0.0625;
            } else ((DebugItem) TItems.DEBUG).offsetY += event.getScrollDelta() > 0 ? 0.0625 : -0.0625;
        }
    }

    @SubscribeEvent
    public static void onRenderOverlayPost(RenderGameOverlayEvent.Post event) {
        FontRenderer fr = Minecraft.getInstance().fontRenderer;
        int scaledWidth = Minecraft.getInstance().mainWindow.getScaledWidth();
        int scaledHeight = Minecraft.getInstance().mainWindow.getScaledHeight();
        PlayerEntity player = Tardis.proxy.getClientPlayer();
        if (player != null) {
            if (PlayerHelper.isInEitherHand(player, TItems.SONIC)) {
                ItemStack stack = PlayerHelper.getHeldStack(player, Hand.MAIN_HAND);
                stack.getCapability(Capabilities.SONIC_CAPABILITY).ifPresent(sonic -> {
                    if (sonic.getMode() == 0) { //If in Block Interaction Mode
                        RayTraceResult result = PlayerHelper.getPosLookingAt(player, 4);
                        if (result instanceof BlockRayTraceResult) {
                            BlockRayTraceResult btr = (BlockRayTraceResult) result;
                            TileEntity te = player.world.getTileEntity(btr.getPos());
                            if (te != null) {
                                te.getCapability(CapabilityEnergy.ENERGY).ifPresent(cap -> {
                                    int oldFE = sonic.getForgeEnergy();
                                    int maxStorage = cap.getMaxEnergyStored();
                                    int storage = cap.getEnergyStored();
                                    TranslationTextComponent energy = new TranslationTextComponent("message.tardis.energy_buffer", oldFE, maxStorage);
                                    if (oldFE != storage) {
                                        sonic.setForgeEnergy(storage); //Sets sonic's forge energy to new value. Sonic item will sync to client
                                    } else {
                                        GlStateManager.pushMatrix();
                                        fr.drawStringWithShadow(energy.getFormattedText(), scaledWidth / 2 - 60, scaledHeight / 2 - 45, 0xFFFFFF);
                                        GlStateManager.popMatrix();
                                    }
                                });
                                if (te instanceof TransductionBarrierTile) {
                                    TransductionBarrierTile barrier = (TransductionBarrierTile) te;
                                    String code = barrier.getCode();
                                    TranslationTextComponent codeText = new TranslationTextComponent("message.tardis.transduction_barrier.code", code.isEmpty() ? "null" : code);
                                    GlStateManager.pushMatrix();
                                    fr.drawStringWithShadow(codeText.getFormattedText(), scaledWidth / 2 - 60, scaledHeight / 2 - 55, 0xFFFFFF);
                                    GlStateManager.popMatrix();
                                }
                            }
                        }
                    }
                });
            }
            player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
                if (cap.getCountdown() > 0) {
                    String countDown = "" + (cap.getCountdown() / 20);
                    int countWidth = fr.getStringWidth(countDown);
                    TranslationTextComponent roomDeletionMessage = new TranslationTextComponent("ars.message.room.delete_countdown", countDown);
                    fr.drawStringWithShadow(roomDeletionMessage.getFormattedText(), scaledWidth / 2 - countWidth / 2 - 100, scaledHeight / 2 - 30, 0xFFFFFF);
                }
            });
        }

        if (event.getType() == ElementType.TEXT) {
            if (Helper.InEitherHand(player, stack -> stack.getItem() instanceof TardisDiagnosticItem)) {
                if (Minecraft.getInstance().objectMouseOver instanceof BlockRayTraceResult) {
                    TileEntity te = player.world.getTileEntity(((BlockRayTraceResult) Minecraft.getInstance().objectMouseOver).getPos());
                    if (te instanceof ExteriorTile) {
                        ExteriorTile ext = (ExteriorTile) te;
                        try {
                            UUID id = UUID.fromString(DimensionType.getKey(ext.getInterior()).getPath());
                            String owner = "Timeship Owner: " + ClientHelper.getUsernameFromUUID(id).getFormattedText();
                            fr.drawStringWithShadow(owner, scaledWidth / 2 - fr.getStringWidth(owner) / 2, 70, 0xFFFFFF);
                        } catch (Exception e) {
                        }
                    } else if (te instanceof ConsoleTile) {
                        player.getHeldItemMainhand().getCapability(Capabilities.DIAGNOSTIC).ifPresent(loc -> {
                            int i = 0;
                            int x = scaledWidth / 2 - 50;
                            int y = 0;
                            net.minecraft.client.renderer.RenderHelper.enableGUIStandardItemLighting();
                            TranslationTextComponent subsystem_title = new TranslationTextComponent("overlay.tardis.system_title");
                            fr.drawStringWithShadow(subsystem_title.getFormattedText(), x - 20, y + 10, 0xFFFFFF);
                            for (SubsystemInfo info : loc.getSubsystems()) {
                                y = (int) (fr.FONT_HEIGHT * i * 1.5F) + 20;
                                float percent = info.health * 100.0F;

                                TextFormatting color = TextFormatting.GREEN;

                                if (percent <= 100) {
                                    color = TextFormatting.DARK_GREEN;

                                    if (percent <= 75) {
                                        color = TextFormatting.GREEN;

                                        if (percent <= 50) {
                                            color = TextFormatting.YELLOW;

                                            if (percent <= 25)
                                                color = TextFormatting.DARK_RED;
                                        }
                                    }
                                }
                                StringTextComponent percentToString = new StringTextComponent(FORMAT.format(percent) + "%");
                                fr.drawStringWithShadow(new TranslationTextComponent(info.translationKey).getFormattedText() + ": " +
                                        percentToString.applyTextStyle(color).getFormattedText(), x, y, 0xFFFFFF);
                                Minecraft.getInstance().getItemRenderer()
                                        .renderItemAndEffectIntoGUI(new ItemStack(info.key), x - 16, y - 4);
                                ++i;
                            }
                            TranslationTextComponent tardisPower = new TranslationTextComponent("overlay.tardis.fe_power", loc.getPower());
                            fr.drawStringWithShadow(tardisPower.getFormattedText(), x, y + fr.FONT_HEIGHT + 10, 0xFFFFFF);
                            net.minecraft.client.renderer.RenderHelper.disableStandardItemLighting();
                        });
                    }
                }
            }
        }

    }

    @SubscribeEvent
    public static void onMove(InputUpdateEvent update) {
        LivingEntity liv = update.getEntityLiving();
        if (liv.dimension == TDimensions.SPACE_TYPE) {
            if (update.getMovementInput().sneak) {
                if (liv.getMotion().y > -0.1)
                    liv.setMotion(liv.getMotion().subtract(0, 0.1, 0));
            } else if (update.getMovementInput().jump) {
                if (liv.getMotion().y < 0.1)
                    liv.setMotion(update.getEntityLiving().getMotion().add(0, 0.1, 0));
            }
        }
    }

    @SubscribeEvent
    public static void onKey(InputEvent.KeyInputEvent event) {
        if (ModelRegistry.SNAP_KEY.isPressed())
            Network.sendToServer(new SnapMessage());
    }

}
