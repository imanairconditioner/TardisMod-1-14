package net.tardis.mod.sounds;

import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class SoundSchemeBase extends ForgeRegistryEntry<SoundSchemeBase> {

	public abstract void playFlightLoop(ConsoleTile console);
	public abstract void playInteriorTakeOff(ConsoleTile console);
	public abstract void playExteriorTakeOff(ConsoleTile console);
	public abstract void playInteriorLand(ConsoleTile console);
	public abstract void playExteriorLand(ConsoleTile console);
	abstract void playInteriorLandAfter(ConsoleTile console);
	
	//Use these in most cases
	public abstract void playTakeoffSounds(ConsoleTile console);
	public abstract void playLandSounds(ConsoleTile console);
	
	/*
	 * Should be zero only if you enjoy / by zero errors
	 */
	public abstract int getLoopTime();
	public abstract int getLandTime();
	public abstract int getTakeoffTime();
	
}
