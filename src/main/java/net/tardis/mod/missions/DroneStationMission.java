package net.tardis.mod.missions;

import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.MissionRegistry;

public class DroneStationMission extends KillMission{
	
	public DroneStationMission(BlockPos pos, int range) {
		super(MissionRegistry.STATION_DRONE, pos, range);
	}

	@Override
	public ItemStack getReward() {
		return new ItemStack(TItems.TELE_STRUCTURE_UPGRADE);
	}

	@Override
	public int getMaxStage() {
		return 15;
	}

	@Override
	public EntityType<?> getEntityType() {
		return TEntities.SECURITY_DROID;
	}

	@Override
	public float getProgressBarPercent() {
		return 0;
	}

	@Override
	public boolean shouldKillAdvance() {
		return this.getStage() > 0 && this.getStage() < 15;
	}

}
