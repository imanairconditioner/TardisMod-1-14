package net.tardis.mod.missions;

import net.minecraft.util.math.BlockPos;
import net.minecraftforge.registries.ForgeRegistryEntry;

public class MiniMissionType extends ForgeRegistryEntry<MiniMissionType>{

	private IMissionBuilder<MiniMission> supplier;
	
	public MiniMissionType(IMissionBuilder<MiniMission> supplier) {
		this.supplier = supplier;
	}
	
	public MiniMission create(BlockPos pos, int range) {
		return this.supplier.create(pos, range);
	}
	
	public static interface IMissionBuilder<T extends MiniMission>{
		T create(BlockPos pos, int range);
	}
}
