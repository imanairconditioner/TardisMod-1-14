package net.tardis.mod.missions;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.util.math.BlockPos;

public abstract class KillMission extends MiniMission{

	public KillMission(MiniMissionType type, BlockPos pos, int range) {
		super(type, pos, range);
	}
	
	public void onKill(Entity entity) {
		if(!entity.world.isRemote && entity.getType() == this.getEntityType()) {
			if(this.shouldKillAdvance()) {
				this.advanceStage();
			}
		}
	}
	
	public abstract EntityType<?> getEntityType();
	public abstract boolean shouldKillAdvance();

}
