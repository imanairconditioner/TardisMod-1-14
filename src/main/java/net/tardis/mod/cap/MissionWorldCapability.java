package net.tardis.mod.cap;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.registries.TardisForgeRegistries;

public class MissionWorldCapability implements IMissionCap{

	private List<MiniMission> missions = Lists.newArrayList();

	@Nullable
	@Override
	public MiniMission getMissionForPos(BlockPos pos) {
		for(MiniMission mission : missions) {
			if(mission.getPos().withinDistance(pos, mission.getRange()))
				return mission;
		}
		return null;
	}
	
	@Override
	public void addMission(MiniMission mission) {
		this.missions.add(mission);
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		
		ListNBT list = new ListNBT();
		for(MiniMission mis : this.missions) {
			CompoundNBT misTag = mis.serializeNBT();
			misTag.putString("registry_name", mis.getType().getRegistryName().toString());
			list.add(misTag);
		}
		tag.put("missions", list);
		
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		
		this.missions.clear();
		ListNBT list = tag.getList("missions", NBT.TAG_COMPOUND);
		for(INBT nbt : list) {
			CompoundNBT comp = (CompoundNBT)nbt;
			ResourceLocation key = new ResourceLocation(comp.getString("registry_name"));
			MiniMissionType missionType = TardisForgeRegistries.MISSIONS.getValue(key);
			if(missionType != null) {
				MiniMission mission = missionType.create(BlockPos.ZERO, 0);
				mission.deserializeNBT(comp);
				this.missions.add(mission);
			}
		}
		
	}

	@Override
	public void tick(World world) {
		final ServerWorld serv = (ServerWorld)world;
		for(MiniMission m : this.missions) {
			for(ServerPlayerEntity player : serv.getPlayers()) {
				//If it's tracking this player
				if(m.getTrackingPlayers().contains(player)) {
					//If player out of range or mission complete
					if(m.isComplete() || !m.isInsideArea(player))
						m.removeTrackingPlayer(player);
				}
				//If not tracking
				else {
					//If in range and not complete
					if(!m.isComplete() && m.isInsideArea(player)) {
						m.addTrackingPlayer(player);
					}
				}
			}
		}
	}
	
}
