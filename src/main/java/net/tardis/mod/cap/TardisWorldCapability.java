package net.tardis.mod.cap;

import java.util.List;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.IntNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.items.ItemStackHandler;
import net.tardis.mod.artron.IArtronBattery;
import net.tardis.mod.boti.BlockStore;
import net.tardis.mod.boti.EntityStorage;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.energy.TardisEnergy;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.BOTIMessage;
import net.tardis.mod.network.packets.WorldShellEntityMessage;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public class TardisWorldCapability implements ITardisWorldData{

	public static final int SHELL_RADIUS = 10;
	public static final AxisAlignedBB ENTITY_BOTI_BOX = new AxisAlignedBB(-SHELL_RADIUS, -SHELL_RADIUS, -SHELL_RADIUS, SHELL_RADIUS, SHELL_RADIUS, SHELL_RADIUS);
	private World world;
	
	public PanelInventory northInv;
	public PanelInventory eastInv;
	public PanelInventory southInv;
	public PanelInventory westInv;
	
	private TardisEnergy power;
	private ItemStackHandler itemBuffer;
	
	private WorldShell shell;
	
	public TardisWorldCapability(World world) {
		this.northInv = new PanelInventory(Direction.NORTH);
        this.eastInv = new PanelInventory(Direction.EAST);
        this.southInv = new PanelInventory(Direction.SOUTH);
        this.westInv = new PanelInventory(Direction.WEST);
        this.power = new TardisEnergy(2147483646);
        this.itemBuffer = new ItemStackHandler(9);
		this.world = world;
	}
	
	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.put("north_inv", northInv.serializeNBT());
		tag.put("east_inv", eastInv.serializeNBT());
		tag.put("south_inv", southInv.serializeNBT());
		tag.put("west_inv", westInv.serializeNBT());
		tag.put("power", this.power.serialize());
		tag.put("item_buffer", this.itemBuffer.serializeNBT());
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		northInv.deserializeNBT(tag.getList("north_inv", Constants.NBT.TAG_COMPOUND));
		eastInv.deserializeNBT(tag.getList("east_inv", Constants.NBT.TAG_COMPOUND));
		southInv.deserializeNBT(tag.getList("south_inv", Constants.NBT.TAG_COMPOUND));
		westInv.deserializeNBT(tag.getList("west_inv", Constants.NBT.TAG_COMPOUND));
		power.deserialize((IntNBT)tag.get("power"));
		this.itemBuffer.deserializeNBT(tag.getCompound("item_buffer"));
	}

	@Override
	public PanelInventory getEngineInventoryForSide(Direction dir) {
		switch(dir) {
			case EAST: return eastInv;
			case SOUTH: return southInv;
			case WEST: return westInv;
			default: return northInv;
		}
	}
	
	@Override
	public void tick() {
		
		//Artron charging
		if(!world.isRemote && world.getGameTime() % 200 == 0) {
			for(int index = 0; index < eastInv.getSizeInventory(); ++index) {
				ItemStack slotStack = eastInv.getStackInSlot(index);
				if(slotStack.getItem() instanceof IArtronBattery) {
					((IArtronBattery)slotStack.getItem())
						.charge(slotStack, 10);
				}
			}
		}
		
		if(!world.isRemote) {
			if(world.getGameTime() % 20 == 0)
				this.buildBoti();
			if(!world.isRemote)
				this.updateBotiEntities();
			else if(this.getBotiWorld() != null)
				this.getBotiWorld().tick(true);
			
		}
	}

	@Override
	public TardisEnergy getEnergy() {
		return this.power;
	}

	@Override
	public ItemStackHandler getItemBuffer() {
		return this.itemBuffer;
	}

	@Override
	public WorldShell getBotiWorld() {
		return this.shell;
	}

	@Override
	public void setBotiWorld(WorldShell shell) {
		this.shell = shell;
		System.out.println("Set shell");
	}
	
	public void buildBoti() {
		
		TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
			
			WorldShell shell = new WorldShell(tile.getLocation(), tile.getDimension());
			
			if(this.shell == null || !this.shell.getOffset().equals(shell.getOffset()))
				this.setBotiWorld(shell);
			
			ServerWorld world = this.world.getServer().getWorld(tile.getDimension());
			
			for(int x = -SHELL_RADIUS; x <= SHELL_RADIUS; ++x) {
				for(int y = -SHELL_RADIUS; y <= SHELL_RADIUS; ++y) {
					for(int z = -SHELL_RADIUS; z <= SHELL_RADIUS; ++z) {
						BlockPos newP = tile.getLocation().add(x, y, z);
						
						if(world.getBlockState(newP).isAir())
							continue;
						
						//if closed in, won't be seen
						/*boolean add = false;
						for(Direction dir : Direction.values()) {
							if(!world.getBlockState(newP.offset(dir)).isSolid()) {
								add = true;
								break;
							}
						}
						
						if(!add)
							continue;*/
						
						BlockStore store = new BlockStore(world.getBlockState(newP), world.getLightValue(newP), world.getTileEntity(newP));
						shell.put(newP, store);
					}
				}
			}
			
			Network.sendToAllInWorld(new BOTIMessage(shell), (ServerWorld)this.world);
			
			this.shell.combine(shell);
			
		});
		
	}
	
	public void updateBotiEntities() {
		TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
			ServerWorld sw = world.getServer().getWorld(tile.getDimension());
			
			List<EntityStorage> entities = Lists.newArrayList();
			for(Entity e : sw.getEntitiesWithinAABB(Entity.class, ENTITY_BOTI_BOX.offset(tile.getLocation()))) {
				entities.add(new EntityStorage(e));
			}
			
			Network.sendToAllInWorld(new WorldShellEntityMessage(entities), (ServerWorld)world);
		});
	}

}
