package net.tardis.mod.dimensions;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.entity.EntityClassification;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biome.SpawnListEntry;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.GenerationStage.Carving;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.IFeatureConfig;
import net.minecraft.world.gen.feature.structure.Structure;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.dimensions.TardisChunkGenerator.GeneratorSettingsTardis;

public class TardisChunkGenerator extends ChunkGenerator<GeneratorSettingsTardis>{
	
	public TardisChunkGenerator(IWorld p_i49954_1_, BiomeProvider p_i49954_2_, GeneratorSettingsTardis p_i49954_3_) {
		super(p_i49954_1_, p_i49954_2_, p_i49954_3_);
	}

	@Override
	public void carve(IChunk chunkIn, Carving carvingSettings) {}

	@Override
	public BlockPos findNearestStructure(World worldIn, String name, BlockPos pos, int radius, boolean p_211403_5_) {
		return BlockPos.ZERO;
	}

	@Override
	public void decorate(WorldGenRegion region) {}

	@Override
	public void spawnMobs(WorldGenRegion region) {}

	@Override
	public void spawnMobs(ServerWorld worldIn, boolean spawnHostileMobs, boolean spawnPeacefulMobs) {}

	@Override
	public boolean hasStructure(Biome biomeIn, Structure<? extends IFeatureConfig> structureIn) {
		return false;
	}

	@Override
	public int getGroundHeight() {
		return 128;
	}

	@Override
	public int func_222529_a(int x, int z, Type type) {
		return 0;
	}

	@Override
	public void makeBase(IWorld worldIn, IChunk chunk) {}

	@Override
	public void generateSurface(IChunk chunkIn) {}
	
	

	@Override
	public List<SpawnListEntry> getPossibleCreatures(EntityClassification creatureType, BlockPos pos) {
		return new ArrayList<SpawnListEntry>();
	}

	public static class GeneratorSettingsTardis extends GenerationSettings{
		
	}
}
