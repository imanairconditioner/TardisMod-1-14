package net.tardis.mod.dimensions;

import net.minecraft.world.IWorld;
import net.minecraft.world.biome.provider.BiomeProvider;
import net.minecraft.world.chunk.IChunk;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.Heightmap.Type;

public class SpaceChunkGenerator extends ChunkGenerator<SpaceGenerationSettings> {

	public SpaceChunkGenerator(IWorld worldIn, BiomeProvider biomeProviderIn, SpaceGenerationSettings generationSettingsIn) {
		super(worldIn, biomeProviderIn, generationSettingsIn);
	}

	@Override
	public void generateSurface(IChunk chunkIn) {}

	@Override
	public int getGroundHeight() {
		return 0;
	}

	@Override
	public void makeBase(IWorld worldIn, IChunk chunkIn) {
		
	}

	@Override
	public int func_222529_a(int p_222529_1_, int p_222529_2_, Type heightmapType) {
		return 0;
	}

}
