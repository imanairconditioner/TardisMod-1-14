package net.tardis.mod.flight;

import java.util.List;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.tileentities.ConsoleTile;

public class ResidualArtronEvent extends FlightEvent{

	public ResidualArtronEvent(List<ResourceLocation> controls) {
		super(controls);
	}

	@Override
	public boolean onComplete(ConsoleTile tile) {
		boolean success = tile.getFlightEvent() == null || tile.getFlightEvent().getControls().isEmpty();
		if(success) {
			tile.setArtron(tile.getArtron() + (float)((10 + ConsoleTile.rand.nextDouble() * 10)));
			tile.getEmotionHandler().addLoyaltyIfOwner(1, tile.getPilot());
		}
		return success;
	}
}
