package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.registries.FlightEventRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

public class TardisCollideInstagate extends FlightEvent{

	private ConsoleTile other;
	
	public static final Supplier<ArrayList<ResourceLocation>> CONTROLS = () -> Lists.newArrayList(
			ControlRegistry.COMMUNICATOR.getRegistryName(),
			ControlRegistry.THROTTLE.getRegistryName(),
			ControlRegistry.RANDOM.getRegistryName()
	);
	
	public TardisCollideInstagate(ArrayList<ResourceLocation> list) {
		super(list);
	}

	@Override
	public boolean onComplete(ConsoleTile tile) {
		boolean complete = super.onComplete(tile);
		
		if(complete && other != null) {
			other.setFlightEvent(((TardisCollideRecieve)FlightEventRegistry.COLLIDE_RECIEVE.create(other)).setOtherTARDIS(tile));
			other.updateClient();
		}
		
		return complete;
	}
	
	public TardisCollideInstagate setOtherTARDIS(ConsoleTile tile) {
		this.other = tile;
		return this;
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		
		if(other != null)
			other.damage(40F);
		
		tile.damage(150F);
		tile.crash();
		tile.updateClient();
	}
	
	@Override
	public int calcTime(ConsoleTile console) {
		return console.flightTicks + 200;
	}
}
