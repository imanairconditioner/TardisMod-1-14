package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.util.ResourceLocation;
import net.minecraft.world.dimension.DimensionType;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;

public class DimensionFlightEvent extends FlightEvent{
	
	public DimensionFlightEvent(ArrayList<ResourceLocation> controls) {
		super(controls);
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		
		List<DimensionType> types = Helper.getAllValidDimensionTypes();
		tile.setDestination(types.get(ConsoleTile.rand.nextInt(types.size())), tile.getDestination());
		
	}

}
