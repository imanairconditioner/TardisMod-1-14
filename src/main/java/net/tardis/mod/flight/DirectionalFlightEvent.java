package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.Random;

import net.minecraft.util.Direction.Axis;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.tileentities.ConsoleTile;

public class DirectionalFlightEvent extends FlightEvent{

	private Axis axis;
	
	public DirectionalFlightEvent(Axis axis, ArrayList<ResourceLocation> sequence) {
		super(sequence.toArray(new ResourceLocation[0]));
		this.axis = axis;
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		super.onMiss(tile);
		Random rand = tile.getWorld().rand;
		if(axis == Axis.X)
			tile.setDestination(tile.getDestinationDimension(), tile.getDestination().add(-10 + rand.nextInt(20), 0, 0));
		else if(axis == Axis.Y)
			tile.setDestination(tile.getDestinationDimension(), tile.getDestination().add(0, -10 + rand.nextInt(20), 0));
		else if(axis == Axis.Z)
			tile.setDestination(tile.getDestinationDimension(), tile.getDestination().add(0, 0, -10 + rand.nextInt(20)));
	}
}
