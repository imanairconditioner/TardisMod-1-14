package net.tardis.mod.flight;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.util.ResourceLocation;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.tileentities.ConsoleTile;

public class CrashEvent extends FlightEvent{

	public static final Supplier<ArrayList<ResourceLocation>> CONTROLS = () -> Lists.newArrayList(
			ControlRegistry.THROTTLE.getRegistryName(),
			ControlRegistry.RANDOM.getRegistryName(),
			ControlRegistry.THROTTLE.getRegistryName()
	);
	
	public CrashEvent(List<ResourceLocation> controls) {
		super(controls);
	}
	
	@Override
	public boolean onComplete(ConsoleTile tile) {
		// TODO Auto-generated method stub
		return super.onComplete(tile);
	}

	@Override
	public void onMiss(ConsoleTile tile) {
		// TODO Auto-generated method stub
		super.onMiss(tile);
	}

	@Override
	public int calcTime(ConsoleTile console) {
		return super.calcTime(console);
	}

}
