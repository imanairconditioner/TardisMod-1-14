package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Iterator;
import java.util.function.Function;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.state.IProperty;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;

public class TardisBlockstateGen implements IDataProvider {

	private final DataGenerator generator;
	private HashMap<IProperty<?>, Function<Block, JsonObject>> serializers = Maps.newHashMap();
	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	public TardisBlockstateGen(DataGenerator generator) {
		this.generator = generator;
		
		this.serializers.put(BlockStateProperties.HORIZONTAL_FACING, block -> {
			ResourceLocation key = block.getRegistryName();
			
			JsonObject variants = new JsonObject();
			Iterator<Direction> it = Direction.Plane.HORIZONTAL.iterator();
			while(it.hasNext()) {
				Direction dir = it.next();
				
				JsonObject dirObj = new JsonObject();
				
				dirObj.add("model", new JsonPrimitive(key.getNamespace() + ":block/" + key.getPath()));
				dirObj.add("y", new JsonPrimitive((int)Helper.getAngleFromFacing(dir)));
				
				variants.add("facing=" + dir.getName2(), dirObj);
				
			}
			return variants;
		});
		
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		
		final Path path = this.generator.getOutputFolder();
		
		for(Block block : ForgeRegistries.BLOCKS) {
			if(block.getRegistryName().getNamespace().equals(Tardis.MODID)) {
				
				JsonObject root = this.createBlockState(block);
				
				IDataProvider.save(GSON, cache, root, getPath(path, block));
				
			}
		}
		
	}
	
	public JsonObject createBlockState(Block block) {
		
		ResourceLocation key = block.getRegistryName();
		
		JsonObject root = new JsonObject();
		
		boolean hasSpecificSerializer = false;
		
		for(IProperty<?> prop : this.serializers.keySet()) {
			for(IProperty<?> otherProp : block.getStateContainer().getProperties()) {
				if(prop == otherProp) {
					
					root.add("variants", this.serializers.get(prop).apply(block));
					
					hasSpecificSerializer = true;
					break;
				}
			}
		}
		
		//Default state
		if(!hasSpecificSerializer) {
			JsonObject obj = new JsonObject();
			
			JsonObject blank = new JsonObject();
			blank.add("model", new JsonPrimitive(key.getNamespace() + ":block/" + key.getPath()));
			obj.add("", blank);
			
			
			root.add("variants", obj);
		}
		
		return root;
	}
	
	public static Path getPath(Path path, Block block) {
		ResourceLocation key = block.getRegistryName();
		return path.resolve("assets/" + key.getNamespace() + "/blockstates/" + key.getPath() + ".json");
	}

	@Override
	public String getName() {
		return "TARDIS Mod BlockState generator";
	}

}
