package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.items.TItems;

/**
 * Generate recipes and repair recipes for Quantiscope
 */
public class QuantiscopeRecipeGen implements IDataProvider{
    private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
    private final DataGenerator generator;
    
    public QuantiscopeRecipeGen(DataGenerator generator) {
        this.generator = generator;
    }
    
    @Override
    public void act(DirectoryCache cache) throws IOException {

        final Path path = this.generator.getOutputFolder();
        createRepairRecipe(path, cache, TItems.KEY_FOB_UPGRADE, TItems.CIRCUITS, Items.ENDER_PEARL, Items.GOLD_INGOT);
    }

    @Override
    public String getName() {
        return "TARDIS Quantiscope Recipe Generator";
    }
    
    public static Path getPath(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/quantiscope/" + key.getPath() + ".json");
    }
    
    public static Path getPathRepair(Path base, Item output) {
        ResourceLocation key = output.getRegistryName();
        return base.resolve("data/" + key.getNamespace() + "/quantiscope/" + key.getPath() + "_repair" + ".json");
    }

    /**
     * Create a non-repair recipe. Make sure it is a unique recipe
     * @param output
     * @param ingredients - items that are used to craft the output item. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createNormalRecipe(Path base, DirectoryCache cache, Item output, Item... ingredients) throws IOException {
        IDataProvider.save(GSON, cache, this.createRecipe(output, ingredients), getPath(base, output));
    }
    
    /**
     * Create a repair recipe. Make sure it is a unique recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - items that are used to repair itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @throws IOException 
     */
    public void createRepairRecipe(Path base, DirectoryCache cache, Item itemToRepair, Item... repairItems) throws IOException {
        IDataProvider.save(GSON, cache, this.createRepairRecipe(itemToRepair, repairItems), getPathRepair(base, itemToRepair));
    }
    
    /**
     * Creates a non-repair recipe. Make sure it is a unique recipe
     * @param output - this is the item that is output at the end
     * @param ingredients - the items that are used to craft output. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRecipe(Item output, Item... ingredients) {
        JsonObject root = new JsonObject();
        
        JsonArray ingred = new JsonArray();
        List<Item> ingredientList = Arrays.asList(ingredients);
        root.addProperty("repair", false);
        ingredientList.forEach(ingredient -> {
            ingred.add(ingredient.getRegistryName().toString());
        });
        root.add("ingredients", ingred);
        JsonObject result = new JsonObject();
        result.addProperty("item", output.getRegistryName().toString());
        root.add("result", result);
        
        return root;
    }
    
    /**
     * Creates a repair recipe
     * @param itemToRepair - this is the item that needs repairing
     * @param repairItems - the items that are used to repair the itemToRepair. <br>Maximum of 5 items. <br>This can be in any order because the Quantiscope only checks if items are present, it doesn't care about the order of the items.
     * @return {@link JsonObject}
     */
    public JsonObject createRepairRecipe(Item itemToRepair, Item... repairItems) {
        JsonObject root = new JsonObject();
        
        JsonArray repairItemArray = new JsonArray();
        List<Item> repairList = Arrays.asList(repairItems);
        
        root.addProperty("repair", true);
        root.addProperty("repair_item", itemToRepair.getRegistryName().toString());
        repairList.forEach(repairItem -> {
            repairItemArray.add(repairItem.getRegistryName().toString());
        });
        root.add("ingredients", repairItemArray);
        
        return root;
    }

}
