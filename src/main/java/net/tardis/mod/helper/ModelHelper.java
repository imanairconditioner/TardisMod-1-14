package net.tardis.mod.helper;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.renderer.entity.model.RendererModel;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ModelHelper {

    public static float RENDER_SCALE = 0.0625F;

	public static void renderPartBrightness(float bright, RendererModel... parts) {
		GlStateManager.pushMatrix();
		float coord = 240.0F * bright;
		RenderHelper.setLightmapTextureCoords(coord, coord);
		for(RendererModel part : parts) {
			part.render(0.0625F);
		}
		GlStateManager.popMatrix();
	}

	public static void renderPartTransparent(float alpha, RendererModel... parts) {
		GlStateManager.pushMatrix();
		GlStateManager.enableAlphaTest();
		GlStateManager.enableBlend();
		GlStateManager.color4f(1.0F, 1.0F, 1.0F, alpha);
		for(RendererModel part : parts) {
			part.render(0.0625F);
		}
		GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1F);
		GlStateManager.disableAlphaTest();
		GlStateManager.disableBlend();
		GlStateManager.popMatrix();
	}
	
	public static void copyAngle(RendererModel parent, RendererModel child) {
		child.rotateAngleX = parent.rotateAngleX;
		child.rotateAngleY = parent.rotateAngleY;
		child.rotateAngleZ = parent.rotateAngleZ;
	}

}
