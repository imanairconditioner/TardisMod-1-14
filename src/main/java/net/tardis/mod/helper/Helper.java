package net.tardis.mod.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.advancements.Advancement;
import net.minecraft.block.BlockState;
import net.minecraft.block.IWaterLoggable;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.item.minecart.MinecartEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.entity.projectile.FishingBobberEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.play.server.SPlayEntityEffectPacket;
import net.minecraft.network.play.server.SPlayerAbilitiesPacket;
import net.minecraft.network.play.server.SServerDifficultyPacket;
import net.minecraft.network.play.server.SSetExperiencePacket;
import net.minecraft.potion.Effect;
import net.minecraft.potion.EffectInstance;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.DamageSource;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.server.ServerWorld;
import net.minecraft.world.server.TicketType;
import net.minecraft.world.storage.WorldInfo;
import net.minecraft.world.storage.loot.LootContext;
import net.minecraft.world.storage.loot.LootParameterSets;
import net.minecraft.world.storage.loot.LootParameters;
import net.minecraft.world.storage.loot.LootTable;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.constants.Constants.Translations;
import net.tardis.mod.containers.BaseContainer;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.experimental.advancement.TTriggers;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.tileentities.ConsoleTile;

public class Helper {

    private static Random rand = new Random();

	public static String formatBlockPos(BlockPos pos) {
		return pos.getX() + ", " + pos.getY() + ", " + pos.getZ();
	}
	/** Formats a BlockPos for use in quick Copy Pasting*/
	public static String formatBlockPosDebug(BlockPos pos) {
        return pos.getX() + " " + pos.getY() + " " + pos.getZ();
    }

	public static String formatDimName(DimensionType dim) {
		if(dim == null || DimensionType.getKey(dim) == null)
			return "UNKNOWN";

		String original = DimensionType.getKey(dim).getPath().trim().replace("	", "").replace("_", " ");
		String output = Arrays.stream(original.split("\\s+"))
				.map(t -> t.substring(0,1).toUpperCase() + t.substring(1))
				.collect(Collectors.joining(" "));

		return output;
	}
	
	public static float getAngleFromFacing(Direction dir) {
		if(dir == Direction.NORTH)
			return 0F;
		else if(dir == Direction.EAST)
			return 90;
		else if(dir == Direction.SOUTH)
			return 180;
		return 270F;
	}
	
	public static float getRotationDifference(float one, float two) {
		if(one > two)
			return one - two;
		return two - one;
	}
	
	/**
	 * Used to determine if an object is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */

	public static boolean canTravelToDimension(DimensionType dimension) {
		ResourceLocation key = DimensionType.getKey(dimension);
		if(key == null)
			return false;
		List<? extends String> blacklist = TConfig.COMMON.blacklistedDims.get();
		for(String s : blacklist) {
			if(key.toString().contentEquals(s))
				return false;
		}
        return dimension.getModType() != TDimensions.TARDIS &&
        		dimension.getModType() != TDimensions.VORTEX;
	}
	
	/**
	 * Used to determine if the VM is able to travel to the target dimension
	 * @implNote Usage: Config defined values
	 * @implSpec Will require a config option to blacklist dimensions
	 * @param dimension
	 * @return Boolean value
	 */
	public static boolean canVMTravelToDimension(DimensionType dimension) {
		ResourceLocation key = DimensionType.getKey(dimension);
		if(key == null)
			return false;
		if (TConfig.COMMON.toggleVMWhitelistDims.get()) { //If using whitelist
			return TConfig.COMMON.whitelistedVMDims.get().stream().allMatch(dimName ->
		         key.toString().contentEquals(dimName) && canVMBeUsedInDim(dimension));
		} //uses anyMatch to allow for VM to not be able be used in ANY player's tardis dimension, because every player's tardis name contains uuid which is different
		else if (!TConfig.COMMON.toggleVMWhitelistDims.get()){ //If using blacklist
			return TConfig.COMMON.blacklistedVMDims.get().stream().allMatch(dimName ->
		         !key.toString().contentEquals(dimName) && canVMBeUsedInDim(dimension));
		}
		return canVMBeUsedInDim(dimension);
	}
	/**
	 * Determines if VM can be opened/used in a particular dimension. More lightweight method than the aboveW
	 * @param dimension
	 * @return true if can be used, false if cannot be used
	 */
	public static boolean canVMBeUsedInDim(DimensionType dimension) {
		return dimension.getModType() != TDimensions.TARDIS && dimension.getModType() != TDimensions.VORTEX;
	}
	
	/**
	 * A more performance efficient version of the above to check if an object can be used in the current dimension
	 * @implNote Used for items/blocks that can only be used in the Tardis dimension
	 * @param dimension
	 * @return true if blocked, false if not blocked
	 */
	public static boolean isDimensionBlocked(DimensionType dimension) {
		return dimension.getModType() == TDimensions.TARDIS;
	}

	public static boolean isInBounds(int testX, int testY, int x, int y, int u, int v) {
		return (testX > x &&
				testX < u &&
				testY > y &&
				testY < v);
	}

	public static ResourceLocation getKeyFromDimType(DimensionType dimension) {
		ResourceLocation rl = DimensionType.getKey(dimension);
		return rl != null ? rl : new ResourceLocation("overworld");
	}
	
	/**
	 * Checks if Tardis owner is online in the dedicated server
	 * @param world
	 * @param type
	 * @return
	 */
	public static boolean isOwnerOn(World world, DimensionType type) {
		if(!world.isRemote) {
			return world.getServer().getPlayerList().getPlayerByUUID(getPlayerFromTARDIS(type)) != null;
		}
		return false;
	}
	
	public static UUID getPlayerFromTARDIS(DimensionType type) {
		ResourceLocation loc = DimensionType.getKey(type);
		return UUID.fromString(loc.getPath());
	}
	
	public static void teleportEntities(Entity e, ServerWorld world, double x, double y, double z, float yaw, float pitch) {
		if(e instanceof ServerPlayerEntity) {
			ServerPlayerEntity player = (ServerPlayerEntity)e;
			ChunkPos chunkpos = new ChunkPos(new BlockPos(x, y, z));
			world.getChunkProvider().registerTicket(TicketType.POST_TELEPORT, chunkpos, 1, player.getEntityId());
			if(player.world == world) {
				player.connection.setPlayerLocation(x, y, z, yaw, pitch);
			}
			if (player.isSleeping()) {
				player.wakeUpPlayer(true, true, false);
			}
			else {
				player.teleport(world, x, y, z, yaw, pitch);
				//Handle player active potion effects
				for(EffectInstance effectinstance : player.getActivePotionEffects()) {
					player.connection.sendPacket(new SPlayEntityEffectPacket(player.getEntityId(), effectinstance));
		         }
				//Handle player experience not getting received on client after interdimensional teleport
				WorldInfo worldinfo = player.world.getWorldInfo();
				player.connection.sendPacket(new SSetExperiencePacket(player.experience, player.experienceTotal, player.experienceLevel));
				player.connection.sendPacket(new SPlayerAbilitiesPacket(player.abilities));//Keep abilities like creative mode flight height etc.
                player.connection.sendPacket(new SServerDifficultyPacket(worldinfo.getDifficulty(), worldinfo.isDifficultyLocked()));
			}
		}
		else {
			//In in same dimension
			if(e.world == world) {
				e.setPosition(x, y, z);
				e.rotationYaw = yaw;
				e.rotationPitch = pitch;
			}
			else {
				//If interdimensional
				DimensionType targetDim = world.getDimension().getType();
				e.dimension = targetDim;
				if (e != null && e.getType() != ForgeRegistries.ENTITIES.getValue(EntityType.ENDER_DRAGON.getRegistryName())) { //Prevent Ender dragon from teleporting inside the interior and destroying it
					Entity old = e;
					final ServerWorld sourceWorld = old.getServer().getWorld(old.world.getDimension().getType());
					old.detach(); //Dismounts from entities and dismounts other entities riding the entity
					Entity newE = e.getType().create(world);
					if (old instanceof FishingBobberEntity) { //Explicit handling of fishing rod because it doesn't have an entity type, hence it will cause an NPE
						FishingBobberEntity bobber = (FishingBobberEntity)old;
						ServerPlayerEntity player = (ServerPlayerEntity)bobber.getAngler();
						newE = new FishingBobberEntity(world, player, x, y, z); //This means bobber didn't "catch" anything and player's fishing rod won't lose durability
					}
					//Handle Minecarts
					if (old.isAlive() && old instanceof MinecartEntity) {
						old.remove(true); //equivalent of old.removed = true;
						old.changeDimension(targetDim);
						old.revive(); //equivalent of old.removed = false;
			        }
					if(old.world instanceof ServerWorld) {
						sourceWorld.removeEntity(old);
					}
					if (newE != null) { //Sanity check
						newE.copyDataFromOld(old);
						newE.setLocationAndAngles(x, y, z, yaw, pitch);
						world.func_217460_e(newE);
						final boolean flag = newE.forceSpawn;
				        newE.forceSpawn = true;
				        world.addEntity(newE);
				        newE.forceSpawn = flag;
				        world.updateEntity(newE);
					}
					else {
						System.err.println("Error teleporting entity: " + old);
						System.err.println("Entity that was teleported: " + newE);
					}
				}
			}
			//Handle Elytra flying
			if (!(e instanceof LivingEntity) || !((LivingEntity)e).isElytraFlying()) {
		        e.setMotion(e.getMotion().mul(1.0D, 0.0D, 1.0D));
		        e.onGround = true;
		    }
		}
	}


    public static void dropEntityLoot(Entity target, PlayerEntity attacker) {
    	LivingEntity targeted = (LivingEntity) target;
    	ResourceLocation resourcelocation = targeted.getLootTableResourceLocation();
        LootTable loot_table = target.world.getServer().getLootTableManager().getLootTableFromLocation(resourcelocation);
        LootContext.Builder lootcontext$builder = getLootContextBuilder(true, DamageSource.GENERIC, targeted, attacker);
        LootContext ctx = lootcontext$builder.build(LootParameterSets.ENTITY);
        loot_table.generate(ctx).forEach(target::entityDropItem);
    }

    public static LootContext.Builder getLootContextBuilder(boolean p_213363_1_, DamageSource damageSourceIn, LivingEntity entity, PlayerEntity attacker) {
        LootContext.Builder lootcontext$builder = (new LootContext.Builder((ServerWorld) entity.world)).withRandom(rand).withParameter(LootParameters.THIS_ENTITY, entity).withParameter(LootParameters.POSITION, new BlockPos(entity)).withParameter(LootParameters.DAMAGE_SOURCE, damageSourceIn).withNullableParameter(LootParameters.KILLER_ENTITY, damageSourceIn.getTrueSource()).withNullableParameter(LootParameters.DIRECT_KILLER_ENTITY, damageSourceIn.getImmediateSource());
         if (p_213363_1_ && entity.getAttackingEntity() != null) {
        	 attacker = (PlayerEntity) entity.getAttackingEntity();
            lootcontext$builder = lootcontext$builder.withParameter(LootParameters.LAST_DAMAGE_PLAYER, attacker).withLuck(attacker.getLuck());
         }
         return lootcontext$builder;
    }
    
    public static void doIfAdvancementPresent(String name, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(new ResourceLocation(Tardis.MODID, name));
    	if(adv != null)
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    }
    /**
     * Run if a specified advancement has been obtained (Works for other mods or Vanilla MC)
     * @param name
     * @param ent
     * @param run
     */
    public static void doIfAdvancementPresentOther(ResourceLocation loc, ServerPlayerEntity ent, Runnable run) {
    	Advancement adv = ent.getServer().getAdvancementManager().getAdvancement(loc);
    	if(adv != null)
    		if(ent.getAdvancements().getProgress(adv).isDone()) {
    			run.run();
    		}
    }

	public static boolean unlockInterior(ConsoleTile console, @Nullable ServerPlayerEntity player, ConsoleRoom room) {
		if(!console.getUnlockManager().getUnlockedConsoleRooms().contains(room)) {
			console.getUnlockManager().addConsoleRoom(room);
			if(player != null) {
				TTriggers.INTERIOR_UNLOCK.trigger(player);
				player.sendStatusMessage(new TranslationTextComponent(Translations.UNLOCKED_INTERIOR, room.getDisplayName()), true);
			}
			return true;
		}
		return false;
	}
	
	public static boolean unlockExterior(ConsoleTile console, @Nullable ServerPlayerEntity player, IExterior ext) {
		if(!console.getUnlockManager().getUnlockedExteriors().contains(ext)) {
			console.getUnlockManager().addExterior(ext);
			if(player != null) {
				TTriggers.EXTERIOR_UNLOCK.trigger(player);
				player.sendStatusMessage(new TranslationTextComponent("status.tardis.exterior.unlock", ext.getDisplayName().getFormattedText()), true);
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Converts from Direction to Rotation, assuming the default facing is north
	 * @param dir
	 * @return
	 */
	public static Rotation getRotationFromDirection(Direction dir) {
		switch(dir) {
		case NORTH: return Rotation.NONE;
		case EAST: return Rotation.CLOCKWISE_90;
		case SOUTH: return Rotation.CLOCKWISE_180;
		case WEST: return Rotation.COUNTERCLOCKWISE_90;
		default: return Rotation.NONE;
		}
	}
	
	/**
	 * Only supports Horrizontal directions, rotates around 0, 0, 0 
	 * @param pos - The position to rotate
	 * @param dir - The direction to rotate it, assumes facing north by default
	 * @return - rotated block pos
	 */
	public static BlockPos rotateBlockPos(BlockPos pos, Direction dir) {
		switch(dir) {
			case NORTH: return pos;
			case EAST: return new BlockPos(-pos.getZ(), pos.getY(), pos.getX());
			case SOUTH: return new BlockPos(-pos.getX(), pos.getY(), -pos.getZ());
			case WEST: return new BlockPos(pos.getZ(), pos.getY(), -pos.getX());
			default: return pos;
		}
	}
	
	public static Effect getEffectFromRL(ResourceLocation loc) {
		return ForgeRegistries.POTIONS.getValue(loc);
	}
	
	public static ResourceLocation getKeyFromEffect(Effect effect) {
		return ForgeRegistries.POTIONS.getKey(effect);
	}

	public static void addPlayerInvContainer(BaseContainer container, PlayerInventory player, int x, int y) {
		
		//Player Main
		for(int i = 0; i < player.mainInventory.size() - 9; ++i) {
			container.addSlot(new Slot(player, i + 9, x + 8 + (i % 9) * 18, 86 + y + (i / 9) * 18));
		}
		
		//hotbar
		for(int i = 0; i < 9; ++i) {
			container.addSlot(new Slot(player, i, 8 + x + (i * 18), y + 144));
		}
	}
	/** Drops Inventory of an IItemHandler Container*/
	public static void dropInventoryItems(World worldIn, BlockPos pos, IItemHandler inventory)
    {
        for (int i = 0; i < inventory.getSlots(); ++i)
        {
            ItemStack itemstack = inventory.getStackInSlot(i);

            if (itemstack.getCount() > 0)
            {
                InventoryHelper.spawnItemStack(worldIn, (double) pos.getX(), (double) pos.getY(), (double) pos.getZ(), itemstack);
            }
        }
    }

	public static List<DimensionType> getAllValidDimensionTypes() {
		
		List<DimensionType> dims = new ArrayList<DimensionType>();
		
		for(DimensionType type : DimensionType.getAll()) {
			if(Helper.canTravelToDimension(type))
				dims.add(type);
		}
		
		return dims;
	}
	
	public static BlockPos validateBlockPos(BlockPos pos, int maxHeight) {
		if(pos.getY() < 0)
			return new BlockPos(pos.getX(), 0, pos.getZ());
		if(pos.getY() > maxHeight)
			return new BlockPos(pos.getX(), maxHeight, pos.getZ());
		return pos;
	}

	public static boolean InEitherHand(LivingEntity entity, Predicate<ItemStack> stack) {
		return stack.test(entity.getHeldItemMainhand()) || stack.test(entity.getHeldItemOffhand());
	}
	
	public static void setFluidStateIfNot(IFluidState state, World world, BlockPos pos) {
		BlockState currentState = world.getBlockState(pos);
		boolean isEmpty = state.getFluid() == Fluids.EMPTY;
		if(world.getFluidState(pos).getFluid() != state.getFluid()) {
			if(currentState.isAir(world, pos) || (isEmpty && currentState.getMaterial().isLiquid())) {
				world.setBlockState(pos, state.getBlockState());
			}
			else if(world.getBlockState(pos).getBlock() instanceof IWaterLoggable) {
				IWaterLoggable wat = (IWaterLoggable)currentState.getBlock();
				if(wat.canContainFluid(world, pos, currentState, state.getFluid()))
					wat.receiveFluid(world, pos, currentState, state.getFluidState());
				else if(state.getFluid() == Fluids.EMPTY)
					wat.pickupFluid(world, pos, currentState);
			}
		}
	}
	
	public static float getAngleBetweenPoints(Vec3d start, Vec3d end) {
		float rot = 0F;
		
		Vec3d p = start.subtract(end).normalize();
		
		float hype = (float)Math.sqrt(p.x * p.x + p.z * p.z);
		
		if(p.z < 0)
			rot = (float)Math.toDegrees(Math.asin(p.x / hype));
		else rot = -(float)Math.toDegrees(Math.asin(p.x / hype)) - 180;
		
		return rot;
	}

	public static Vec3d vecFromPos(Vec3i location) {
		return new Vec3d(location.getX() + 0.5, location.getY() + 0.5, location.getZ() + 0.5);
	}
	
	public static float getFixedRotation(float rot) {
		float newR = rot % 360.0F;
		if(newR < 0)
			return 360.0F + newR;
		
		return newR;
	}

	public static BlockPos scaleBlockPos(BlockPos pos, double scale) {
		return new BlockPos(pos.getX() * scale, pos.getY() * scale, pos.getZ() * scale);
	}

	public static ResourceLocation createRL(String string) {
		return new ResourceLocation(Tardis.MODID, string);
	}
	/**
	 * Tests if a blockstate is blacklisted from rendering in BOTI applications
	 * @param state
	 * @return false if blacklisted, true if allowed
	 */
	public static boolean canRenderInBOTI(BlockState state) {
		for(String blocked : TConfig.CLIENT.scannerBlacklistedBlocks.get()) {
			if(state.getBlock().getRegistryName().toString().equals(blocked))
				return false;
			if(blocked.endsWith("*")) {
				String modid = blocked.substring(0, blocked.indexOf(':'));
				if(state.getBlock().getRegistryName().getNamespace().equals(modid))
					return false;
			}
		}
		return true;
	}

	public static <T extends Object> void addAllToListNotDuplicate(ArrayList<T> list1, List<T> list2) {
		for(T obj : list2) {
			if(!list1.contains(obj))
				list1.add(obj);
		}
	}

	public static boolean forceChunkIfNot(ServerWorld world, ChunkPos pos) {
		if(!world.getForcedChunks().contains(pos.asLong())) {
			world.forceChunk(pos.x, pos.z, true);
			return true;
		}
		return false;
	}

	public static <T extends INBT> List<T> getListFromNBT(ListNBT nbt, Class<T> nbtClass) {
		List<T> list = Lists.newArrayList();
		for(INBT n : nbt) {
			list.add(nbtClass.cast(n));
		}
		return list;
	}

	public static String[] getConsoleText(ConsoleTile console) {
		
		if(console.getFlightEvent() != null)
			return new String[] {
					"",
					console.getFlightEvent().getTranslation().getFormattedText()
					};
		
		if(console.getInteriorManager().getMonitorOverrides() != null)
			return console.getInteriorManager().getMonitorOverrides().getText();
		
		return new String[] {
		        Constants.Translations.LOCATION.getFormattedText() + Helper.formatBlockPos(console.getLocation()),
				Constants.Translations.DIMENSION.getFormattedText() + Helper.formatDimName(console.getDimension()),
				Constants.Translations.FACING.getFormattedText() + console.getDirection().getName().toUpperCase(),
				Constants.Translations.TARGET.getFormattedText() + Helper.formatBlockPos(console.getDestination()),
				Constants.Translations.TARGET_DIM.getFormattedText() + Helper.formatDimName(console.getDestinationDimension()),
				Constants.Translations.ARTRON.getFormattedText() + Constants.TEXT_FORMAT_NO_DECIMALS.format(console.getArtron()) + "AU",
				Constants.Translations.JOURNEY.getFormattedText() + Constants.TEXT_FORMAT_NO_DECIMALS.format(console.getPercentageJourney() * 100.0F) + "%"
				};
	}

	public static List<TileEntity> getTEsInChunks(ServerWorld world, ChunkPos chunkPos, int i) {
		List<TileEntity> list = Lists.newArrayList();
		for(int x = -i; x <= i; ++x) {
			for(int z = -i; z <= i; ++z) {
				list.addAll(world.getChunk(chunkPos.x + x, chunkPos.z + z).getTileEntityMap().values());
			}
		}
		return list;
	}

	public static AxisAlignedBB createBBWithRaduis(int i) {
		return new AxisAlignedBB(-i, -i, -i, i, i, i);
	}

}
