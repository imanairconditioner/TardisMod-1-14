package net.tardis.mod.trades;

import java.lang.reflect.Method;

import com.google.common.collect.ImmutableSet;

import net.minecraft.block.Block;
import net.minecraft.entity.merchant.villager.VillagerProfession;
import net.minecraft.util.ResourceLocation;
import net.minecraft.village.PointOfInterestType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class Villager {
	
	public static VillagerProfession STORY_TELLER;
	
	public static PointOfInterestType TELESCOPE;
	
	@SubscribeEvent
	public static void registerTrades(RegistryEvent.Register<VillagerProfession> event) {
		event.getRegistry().registerAll(
				STORY_TELLER = registerProfesion("story_teller", TELESCOPE)
		);
	}
	
	@SubscribeEvent
	public static void registerPOI(RegistryEvent.Register<PointOfInterestType> event) {
		event.getRegistry().registerAll(
			TELESCOPE = registerPOI("story_teller", TBlocks.telescope)
		);
		
		try {
			Method m = ObfuscationReflectionHelper.findMethod(PointOfInterestType.class, "func_221052_a", PointOfInterestType.class);
			m.invoke(null, TELESCOPE);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static PointOfInterestType registerPOI(String name, Block block) {
		return new PointOfInterestType("tardis:" + name, 
				ImmutableSet.copyOf(block.getStateContainer().getValidStates()),
				1, null, 1)
				.setRegistryName(Tardis.MODID, name);
	}
	
	public static VillagerProfession registerProfesion(String name, PointOfInterestType poi) {
		return new VillagerProfession("tardis:" + name, poi, ImmutableSet.of(), ImmutableSet.of())
				.setRegistryName(new ResourceLocation(Tardis.MODID, name));
	}

}
