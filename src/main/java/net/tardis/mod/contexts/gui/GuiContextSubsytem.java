package net.tardis.mod.contexts.gui;

import java.util.List;

import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.packets.DiagnosticMessage.ArtronUseInfo;
import net.tardis.mod.subsystem.SubsystemInfo;

public class GuiContextSubsytem extends GuiContext {

	public List<SubsystemInfo> infos;
	public SpaceTimeCoord location;
	public List<ArtronUseInfo> artronInfo;
	
	public GuiContextSubsytem(List<SubsystemInfo> infos, SpaceTimeCoord location, List<ArtronUseInfo> artronInfo) {
		this.infos = infos;
		this.location = location;
		this.artronInfo = artronInfo;
	}
}
