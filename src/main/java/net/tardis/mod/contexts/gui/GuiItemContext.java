package net.tardis.mod.contexts.gui;

import net.minecraft.item.ItemStack;
import net.tardis.mod.misc.GuiContext;

public class GuiItemContext extends GuiContext{

	ItemStack stack;
	
	public GuiItemContext(ItemStack stack) {
		this.stack = stack;
	}
	
	public ItemStack getItemStack() {
		return this.stack;
	}
}
