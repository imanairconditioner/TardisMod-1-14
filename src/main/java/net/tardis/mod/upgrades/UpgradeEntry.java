package net.tardis.mod.upgrades;

import javax.annotation.Nullable;

import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class UpgradeEntry<T extends Upgrade> implements IRegisterable<UpgradeEntry<T>>{

	private ResourceLocation registryName;
	private IConsoleSpawner<T> spawn;
	private Item item;
	private Class<? extends Subsystem> system;
	
	/**
	 * 
	 * @param spawn - Functional Interface to create your Upgrade
	 * @param item - Item key
	 * @param system - Parent system, for those that depend on it. Can be null
	 */
	public UpgradeEntry(IConsoleSpawner<T> spawn, Item item, @Nullable Class<? extends Subsystem> system) {
		this.spawn = spawn;
		this.item = item;
		this.system = system;
	}
	
	@Override
	public UpgradeEntry<T> setRegistryName(ResourceLocation regName) {
		this.registryName = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	public Item getItem() {
		return this.item;
	}
	
	public T create(ConsoleTile tile) {
		T upgrade = spawn.create(this, tile, system);
		return upgrade;
	}
	
	public static interface IConsoleSpawner<T>{
		T create(UpgradeEntry<?> entry, ConsoleTile console, @Nullable Class<? extends Subsystem> sys);
	}

}
