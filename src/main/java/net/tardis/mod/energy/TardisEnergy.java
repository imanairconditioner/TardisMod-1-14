package net.tardis.mod.energy;

import net.minecraft.nbt.IntNBT;
import net.minecraftforge.energy.EnergyStorage;

public class TardisEnergy extends EnergyStorage{

	public TardisEnergy(int capacity) {
		super(capacity);
	}
	
	public IntNBT serialize() {
		return new IntNBT(this.energy);
	}
	
	public void deserialize(IntNBT tag) {
		this.energy = tag.getInt();
	}

}
