package net.tardis.mod.registries;

import net.minecraftforge.event.RegistryEvent.Register;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.tardis.mod.Tardis;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.missions.DroneStationMission;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.missions.MiniMissionType.IMissionBuilder;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class MissionRegistry {
	
	public static MiniMissionType STATION_DRONE;
	
	@SubscribeEvent
	public static void register(Register<MiniMissionType> event) {
		event.getRegistry().registerAll(
				STATION_DRONE = register(DroneStationMission::new, "drone_station")
		);
	}
	
	public static MiniMissionType register(IMissionBuilder<MiniMission> builder, String name){
		MiniMissionType type = new MiniMissionType(builder);
		type.setRegistryName(Helper.createRL(name));
		return type;
	}
}
