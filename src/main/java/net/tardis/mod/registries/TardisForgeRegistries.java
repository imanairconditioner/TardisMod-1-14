package net.tardis.mod.registries;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.flight.FlightEventFactory;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.Disguise;
import net.tardis.mod.missions.MiniMissionType;
import net.tardis.mod.sounds.SoundSchemeBase;
import net.tardis.mod.traits.TardisTraitType;

@EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TardisForgeRegistries {

	public static IForgeRegistry<Disguise> DISGUISE;
	public static IForgeRegistry<SoundSchemeBase> SOUND_SCHEME;
	public static IForgeRegistry<FlightEventFactory> FLIGHT_EVENTS_REGISTRY;
	public static IForgeRegistry<MiniMissionType> MISSIONS;
	public static IForgeRegistry<TardisTraitType> TRAITS;
	
	
	@SubscribeEvent
	public static void registerRegistries(RegistryEvent.NewRegistry event) {
		
		DISGUISE = new RegistryBuilder<Disguise>()
				.setName(new ResourceLocation(Tardis.MODID, "disguises"))
				.setType(Disguise.class)
				.create();
		
		SOUND_SCHEME = new RegistryBuilder<SoundSchemeBase>()
				.setName(Helper.createRL("sound_schemes"))
				.setType(SoundSchemeBase.class)
				.create();
		
		FLIGHT_EVENTS_REGISTRY = new RegistryBuilder<FlightEventFactory>()
				.setName(new ResourceLocation(Tardis.MODID, "flight_events"))
				.setType(FlightEventFactory.class).create();
		
		MISSIONS = new RegistryBuilder<MiniMissionType>()
				.setType(MiniMissionType.class)
				.setName(Helper.createRL("missions")).create();
		
		TRAITS = new RegistryBuilder<TardisTraitType>()
				.setType(TardisTraitType.class)
				.setName(Helper.createRL("traits")).create();
		
	}
	
}
