package net.tardis.mod.items;

import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.minecraftforge.common.UsernameCache;
import net.tardis.mod.Tardis;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.items.IDiagnostic;
import net.tardis.mod.constants.Constants;

public class TardisDiagnosticItem extends Item {

	public TardisDiagnosticItem(Properties properties) {
		super(properties);
		
        this.addPropertyOverride(new ResourceLocation(Tardis.MODID, "on"), (stack, worldIn, entityIn) -> {
			
        	IDiagnostic cap = stack.getCapability(Capabilities.DIAGNOSTIC).orElse(null);
			if(cap == null)
				return 0F;
			return cap.getOn() ? 1F : 0F;
		});
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(!worldIn.isRemote) {
			playerIn.getHeldItem(handIn).getCapability(Capabilities.DIAGNOSTIC).ifPresent(loc -> loc.onRightClick(worldIn, playerIn, handIn));
		}
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}
	
	@Override
	public void inventoryTick(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected) {
		if (!worldIn.isRemote) {
			if(entityIn instanceof LivingEntity && isSelected)
				stack.getCapability(Capabilities.DIAGNOSTIC).ifPresent(loc -> loc.tick((LivingEntity)entityIn));
		}
		
	}
	
	@Override
	public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
		return oldStack.getItem() != newStack.getItem();
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		stack.getCapability(Capabilities.DIAGNOSTIC).ifPresent(cap -> {
			if(cap.getOwner() != null) {
				UUID id = cap.getOwner();
				tooltip.add(new TranslationTextComponent("tooltip.tardis.diagnostic.owner", UsernameCache.containsUUID(id) ? UsernameCache.getLastKnownUsername(id) : id.toString()));
			}
		});
		tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
		if (Screen.hasShiftDown()) {
			tooltip.clear();
			tooltip.add(0, this.getDisplayName(stack));
			tooltip.add(new TranslationTextComponent("tooltip.tardis.diagnostic.use"));
			tooltip.add(new TranslationTextComponent("tooltip.tardis.diagnostic.use2"));
		}
		super.addInformation(stack, worldIn, tooltip, flagIn);
	}
	
	@Nullable
    @Override
    public CompoundNBT getShareTag(ItemStack stack) { //sync capability to client
        CompoundNBT tag = stack.getOrCreateTag();
        stack.getCapability(Capabilities.DIAGNOSTIC).ifPresent(cap -> tag.put("cap_sync", cap.serializeNBT()));
        return tag;
    }

    @Override
    public void readShareTag(ItemStack stack, @Nullable CompoundNBT nbt) {
        super.readShareTag(stack, nbt);
        if (nbt != null) {
            if (nbt.contains("cap_sync")) {
                stack.getCapability(Capabilities.DIAGNOSTIC).ifPresent(cap -> cap.deserializeNBT(nbt.getCompound("cap_sync")));
            }
        }
    }
    
	public static void syncCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.getOrCreateTag().merge(stack.getShareTag());
        }
    }

    public static void readCapability(ItemStack stack) {
        if (stack.getShareTag() != null) {
            stack.readShareTag(stack.getOrCreateTag());
        }
    }
}
