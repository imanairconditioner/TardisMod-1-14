package net.tardis.mod.items;

import java.util.List;

import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.RayTraceContext;
import net.minecraft.util.math.RayTraceContext.BlockMode;
import net.minecraft.util.math.RayTraceContext.FluidMode;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.contexts.gui.GuiContextBlock;
import net.tardis.mod.itemgroups.TItemGroups;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.properties.Prop;

public class MonitorRemoteItem extends Item {

	public MonitorRemoteItem() {
		super(Prop.Items.ONE.get().group(TItemGroups.MAIN));
	}

	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		if(worldIn.isRemote) {
			Vec3d pos = playerIn.getPositionVector().add(0, playerIn.getEyeHeight(), 0);
			RayTraceResult result = worldIn.rayTraceBlocks(new RayTraceContext(pos, pos.add(playerIn.getLookVec().scale(32)), BlockMode.COLLIDER, FluidMode.NONE, playerIn));
			if(result instanceof BlockRayTraceResult) {
				BlockRayTraceResult block = (BlockRayTraceResult)result;
				if(worldIn.getTileEntity(block.getPos()) instanceof IMonitor) {
					Tardis.proxy.openGUI(Constants.Gui.MONITOR_REMOTE, new GuiContextBlock(worldIn, block.getPos()));
					return new ActionResult<ItemStack>(ActionResultType.SUCCESS, playerIn.getHeldItem(handIn));
				}
			}
			
		}
		
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(Constants.Translations.TOOLTIP_HOLD_SHIFT);
		if (Screen.hasShiftDown()) {
		    tooltip.clear();
		    tooltip.add(0, this.getDisplayName(stack));
		    tooltip.add(new TranslationTextComponent("tooltip.monitor_remote.use"));
		}
	}

}
