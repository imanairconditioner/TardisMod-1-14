package net.tardis.mod.items;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.Level;

import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUseContext;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ActionResult;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.World;
import net.tardis.mod.Tardis;
import net.tardis.mod.properties.Prop;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class PlasmicShellItem extends Item {

	public PlasmicShellItem() {
		super(Prop.Items.ONE.get().group(null));
	}

	@SuppressWarnings("deprecation")
	@Override
	public ActionResultType onItemUse(ItemUseContext context) {
		ItemStack stack = context.getItem();
		
		CompoundNBT tag = stack.getOrCreateTag();
		
		if(tag.contains("pos1") && tag.contains("pos2") && context.getWorld().getTileEntity(context.getPos()) instanceof ExteriorTile) {
			BlockPos pos1 = BlockPos.fromLong(tag.getLong("pos1"));
			BlockPos pos2 = BlockPos.fromLong(tag.getLong("pos2"));
			
			HashMap<BlockPos, Block> blocks = new HashMap<>();
			
			for(BlockPos pos : BlockPos.getAllInBoxMutable(pos1, pos2)) {
				BlockState state = context.getWorld().getBlockState(pos);
				if(!state.isAir() && !pos.equals(context.getPos()) && !pos.equals(context.getPos().down())) {
					blocks.put(pos.toImmutable(), state.getBlock());
				}
			}
			
			//Write File
			
			try {
				JsonWriter writer = new GsonBuilder().setPrettyPrinting().create().newJsonWriter(new FileWriter(new File("plasmic.json")));
				
				writer.beginArray();
				for(Entry<BlockPos, Block> entry : blocks.entrySet()) {
					writer.beginObject();
					
					writer.name("block").value(entry.getValue().getRegistryName().toString());
					writer.name("x").value(entry.getKey().getX() - context.getPos().getX());
					writer.name("y").value(entry.getKey().getY() - context.getPos().getY());
					writer.name("z").value(entry.getKey().getZ() - context.getPos().getZ());
					
					writer.endObject();
				}
				writer.endArray();
				
				writer.close();
				
				if(!context.getWorld().isRemote) {
					context.getPlayer().sendMessage(new StringTextComponent("Saved as plasmic.json in the minecraft instance!"));
					context.getItem().getOrCreateTag().putBoolean("complete", true);
				}
			}
			catch(IOException e) {
				Tardis.LOGGER.catching(Level.DEBUG, e);
			}
			
			return ActionResultType.SUCCESS;
		}
		
		if(!tag.contains("pos1")) {
			tag.putLong("pos1", context.getPos().up().toLong());
			return ActionResultType.SUCCESS;
		}
		
		return super.onItemUse(context);
	}
	
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, PlayerEntity playerIn, Hand handIn) {
		ItemStack stack = playerIn.getHeldItem(handIn);
		if(stack.getOrCreateTag().contains("pos1")) {
			stack.getOrCreateTag().putLong("pos2", playerIn.getPosition().toLong());
		}
		
		if(playerIn.isSneaking())
			stack.setTag(new CompoundNBT());
		
		return super.onItemRightClick(worldIn, playerIn, handIn);
	}

	@Override
	public void addInformation(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn) {
		super.addInformation(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslationTextComponent("tooltip.plasmic_shell.use1"));
		tooltip.add(new TranslationTextComponent("tooltip.plasmic_shell.use2"));
		tooltip.add(new TranslationTextComponent("tooltip.plasmic_shell.use3"));
	}
	
	public static AxisAlignedBB getBox(BlockPos pos, BlockPos pos1) {
		int minX, maxX;
		int minY, maxY;
		int minZ, maxZ;
		
		if(pos.getX() > pos1.getX()) {
			minX = pos1.getX();
			maxX = pos.getX();
		}
		else {
			minX = pos.getX();
			maxX = pos1.getX();
		}
		
		if(pos.getY() > pos1.getY()) {
			minY = pos1.getY();
			maxY = pos.getY();
		}
		else {
			minY = pos.getY();
			maxY = pos1.getY();
		}
		
		if(pos.getZ() > pos1.getZ()) {
			minZ = pos1.getZ();
			maxZ = pos.getZ();
		}
		else {
			minZ = pos.getZ();
			maxZ = pos1.getZ();
		}
		
		
		return new AxisAlignedBB(minX, minY, minZ, maxX, maxY, maxZ);
	}

}
