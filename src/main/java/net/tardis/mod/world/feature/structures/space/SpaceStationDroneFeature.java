package net.tardis.mod.world.feature.structures.space;

import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.block.Blocks;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.WorldGenRegion;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;
import net.minecraft.world.gen.feature.template.PlacementSettings;
import net.minecraft.world.gen.feature.template.Template;
import net.minecraft.world.gen.feature.template.Template.BlockInfo;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.dimensions.TDimensions;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.humanoid.CrewmateEntity;
import net.tardis.mod.entity.humanoid.ShipCaptainEntity;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.registries.MissionRegistry;

public class SpaceStationDroneFeature extends Feature<ProbabilityConfig> {

	public static final ResourceLocation LOCATION = Helper.createRL("tardis/structures/worldgen/space/spacestation_drone");
	
	public SpaceStationDroneFeature(Function<Dynamic<?>, ? extends ProbabilityConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, ProbabilityConfig config) {
		if(worldIn.getDimension() != null && worldIn.getDimension().getType() == TDimensions.SPACE_TYPE) {
			if(worldIn instanceof WorldGenRegion){
				WorldGenRegion world = (WorldGenRegion)worldIn;
				
				final ServerWorld serverWorld = world.getWorld();
				
				pos = new ChunkPos(world.getMainChunkX(), world.getMainChunkZ()).asBlockPos().subtract(new BlockPos(75, 0, 75));
				pos = pos.add(0, 64, 0);
				
				Template temp = world.getWorld().getStructureTemplateManager().getTemplate(LOCATION);
				
				if(!worldIn.isAreaLoaded(pos, pos.add(temp.getSize())))
					return false;
				
				PlacementSettings settings = new PlacementSettings();
				temp.addBlocksToWorld(world, pos, settings);
				for(BlockInfo info : temp.func_215381_a(pos, settings, Blocks.STRUCTURE_BLOCK)){
					if(info.nbt != null && info.nbt.contains("metadata")) {
						String data = info.nbt.getString("metadata");
						
						if(data.contentEquals("captain_spawn")) {
							final BlockPos entCap = info.pos.toImmutable();
							ShipCaptainEntity cap = TEntities.SHIP_CAPTIAN.create(serverWorld);
							cap.setPosition(entCap.getX() + 0.5, entCap.getY() + 1, entCap.getZ() + 0.5);
							cap.setupDefaultEquipment();
							world.addEntity(cap);
						}
						else if(data.contentEquals("mission_marker")) {
							serverWorld.getServer().enqueue(new TickDelayedTask(0, () -> {
								serverWorld.getCapability(Capabilities.MISSION).ifPresent(missions -> {
									missions.addMission(MissionRegistry.STATION_DRONE.create(info.pos, 64));
								});
							}));
						}
						else if(data.contentEquals("crewmates")) {
							int num = 5 + world.getRandom().nextInt(3);
							for(int i = 0 ; i < num; ++i) {
								CrewmateEntity entity = TEntities.CREWMATE.create(serverWorld);
								entity.setPosition(info.pos.getX() + 0.5, info.pos.getY() + 1, info.pos.getZ() + 0.5);
								world.addEntity(entity);
							}
						}
					}
					world.setBlockState(info.pos, Blocks.AIR.getDefaultState(), 3);
				}
			}
		}
		return false;
	}

}
