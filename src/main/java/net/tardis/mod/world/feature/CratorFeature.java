package net.tardis.mod.world.feature;

import java.util.Random;
import java.util.function.Function;

import com.mojang.datafixers.Dynamic;

import net.minecraft.block.Blocks;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorld;
import net.minecraft.world.gen.ChunkGenerator;
import net.minecraft.world.gen.GenerationSettings;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.ProbabilityConfig;

public class CratorFeature extends Feature<ProbabilityConfig> {

	public CratorFeature(Function<Dynamic<?>, ? extends ProbabilityConfig> configFactoryIn) {
		super(configFactoryIn);
	}

	@Override
	public boolean place(IWorld worldIn, ChunkGenerator<? extends GenerationSettings> generator, Random rand, BlockPos pos, ProbabilityConfig config) {
		int radius = 6 + rand.nextInt(10);
		pos = worldIn.getHeight(Type.WORLD_SURFACE_WG, pos).up(radius / 3);
		for(BlockPos p : BlockPos.getAllInBoxMutable(pos.add(-radius, -radius, -radius), pos.add(radius, radius, radius))) {
			if(p.withinDistance(pos, radius - 1))
				worldIn.setBlockState(p, Blocks.AIR.getDefaultState(), 2);
		}
		return true;
	}

}
