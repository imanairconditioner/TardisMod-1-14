package net.tardis.mod.entity.ai.dalek;

import java.util.ArrayList;
import java.util.Iterator;

import net.minecraft.util.math.Vec3d;
import net.tardis.mod.entity.DalekEntity;

public class DalekFormationSingle extends DalekFormation{

	public DalekFormationSingle(ArrayList<Vec3d> offsets) {
		super(offsets);
	}

	@Override
	public Vec3d getPlace(DalekEntity commander, DalekEntity drone) {
		int index = 0;
		Iterator<DalekEntity> it = commander.getDrones().iterator();
		while(it.hasNext()) {
			if(drone.getEntityId() == it.next().getEntityId())
				break;
			++index;
		}
		return new Vec3d(0, 0, -(index * 1.5));
	}

}
