package net.tardis.mod.entity.humanoid;


import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ILivingEntityData;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.ai.humanoids.SitInChairGoal;
import net.tardis.mod.items.TItems;
import net.tardis.mod.missions.MiniMission;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;

public class ShipCaptainEntity extends AbstractHumanoidEntity{

	private MiniMission mission;
	private static final ResourceLocation TEXTURE = new ResourceLocation("textures/entity/steve.png");
	private Dialog dialog = this.setupDialog();
	
	public ShipCaptainEntity(EntityType<? extends CreatureEntity> type, World worldIn) {
		super(type, worldIn);
	}
	
	public ShipCaptainEntity(World worldIn) {
		super(TEntities.SHIP_CAPTIAN, worldIn);
	}

	@Override
	public Dialog getCurrentDialog() {
		return this.dialog;
	}

	@Override
	protected boolean processInteract(PlayerEntity player, Hand hand) {
		return super.processInteract(player, hand);
	}

	@Override
	public ResourceLocation getSkin() {
		return TEXTURE;
	}

	@Override
	public ILivingEntityData onInitialSpawn(IWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, ILivingEntityData spawnDataIn, CompoundNBT dataTag) {
		this.setupDefaultEquipment();
		return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}
	
	public void setupDefaultEquipment() {
		if(!world.isRemote()) {
			this.setItemStackToSlot(EquipmentSlotType.CHEST, new ItemStack(TItems.SPACE_CHEST));
			this.setItemStackToSlot(EquipmentSlotType.LEGS, new ItemStack(TItems.SPACE_LEGS));
			this.setItemStackToSlot(EquipmentSlotType.FEET, new ItemStack(TItems.SPACE_BOOTS));
		}
	}

	@Override
	protected void registerGoals() {
		super.registerGoals();
		
		this.goalSelector.addGoal(3, new SitInChairGoal(this, 0.2334, 16));
	}
	
	public Dialog setupDialog() {
		Dialog root = new Dialog("What are you doing here?");
		
		root.addDialogOption(new DialogOption(null, "I broke in"));
		
		Dialog distraction = new Dialog("Oh, it's right down th - N- No! How did you get on my ship?!");
		distraction.addDialogOption(new DialogOption(null, "Oh, my ship teleports, I got your SOS"));
		distraction.addDialogOption(new DialogOption(null, "I walked"));
		
		root.addDialogOption(new DialogOption(distraction, "Just looking for the bathroom"));
		
		return root;
		
	}

}
