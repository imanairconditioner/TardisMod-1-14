package net.tardis.mod.subsystem;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.nbt.CompoundNBT;
import net.tardis.mod.tileentities.ConsoleTile;

public class StabilizerSubsystem extends Subsystem{
	
	private boolean activated;
	private int seconds = 0;

	public StabilizerSubsystem(ConsoleTile console, Item item) {
		super(console, item);
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("activated", this.activated);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		this.activated = nbt.getBoolean("activated");
	}

	@Override
	public void onTakeoff() {}

	@Override
	public void onLand() {}

	@Override
	public void onFlightSecond() {
		if(!console.getWorld().isRemote && this.isActivated()) {
			++this.seconds;
			if(this.seconds >= 10) {
				this.seconds = 0;
				this.damage((ServerPlayerEntity)console.getPilot(), 1);
			}
		}
			
	}
	
	public boolean isActivated() {
		
		if(!this.canBeUsed())
			this.activated = false;
		
		return this.activated;
	}

	public void setActivated(boolean b) {
		if(!this.canBeUsed())
			this.activated = false;
		else this.activated = b;
		this.console.updateClient();
	}

	@Override
	public boolean shouldSpark() {
		return false;
	}

	@Override
	public boolean stopsFlight() {
		return false;
	}

}
