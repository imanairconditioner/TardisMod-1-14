package net.tardis.mod.subsystem;

import javax.annotation.Nullable;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.SubsystemData;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;

public abstract class Subsystem implements IRegisterable<Subsystem>, INBTSerializable<CompoundNBT>{

	private Item itemKey;
	private ResourceLocation registryName;
	protected ConsoleTile console;
	
	//for client use
	protected boolean canBeUsed = false;
	
	/**
	 * 
	 * @param Console - The console tile that this subsystem is attached to
	 * @param Item - The Item Component for this subsystem, the game looks for this item in the component
	 * 			panel of the engine, make sure the item is damagable
	 */
	
	public Subsystem(ConsoleTile console, Item item) {
		this.console = console;
		this.itemKey = item;
	}

	@Override
	public Subsystem setRegistryName(ResourceLocation regName) {
		this.registryName = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.registryName;
	}
	
	public Item getItemKey() {
		return this.itemKey;
	}
	
	/**
	 * 
	 * @return ItemStack in the engine matching this Subsystem's component item
	 */
	public ItemStack getItem() {
		if(this.console != null && console.hasWorld()) {
			ITardisWorldData data = this.console.getWorld().getCapability(Capabilities.TARDIS_DATA).orElse(null);
			if(data != null) {
				PanelInventory inv = data.getEngineInventoryForSide(Direction.NORTH);
				for(int i = 0; i < inv.getSizeInventory(); ++i) {
					if(inv.getStackInSlot(i).getItem() == this.itemKey)
						return inv.getStackInSlot(i);
				}
			}
		}
		return ItemStack.EMPTY;
	}
	
	/**
	 * Damages the component's item in the engine
	 * @param player
	 * @param amt
	 */
	public void damage(@Nullable ServerPlayerEntity player, int amt) {
		this.getItem().attemptDamageItem(amt, console.getWorld().rand, player);
		
		//Removes it if less than 0 health
		if(this.getItem().getDamage() >= this.getItem().getMaxDamage())
			this.console.getWorld().getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
				PanelInventory inv = cap.getEngineInventoryForSide(Direction.NORTH);
				for(int i = 0; i < inv.getSizeInventory(); ++i) {
					if(inv.getStackInSlot(i).getItem() == this.getItemKey()) {
						inv.setInventorySlotContents(i, ItemStack.EMPTY);
						break;
					}
				}
			});
		if(!this.canBeUsed())
			this.onBreak();
		
		this.updateClientIfNeeded();
	}
	
	/**
	 * 
	 * @return If the component can do it's thing
	 */
	public boolean canBeUsed() {
		
		if(console.getWorld().isRemote)
			return this.canBeUsed;
		
		if(this.getItem().isEmpty())
			return false;
		
		return (this.canBeUsed = this.getItem().getDamage() < this.getItem().getMaxDamage());
	}
	
	public float getHealth() {
		return 1.0F - MathHelper.clamp(this.getItem().getDamage() / (float)this.getItem().getMaxDamage(), 0, 1F);
	}
	
	/**
	 * 
	 * @return If this system will stop the TARDIS Taking off
	 * 			If you're adding a system not essential to flight, return false
	 */
	public boolean stopsFlight() {
		return !this.canBeUsed();
	}

    public abstract void onTakeoff();

    public abstract void onLand();

    public abstract void onFlightSecond();
    
    public void onBreak() {};
    
    /**
     * 
     * @return If this subsystem should make the console spark, non-essential probably should just return false
     */
    public boolean shouldSpark() {
    	ItemStack stack = this.getItem();
    	
    	//Non-damagable items and stop / by 0
    	if(stack.getMaxDamage() <= 0)
    		return true;
    	
    	return this.getHealth() < 0.1;
    }

    /**
     * 
     * @param softCrash - if this is true, was just a missed control, otherwise was a subsystem failure or something to knock it out of flight
     */
	public void explode(boolean softCrash) {
		this.damage(null, softCrash ? 10 : 50);
	}
	
	/**
	 * Only effects client, do not set
	 * @param used
	 */
	public void setCanBeUsed(boolean used) {
		this.canBeUsed = used;
	}
	
	private void updateClientIfNeeded() {
		
		if(this.console.getWorld().isRemote)
			return;
		
		if(this.canBeUsed != this.canBeUsed())
			Network.sendToAllInWorld(new ConsoleUpdateMessage(DataTypes.SUBSYSTEM, new SubsystemData(this.getRegistryName(), this.canBeUsed())), (ServerWorld)console.getWorld());
	}
	
	
}
