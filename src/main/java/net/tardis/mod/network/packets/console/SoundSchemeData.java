package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.registries.TardisForgeRegistries;
import net.tardis.mod.tileentities.ConsoleTile;

public class SoundSchemeData implements ConsoleData{

	private ResourceLocation key;
	
	public SoundSchemeData(ResourceLocation key) {
		this.key = key;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		tile.setSoundScheme(TardisForgeRegistries.SOUND_SCHEME.getValue(key));
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeResourceLocation(key);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.key = buff.readResourceLocation();
	}

}
