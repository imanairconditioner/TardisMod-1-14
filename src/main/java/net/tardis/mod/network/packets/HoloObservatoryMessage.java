package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.tileentities.HoloObservatoryTile;

public class HoloObservatoryMessage {

	BlockPos pos;
	DimensionType type;
	
	public HoloObservatoryMessage(BlockPos pos, DimensionType type) {
		this.pos = pos;
		this.type = type;
	}
	
	public static void encode(HoloObservatoryMessage mes, PacketBuffer buffer) {
		buffer.writeBlockPos(mes.pos);
		buffer.writeResourceLocation(DimensionType.getKey(mes.type));
	}
	
	public static HoloObservatoryMessage decode(PacketBuffer buffer) {
		return new HoloObservatoryMessage(buffer.readBlockPos(), DimensionType.byName(buffer.readResourceLocation()));
	}
	
	public static void handle(HoloObservatoryMessage mes, Supplier<NetworkEvent.Context> context) {
		
		context.get().enqueueWork(() -> {
			TileEntity te = context.get().getSender().getServerWorld().getTileEntity(mes.pos);
			if(te instanceof HoloObservatoryTile) {
				HoloObservatoryTile observ = (HoloObservatoryTile)te;
				
				observ.setDimensionToRender(mes.type);
				observ.updateClient();
				
			}
		});
		
		context.get().setPacketHandled(true);
	}
}
