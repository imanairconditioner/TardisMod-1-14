package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointDeleteMessage {

	BlockPos pos;
	int index = 0;
	
	public WaypointDeleteMessage(BlockPos pos, int index) {
		this.pos = pos;
		this.index = index;
	}
	
	public static void encode(WaypointDeleteMessage mes, PacketBuffer buf) {
		buf.writeBlockPos(mes.pos);
		buf.writeInt(mes.index);
	}
	
	public static WaypointDeleteMessage decode(PacketBuffer buf) {
		return new WaypointDeleteMessage(buf.readBlockPos(), buf.readInt());
	}
	
	public static void handle(WaypointDeleteMessage mes, Supplier<NetworkEvent.Context> con) {
		con.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(con.get().getSender().world).ifPresent(tile -> {
				TileEntity te = tile.getWorld().getTileEntity(mes.pos);
				if(te instanceof WaypointBankTile)
					((WaypointBankTile)te).deleteWaypoint(mes.index);
			});
		});
		con.get().setPacketHandled(true);
	}
}
