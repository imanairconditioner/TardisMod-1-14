package net.tardis.mod.network.packets;

import java.util.Random;
import java.util.function.Supplier;

import net.minecraft.client.Minecraft;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.network.PacketBuffer;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class MissControlMessage {

    public MissControlMessage() {}

    public static void encode(MissControlMessage mes, PacketBuffer buf) {}

    public static MissControlMessage decode(PacketBuffer buf) {
        return new MissControlMessage();
    }

    public static void handle(MissControlMessage mes, Supplier<NetworkEvent.Context> cont) {
    	cont.get().enqueueWork(() -> {
            TileEntity te = ((ClientWorld)Minecraft.getInstance().world).getTileEntity(TardisHelper.TARDIS_POS);
            if (te instanceof ConsoleTile) {
                double x = te.getPos().getX() + 0.5, y = te.getPos().getY(), z = te.getPos().getZ() + 0.5;
                for (int i = 0; i < 40; ++i) {
                    double rot = Math.toRadians(i * 9);
                    ((ClientWorld)Minecraft.getInstance().world).addParticle(ParticleTypes.CLOUD, x, y, z, Math.sin(rot) * 0.1, 0.1, Math.cos(rot) * 0.1);
                }
                Random rand = Minecraft.getInstance().world.rand;
                Minecraft.getInstance().player.setMotion(Minecraft.getInstance().player.getMotion().add(0.5 - rand.nextDouble(), rand.nextDouble() * 0.5, 0.5 - rand.nextDouble()));
            }
        });
        cont.get().setPacketHandled(true);
    }

}
