package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.items.TItems;

public class UpdateManualPageMessage {
	
	int page;
	
	public UpdateManualPageMessage(int page) {
		this.page = page;
	}
	
	public static void encode(UpdateManualPageMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.page);
	}
	
	public static UpdateManualPageMessage decode(PacketBuffer buf) {
		return new UpdateManualPageMessage(buf.readInt());
	}

	public static void handle(UpdateManualPageMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			PlayerEntity ent = cont.get().getSender();
			if(ent.getHeldItemMainhand().getItem() == TItems.MANUAL) {
				CompoundNBT tag = ent.getHeldItemMainhand().hasTag() ? ent.getHeldItemMainhand().getTag() : new CompoundNBT();
				tag.putInt("page", mes.page);
				ent.getHeldItemMainhand().setTag(tag);
			}
		});
		cont.get().setPacketHandled(true);
	}
}
