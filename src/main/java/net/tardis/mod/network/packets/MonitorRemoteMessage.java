package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.entity.Entity;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.misc.IMonitor;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;
import net.tardis.mod.tileentities.monitors.RotateMonitorTile;

public class MonitorRemoteMessage {

	int entityID;
	BlockPos pos = null;
	MonitorView view;
	MonitorMode mode;
	float extend;
	
	public MonitorRemoteMessage(BlockPos pos, MonitorMode mode, MonitorView view, float extend) {
		this.pos = pos;
		this.view = view;
		this.mode = mode;
		this.extend = extend;
	}
	
	public MonitorRemoteMessage(int id, MonitorMode mode, MonitorView view, float extend) {
		this.entityID = id;
		this.view = view;
		this.mode = mode;
		this.extend = extend;
	}
	
	public static void encode(MonitorRemoteMessage mes, PacketBuffer buf) {
		if(mes.pos != null) {
			buf.writeInt(0);
			buf.writeBlockPos(mes.pos);
		}
		else {
			buf.writeInt(1);
			buf.writeInt(mes.entityID);
		}
		buf.writeInt(mes.mode.ordinal());
		buf.writeInt(mes.view.ordinal());
		buf.writeFloat(mes.extend);
	}
	
	public static MonitorRemoteMessage decode(PacketBuffer buf) {
		
		int type = buf.readInt();
		
		if(type == 0)
			return new MonitorRemoteMessage(buf.readBlockPos(),
					MonitorMode.values()[buf.readInt()],
					MonitorView.values()[buf.readInt()],
					buf.readFloat());
		
		return new MonitorRemoteMessage(buf.readInt(),
				MonitorMode.values()[buf.readInt()],
				MonitorView.values()[buf.readInt()],
				buf.readFloat());
	}
	
	public static void handle(MonitorRemoteMessage mes, Supplier<NetworkEvent.Context> context) {
		
		context.get().enqueueWork(() -> {
			if(mes.pos != null) {
				TileEntity te = context.get().getSender().world.getTileEntity(mes.pos);
				if(te instanceof IMonitor) {
					IMonitor monitor = (IMonitor)te;
					
					monitor.setMode(mes.mode);
					monitor.setView(mes.view);
					
					if(monitor instanceof RotateMonitorTile) {
						((RotateMonitorTile)monitor).setExtendAnount(mes.extend);
					}
				}
			}
			else {
				Entity ent = context.get().getSender().world.getEntityByID(mes.entityID);
				if(ent instanceof ControlEntity && ((ControlEntity)ent).getControl() instanceof IMonitor) {
					IMonitor mon = (IMonitor)((ControlEntity)ent).getControl();
					mon.setMode(mes.mode);
					mon.setView(mes.view);
				}
			}
		});
		
		context.get().setPacketHandled(true);
	}
	
	
}
