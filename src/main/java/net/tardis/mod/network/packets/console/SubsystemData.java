package net.tardis.mod.network.packets.console;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent.Context;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class SubsystemData implements ConsoleData{

	private boolean canBeUsed;
	private ResourceLocation key;
	
	public SubsystemData(ResourceLocation key, boolean canBeUsed) {
		this.key = key;
		this.canBeUsed = canBeUsed;
	}
	
	@Override
	public void applyToConsole(ConsoleTile tile, Supplier<Context> context) {
		for(Subsystem s : tile.getSubSystems()){
			if(s.getRegistryName().equals(key)) {
				s.setCanBeUsed(this.canBeUsed);
				break;
			}
		}
	}

	@Override
	public void serialize(PacketBuffer buff) {
		buff.writeResourceLocation(key);
		buff.writeBoolean(canBeUsed);
	}

	@Override
	public void deserialize(PacketBuffer buff) {
		this.key = buff.readResourceLocation();
		this.canBeUsed = buff.readBoolean();
	}

}
