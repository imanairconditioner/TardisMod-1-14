package net.tardis.mod.network.packets;

import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.EntityStorage;
import net.tardis.mod.boti.IBotiEnabled;
import net.tardis.mod.cap.Capabilities;

public class WorldShellEntityMessage {

	List<EntityStorage> entities = Lists.newArrayList();
	BlockPos pos;
	int type = 0;
	
	public WorldShellEntityMessage(BlockPos pos, List<EntityStorage> entity) {
		
		this.pos = pos;
		type = 0;
		
		this.entities = Lists.newArrayList(entities);
	}
	
	public WorldShellEntityMessage(List<EntityStorage> entities) {
		type = 1;
		this.entities = Lists.newArrayList(entities);
	}
	

	public static void encode(WorldShellEntityMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.type);
		if(mes.type == 0)
			buf.writeBlockPos(mes.pos);
		
		buf.writeInt(mes.entities.size());
		for(EntityStorage store : mes.entities) {
			store.encode(buf);
		}
		
	}
	
	public static WorldShellEntityMessage decode(PacketBuffer buf) {
		int type = buf.readInt();
		
		BlockPos pos = null;
		
		if(type == 0)
			pos = buf.readBlockPos();
		
		int size = buf.readInt();
		
		List<EntityStorage> list = Lists.newArrayList();
		
		for(int i = 0; i < size; ++i) {
			list.add(new EntityStorage(buf));
		}
		if(pos == null || type == 1)
			return new WorldShellEntityMessage(list);
		return new WorldShellEntityMessage(pos, list);
	}
	
	
	public static void handle(WorldShellEntityMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			if(mes.type == 0) {
				TileEntity te = Minecraft.getInstance().world.getTileEntity(mes.pos);
				if(te instanceof IBotiEnabled) {
					IBotiEnabled boti = (IBotiEnabled)te;
					
					if(boti.getBotiWorld() != null)
						boti.getBotiWorld().updateEntities(mes.entities);
					
				}
			}
			else {
				Minecraft.getInstance().world.getCapability(Capabilities.TARDIS_DATA).ifPresent(tile -> {
					if(tile.getBotiWorld() != null) {
						tile.getBotiWorld().updateEntities(mes.entities);
					}
				});
			}
		});
		context.get().setPacketHandled(true);
	}
}
