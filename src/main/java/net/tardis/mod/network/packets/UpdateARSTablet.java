package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.ARSTabletItem;
import net.tardis.mod.items.TItems;
import net.tardis.mod.registries.TardisRegistries;

public class UpdateARSTablet {
	
	private ResourceLocation name;
	
	public UpdateARSTablet(ResourceLocation name) {
		this.name = name;
	}
	
	public static void encode(UpdateARSTablet mes, PacketBuffer buf) {
		buf.writeResourceLocation(mes.name);
	}
	
	public static UpdateARSTablet decode(PacketBuffer buf) {
		return new UpdateARSTablet(buf.readResourceLocation());
	}
	
	public static void handle(UpdateARSTablet mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ItemStack stack = context.get().getSender().getHeldItemMainhand();
			if(stack.getItem() == TItems.ARS_TABLET)
				ARSTabletItem.setPiece(stack, TardisRegistries.ARS_PIECES.getValue(mes.name));
				PlayerHelper.sendMessageToPlayer(context.get().getSender(), new TranslationTextComponent("ars.message.selected_piece", TardisRegistries.ARS_PIECES.getValue(mes.name).getTranslation()), false);
		});
		context.get().setPacketHandled(true);
	}

}
