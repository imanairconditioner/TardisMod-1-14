package net.tardis.mod.commands.permissions;

import java.text.MessageFormat;

import net.minecraftforge.server.permission.DefaultPermissionLevel;
import net.tardis.mod.Tardis;

public enum PermissionEnum {
    CREATE("create", DefaultPermissionLevel.OP, ""),
    INTERIOR("interior", DefaultPermissionLevel.OP, ""),
    EXTERIOR("exterior", DefaultPermissionLevel.OP, ""),
    REFUEL("refuel", DefaultPermissionLevel.OP, ""),
    UNLOCK("unlock", DefaultPermissionLevel.OP, ""),
    COLLIDE("unlock", DefaultPermissionLevel.OP, "");

    private String path;
    private DefaultPermissionLevel level;
    private String description;

    PermissionEnum(String path, DefaultPermissionLevel level, String description) {
        this.path = path;
        this.level = level;
        this.description = description;
    }

    public String getNode(){
        return MessageFormat.format("{0}.command.{1}", Tardis.MODID, this.path);
    }

    public DefaultPermissionLevel getLevel() {
        return level;
    }

    public String getDescription() {
        return description;
    }
}
