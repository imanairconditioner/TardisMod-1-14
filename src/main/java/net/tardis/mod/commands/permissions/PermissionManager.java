package net.tardis.mod.commands.permissions;

import java.util.Arrays;

import net.minecraftforge.server.permission.PermissionAPI;

public class PermissionManager {

    public static void init(){
        Arrays.stream(PermissionEnum.values()).forEach(
                perm -> PermissionAPI.registerNode(perm.getNode(), perm.getLevel(), perm.getDescription())
        );
    }
}
