package net.tardis.mod.commands.subcommands;

import java.util.ArrayList;
import java.util.List;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

public class UnlockCommand extends TCommand{

	private static final UnlockCommand CMD = new UnlockCommand(PermissionEnum.UNLOCK);
	
	public UnlockCommand(PermissionEnum permission) {
		super(permission);
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
		CommandSource source = context.getSource();
		if(this.canExecute(context.getSource())) {
			ConsoleTile console = TardisHelper.getConsole(context.getSource().getServer(), context.getSource().asPlayer().getUniqueID()).orElse(null);
			String type = context.getArgument("type", String.class);
			String item = context.getArgument("item", String.class);
				//For exteriors
				if(type.contentEquals("exterior")) {
					if(item.contentEquals("*")) {
						for(IExterior ext : ExteriorRegistry.getRegistry().values()) {
							Helper.unlockExterior(console, context.getSource().asPlayer(), ext);
						}
						source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.exterior_all", source.asPlayer().getDisplayName()), true);
					}
					else {
						IExterior ext = ExteriorRegistry.getExterior(new ResourceLocation(item));
						Helper.unlockExterior(console, context.getSource().asPlayer(), ext);
						source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.exterior", ext.getDisplayName(),source.asPlayer().getDisplayName()), true);
					}
					
				}
				//For interior
				else if(type.contentEquals("interior")) {
					if(item.contentEquals("*")) {
						for(ConsoleRoom room : ConsoleRoom.REGISTRY.values()) {
							Helper.unlockInterior(console, context.getSource().asPlayer(), room);
						}
						source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.interior_all", source.asPlayer().getDisplayName()), true);
					}
					else {
						ConsoleRoom room = ConsoleRoom.REGISTRY.get(new ResourceLocation(item));
						Helper.unlockInterior(console, context.getSource().asPlayer(), room);
						source.sendFeedback(new TranslationTextComponent("command.tardis.unlock.interior", room.getDisplayName(),source.asPlayer().getDisplayName()), true);
					}
				}
				console.updateClient();
		}
		else {
			source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
		}
		return Command.SINGLE_SUCCESS;
	}
	
	 public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
	        return Commands.literal("unlock")
	        		.then(Commands.argument("type", StringArgumentType.string())
	        				.suggests((context, builder) ->
	        					ISuggestionProvider.suggest(new String[]{"interior", "exterior"}, builder))
	        		.then(Commands.argument("item", StringArgumentType.greedyString())
	        				.suggests((context, builder) -> {
	        					String type = context.getArgument("type", String.class);
	        					List<String> items = new ArrayList<String>();
	        					items.add("*");
	        					if(type.contentEquals("exterior")) {
	        						for(IExterior ext : ExteriorRegistry.getRegistry().values()) {
		        						items.add(ext.getRegistryName().toString());
		        					}
	        					}
	        					else if(type.contentEquals("interior")) {
	        						for(ConsoleRoom room : ConsoleRoom.REGISTRY.values()) {
	        							items.add(room.getRegistryName().toString());
	        						}
	        					}
	        					return ISuggestionProvider.suggest(items, builder);
	        				})
	        		.executes(CMD)));
	    }

}
