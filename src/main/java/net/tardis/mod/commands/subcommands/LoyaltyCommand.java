package net.tardis.mod.commands.subcommands;

import org.apache.logging.log4j.Level;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.Tardis;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.TardisHelper;

public class LoyaltyCommand extends TCommand{

	public static final LoyaltyCommand CMD = new LoyaltyCommand(PermissionEnum.UNLOCK);
	
	public LoyaltyCommand(PermissionEnum permission) {
		super(permission);
	}

	@Override
	public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
	    final CommandSource source = context.getSource();
	    if (canExecute(source)) {
	        ServerPlayerEntity player;
    	    try {
    	        final String playerName = context.getArgument("username", String.class);
    	        player = source.getServer().getPlayerList().getPlayerByUsername(playerName);
    		}
    		catch(Exception e) {
    		    player = source.asPlayer();
    			Tardis.LOGGER.log(Level.INFO, e);
    		}
    	    int loyaltyToSet = context.getArgument("loyalty", Integer.class);
            TardisHelper.getConsole(context.getSource().getServer(), player.getUniqueID()).ifPresent(tile -> {
                tile.getEmotionHandler().setLoyalty(loyaltyToSet);
            });
            source.sendFeedback(new TranslationTextComponent("message.tardis.loyalty.success", player.getDisplayName(), loyaltyToSet), true);
	    }
	    else {
	        source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
	    }
		return Command.SINGLE_SUCCESS;
	}

	public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
		return Commands.literal("loyalty")
		        .then(Commands.argument("username", StringArgumentType.string())
	                    .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
				        .then(Commands.argument("loyalty", IntegerArgumentType.integer(0, 200)) //Many functions need a loyalty higher than 100. This cap should be sufficient for now.
				.executes(CMD)));
	}

	
}
