package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.commands.permissions.PermissionEnum;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.helper.TeleportUtil;

public class InteriorCommand extends TCommand {
    private static final InteriorCommand CMD = new InteriorCommand();

    public InteriorCommand() {
        super(PermissionEnum.INTERIOR);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("interior").executes(CMD)
                .then(Commands.argument("username", StringArgumentType.string())
                    .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                .then(Commands.argument("target",StringArgumentType.string())
                    .suggests((context, builder) -> ISuggestionProvider.suggest(ServerLifecycleHooks.getCurrentServer().getOnlinePlayerNames(), builder))
                .executes(CMD)));
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {
        final CommandSource source = context.getSource();

        if (canExecute(source)) {
            ServerPlayerEntity playerToTeleport;
            ServerPlayerEntity playerTardisTarget;

            try {
                final String player = context.getArgument("username", String.class);
                final String target = context.getArgument("target", String.class);
                playerToTeleport = source.getServer().getPlayerList().getPlayerByUsername(player);
                playerTardisTarget = source.getServer().getPlayerList().getPlayerByUsername(target);
            }
            catch(Exception e) {
                playerToTeleport = source.asPlayer();
                playerTardisTarget = source.asPlayer();
            }
            
            final DimensionType consoleDimension = TardisHelper.getTardisByUUID(playerTardisTarget.getUniqueID());
            TeleportUtil.teleportEntity(playerToTeleport, consoleDimension, TardisHelper.TARDIS_POS.getX() + 2, TardisHelper.TARDIS_POS.getY(), TardisHelper.TARDIS_POS.getZ());
            
            TardisHelper.getConsole(source.getServer(), consoleDimension).ifPresent(tile -> {
            	tile.getDoor().ifPresent(door -> door.setOpenState(EnumDoorState.CLOSED));
            });
            
            source.sendFeedback(new TranslationTextComponent("message.tardis.interior_command", playerTardisTarget.getDisplayName()), true);
        }
        else {
            source.sendErrorMessage(new StringTextComponent(Constants.Message.NO_PERMISSION));
        }

        return Command.SINGLE_SUCCESS;
    }

}
