package net.tardis.mod.commands.subcommands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.builder.ArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.ISuggestionProvider;
import net.tardis.mod.commands.permissions.PermissionEnum;

public class CreateCommand extends TCommand {
    private static final CreateCommand CMD = new CreateCommand();

    public CreateCommand() {
        super(PermissionEnum.CREATE);
    }

    public static ArgumentBuilder<CommandSource, ?> register(CommandDispatcher<CommandSource> dispatcher) {
        return Commands.literal("create")
        		.then(Commands.argument("username", StringArgumentType.word())
        				.suggests((context, builder) -> ISuggestionProvider.suggest(context.getSource().getServer().getPlayerList().getOnlinePlayerNames(), builder))
        		.executes(CMD))
                .executes(CMD);
    }

    @Override
    public int run(CommandContext<CommandSource> context) throws CommandSyntaxException {

        return Command.SINGLE_SUCCESS;
    }
}
