package net.tardis.mod.boti;

public interface IBotiEnabled {
	
	WorldShell getBotiWorld();
	void setBotiWorld(WorldShell shell);

}
