package net.tardis.mod.boti;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import javax.annotation.Nullable;

import com.google.common.collect.Maps;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.fluid.Fluids;
import net.minecraft.fluid.IFluidState;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IEnviromentBlockReader;
import net.minecraft.world.LightType;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.Biomes;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.client.renderers.boti.BotiManager;

public class WorldShell implements IEnviromentBlockReader{

	private final int maxRange = 32;
	private HashMap<BlockPos, BlockStore> blocks = new HashMap<>();
	private HashMap<BlockPos, TileEntity> tileEntities = new HashMap<>();
	private HashMap<UUID, Entity> entities = Maps.newHashMap();
	private boolean update = true;
	private BlockPos offset = BlockPos.ZERO;
	private Biome biome = Biomes.THE_VOID;
	private DimensionType type;
	
	//Object type to not make servers sad -- on client will be a BotiWorld
	private static Object botiWorld;
	private static Object botiManager;
	
	public WorldShell(BlockPos offset, DimensionType type){
		this.offset = offset.toImmutable();
		this.type = type;
	}
	
	public void put(BlockPos pos, BlockStore store) {
		this.blocks.put(pos, store);
	}
	
	public BlockStore get(BlockPos pos) {
		return this.blocks.get(pos);
	}
	
	public Map<BlockPos, BlockStore> getMap(){
		return this.blocks;
	}

	@Override
	public TileEntity getTileEntity(BlockPos pos) {
		return tileEntities.get(pos);
	}

	@Override
	public BlockState getBlockState(BlockPos pos) {
		return get(pos) != null ? get(pos).getState() : Blocks.AIR.getDefaultState();
	}

	@Override
	public IFluidState getFluidState(BlockPos pos) {
		return get(pos) != null ? get(pos).getState().getFluidState() : Fluids.EMPTY.getDefaultState();
	}

	@Override
	public Biome getBiome(BlockPos pos) {
		return biome;
	}
	
	public void setBiome(Biome b) {
		this.biome = b;
	}

	@Override
	public int getLightFor(LightType type, BlockPos pos) {
		return get(pos) != null ? get(pos).getLight() : 15;
	}
	
	@OnlyIn(Dist.CLIENT)
	public void setupTEs(BotiWorld world){
		
		//for(TileEntity te : this.tileEntities.values())
			//te.remove();
		
		tileEntities.clear();
		
		this.setWorld(world);
		for(Entry<BlockPos, BlockStore> entry : blocks.entrySet()) {
			TileEntity te = entry.getValue().getTile();
			if(te != null) {
				te.setWorld(this.getWorld());
				te.setPos(entry.getKey());
				tileEntities.put(entry.getKey(), te);
				world.loadedTileEntityList.add(te);
				if(te instanceof ITickableTileEntity)
					world.tickableTileEntities.add(te);
				te.updateContainingBlockInfo();
			}
		}
	}
	
	@Nullable
	@OnlyIn(Dist.CLIENT)
	public BotiWorld getWorld() {
		
		if(botiWorld instanceof BotiWorld) {
			BotiWorld world = ((BotiWorld)botiWorld);
			if(this.getDimensionType() != world.dimension.getType()) {
				
			}
		}
		
		return (BotiWorld)botiWorld;
	}
	
	@OnlyIn(Dist.CLIENT)
	public void setWorld(BotiWorld world) {
		botiWorld = world;
		for(TileEntity te : this.tileEntities.values())
			te.setWorld(world);
	}
	
	@OnlyIn(Dist.CLIENT)
	public BotiManager getRenderManager() {
		if(botiManager == null)
			this.setupRenderManager();
		return (BotiManager)botiManager;
	}
	
	@OnlyIn(Dist.CLIENT)
	public void setupRenderManager() {
		botiManager = new BotiManager();
		BotiManager boti = (BotiManager)botiManager;
		boti.worldRenderer = new WorldRenderer(Minecraft.getInstance());
		boti.world = new BotiWorld(type == null ? (type = DimensionType.OVERWORLD) : type, this, boti.worldRenderer);
		boti.worldRenderer.setWorldAndLoadRenderers(boti.world);
		this.setWorld(boti.world);
	}
	
	public Map<BlockPos, TileEntity> getTiles(){
		return tileEntities;
	}
	
	public boolean needsUpdate() {
		return this.update;
	}
	
	public void setNeedsUpdate(boolean update) {
		this.update = update;
	}
	
	public BlockPos getOffset() {
		return this.offset;
	}
	
	@Nullable
	public DimensionType getDimensionType() {
		return this.type;
	}
	
	public void writeToBuffer(PacketBuffer buf) {
		buf.writeBlockPos(offset);
		buf.writeResourceLocation(DimensionType.getKey(type));
		buf.writeResourceLocation(this.biome.getRegistryName());
		buf.writeInt(this.blocks.size());
		for(Entry<BlockPos, BlockStore> entry : this.getMap().entrySet()) {
			buf.writeBlockPos(entry.getKey());
			entry.getValue().encode(buf);
		}
	}
	
	public static WorldShell readFromBuffer(PacketBuffer buffer) {
		WorldShell shell = new WorldShell(buffer.readBlockPos(), DimensionType.byName(buffer.readResourceLocation()));
		shell.setBiome(ForgeRegistries.BIOMES.getValue(buffer.readResourceLocation()));
		int size = buffer.readInt();
		for(int i = 0; i < size; ++i) {
			shell.put(buffer.readBlockPos(), new BlockStore(buffer));
		}
		return shell;
	}

	@OnlyIn(Dist.CLIENT)
	public void updateEntities(List<EntityStorage> entities) {
		
		for(EntityStorage storage : entities) {
			if(this.entities.containsKey(storage.id)) {
				Entity e = this.entities.get(storage.id);
				storage.updateEntity(e);
				if(e.removed || !e.getPosition().withinDistance(this.getOffset(), 32))
					this.entities.remove(storage.id);
			}
			else if(this.getWorld() != null){
				EntityType<?> type = ForgeRegistries.ENTITIES.getValue(storage.type);
				if(type != null) {
					Entity e = type.create(this.getWorld());
					e.setUniqueId(storage.id);
					
					try {
						e.read(storage.data);
					}
					catch(Exception exception) {}
					
					storage.updateEntity(e);
					this.entities.put(storage.id, e);
				}
			}
		}
		
	}

	@OnlyIn(Dist.CLIENT)
	public Collection<Entity> getEntities() {
		return this.entities.values();
	}

	public void tick(boolean remote) {
		if(remote)
			this.tickWorld();
	}
	
	@OnlyIn(Dist.CLIENT)
	private void tickWorld() {
		try {
			if(this.getWorld() != null) {
				this.getWorld().tickEntities();
			}
		}
		catch(Exception e) {}
	}

	public void setMap(HashMap<BlockPos, BlockStore> newShell) {
		this.blocks.clear();
		this.blocks.putAll(newShell);
	}

	public void combine(WorldShell shell) {
		if(!shell.getMap().isEmpty() && !shell.getMap().equals(this.getMap())) {
			this.blocks.putAll(shell.getMap());
			this.setNeedsUpdate(true);
		}
	}

	public void setDimensionType(DimensionType type) {
		this.type = type;
	}
}
