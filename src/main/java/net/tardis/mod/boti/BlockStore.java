package net.tardis.mod.boti;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.PacketBuffer;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockStore {

	private BlockState state = Blocks.AIR.getDefaultState();
	private int lightVal = 0;
	private CompoundNBT tileTag = null;
	
	public BlockStore(BlockState state, int lightValue, CompoundNBT te) {
		this.state = state;
		this.lightVal = lightValue;
		if(te != null)
			this.tileTag = te;
	}
	
	public BlockStore(BlockState state, int lightValue, TileEntity te) {
		this.state = state;
		this.lightVal = lightValue;
		if(te != null)
			this.tileTag = te.serializeNBT();
	}
	
	public BlockStore(PacketBuffer buf) {
		this.decode(buf);
	}
	
	@OnlyIn(Dist.CLIENT)
	public TileEntity getTile() {
		return this.tileTag == null ? null : TileEntity.create(this.tileTag);
	}
	
	public BlockState getState() {
		return this.state;
	}
	
	public int getLight() {
		return this.lightVal;
	}
	
	public CompoundNBT serialize() {
		CompoundNBT nbt = new CompoundNBT();
		nbt.put("state", NBTUtil.writeBlockState(state));
		nbt.putInt("light", lightVal);
		if(this.tileTag != null)
			nbt.put("tile", this.tileTag);
		return nbt;
	}
	
	public static BlockStore deserialize(CompoundNBT tag) {
		CompoundNBT tile = tag.getCompound("tile");
		return new BlockStore(
				NBTUtil.readBlockState(tag.getCompound("state")),
				tag.getInt("light"),
				tile.isEmpty() ? null : tile);
	}
	
	public void encode(PacketBuffer buf) {
		buf.writeCompoundTag(NBTUtil.writeBlockState(state));
		buf.writeInt(this.lightVal);
		
		buf.writeBoolean(this.tileTag != null);
		
		if(this.tileTag != null) {
			buf.writeCompoundTag(tileTag);
		}
		
	}
	
	public void decode(PacketBuffer buf) {
		this.state = NBTUtil.readBlockState(buf.readCompoundTag());
		this.lightVal = buf.readInt();
		if(buf.readBoolean()) {
			this.tileTag = buf.readCompoundTag();
		}
	}
	

	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof BlockStore))
			return false;
		
		BlockStore other = (BlockStore)obj;
		
		if(this.state.equals(other.state) && isTileNBTSame(other))
			return true;
			
		return false;
		
	}
	
	private boolean isTileNBTSame(BlockStore other) {
		if(this.tileTag == other.tileTag)
			return true;
		
		if(this.tileTag == null && other.tileTag == null)
			return true;
		
		//If this tile tag isn't null
		if(this.tileTag != null) {
			if(other.tileTag == null)
				return false;
			if(this.tileTag.equals(other.tileTag))
				return true;
			return false;
		}
		
		if(other.tileTag != null) {
			if(this.tileTag == null)
				return false;
			if(other.tileTag.equals(this.tileTag))
				return true;
			return false;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return state.hashCode();
	}
}
