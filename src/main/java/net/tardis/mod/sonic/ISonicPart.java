package net.tardis.mod.sonic;

import net.minecraft.item.Item;
import net.tardis.mod.items.sonicparts.SonicBasePart;

/**
 * Created by Swirtzly
 * on 21/03/2020 @ 19:16
 */
public interface ISonicPart {

    SonicPart getSonicPart();
    void update(); //May not be needed

    enum SonicPart{
        EMITTER(0), ACTIVATOR(1), HANDLE(2), END(3);

        private final int invID;

        SonicPart(int invId) {
            this.invID = invId;
        }
        
        public int getInvID() {
            return invID;
        }
        
        public static SonicPart getFromItem(Item item) {
        	if(item instanceof SonicBasePart){
        	    SonicBasePart sonicBasePart = (SonicBasePart) item;
        	    return sonicBasePart.getSonicPart();
            }
        	throw new IllegalArgumentException(item + " is not a valid Sonic part item!");
        }
    }

}
