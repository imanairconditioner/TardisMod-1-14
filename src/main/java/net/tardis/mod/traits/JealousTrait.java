package net.tardis.mod.traits;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.console.misc.TardisTrait;

public class JealousTrait extends TardisTrait{

	public JealousTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {
		
		if(!tile.getWorld().isRemote) {
			if(tile.getWorld().getGameTime() % 200 == 0) {
				ServerPlayerEntity owner = tile.getWorld().getServer().getPlayerList().getPlayerByUUID(tile.getOwner());
				if(owner != null && owner.isAlive()) {
					//If cheating
					if(TardisHelper.isInATardis(owner) && !TardisHelper.isInOwnedTardis(owner)) {
						TardisHelper.getConsoleInWorld(owner.getServerWorld()).ifPresent(other -> {
							if(other.isInFlight() && tile.getEmotionHandler().getMood() > EnumHappyState.SAD.getTreshold()) {
								double mod = this.getModifier();
								tile.getEmotionHandler().addMood(-(1 * (int)Math.round(4 * mod)));
								tile.getEmotionHandler().addLoyalty(-(1 + (int)Math.round(4 * mod)));
							}
						});
					}
					
					if(tile.isInFlight() && tile.getPilot() == owner && tile.getEmotionHandler().getMood() < EnumHappyState.HAPPY.getTreshold()) {
						tile.getEmotionHandler().addMood(10);
					}
					
				}
			}
		}
		
	}

}
