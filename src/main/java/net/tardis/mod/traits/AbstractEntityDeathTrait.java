package net.tardis.mod.traits;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.TardisTrait;

/**
 * 
 * @author Spectre
 *
 * Extend this if you need the trait to deal with an entity's death, see @code {@link #onOwnerKilledEntity(PlayerEntity, ConsoleTile, LivingEntity)}
 */
public abstract class AbstractEntityDeathTrait extends TardisTrait{

	public AbstractEntityDeathTrait(TardisTraitType type) {
		super(type);
	}
	
	/**
	 * Called when the owner of this TARDIS kills something
	 * @param owner - Owner of the TARDIS
	 * @param tardis - ConsoleTile this trait is attached to
	 * @param victim - The entity the owner killed
	 */
	public abstract void onOwnerKilledEntity(PlayerEntity owner, ConsoleTile tardis, LivingEntity victim);

}
