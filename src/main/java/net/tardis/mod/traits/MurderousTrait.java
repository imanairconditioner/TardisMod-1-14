package net.tardis.mod.traits;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.merchant.villager.VillagerEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;

public class MurderousTrait extends AbstractEntityDeathTrait{

	public MurderousTrait(TardisTraitType type) {
		super(type);
	}

	@Override
	public void tick(ConsoleTile tile) {}

	@Override
	public void onOwnerKilledEntity(PlayerEntity owner, ConsoleTile tardis, LivingEntity victim) {
		if(victim instanceof PlayerEntity || victim instanceof VillagerEntity) {
			if(tardis.getEmotionHandler().getMood() < EnumHappyState.HAPPY.getTreshold()) {
				double mod = 1.0 - this.getWeight();
				
				tardis.getEmotionHandler().addLoyalty((int)Math.ceil(1 + (2 * mod)));
				tardis.getEmotionHandler().addMood(5 + (int)Math.ceil(5 * mod));
				
				this.warnPlayer(tardis, GENERIC_LIKE);
			}
		}
	}

}
