package net.tardis.mod.schematics;

import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.registries.TardisRegistries;

public class Schematics {
	
	public static ExteriorUnlockSchematic POLICE_BOX;
	public static ExteriorUnlockSchematic POLICE_BOX_MODERN;
	
	public static void registerAll() {
		TardisRegistries.registerRegisters(() -> {
			TardisRegistries.SCHEMATICS.register("police_box", POLICE_BOX = new ExteriorUnlockSchematic(() -> ExteriorRegistry.POLICE_BOX));
			TardisRegistries.SCHEMATICS.register("police_box_modern", POLICE_BOX_MODERN = new ExteriorUnlockSchematic(() -> ExteriorRegistry.MODERN_POLICE_BOX));
		});
	}

}
