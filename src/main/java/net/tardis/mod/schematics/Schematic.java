package net.tardis.mod.schematics;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.registries.IRegisterable;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class Schematic implements IRegisterable<Schematic>{

	private ResourceLocation name;
	
	public abstract void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity player);
	
	@Override
	public Schematic setRegistryName(ResourceLocation regName) {
		this.name = regName;
		return this;
	}

	@Override
	public ResourceLocation getRegistryName() {
		return this.name;
	}

	
}
