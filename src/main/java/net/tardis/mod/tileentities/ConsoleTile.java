package net.tardis.mod.tileentities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.UUID;

import javax.annotation.Nullable;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Maps;

import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.concurrent.TickDelayedTask;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.Explosion;
import net.minecraft.world.Explosion.Mode;
import net.minecraft.world.GameRules;
import net.minecraft.world.dimension.DimensionType;
import net.minecraft.world.gen.Heightmap.Type;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.config.TConfig;
import net.tardis.mod.constants.Constants.Translations;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.controls.ControlRegistry.ControlEntry;
import net.tardis.mod.controls.IControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.controls.RefuelerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.entity.TEntities;
import net.tardis.mod.entity.TardisDisplayEntity;
import net.tardis.mod.entity.TardisEntity;
import net.tardis.mod.events.TardisEvent;
import net.tardis.mod.exterior.ExteriorRegistry;
import net.tardis.mod.exterior.IExterior;
import net.tardis.mod.flight.FlightEvent;
import net.tardis.mod.flight.TardisCollideInstagate;
import net.tardis.mod.flight.TardisCollideRecieve;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.helper.LandingSystem;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.items.ArtronCapacitorItem;
import net.tardis.mod.misc.ITickable;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.misc.TexVariant;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.MissControlMessage;
import net.tardis.mod.network.packets.console.CrashData;
import net.tardis.mod.network.packets.console.Fuel;
import net.tardis.mod.registries.FlightEventRegistry;
import net.tardis.mod.registries.SoundSchemeRegistry;
import net.tardis.mod.registries.TardisForgeRegistries;
import net.tardis.mod.registries.TardisRegistries;
import net.tardis.mod.sounds.SoundSchemeBase;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.AntennaSubsystem;
import net.tardis.mod.subsystem.FluidLinksSubsystem;
import net.tardis.mod.subsystem.ShieldGeneratorSubsystem;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.subsystem.SubsystemEntry;
import net.tardis.mod.tags.TardisBlockTags;
import net.tardis.mod.tileentities.console.misc.AlarmType;
import net.tardis.mod.tileentities.console.misc.ArtronUse;
import net.tardis.mod.tileentities.console.misc.ArtronUse.ArtronType;
import net.tardis.mod.tileentities.console.misc.ArtronUse.IArtronType;
import net.tardis.mod.tileentities.console.misc.ControlOverride;
import net.tardis.mod.tileentities.console.misc.DistressSignal;
import net.tardis.mod.tileentities.console.misc.EmotionHandler;
import net.tardis.mod.tileentities.console.misc.EmotionHandler.EnumHappyState;
import net.tardis.mod.tileentities.console.misc.ExteriorPropertyManager;
import net.tardis.mod.tileentities.console.misc.InteriorManager;
import net.tardis.mod.tileentities.console.misc.MonitorOverride;
import net.tardis.mod.tileentities.console.misc.UnlockManager;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;
import net.tardis.mod.tileentities.inventory.PanelInventory;
import net.tardis.mod.tileentities.machines.LandingPadTile;
import net.tardis.mod.tileentities.machines.TransductionBarrierTile;
import net.tardis.mod.upgrades.Upgrade;
import net.tardis.mod.upgrades.UpgradeEntry;

public class ConsoleTile extends TileEntity implements ITickableTileEntity{
	
	private static final AxisAlignedBB CONRTROL_HITBOX = new AxisAlignedBB(-1, 0, -1, 2, 2, 2);
	public static final int TARDIS_MAX_SPEED = 10;
	//Ten AU a tick at max
	public static final float BASIC_FUEL_USEAGE = 1;
	public static Random rand = new Random();
	
	public int flightTicks = 0;
	private int reachDestinationTick = 0;
	private EmotionHandler emotionHandler;
	private InteriorManager interiorManager;
	private List<ITickable> tickers = new ArrayList<ITickable>();
	private HashMap<ResourceLocation, INBTSerializable<CompoundNBT>> dataHandlers = new HashMap<ResourceLocation, INBTSerializable<CompoundNBT>>();
	private ArrayList<ControlEntity> controls = new ArrayList<ControlEntity>();
	private ArrayList<ControlEntry<?>> controlEntries = new ArrayList<ControlEntry<?>>();
	private IExterior exterior;
	private SoundSchemeBase scheme;
	private BlockPos location = BlockPos.ZERO;
	private BlockPos destination = BlockPos.ZERO;
	private DimensionType dimension;
	private DimensionType destinationDimension;
	private Direction facing = Direction.NORTH;
	public int coordIncr = 10;
	private float max_artron = 0;
	private float artron = 0;
	private float rechargeMod = 1F;
	private ConsoleRoom consoleRoom = ConsoleRoom.STEAM;
	private List<Subsystem> subsystems = new ArrayList<>();
	private List<Upgrade> upgrades = new ArrayList<>();
	private String customName = "";
	private ExteriorPropertyManager exteriorProps;
	private SpaceTimeCoord returnLocation = SpaceTimeCoord.UNIVERAL_CENTER;
	private HashMap<UUID, BlockPos> bedPositions = new HashMap<UUID, BlockPos>();
	private FlightEvent currentEvent = null;
	private List<DistressSignal> distressSignal = new ArrayList<DistressSignal>();
	private ItemStack sonic = ItemStack.EMPTY;
	protected TexVariant[] variants = {};
	private int variant = 0;
	private boolean antiGravs = false;
	private UUID tardisEntityID = null;
	private TardisEntity tardisEntity = null;
	private boolean shouldSpark = false;
	private String landingCode = "";
	private int landTime = 0;
	private HashMap<IArtronType, ArtronUse> artronUses = Maps.newHashMap();
	private LazyOptional<ExteriorTile> exteriorHolder = LazyOptional.empty();
	private boolean isCrashing = false;
	private UnlockManager unlockManager;
	protected HashMap<Class<?>, ControlOverride> controlOverrides = Maps.newHashMap();
	
	//What ever you do do not save / sync this (If you use onLoad() the world won't load)
	private int timeUntilControlSpawn = 10;
	
	//Last player to interact with a control, used for loyalty
	private PlayerEntity pilot;
	
	//Data handlers Read from this
	private Runnable onLoadAction;

	public ConsoleTile(TileEntityType<?> type) {
		super(type);
		this.emotionHandler = new EmotionHandler(this);
		this.interiorManager = new InteriorManager(this);
		this.exteriorProps = new ExteriorPropertyManager(this);
		this.exterior = ExteriorRegistry.STEAMPUNK;
		this.dimension = DimensionType.OVERWORLD;
		this.destinationDimension = DimensionType.OVERWORLD;
		this.unlockManager = new UnlockManager(this);
		this.scheme = SoundSchemeRegistry.BASIC;
		this.registerControlEntry(ControlRegistry.DEMAT);
		this.registerControlEntry(ControlRegistry.THROTTLE);
		this.registerControlEntry(ControlRegistry.RANDOM);
		this.registerControlEntry(ControlRegistry.DIMENSION);
		this.registerControlEntry(ControlRegistry.FACING);
		this.registerControlEntry(ControlRegistry.X);
		this.registerControlEntry(ControlRegistry.Y);
		this.registerControlEntry(ControlRegistry.Z);
		this.registerControlEntry(ControlRegistry.INC_MOD);
		this.registerControlEntry(ControlRegistry.LAND_TYPE);
		this.registerControlEntry(ControlRegistry.REFUELER);
		this.registerControlEntry(ControlRegistry.FAST_RETURN);
		this.registerControlEntry(ControlRegistry.TELEPATHIC);
		this.registerControlEntry(ControlRegistry.STABILIZERS);
		this.registerControlEntry(ControlRegistry.SONIC_PORT);
		this.registerControlEntry(ControlRegistry.COMMUNICATOR);
		this.registerControlEntry(ControlRegistry.DOOR);
		
		for(SubsystemEntry<?> entry : TardisRegistries.SUBSYSTEM_REGISTRY.getRegistry().values()) {
			this.subsystems.add(entry.create(this));
		}
		
		for(UpgradeEntry<?> entry : TardisRegistries.UPGRADES.getRegistry().values()) {
			this.upgrades.add(entry.create(this));
		}
		
	}
	
	//Fly loop, called every tick
	public void fly() {
		//This is not a duplicate of isInFlight(), I have a plan
		//If this is true, this is a "real flight", as in it's going somewhere
		if(this.isInFlight()) {
			
			++this.flightTicks;
			
			//If crashing, play crash effects
			if(this.isCrashing)
				this.playCrashEffects();
			
			//Land if reachined destination and stabilized
			if(!world.isRemote && this.flightTicks >= this.reachDestinationTick && landTime <= 0){
				this.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
					if(sys.isActivated())
						this.initLand();
				});
			}
			
			if(!world.isRemote && this.flightTicks > this.landTime && this.landTime > 0) {
				this.flightTicks = this.reachDestinationTick = this.landTime = 0;
				this.updateClient();
			}
			
			//Crash if it can't fly
			if(!world.isRemote && !this.canFly()) {
				crash();
				return;
			}
			
			//Artron usage
			if(!world.isRemote) {
				ArtronUse use = this.getOrCreateArtronUse(ArtronType.FLIGHT);
				use.setArtronUse(this.calcFuelUse());
				use.setTimeToDrain(1);

				if (this.flightTicks % 20 == 0) {
					for (Subsystem sub : this.getSubSystems()) {
						sub.onFlightSecond();
					}
					for (Upgrade up : this.getUpgrades()) {
						up.onFlightSecond();
					}
				}
				
				if(world.getGameTime() % 20 == 0)
					Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FUEL, new Fuel(this.artron, this.max_artron)), world.dimension.getType(), this.getPos(), 20);
				
			}

			if (!world.isRemote) {
				
				//If this has an event and it's time, complete it
				if(currentEvent != null && this.currentEvent.getMissedTime() < this.flightTicks) {
					currentEvent.onComplete(this);
					
					//Search for collisions
					this.currentEvent = null;

					//If Not landing
					if(this.landTime <= 0) {
						ObjectWrapper<Boolean> collided = new ObjectWrapper<>(false);
						Iterator<ServerWorld> it = world.getServer().getWorlds().iterator();
						while(it.hasNext()) {
							ServerWorld world = it.next();
							
							//Stop if found one to collide with
							if(collided.getValue())
								break;
							
							TardisHelper.getConsoleInWorld(world).ifPresent(tile -> {
								//if unstabilized and not ourselves
								this.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
									if(tile != this && tile.isInFlight() && !sys.isActivated()) {
										//If not landing and not already colliding
										if(tile.getLandTime() == 0 && !(tile.getFlightEvent() instanceof TardisCollideInstagate) && !(tile.getFlightEvent() instanceof TardisCollideRecieve)) {
											if(tile.getPositionInFlight().getPos().withinDistance(this.getPositionInFlight().getPos(), TConfig.COMMON.collisionRange.get())) {
												this.setFlightEvent(((TardisCollideInstagate)FlightEventRegistry.COLLIDE_INSTAGATE.create(this)).setOtherTARDIS(tile));
												collided.setValue(true);
											}
										}
									}
								});
							});
						}
					}
					
					if(this.canGiveNewEvent() && this.currentEvent == null)
						this.setFlightEvent(FlightEventRegistry.getRandomEvent(rand).create(this));
					
				}
				
				else if(this.currentEvent == null && this.canGiveNewEvent())
					this.setFlightEvent(FlightEventRegistry.getRandomEvent(rand).create(this));
				
			}
			
			//Shake
			if(!world.isRemote && world.getGameTime() % 3 == 0) {
				if(this.currentEvent != null && !this.currentEvent.getControls().isEmpty())
					for(PlayerEntity player : world.getPlayers()) {
						player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
							cap.setShaking(5);
							cap.update();
						});
					}
			}
		}

		this.playFlightLoop();
	}
	
	//Landing code, handles exiting flight and seting up the exterior
	public void land() {
		
		if(!world.isRemote) {
			
			ServerWorld otherWorld = world.getServer().getWorld(this.destinationDimension);
			
			//Pre-load the likely landing chunks
			ChunkPos loadedChunkPos = new ChunkPos(this.destination);
			boolean forced = !otherWorld.getForcedChunks().contains(loadedChunkPos.asLong());
			if(forced)
				otherWorld.forceChunk(loadedChunkPos.x, loadedChunkPos.z, true);
			
			//Delay all this until after world loaded
			world.getServer().enqueue(new TickDelayedTask(1, () -> {
				ServerWorld ws = otherWorld;
				this.dimension = this.destinationDimension;
				
				this.playLandSound();

				//Clear the flight mini-game controls
				this.currentEvent = null;
				
				//Emotional- induced inaccuracy
				if(this.getEmotionHandler() != null && this.getEmotionHandler().getMood() < EnumHappyState.APATHETIC.getTreshold()) {
					this.destination = this.randomizeCoords(this.destination, 100);
				}
				
				//Sanity check destination
				this.destination = Helper.validateBlockPos(destination, ws.getHeight());
				
				//World border- redirect
				if(!ws.getWorldBorder().contains(this.destination)) {
					BlockPos pos = ws.getSpawnPoint();
					if(pos == null)
						pos = BlockPos.ZERO;
					this.destination = this.randomizeCoords(ws.getHeight(Type.MOTION_BLOCKING_NO_LEAVES, pos), 50);
					for(PlayerEntity ent : world.getEntitiesWithinAABB(PlayerEntity.class, new AxisAlignedBB(this.getPos()).grow(30))) {
						ent.sendStatusMessage(Translations.OUTSIDE_BORDER, true);
					}
					this.getInteriorManager().soundAlarm(AlarmType.LOW);
				}

				//Get landing type, up or down
				EnumLandType landType = EnumLandType.DOWN;
				LandingTypeControl landControl = this.getControl(LandingTypeControl.class);
				if(landControl != null)
					landType = landControl.getLandType();
				
				BlockPos landSpot = LandingSystem.getLand(ws, destination, landType, this);
				
				//TARDIS - in - TARDIS
				if(ws.getTileEntity(this.destination.down(1)) instanceof ExteriorTile) {
					ExteriorTile other = (ExteriorTile)ws.getTileEntity(this.destination.down(1));
					
					//If not our own
					if(other.getInterior() != this.world.getDimension().getType()) {

						DimensionType otherTardisType = other.getInterior();
						ServerWorld otherTardisWorld = this.world.getServer().getWorld(otherTardisType);
						ConsoleTile otherConsole = TardisHelper.getConsoleInWorld(otherTardisWorld).orElse(null);
						if(otherConsole != null) {
							ShieldGeneratorSubsystem sys = otherConsole.getSubsystem(ShieldGeneratorSubsystem.class).orElse(null);
							if(sys == null || !sys.canBeUsed()) {
								this.destinationDimension = other.getInterior();
								ws = world.getServer().getWorld(this.destinationDimension);
								landSpot = LandingSystem.getLand(ws, this.randomizeCoords(new BlockPos(0, 128, 0), 10), EnumLandType.DOWN, this);
							}
							else if(sys != null)
								sys.damage(null, 1);
						}
					}
				}
				else {
					if(landSpot.equals(BlockPos.ZERO) || ws.getBlockState(landSpot.down()).isIn(TardisBlockTags.BLOCKED)) {
						for(int i = 0; i < 30; ++i) {
							 if(landSpot.equals(BlockPos.ZERO) || ws.getBlockState(landSpot.down()).isIn(TardisBlockTags.BLOCKED) || !ws.isAreaLoaded(landSpot, 3))
								 landSpot = LandingSystem.getLand(ws, this.randomizeCoords(destination, 30), landType, this);
							 else break;
						}
					}
				}
				
				List<TileEntity> possibleHazards = Helper.getTEsInChunks(ws, new ChunkPos(landSpot), 3);
				
				
				//Look for tiles that effect TARDIS Landing
				for(TileEntity te : possibleHazards) {
					//Look for open landing pads and land on them
					if(te instanceof LandingPadTile && !((LandingPadTile)te).getOccupied() && te.getPos().withinDistance(landSpot, 16)) {
						landSpot = te.getPos().up();
						BlockState landingPadState = te.getBlockState();
						if(landingPadState != null && landingPadState.get(BlockStateProperties.HORIZONTAL_FACING) != null)
							this.facing = landingPadState.get(BlockStateProperties.HORIZONTAL_FACING);
						Tardis.LOGGER.log(Level.DEBUG, "Found Empty Landing pad! Redirecting to " + te.getPos());
						break;
					}
				}
				
				for(TileEntity te : possibleHazards) {
					//Look for Transduction Barriers and fuck off if there is one
					if(te instanceof TransductionBarrierTile && te.getPos().withinDistance(landSpot, 32)) {
						//Check landing codes
						TransductionBarrierTile barrier = (TransductionBarrierTile)te;
						if(!barrier.canLand(this)) {
							
							final ServerWorld lws = ws;
							final BlockPos displayPos = landSpot;
							world.getServer().enqueue(new TickDelayedTask(1, () -> {
								
								if(!lws.isBlockLoaded(displayPos))
									return;
								
								TardisDisplayEntity entity = TEntities.DISPLAY_TARDIS.create(lws);
								TileEntity ext = this.getExterior().getDefaultState().createTileEntity(lws);
								if(ext != null)
									entity.setTile((ExteriorTile)ext);
								entity.setPosition(displayPos.getX() + 0.5, displayPos.getY() + 1, displayPos.getZ() + 0.5);
								entity.setMotion((rand.nextDouble() - 0.5) * 0.5, rand.nextDouble() * 0.5, (rand.nextDouble() - 0.5) * 0.5);
								lws.addEntity(entity);
							}));
							
							landSpot = LandingSystem.getLand(ws, landSpot.offset(Direction.byHorizontalIndex(rand.nextInt(4)), 64), landType, this);
							this.getInteriorManager().soundAlarm(AlarmType.LOW);
							barrier.onBlockedTARDIS(this);
							this.getInteriorManager().setMonitorOverrides(new MonitorOverride(this, 100, "", "WARNING:", "Redirection device detected!"));
							
						}
					}
				}
				
				if(landSpot.equals(BlockPos.ZERO))
					landSpot = this.destination;
				
				this.location = this.destination = landSpot.toImmutable();
				
				if(this.isCrashing) {
					//Explode
					if(world.getServer().getGameRules().getBoolean(GameRules.MOB_GRIEFING)) {
						Explosion exp = ws.createExplosion(null, landSpot.getX(), landSpot.getY(), landSpot.getZ(), 3, Mode.BREAK);
						exp.doExplosionA();
						exp.doExplosionB(true);
					}
				}
				
				this.exterior.remat(this);
				this.getSoundScheme().playExteriorLand(this);
				
				this.getControl(ThrottleControl.class).setAmount(0.0F);
				this.landTime = this.flightTicks + this.getSoundScheme().getLandTime();
				this.updateClient();
				
				MinecraftForge.EVENT_BUS.post(new TardisEvent.Land(this));
				
				//Remove chunks we loaded for the purpose of landing
				if(!loadedChunkPos.equals(new ChunkPos(this.destination)) && forced)
					world.getServer().enqueue(new TickDelayedTask(3, () -> otherWorld.forceChunk(loadedChunkPos.x, loadedChunkPos.z, false)));
				
			}));
		}

		for (Subsystem sub : this.getSubSystems()) {
			sub.onLand();
		}
		for (Upgrade up : this.getUpgrades()) {
			up.onLand();
		}
	}
	
	//Take off, removes exterior, sets up flight and starts flight loop
	public boolean takeoff() {
		if(this.isInFlight() || world.isRemote)
			return false;
		if(!this.canFly()) {
			this.world.playSound(null, this.getPos(), TSounds.CANT_START, SoundCategory.BLOCKS, 1F, 1F);
			return false;
		}
		
		this.isCrashing = false;
		
		if(this.getEntity() != null)
			this.getEntity().remove();

		this.currentEvent = null;
		
		this.returnLocation = new SpaceTimeCoord(this.getDimension(), this.getLocation(), this.getExteriorDirection());
		
		this.getEmotionHandler().addMood(10);
		this.getEmotionHandler().addLoyaltyIfOwner(1, this.getPilot());
		
		ServerWorld otherWorld = world.getServer().getWorld(this.dimension);
		ChunkPos chunkPos = new ChunkPos(this.location);
		Helper.forceChunkIfNot(otherWorld, chunkPos);
		
		world.getServer().enqueue(new TickDelayedTask(1, () -> {
			this.landTime = 0;
			this.reachDestinationTick = this.calcFlightTicks(false) +
					this.getSoundScheme().getTakeoffTime();
			this.flightTicks = 1;
			this.exterior.demat(this);
			this.scheme.playTakeoffSounds(this);
			System.out.println("TARDIS: Started a flight to last " + this.reachDestinationTick);
			this.getControl(RefuelerControl.class).setRefuling(false);
			MinecraftForge.EVENT_BUS.post(new TardisEvent.Takeoff(this));
			
		}));
		
		this.updateClient();

		for (Subsystem sub : this.getSubSystems()) {
			sub.onTakeoff();
		}
		for (Upgrade up : this.getUpgrades()) {
			up.onTakeoff();
		}
		
		//Shake player's screens
		if(!world.isRemote) {
			for(PlayerEntity player : world.getPlayers()) {
				player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
					cap.setShaking(this.getSoundScheme().getTakeoffTime());
					cap.update();
				});
			}
		}
		
		return true;
	}
	
	public void initLand() {
		this.scaleDestination();
		this.land();
		this.updateClient();
	}
	
	//Violently fall out of flight
	public void crash() {
		
		if(world.isRemote || this.isCrashing)
			return;
		
		Network.sendToAllInWorld(new ConsoleUpdateMessage(DataTypes.CRASH, new CrashData()), (ServerWorld)world);
		
		world.playSound(null, this.getPos(), SoundEvents.ENTITY_GENERIC_EXPLODE, SoundCategory.BLOCKS, 1F, 0.25F);
		this.isCrashing = true;
		this.getInteriorManager().soundAlarm(AlarmType.LOW);
		this.scaleDestination();
		this.destination = this.randomizeCoords(this.destination, 50);
		this.land();
		this.landTime = this.flightTicks = this.reachDestinationTick = 0;
		
		//Shimmy-Shake
		for (LivingEntity ent : this.world.getEntitiesWithinAABB(LivingEntity.class, new AxisAlignedBB(this.getPos()).grow(20))) {
			ent.setMotion(ent.getMotion().add(rand.nextDouble() - 0.5, rand.nextDouble(), rand.nextDouble() - 0.5));
			if (ent instanceof ServerPlayerEntity) {
				Network.sendTo(new MissControlMessage(), (ServerPlayerEntity) ent);
			}
		}
		
		world.getServer().enqueue(new TickDelayedTask(20, () -> {
			//Set the exterior to be crashed
			if(!world.isRemote) {
				world.getServer().enqueue(new TickDelayedTask(1, () -> {
					ExteriorTile ext = this.getExterior().getExterior(this);
					if(ext != null) {
						ext.setCrashed(true);
					}
				}));
			}
		}));
		
	}
	
	public void playCrashEffects() {
		if(world.isRemote && world.getGameTime() % 40 == 0) {
			world.addParticle(ParticleTypes.EXPLOSION, getPos().getX() + 0.5, getPos().getY() + 1, getPos().getZ(), 0, 0, 0);
		}
	}
	
	//Attacked
	public void damage(float damage) {
		
		int systemAmt = 0;
		
		for(Subsystem sys : this.subsystems) {
			if(sys.canBeUsed())
				++systemAmt;
		}
		
		int dam = (int)Math.ceil(damage / (float)systemAmt);
		for(Subsystem sub : this.getSubSystems()) {
			sub.damage(null, dam);
		}
	}
	
	protected void handleRefueling() {
		if(!world.isRemote && !this.isInFlight() && this.getControl(RefuelerControl.class).isRefueling() && artron < this.max_artron)
			this.getSubsystem(FluidLinksSubsystem.class).ifPresent(link -> {
				if(link.canBeUsed()) {
					this.artron += (0.025F * this.rechargeMod);
					
					if(artron > this.max_artron)
						this.artron = this.max_artron;
					
					if(this.artron < 0)
						this.artron = 0;
					
					if(world.getGameTime() % 20 == 0)
						Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FUEL, new Fuel(this.artron, this.max_artron)), world.dimension.getType(), this.getPos(), 20);
				}
			});
	}
	
	public boolean isInFlight() {
		return this.flightTicks > 0;
	}
	
	public boolean isLanding() {
		return this.isInFlight() && this.landTime > 0;
	}
	
	/**
	 * 
	 * @return - True if Journey is complete, might be in flight, or not.
	 */
	public boolean hasReachedDestination() {
		return this.flightTicks >= this.reachDestinationTick;
	}
	
	public int getTimeInFlight() {
		return this.flightTicks;
	}
	
	public int getLandTime() {
		return this.landTime;
	}
	
	public void setDestinationReachedTick(int max) {
		this.reachDestinationTick = max;
		if(!world.isRemote)
			this.updateClient();
	}
	
	public void setDestination(DimensionType type, BlockPos pos) {
		this.destination = pos.toImmutable();
		this.destinationDimension = type;
		this.markDirty();
		if(this.isInFlight())
			this.updateFlightTime();
		this.updateClient();
	}
	
	public void setDestination(SpaceTimeCoord coord) {
			this.setDestination(coord.getDim(), coord.getPos());
	}
	
	public void setConsoleRoom(ConsoleRoom room) {
		this.consoleRoom = room;
		if(room == null)
			throw(new NullPointerException());
		this.markDirty();
		this.updateClient();
	}
	
	public void playFlightLoop() {
		if(!world.isRemote) {
			if(this.flightTicks % this.scheme.getLoopTime() == 0 && this.flightTicks > this.scheme.getTakeoffTime() && this.landTime <= 0)
				this.scheme.playFlightLoop(this);
		}
	}
	
	private void playLandSound() {
		if(!world.isRemote) {
			this.scheme.playInteriorLand(this);
			this.scheme.playExteriorLand(this);
			
			for(PlayerEntity player : world.getPlayers()) {
				player.getCapability(Capabilities.PLAYER_DATA).ifPresent(cap -> {
					cap.setShaking(this.getSoundScheme().getLandTime());
					cap.update();
				});
			}
		}
	}
	
	private void playAmbiantNoises() {
		
		//Client managed Sounds
		if(world.isRemote) {
			PlayerEntity player = Tardis.proxy.getClientPlayer();
			//Creaks
			if(player.ticksExisted % 2400 == 0)
                Tardis.proxy.playMovingSound(player, TSounds.AMBIENT_CREAKS, SoundCategory.AMBIENT, 0.7F, false);
			if(this.consoleRoom == ConsoleRoom.NAUTILUS) {
				if(player.ticksExisted % 600 == 0)
                    Tardis.proxy.playMovingSound(player, SoundEvents.AMBIENT_UNDERWATER_LOOP_ADDITIONS_ULTRA_RARE, SoundCategory.AMBIENT, 1F, false);
			}
			
			//Sparking sound
			if(this.shouldSpark && world.getGameTime() % 60 == 0)
				world.playSound(Tardis.proxy.getClientPlayer(), this.getPos(), TSounds.ELECTRIC_SPARK, SoundCategory.BLOCKS, 0.5F, 1F);
			
		}
	}
	
	public IExterior getExterior() {
		return this.exterior;
	}
	
	public void setExterior(IExterior ext) {
		this.exterior = ext;
		this.markDirty();
		this.updateClient();
	}
	
	//Getters 'n' such
	
	public EmotionHandler getEmotionHandler() {
		return this.emotionHandler;
	}
	
	public BlockPos getLocation() {
		return this.location;
	}
	
	public BlockPos getDestination() {
		return this.destination;
	}
	
	public DimensionType getDimension() {
		return this.dimension;
	}
	
	public DimensionType getDestinationDimension() {
		return this.destinationDimension;
	}
	
	public Direction getDirection() {
		return this.facing == Direction.DOWN || this.facing == Direction.UP ? facing = Direction.NORTH : facing;
	}
	
	public SpaceTimeCoord getPositionInFlight() {
		
		BlockPos diff = Helper.scaleBlockPos(this.location.subtract(this.destination), this.getPercentageJourney());
		return new SpaceTimeCoord(this.getPercentageJourney() < 0.5 ? this.dimension : this.destinationDimension, this.getLocation().add(diff), this.facing);
	}
	
	public boolean canFly() {
		for(Subsystem s : this.subsystems) {
			if(s.stopsFlight()) {
				return false;
			}
		}
		return this.artron > 0;
	}
	
	
	//If not landing and not stabilized
	public boolean canGiveNewEvent() {
		StabilizerSubsystem sys = this.getSubsystem(StabilizerSubsystem.class).orElse(null);
		
		if(sys == null || sys.isActivated())
			return false;
		
		return this.landTime <= 0 &&
				this.flightTicks < this.reachDestinationTick;
		
	}
	
	public void setDirection(Direction dir) {
		if(dir != Direction.DOWN && dir != Direction.UP) {
			this.facing = dir;
			this.markDirty();
			this.updateClient();
		}
	}
	
	public void setLocation(DimensionType type, BlockPos location) {
		this.dimension = type;
		this.location = location.toImmutable();
		this.markDirty();
		this.updateClient();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends IControl> T getControl(Class<T> clazz){
		for(ControlEntity control : controls) {
			if(control.getControl().getClass() == clazz)
				return (T)control.getControl();
		}
		//Make it not crash if this is null for some reason
		
		//this.sendControls();
		try {
			return clazz.getConstructor(ConsoleTile.class, ControlEntity.class).newInstance(this, new ControlEntity(this.world));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * Returns: a float between 0 and 1
	 */
	public double getPercentageJourney() {
		return this.reachDestinationTick == 0 ? 0 : MathHelper.clamp(this.flightTicks / (double)this.reachDestinationTick, 0.0, 1.0);
	}
	
	public SoundSchemeBase getSoundScheme() {
		return this.scheme;
	}
	
	public void setSonicItem(ItemStack sonic) {
		this.sonic = sonic;
		this.markDirty();
		this.updateClient();
	}
	
	public ItemStack getSonicItem() {
		return this.sonic;
	}
	
	//Register stuff
	
	public void registerTicker(ITickable ticker) {
		this.tickers.add(ticker);
	}
	
	public void registerDataHandler(ResourceLocation loc, INBTSerializable<CompoundNBT> handler) {
		this.dataHandlers.put(loc, handler);
	}
	
	public void registerControlEntry(ControlEntry<?> entry) {
		this.controlEntries.add(entry);
	}
	public void setCoordIncr(int incr) {
		this.coordIncr = incr;
		this.markDirty();
		this.updateClient();
	}
	
	public int getCoordIncr() {
		return this.coordIncr;
	}
	
	public float getArtron() {
		return this.artron;
	}
	
	public float getMaxArtron(){
		return this.max_artron;
	}
	
	public void setArtron(float artron) {
		if (artron < this.max_artron) {
			if(artron > 0)
				this.artron = artron;
			else this.artron = 0;
		}
		else {
			this.artron = this.max_artron;
		}
		this.markDirty();
		this.updateClient();
	}
	
	public void setMaxArtron(float maxArtron) {
		this.max_artron = maxArtron;
	}
	
	public BlockPos randomizeCoords(BlockPos pos, int radius) {
		int x = -radius + (rand.nextInt(radius * 2));
		int y = -radius + (rand.nextInt(radius * 2));
		int z = -radius + (rand.nextInt(radius * 2));
		return pos.add(x, y < 0 ? 5 : y, z).toImmutable();
	}
	
	public void scaleDestination() {
		
		//Demat if landing
		if(this.isInFlight() && this.landTime > 0) {
			ExteriorTile ext = this.exterior.getExterior(this);
			if(ext != null)
				ext.demat();
		}
		
		double per = this.getPercentageJourney();
		if(per < 0)
			this.destination = this.getLocation();
		
		//Reset dimension if not more than half way there
		if(per < 0.5)
			this.destinationDimension = this.dimension;
		
		BlockPos diff = this.getDestination().subtract(this.getLocation());
		this.destination = this.getLocation().add(new BlockPos(diff.getX() * per, diff.getY() * per, diff.getZ() * per)).toImmutable();
	}
	
	/*
	 * This gets the first door in this dimension
	 */
	public LazyOptional<DoorEntity> getDoor() {
		if(world instanceof ServerWorld) {
			Iterator<Entity> it = ((ServerWorld)world).getEntities().iterator();
			while(it.hasNext()) {
				Entity e = it.next();
				if(e instanceof DoorEntity) 
					return LazyOptional.of(() -> (DoorEntity) e);
			}
		}
		else {
			for(DoorEntity ent : world.getEntitiesWithinAABB(DoorEntity.class, new AxisAlignedBB(this.getPos()).grow(30))) {
				return LazyOptional.of(() -> ent);
			}
		}
		return LazyOptional.empty();
	}
	
	public Direction getExteriorDirection() {
		if(!world.isRemote) {
			ServerWorld other = world.getServer().getWorld(this.dimension);
			if(other.isAreaLoaded(this.getLocation(), 1)) {
				ExteriorTile ext = this.getExterior().getExterior(this);
				if(ext != null) {
					BlockState state = ext.getBlockState();
					if(state.has(BlockStateProperties.HORIZONTAL_FACING))
						return state.get(BlockStateProperties.HORIZONTAL_FACING);
				}
			}
		}
		return this.facing;
	}
	
	public ConsoleRoom getConsoleRoom() {
		return this.consoleRoom;
	}
	
	public UUID getOwner() {
		return Helper.getPlayerFromTARDIS(this.getWorld().getDimension().getType());
	}
	
	/**
	 * If {@link #flightTicks} >= this, then the actual journey part is over, it has reached it's destination.
	 * This does not mean that it's started landing or will soon
	 * 
	 */
	public int getReachDestinationTick() {
		return this.reachDestinationTick;
	}
	
	public Map<IArtronType, ArtronUse> getArtronUses(){
		return this.artronUses;
	}
	
	public void setSoundScheme(SoundSchemeBase scheme) {
		this.scheme = scheme;
	}
	
	public ArtronUse getOrCreateArtronUse(IArtronType type) {
		if(this.artronUses.containsKey(type))
			return this.artronUses.get(type);
		
		ArtronUse use = new ArtronUse(type);
		this.artronUses.put(type, use);
		return use;
	}
	
	public float calcFuelUse() {
		float use = BASIC_FUEL_USEAGE * (this.getControl(ThrottleControl.class).getAmount() * 0.025F);
		StabilizerSubsystem sys = this.getSubsystem(StabilizerSubsystem.class).orElse(null);
		if(sys != null)
			use *= sys.isActivated() ? 1 : 0.5F;
		return use;
	}
	
	//Speed in blocks a tick B/T
	public float calcSpeed() {
		return ConsoleTile.TARDIS_MAX_SPEED * MathHelper.clamp(this.getControl(ThrottleControl.class).getAmount(), 0.1F, 1.0F);
	}
	
	public void updateArtronValues() {
		
		this.world.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
			
			float newMax = 0;
			float rate = 0;
			
			int numCap = 0;
			
			PanelInventory inv = cap.getEngineInventoryForSide(Direction.WEST);
			for(int i = 0; i < inv.getSizeInventory(); ++i) {
				ItemStack stack = inv.getStackInSlot(i);
				if(stack.getItem() instanceof ArtronCapacitorItem) {
					ArtronCapacitorItem item = (ArtronCapacitorItem)stack.getItem();
					newMax += item.getMaxStorage();
					rate += item.getRechangeModifier();
					++numCap;
				}
			}
			
			this.max_artron = newMax;
			
			this.rechargeMod = (rate / (float)numCap);
			
			if(artron > this.max_artron)
				this.artron = this.max_artron;
			
		});
	}
	
	public boolean areAdminFunctionsLocked() {
		return this.getEmotionHandler().getLoyalty() > 50;
	}
	
	public boolean canDoAdminFunction(PlayerEntity entity) {
		if(!this.areAdminFunctionsLocked())
			return true;
		if(this.getOwner().equals(entity.getUniqueID()))
			return true;
		return false;
	}
	
	//Doesn't include landing or take off
	public int calcFlightTicks(boolean useFlight) {
		
		BlockPos location = useFlight ? this.getPositionInFlight().getPos() : this.location;
		
		float dist = (float) Math.sqrt(location.distanceSq(this.destination));
		float mod = this.calcSpeed();
		int time = (int)(dist / mod);
		
		if(this.destinationDimension != this.dimension)
			time += 200;
		
		Tardis.LOGGER.log(Level.DEBUG, "Recalculated Flight, Speed(B/T): " + mod + ", time: " + time + ", dist: " + dist);
		
		return time < 0 ? 0 : time;
	}
	
	//Only to be used in flight
	public void updateFlightTime() {
		this.reachDestinationTick = 100 + this.flightTicks + this.calcFlightTicks(true);
		this.landTime = 0;
		this.markDirty();
		this.updateClient();
	}
	
	
	public InteriorManager getInteriorManager() {
		return this.interiorManager;
	}
	
	public ExteriorPropertyManager getExteriorManager() {
		return this.exteriorProps;
	}
	
	public void setCustomName(String name) {
		this.customName = name;
		this.markDirty();
	}
	
	public String getCustomName() {
		return this.customName;
	}

	//Packet shit
	
	/*
	 * To be used by packets *ONLY*, so help me God...
	 */
	public void setFlightTicks(int ticks) {
		this.flightTicks = ticks;
		this.updateClient();
	}
	
	public ArrayList<ControlEntity> getControlList(){
		return this.controls;
	}

	public void addDistressSignal(DistressSignal coord) {
		this.getSubsystem(AntennaSubsystem.class).ifPresent(sys -> {
			if(sys.canBeUsed()) {
				this.distressSignal.add(coord);
				this.markDirty();
				this.updateClient();
			}
		});
	}
	
	public SpaceTimeCoord getReturnLocation() {
		return this.returnLocation;
	}

	public FlightEvent getFlightEvent() {
		return this.currentEvent;
	}
	
	public void setFlightEvent(@Nullable FlightEvent event) {
		this.currentEvent = event;
		if(!world.isRemote) {
			event.warnPlayers(world, this.getPos());
			this.updateClient();
		}
		this.markDirty();
	}
	
	public List<DistressSignal> getDistressSignals(){
		return this.distressSignal;
	}
	
	public void setPilot(PlayerEntity player) {
		this.pilot = player;
	}
	
	public PlayerEntity getPilot() {
		return this.pilot;
	}
	
	public TexVariant[] getVariants() {
		return this.variants;
	}
	
	@Nullable
	public TexVariant getVariant() {
		if(this.variant < this.variants.length)
			return this.variants[this.variant];
		return null;
	}
	
	public void setVariant(int index) {
		if(index < this.variants.length)
			this.variant = index;
		this.markDirty();
	}
	
	//Minecraft Shit
	
	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> saved : this.dataHandlers.entrySet()) {
			saved.getValue().deserializeNBT(compound.getCompound(saved.getKey().toString()));
		}
		
		if(compound.contains("unlock_manager"))
			this.unlockManager.deserializeNBT(compound.getCompound("unlock_manager"));
		
		//Legacy - so previous versions don't loose their unlocks
		if(compound.contains("unlocked_exteriors")) {
			ListNBT unlockedList = compound.getList("unlocked_exteriors", NBT.TAG_STRING);
			for(INBT tag : unlockedList) {
                IExterior ext = ExteriorRegistry.getExterior(new ResourceLocation(tag.getString()));
				if(ext != null && !this.getUnlockManager().getUnlockedExteriors().contains(ext))
					this.getUnlockManager().addExterior(ext);
			}
		}
		
		this.location = BlockPos.fromLong(compound.getLong("location"));
		this.destination = BlockPos.fromLong(compound.getLong("destination"));
		this.dimension = DimensionType.byName(new ResourceLocation(compound.getString("dimension")));
		this.destinationDimension = DimensionType.byName(new ResourceLocation(compound.getString("dest_dim")));
		this.flightTicks = compound.getInt("flight_ticks");
		this.reachDestinationTick = compound.getInt("max_flight_ticks");
		this.exterior = ExteriorRegistry.getExterior(new ResourceLocation(compound.getString("exterior")));
		this.artron = compound.getFloat("artron");
		if(compound.contains("console_room")) {
			ConsoleRoom room = ConsoleRoom.REGISTRY.get(new ResourceLocation(compound.getString("console_room")));
			if(room != null)
				this.consoleRoom = room;
		}
		this.customName = compound.getString("custom_name");
		this.returnLocation = SpaceTimeCoord.deserialize(compound.getCompound("return_pos"));
		this.facing = Direction.values()[compound.getInt("facing")];
		ListNBT bedList = compound.getList("bed_list",Constants.NBT.TAG_COMPOUND);
		for(INBT  base : bedList) {
			CompoundNBT bedTag = ((CompoundNBT)base);
			this.bedPositions.put(UUID.fromString(bedTag.getString("player_id")), BlockPos.fromLong(bedTag.getLong("pos")));
		}
		this.sonic = ItemStack.read(compound.getCompound("sonic_item"));
		ListNBT distressList = compound.getList("distress_list1", Constants.NBT.TAG_COMPOUND);
		this.distressSignal.clear();
		for(INBT dis : distressList) {
			this.distressSignal.add(DistressSignal.deserializeNBT((CompoundNBT)dis));
		}
		
		//Here for legacy purposes
		ListNBT interiors = compound.getList("interiors", NBT.TAG_STRING);
		for(INBT in : interiors) {
			ConsoleRoom room = ConsoleRoom.REGISTRY.get(new ResourceLocation(((StringNBT)in).getString()));
			if(!this.getUnlockManager().getUnlockedConsoleRooms().contains(room))
				this.getUnlockManager().addConsoleRoom(room);
		}
		
		this.max_artron = compound.getFloat("max_artron");
		this.rechargeMod = compound.getFloat("recharge_modifier");
		this.variant = compound.getInt("texture_variant");
		this.antiGravs = compound.getBoolean("anti_gravs");
		
		if(compound.contains("tardis_entity_id"))
			this.tardisEntityID = compound.getUniqueId("tardis_entity_id");
		this.shouldSpark = compound.getBoolean("should_spark");
		this.landingCode = compound.getString("landing_code");
		this.landTime = compound.getInt("landing_time");
		if(compound.contains("sound_scheme"))
			this.scheme = TardisForgeRegistries.SOUND_SCHEME.getValue(new ResourceLocation(compound.getString("sound_scheme")));
		
		this.onLoadAction = () -> {
			for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> saved : this.dataHandlers.entrySet()) {
				saved.getValue().deserializeNBT(compound.getCompound(saved.getKey().toString()));
			}
		};
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		for(Entry<ResourceLocation, INBTSerializable<CompoundNBT>> entry : this.dataHandlers.entrySet()) {
			compound.put(entry.getKey().toString(), entry.getValue().serializeNBT());
		}
		
		compound.put("unlock_manager", this.unlockManager.serializeNBT());
		
		compound.putLong("location", this.location.toLong());
		compound.putLong("destination", this.destination.toLong());
		compound.putInt("flight_ticks", this.flightTicks);
		compound.putInt("max_flight_ticks", this.reachDestinationTick);
		compound.putString("exterior", this.exterior.getRegistryName().toString());
		compound.putString("dimension", Helper.getKeyFromDimType(this.dimension).toString());
		compound.putString("dest_dim", Helper.getKeyFromDimType(this.destinationDimension).toString());
		compound.putFloat("artron", this.artron);
		compound.putString("console_room", this.consoleRoom.getRegistryName().toString());
		compound.putString("custom_name", this.customName);
		compound.put("return_pos", this.returnLocation.serialize());
		compound.putInt("facing", this.facing.ordinal());

		//Bed locations
		ListNBT bedList = new ListNBT();
		for(Entry<UUID, BlockPos> entry : this.bedPositions.entrySet()) {
			CompoundNBT bedTag = new CompoundNBT();
			bedTag.putString("player_id", entry.getKey().toString());
			bedTag.putLong("pos", entry.getValue().toLong());
			bedList.add(bedTag);
		}
		compound.put("bed_list", bedList);
		compound.put("sonic_item", this.sonic.serializeNBT());
		
		//SOSes
		ListNBT distress = new ListNBT();
		for(DistressSignal dis : this.distressSignal)
			distress.add(dis.serializeNBT());
		compound.put("distress_list1", distress);
		
		compound.putFloat("max_artron", this.max_artron);
		compound.putFloat("recharge_modifier", this.rechargeMod);
		compound.putInt("texture_variant", this.variant);
		compound.putBoolean("anti_gravs", this.antiGravs);
		if(this.tardisEntityID != null)
			compound.putUniqueId("tardis_entity_id", this.tardisEntityID);
		compound.putBoolean("should_spark", shouldSpark);
		compound.putString("landing_code", this.landingCode);
		compound.putInt("landing_time", this.landTime);
		compound.putString("sound_scheme", this.scheme.getRegistryName().toString());
		return super.write(compound);
	}

	@Override
	public void tick() {
		//Cycle through tickable objects
		for(ITickable tick : this.tickers) {
			tick.tick(this);
		}

		if(this.isInFlight()) {
			fly();
		}
		this.playAmbiantNoises();
		
		this.handleRefueling();
		
		if(world.getGameTime() % 200 == 0) {
			if(world.isRemote || controls.isEmpty())
				this.getOrCreateControls();
		}
		
		if(timeUntilControlSpawn > 0) {
			--timeUntilControlSpawn;
			if(timeUntilControlSpawn == 0) {
				this.getOrCreateControls();
				if(this.onLoadAction != null)
					this.onLoadAction.run();
			}
		}
		
		//Loop for things that need to be polled semi-constantly
		if(!world.isRemote && world.getGameTime() % 40 == 0) {
			this.updateArtronValues();
			
			boolean spark = false;
			for(Subsystem s : this.subsystems) {
				if(s.shouldSpark()) { 
					spark = true;
					break;
				}
			}
			if(this.shouldSpark != spark) {
				this.shouldSpark = spark;
				this.updateClient();
			}
			
			//Force Field Drain
			this.getSubsystem(ShieldGeneratorSubsystem.class).ifPresent(shield -> {
				if(shield.canBeUsed() && shield.getActivted() && this.artron > 1.0F) {
					ArtronUse use = this.getOrCreateArtronUse(ArtronType.FORCEFIELD);
					use.setTimeToDrain(1);
					shield.damage(null, 1);
				}
				else shield.setActivated(false);
			});
		}
		
		//Fuck TARDIS abusers
		if(!world.isRemote && this.shouldSpark && world.getGameTime() % 60 == 0) {
			this.getEmotionHandler().addMood(-1);
		}
		
		
		//Artron Drains
		if(!world.isRemote) {
			world.getServer().enqueue(new TickDelayedTask(0, () -> {
				float oldArtron = this.artron;
				for(ArtronUse use : this.artronUses.values()) {
					use.tick(this);
				}
				if(oldArtron != this.artron && world.getGameTime() + 4 % 20 == 0)
					Network.sendToAllAround(new ConsoleUpdateMessage(DataTypes.FUEL, new Fuel(this.artron, this.max_artron)), world.dimension.getType(), this.getPos(), 20);
			}));
		}
		
		if(world.isRemote && this.shouldSpark && world.getGameTime() % 5 == 0) {
			world.addParticle(ParticleTypes.LAVA, pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5, 0, 1, 0);
		}
	}

	public void getOrCreateControls() {
		this.gatherOldControls();
		if(!world.isRemote && this.controls.size() < this.controlEntries.size()) {
				this.removeControls();
				for(ControlEntry<?> controlEntry : this.controlEntries) {
				ControlEntity entity = TEntities.CONTROL.create(this.world);
				IControl control = controlEntry.spawn(this, entity);
				
				Vec3d offset = null;
				if(this.controlOverrides.containsKey(control.getClass()))
					offset = this.controlOverrides.get(control.getClass()).getPosition();
				else offset = control.getPos();
				
				
				entity.setPosition(
						this.getPos().getX() + 0.5 + offset.x,
						this.getPos().getY() + 0.5 + offset.y,
						this.getPos().getZ() + 0.5 + offset.z);
				entity.setControl(control);
				entity.setConsole(this);
				((ServerWorld)world).addEntityIfNotDuplicate(entity);
				this.controls.add(entity);
			}
		}
		this.updateClient();
	}
	
	/*
	 * Gets controls after a world reload
	 */
	private void gatherOldControls() {
		this.controls.clear();
		for(ControlEntity control : world.getEntitiesWithinAABB(ControlEntity.class, CONRTROL_HITBOX.offset(getPos()).grow(2))) {
			if(!control.removed) {
				control.setConsole(this);
				this.controls.add(control);
			}
		}
	}

	public void removeControls() {
		for(ControlEntity control : world.getEntitiesWithinAABB(ControlEntity.class, CONRTROL_HITBOX.offset(this.getPos()).grow(5))) {
			control.remove();
		}
		this.controls.clear();
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		this.read(pkt.getNbtCompound());
		if(pkt.getNbtCompound().contains("current_event"))
			this.currentEvent = TardisForgeRegistries.FLIGHT_EVENTS_REGISTRY.getValue(new ResourceLocation(pkt.getNbtCompound().getString("current_event"))).create(this);
		else this.currentEvent = null;
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return new SUpdateTileEntityPacket(this.getPos(), 99, this.getUpdateTag());
	}

	@Override
	public CompoundNBT getUpdateTag() {
		CompoundNBT tag = this.write(new CompoundNBT());
		if(this.currentEvent != null && !this.currentEvent.isComplete())
			tag.putString("current_event", this.currentEvent.getRegistryName().toString());
		else tag.remove("current_event");
		return tag;
	}

	public void updateClient() {
		if(world.isRemote) return;
		
		BlockState state = world.getBlockState(this.getPos());
		world.markAndNotifyBlock(this.getPos(), world.getChunkAt(getPos()), state, state, 2);
	}

	@Override
	public void onLoad() {
		super.onLoad();
		this.timeUntilControlSpawn = 10;
	}
	
	public List<Subsystem> getSubSystems(){
		return this.subsystems;
	}
	
	public List<Upgrade> getUpgrades(){
		return this.upgrades;
	}
	

	@SuppressWarnings("unchecked")
	public <T extends Subsystem> LazyOptional<T> getSubsystem(Class<T> clazz) {
		for(Subsystem sys : this.getSubSystems()) {
			if(sys.getClass() == clazz)
				return LazyOptional.of(() -> (T)sys);
		}
		return LazyOptional.empty();
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Upgrade> LazyOptional<T> getUpgrade(Class<T> clazz) {
		for(Upgrade upgrade : upgrades) {
			if(upgrade.getClass() == clazz)
				return (LazyOptional<T>)LazyOptional.of(() -> upgrade);
		}
		return LazyOptional.empty();
	}
	
	public void setLandingCode(String code) {
		this.landingCode = code;
		this.markDirty();
	}
	
	public String getLandingCode() {
		return this.landingCode;
	}
	
	/**
	 * Adds or updates the player respawn point if they slept in a bed in their tardis
	 * If the entry doesn't exist, it creates one, otherwise it updates the existing one
	 * @param player
	 * @param pos
	 */
	public void addOrUpdateBedLoc(PlayerEntity player, BlockPos pos) {
		this.bedPositions.put(player.getUniqueID(), pos);
	}
	
	public BlockPos getBedPosForPlayer(PlayerEntity player) {
		return this.bedPositions.get(player.getUniqueID());
	}
	/**
	 * Removes the player respawn point position if the object exists
	 * @param player
	 * @param pos
	 */
	public void removePlayerBedLoc(PlayerEntity player) {
		this.bedPositions.remove(player.getUniqueID());
	}

	public void playSoundAtExterior(SoundEvent sound, SoundCategory cat, float vol, float pitch) {
		ExteriorTile tile = this.exterior.getExterior(this);
		if(tile != null)
			tile.getWorld().playSound(null, tile.getPos(), sound, cat, vol, pitch);
	}

	public boolean getAntiGrav() {
		return this.antiGravs;
	}

	public void setAntiGrav(boolean enabled) {
		this.antiGravs = enabled;
		if(!world.isRemote) {
			ExteriorTile tile = this.getExterior().getExterior(this);
			if(tile != null) {
				tile.setAntiGravs(enabled);
			}
			TardisEntity ent = this.getEntity();
			if(ent != null)
				ent.remove();
		}
		this.markDirty();
	}
	
	@Nullable
	public TardisEntity getEntity() {
		if(this.tardisEntityID == null)
			return this.tardisEntity;
		
		if(this.tardisEntity != null && !this.tardisEntity.removed)
			return this.tardisEntity;
		
		if(!world.isRemote) {
			ServerWorld sw = world.getServer().getWorld(getDimension());
			return this.tardisEntity = (TardisEntity)sw.getEntityByUuid(tardisEntityID);
		}
		return null;
	}
	
	public void setEntity(@Nullable TardisEntity ent) {
		this.tardisEntity = ent;
		if(ent != null)
			this.tardisEntityID = ent.getUniqueID();
		else this.tardisEntityID = null;
	}
	
	public void onInitialSpawn() {
		this.getEmotionHandler().onInitialSpawn();
	}
	
	public LazyOptional<ExteriorTile> getOrFindExteriorTile(){
		if(this.exteriorHolder.isPresent())
			return this.exteriorHolder;
		
		this.exteriorHolder.invalidate();
		
		ExteriorTile tile = this.getExterior().getExterior(this);
		if(tile != null) {
			return this.exteriorHolder = LazyOptional.of(() -> tile);
		}
		return this.exteriorHolder = LazyOptional.empty();
		
	}

	public boolean isCrashing() {
		return this.isCrashing;
	}

	public void setCrashing(boolean crash) {
		this.isCrashing = crash;
	}

	public UnlockManager getUnlockManager() {
		return this.unlockManager;
	}
	
	public HashMap<Class<?>, ControlOverride> getControlOverrides(){
		return this.controlOverrides;
	}
}
