package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.texturevariants.ConsoleTextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class NeutronConsoleTile extends ConsoleTile{

	public NeutronConsoleTile(TileEntityType<?> type) {
		super(type);
		this.variants = ConsoleTextureVariants.NEUTRON;
	}
	
	public NeutronConsoleTile() {
		this(TTiles.CONSOLE_NEUTRON);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).expand(3, 4, 3);
	}
}
