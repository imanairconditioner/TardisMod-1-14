package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.texturevariants.ConsoleTextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class NemoConsoleTile extends ConsoleTile{

	public static final AxisAlignedBB RENDER_BOX = new AxisAlignedBB(-2, -1, -2, 2, 2.5, 2);

	public NemoConsoleTile(TileEntityType<?> type) {
		super(type);
		this.variants = ConsoleTextureVariants.NEMO;
	}
	
	public NemoConsoleTile() {
		this(TTiles.CONSOLE_NEMO);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}
}
