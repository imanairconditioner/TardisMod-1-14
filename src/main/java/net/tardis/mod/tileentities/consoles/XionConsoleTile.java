package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.texturevariants.ConsoleTextureVariants;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class XionConsoleTile extends ConsoleTile{


	public XionConsoleTile() {
		this(TTiles.CONSOLE_XION);
		this.registerControlEntry(ControlRegistry.MONITOR);
		this.variants = ConsoleTextureVariants.XION;
	}
	
	public XionConsoleTile(TileEntityType<?> type) {
		super(type);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).expand(3, 4, 3);
	}
}
