package net.tardis.mod.tileentities.consoles;

import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.math.AxisAlignedBB;
import net.tardis.mod.controls.ControlRegistry;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.TTiles;

public class KeltConsoleTile extends ConsoleTile{

	private static final AxisAlignedBB RENDER_BOX = Helper.createBBWithRaduis(2);
	
	public KeltConsoleTile(TileEntityType<?> type) {
		super(type);
		this.registerControlEntry(ControlRegistry.MONITOR);
	}
	
	public KeltConsoleTile() {
		this(TTiles.CONSOLE_KELT);
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return RENDER_BOX.offset(this.getPos());
	}

}
