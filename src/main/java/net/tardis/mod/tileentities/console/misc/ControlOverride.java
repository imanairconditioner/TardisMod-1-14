package net.tardis.mod.tileentities.console.misc;

import net.minecraft.entity.EntitySize;
import net.minecraft.util.math.Vec3d;

public class ControlOverride {

	Vec3d position;
	EntitySize size;
	
	public ControlOverride(Vec3d position, EntitySize size) {
		this.position = position;
		this.size = size;
	}

	public Vec3d getPosition() {
		return position;
	}

	public EntitySize getSize() {
		return size;
	}
}
