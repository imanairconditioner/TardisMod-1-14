package net.tardis.mod.tileentities;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionType;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.DistExecutor;
import net.tardis.mod.boti.BotiWorld;
import net.tardis.mod.boti.WorldShell;
import net.tardis.mod.network.Network;

public class HoloObservatoryTile extends TileEntity implements ITickableTileEntity{

	//WorldRenderer 
	private Object renderer;
	private DimensionType type = DimensionType.THE_END;
	public World renderWorld;
	
	public HoloObservatoryTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public HoloObservatoryTile() {
		super(TTiles.STAR_RENDER);
	}
	
	@OnlyIn(Dist.CLIENT)
	public WorldRenderer getWorldRenderer() {
		if(this.renderer == null) {
			this.renderer = new WorldRenderer(Minecraft.getInstance());
			this.renderWorld = new BotiWorld(type, new WorldShell(BlockPos.ZERO, type), (WorldRenderer)this.renderer);
			((WorldRenderer)this.renderer).setWorldAndLoadRenderers((ClientWorld)this.renderWorld);
		}
		return (WorldRenderer)this.renderer;
	}
	
	public void setDimensionToRender(DimensionType type) {
		this.type = type;
		this.renderer = null;
	}

	@Override
	public AxisAlignedBB getRenderBoundingBox() {
		return new AxisAlignedBB(this.getPos()).grow(32);
	}

	@Override
	public void read(CompoundNBT compound) {
		if(compound.contains("dim_type"))
			this.type = DimensionType.byName(new ResourceLocation(compound.getString("dim_type")));
		super.read(compound);
	}

	@Override
	public CompoundNBT write(CompoundNBT compound) {
		if(this.type != null)
			compound.putString("dim_type", DimensionType.getKey(type).toString());
		return super.write(compound);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
		this.renderer = null;
	}

	@Override
	public void handleUpdateTag(CompoundNBT tag) {
		super.handleUpdateTag(tag);
		this.renderer = null;
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEUpdatePacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	public void updateClient() {
		if(!world.isRemote)
			world.notifyBlockUpdate(getPos(), getBlockState(), getBlockState(), 3);
	}

	@Override
	public void tick() {
		if(world.isRemote)
			DistExecutor.runWhenOn(Dist.CLIENT, () -> () -> {
				if(this.renderWorld instanceof ClientWorld)
					((ClientWorld)this.renderWorld).tick(() -> false);
			});
	}
}
