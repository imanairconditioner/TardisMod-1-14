package net.tardis.mod.tileentities;

import java.util.List;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.NonNullList;
import net.minecraftforge.common.util.Constants.NBT;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;

public class WaypointBankTile extends MultiblockMasterTile{

	private NonNullList<SpaceTimeCoord> waypoints = NonNullList.withSize(5, SpaceTimeCoord.UNIVERAL_CENTER);
	
	public WaypointBankTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public WaypointBankTile() {
		super(TTiles.WAYPOINT_BANK);
	}
	
	public void setWaypoint(int index, SpaceTimeCoord coord) {
		this.waypoints.set(index, coord.toImmutable());
		this.markDirty();
	}
	
	public boolean addWaypoint(SpaceTimeCoord coord) {
		for(int i = 0; i < this.waypoints.size(); ++i) {
			if(waypoints.get(i).equals(SpaceTimeCoord.UNIVERAL_CENTER)) {
				this.setWaypoint(i, coord);
				return true;
			}
		}
		return false;
	}
	
	public List<SpaceTimeCoord> getWaypoints(){
		return this.waypoints;
	}
	
	public void clearWaypoints() {
		for(int i = 0; i < this.waypoints.size(); ++i) {
			this.waypoints.set(i, SpaceTimeCoord.UNIVERAL_CENTER);
		}
		this.markDirty();
	}
	
	
	@Override
	public void read(CompoundNBT compound) {
		super.read(compound);
		if(compound.contains("waypoints")) {
			ListNBT list = compound.getList("waypoints", NBT.TAG_COMPOUND);
			int i = 0;
			for(INBT nbt : list) {
				this.waypoints.set(i, SpaceTimeCoord.deserialize((CompoundNBT)nbt));
				++i;
			}
		}
	}
	
	@Override
	public CompoundNBT write(CompoundNBT compound) {
		
		ListNBT list = new ListNBT();
		for(SpaceTimeCoord coord : this.waypoints) {
			list.add(coord.serialize());
		}
		compound.put("waypoints", list);
		
		return super.write(compound);
	}

	@Override
	public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
		super.onDataPacket(net, pkt);
		this.deserializeNBT(pkt.getNbtCompound());
	}

	@Override
	public SUpdateTileEntityPacket getUpdatePacket() {
		return Network.createTEUpdatePacket(this);
	}

	@Override
	public CompoundNBT getUpdateTag() {
		return this.serializeNBT();
	}

	public void deleteWaypoint(int index) {
		this.waypoints.set(index, SpaceTimeCoord.UNIVERAL_CENTER);
		this.markDirty();
	}

}
