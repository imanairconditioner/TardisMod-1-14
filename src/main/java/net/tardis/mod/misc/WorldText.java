package net.tardis.mod.misc;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class WorldText {

	private static boolean renderBounds = false;
	public float width, height, scale;
	private int color = 0xFFFFFF;
	
	public WorldText(float width, float height, float scale, int color) {
		this.width = width;
		this.height = height;
		this.color = color;
		this.scale = scale;
	}
	
	public WorldText(float width, float height, float scale, TextFormatting color) {
		this(width, height, scale, color.getColor());
	}

	@OnlyIn(Dist.CLIENT)
	public void renderMonitor(String... lines) {
		
		renderBounds = false;
		
		FontRenderer fr = Minecraft.getInstance().fontRenderer;
		double linePos = 0;
		for(String line : lines) {
			
			if(linePos > this.height)
				return;
			
			GlStateManager.pushMatrix();
			float scale = width / (float)fr.getStringWidth(line);
			scale = MathHelper.clamp(scale, 0, this.scale);
			if(scale > this.scale)
				scale = this.scale;
			GlStateManager.translated(0, linePos, 0);
			GlStateManager.scalef(scale, scale, scale);
			this.drawLine(line, fr);
			GlStateManager.popMatrix();
			linePos += scale * (fr.FONT_HEIGHT + 1);
		}
		if(renderBounds)
			this.renderBounds();
	}
	
	@OnlyIn(Dist.CLIENT)
	private void drawLine(String line, FontRenderer fr) {
		fr.drawString(line, 0, 0, this.color);
	}
	
	@OnlyIn(Dist.CLIENT)
	private void renderBounds() {
		BufferBuilder bb = Tessellator.getInstance().getBuffer();
		GlStateManager.disableTexture();
		bb.begin(GL11.GL_LINES, DefaultVertexFormats.POSITION_COLOR);
		
		bb.pos(0, 0, 0).color(1F, 0, 0, 1F).endVertex();
		bb.pos(width, 0, 0).color(1F, 0, 0, 1).endVertex();
		
		bb.pos(0, height, 0).color(1F, 0, 0, 1F).endVertex();
		bb.pos(width, height, 0).color(1F, 0, 0, 1).endVertex();
		
		bb.pos(0, 0, 0).color(0, 1F, 0, 1F).endVertex();
		bb.pos(0, height, 0).color(0, 1F, 0, 1F).endVertex();
		
		bb.pos(width, 0, 0).color(0, 1F, 0, 1F).endVertex();
		bb.pos(width, height, 0).color(0, 1F, 0, 1F).endVertex();
		
		Tessellator.getInstance().draw();
		
		GlStateManager.enableTexture();
	}
}
