package net.tardis.mod.misc;

import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorMode;
import net.tardis.mod.tileentities.monitors.MonitorTile.MonitorView;

public interface IMonitor {

	MonitorMode getMode();
	MonitorView getView();
	
	void setMode(MonitorMode mode);
	void setView(MonitorView view);
}
