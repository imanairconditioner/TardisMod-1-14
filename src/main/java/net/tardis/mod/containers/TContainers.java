package net.tardis.mod.containers;

import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.network.IContainerFactory;
import net.tardis.mod.Tardis;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Bus.MOD)
public class TContainers {
	
	public static ContainerType<QuantiscopeSonicContainer> QUANTISCOPE;
	public static ContainerType<QuantiscopeWeldContainer> QUANTISCOPE_WELD;
	public static ContainerType<AlembicContainer> ALEMBIC;
	public static ContainerType<ReclamationContainer> RECLAMATION_UNIT;
	public static ContainerType<VMContainer> VORTEX_M_BATTERY;
	public static ContainerType<ShipComputerContainer> SHIP_COMPUTER;
	
	@SubscribeEvent
	public static void onContainerRegistry(final RegistryEvent.Register<ContainerType<?>> event) {
		event.getRegistry().registerAll(
				QUANTISCOPE = register(QuantiscopeSonicContainer::new, "quantiscope"),
				QUANTISCOPE_WELD = register(QuantiscopeWeldContainer::new, "quantiscope_weld"),
				ALEMBIC = register(AlembicContainer::new, "alembic"),
				RECLAMATION_UNIT = register(ReclamationContainer::new, "reclaimation_unit"),
				SHIP_COMPUTER = register(ShipComputerContainer::new, "ship_computer"),
				VORTEX_M_BATTERY = register(VMContainer::new, "vm_battery")
		);
	}
	
	public static <T extends Container> ContainerType<T> register(IContainerFactory<T> fact, String name){
		ContainerType<T> type = new ContainerType<T>(fact);
		type.setRegistryName(new ResourceLocation(Tardis.MODID, name));
		return type;
	}
	

}
