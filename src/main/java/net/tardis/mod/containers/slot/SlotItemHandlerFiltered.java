package net.tardis.mod.containers.slot;

import java.util.function.Predicate;

import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotItemHandlerFiltered extends SlotItemHandler{

	private Predicate<ItemStack> filter;
	
	public SlotItemHandlerFiltered(IItemHandler itemHandler, int index, int xPosition, int yPosition, Predicate<ItemStack> filter) {
		super(itemHandler, index, xPosition, yPosition);
		this.filter = filter;
	}

	@Override
	public boolean isItemValid(ItemStack stack) {
		return this.filter.test(stack);
	}

}
