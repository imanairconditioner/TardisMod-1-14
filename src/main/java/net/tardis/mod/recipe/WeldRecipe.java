package net.tardis.mod.recipe;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.StringNBT;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.util.Constants.NBT;
import net.minecraftforge.registries.ForgeRegistries;

public class WeldRecipe {
	
	private Item output;
	private List<Item> inputs;
	private boolean repair = false;

	public static List<WeldRecipe> WELD_RECIPE = new ArrayList<WeldRecipe>();
	
	/**
	 * 
	 * @param stack - If isRepair is true, this is the item to repair, else, this is the item to create
	 * @param isRepair - If this recipe is a repair recipe or a creation one
	 * @param inputs - Items required for this recipe
	 * @impleNote Each recipe must be unique
	 */
	public WeldRecipe(Item stack, boolean isRepair, Item... inputs) {
		this.output = stack;
		this.inputs = Lists.newArrayList(inputs);
		this.repair = isRepair;
	}
	
	public Item getOutput() {
		return output;
	}
	
	public List<Item> getInputs() {
		return this.inputs;
	}
	
	public boolean isRepair() {
		return this.repair;
	}
	
	/**
	 * 
	 * @param repair - Item in the repair slot, or {@link ItemStack#EMPTY} if not a repair recipe
	 * @param recipe - Items to check this recipe against
	 * @return - If it matches this recipe
	 */
	public boolean matches(ItemStack repair, ItemStack... recipe) {
		List<Item> rec = new ArrayList<>();
		
		if(repair.getItem() != this.output && this.repair)
			return false;
		
		//If this is not repairing but has a repair item return false
		if(!this.repair && !repair.isEmpty())
			return false;
		
		for(ItemStack s : recipe) {
			if(!s.isEmpty())
				rec.add(s.getItem());
		}
		if(rec.size() != inputs.size())
			return false;
		
		for(Item s : inputs) {
			if(!rec.contains(s))
				return false;
		}
		
		for(Item s : rec) {
			if(!inputs.contains(s))
				return false;
		}
		
		return true;
	}
	
	public CompoundNBT serialize() {
		CompoundNBT tag = new CompoundNBT();
		tag.putString("result", this.output.getRegistryName().toString());
		ListNBT list = new ListNBT();
		for(Item item : this.inputs) {
			list.add(new StringNBT(item.getRegistryName().toString()));
		}
		tag.put("ingred", list);
		tag.putBoolean("repair", this.repair);
		return tag;
	}
	
	public static WeldRecipe deserialize(CompoundNBT tag) {
		boolean repair = tag.getBoolean("repair");
		Item output = ForgeRegistries.ITEMS.getValue(new ResourceLocation(tag.getString("result")));
		ListNBT list = tag.getList("ingred", NBT.TAG_STRING);
		Item[] items = new Item[list.size()];
		int i = 0;
		for(INBT nbt : list) {
			items[i] = ForgeRegistries.ITEMS.getValue(new ResourceLocation(((StringNBT)nbt).getString()));
			++i;
		}
		
		return new WeldRecipe(output, repair, items);
	}

	public static void read(MinecraftServer server) {
		WELD_RECIPE.clear();
    	Collection<ResourceLocation> locations = server.getResourceManager().getAllResourceLocations("quantiscope", str -> str.endsWith(".json"));
    	for(ResourceLocation loc : locations) {
    		try {
    			JsonObject obj = new JsonParser().parse(new InputStreamReader(server.getResourceManager().getResource(loc).getInputStream())).getAsJsonObject();
    			if(!obj.get("repair").getAsBoolean()) {
    				List<Item> itemList = new ArrayList<Item>();
    				for(JsonElement items : obj.get("ingredients").getAsJsonArray()) {
    					itemList.add(ForgeRegistries.ITEMS.getValue(new ResourceLocation(items.getAsString())));
    				}
    				Item result = null;
    				JsonObject resultObj = obj.get("result").getAsJsonObject();
    				if(resultObj.has("item"))
    					result = ForgeRegistries.ITEMS.getValue(new ResourceLocation(resultObj.get("item").getAsString()));
    				
    				WELD_RECIPE.add(new WeldRecipe(result, false, itemList.toArray(new Item[0])));
    			}
    			else{
    				Item focalPoint = ForgeRegistries.ITEMS.getValue(new ResourceLocation(obj.get("repair_item").getAsString()));
    				List<Item> items = new ArrayList<Item>();
    				for(JsonElement ele : obj.get("ingredients").getAsJsonArray()) {
    					items.add(ForgeRegistries.ITEMS.getValue(new ResourceLocation(ele.getAsString())));
    				}
    				WELD_RECIPE.add(new WeldRecipe(focalPoint, true, items.toArray(new Item[0])));
    			}
    		}
    		catch(Exception e) {
    			System.err.println("Error parsing Quantiscope recipe for " + loc.toString());
    			e.printStackTrace();
    		}
    	}
	}

}
